<?php

namespace App\Services;


use App\Models\Caixa;
use App\Models\Cartao;
use App\Models\Cheque;
use App\Models\Crediario;
use App\Models\Credito;
use App\Models\Dinheiro;
use App\Models\Financiamento;
use App\Models\Parcela;
use App\Models\ParcelaPagamento;
use App\Models\Predatado;
use App\Models\Retirada;
use App\Models\Transacao;
use App\Models\Vencimento;
use App\Models\Venda;
use App\Models\VendaPagamento;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class CaixaService
 * @package App\Services
 */
class CaixaService
{
    /**
     * @param $venda_id
     * @return Transacao
     */
    public function addTransacaoVenda($venda_id)
    {
        Caixa::find(Session::get('caixa_id'))->update(['updated_at' => date('Y-m-d H:i:s')]);
        return Transacao::create([
            'caixa_id' => Session::get('caixa_id'),
            'venda_id' => $venda_id
        ]);
    }

    /**
     * @return Transacao
     */
    public function addTransacao()
    {
        Caixa::findOrFail(Session::get('caixa_id'))->update(['updated_at' => date('Y-m-d H:i:s')]);
        return Transacao::create(['caixa_id' => Session::get('caixa_id')]);
    }

    /**
     * @param array $data
     * @return Transacao
     * @throws \Exception
     */
    public function salvarPagamentoVendas(array $data)
    {
        $this->updateVenda($data);
        $transacao = $this->addTransacaoVenda($data['id']);

        foreach ($data['venda_pagamentos'] as $vp) {

            $this->updateVendaPagamento($vp);

            switch ($vp['tipo_pagamento_id']) {
                case 1 :
                    $this->addVendaDinheiro($vp, $transacao->id);
                    break;
                case 2 :
                    $this->addVendaPredatado($vp, $transacao->id);
                    break;
                case 3 :
                    $this->addVendaCartao($vp, $transacao->id, 'C');
                    break;
                case 4 :
                    $this->addVendaCartao($vp, $transacao->id, 'D');
                    break;
                case 5 :
                    $this->addVendaCrediario($vp, $transacao->id);
                    break;
                case 6 :
                    $this->addVendaFinanciamento($vp, $transacao->id);
                    break;
                default:
                    throw new \Exception('Tipo de pagamento inválido!', 409);
                    break;
            }
        }

        return $transacao;
    }

    /**
     * @param array $data
     * @return void
     */
    private function updateVenda(array $data)
    {
        $venda = Venda::find($data['id']);
        $venda->estatus_venda_id = 6;
        $venda->data_venda = Carbon::now();
        $venda->save();
    }

    /**
     * @param array $vp
     * @return void
     */
    private function updateVendaPagamento(array $vp)
    {
        VendaPagamento::find($vp['id'])->update($vp);
    }

    /**
     * @param array $vp | Array de venda_pagamento
     * @return void
     */
    private function addVendaDinheiro(array $vp, $transacao_id)
    {
        Dinheiro::create([
            'transacao_id'   => $transacao_id,
            'valor_recebido' => $vp['valor'],
            'observacao'     => $vp['observacao']
        ]);
    }

    /**
     * @param array $vp | Array de venda_pagamento
     * @param $transacao_id
     * @param $tipoCartao
     * @return void
     */
    private function addVendaCartao(array $vp, $transacao_id, $tipoCartao)
    {
        Cartao::create([
            'transacao_id'        => $transacao_id,
            'bandeira_id'         => $vp['bandeira_id'],
            'protocolo'           => $vp['protocolo'],
            'quantidade_parcelas' => $vp['parcelas'],
            'valor_parcelas'      => $vp['valor_parcelas'],
            'tipo'                => $tipoCartao
        ]);
    }

    /**
     * @param array $vp | Array de venda_pagamento
     * @return void
     */
    private function addVendaFinanciamento(array $vp, $transacao_id)
    {
        Financiamento::create([
            'transacao_id'        => $transacao_id,
            'financeira_id'       => $vp['financeira_id'],
            'quantidade_parcelas' => $vp['parcelas'],
            'valor_parcelas'      => $vp['valor_parcelas'],
            'protocolo'           => $vp['protocolo'],
            'contrato'            => $vp['contrato']
        ]);
    }

    /**
     * @param array $vp | Array de venda_pagamento
     * @param $transacao_id
     * @return void
     */
    private function addVendaCrediario(array $vp, $transacao_id)
    {
        $crediario = Crediario::create([
            'transacao_id' => $transacao_id,
            'observacao'   => $vp['observacao'],
            'status'       => 'A' // A - Aberto, F - Finalizado
        ]);

        $vencimento = Carbon::parse($vp['vencimento'], 'America/Sao_Paulo');
        for ($i = 0; $i < $vp['parcelas']; $i++) {
            Parcela::create([
                'crediario_id' => $crediario->id,
                'vencimento'   => $i > 0 ? $vencimento->addMonth(1)->format('Y-m-d') : $vencimento->format('Y-m-d'),
                'valor'        => $vp['valor_parcelas'],
                'sequencia'    => $i + 1,
                'status'       => 'A' // A - Aberto, P - Pago
            ]);
        }
    }

    /**
     * @param array $vp
     * @param $transacao_id
     */
    private function addVendaPredatado(array $vp, $transacao_id)
    {
        $predatado = Predatado::create([
            'transacao_id' => $transacao_id,
            'observacao'   => $vp['observacao'],
            'status'       => false
        ]);

        foreach ($vp['cheques'] as $cheque) {
            $cheque['predatado_id'] = $predatado->id;
            Cheque::create($cheque);
        }
    }

    /**
     * @param array $data
     * @return Transacao
     */
    public function salvarPagamentoParcela(array $data)
    {
        $transacao = $this->addTransacao();
        $this->addParcelaPagamentos($data['pagamentos'], $transacao->id);

        foreach ($data['parcelas'] as $p) {
            $parcela = Parcela::findOrFail($p['id']);
            $parcela->multa = $p['multa'];
            $parcela->juros = $p['juros'];
            $parcela->desconto = $p['desconto'];
            $valorFinal = $p['valor'] + $p['multa'] + $p['juros'] - $p['desconto'];

            if (count($p['transacoes']) > 0) {
                $valorTotalTransacoes = 0;

                foreach ($p['transacoes'] as $t) {
                    $valorTotalTransacoes += $t['pivot']['valor'];
                }

                ($valorFinal - $valorTotalTransacoes == 0) ? $parcela->status = 'P' : $parcela->status = 'X';

                $novoAbatimento = array_filter($p['transacoes'], function ($e) {
                    return !isset($e['parcela_id']);
                })[0];

                $parcela->transacoes()->attach($transacao->id, [
                    'abatimento' => true,
                    'valor'      => $novoAbatimento['pivot']['valor'],
                    'observacao' => $novoAbatimento['pivot']['observacao']
                ]);
            } else {
                $parcela->status = 'P';
                $parcela->transacoes()->attach($transacao->id, [
                    'abatimento' => false,
                    'valor'      => $valorFinal
                ]);
            }

            $parcela->save();
            $totalParcelas = $parcela->crediario->parcelas->count();
            $totalParcelasPagas = $parcela->crediario->parcelas->where('status', 'P')->count();
            if ($totalParcelas - $totalParcelasPagas == 0) {
                $parcela->crediario->update(['status' => 'F']);
            }
        }

        return $transacao;
    }

    private function addParcelaPagamentos(array $parcelaPagamentos, $transacao_id)
    {
        foreach ($parcelaPagamentos as $pp) {

            $pp['transacao_id'] = $transacao_id;

            ParcelaPagamento::create($pp);

            if ($pp['tipo_pagamento_id'] == 1) {
                $this->addParcelasDinheiro($pp, $transacao_id);
//            } elseif ($pp['tipo_pagamento_id'] == 2) {
//                $this->addParcelasPredatado($pp, $transacao_id);
            } elseif ($pp['tipo_pagamento_id'] == 3) {
                $this->addParcelasCartao($pp, $transacao_id, 'C');
            } elseif ($pp['tipo_pagamento_id'] == 4) {
                $this->addParcelasCartao($pp, $transacao_id, 'D');
            }
        }
    }

    private function addParcelasDinheiro(array $parcelaPagamento, $transacao_id)
    {
        Dinheiro::create([
            'transacao_id'   => $transacao_id,
            'valor_recebido' => $parcelaPagamento['valor'],
            'observacao'     => 'PAGAMENTO DE CREDIÁRIO'
        ]);
    }

    private function addParcelasPredatado(array $pp, $transacao_id)
    {
        $predatado = Predatado::create([
            'transacao_id' => $transacao_id,
            'observacao'   => $pp['observacao'],
            'status'       => false
        ]);

        foreach ($pp['cheques'] as $cheque) {
            $cheque['predatado_id'] = $predatado->id;
            Cheque::create($cheque);
        }
    }

    private function addParcelasCartao(array $parcelaPagamento, $transacao_id, $tipoCartao)
    {
        Cartao::create([
            'transacao_id'        => $transacao_id,
            'bandeira_id'         => $parcelaPagamento['bandeira_id'],
            'protocolo'           => $parcelaPagamento['protocolo'],
            'quantidade_parcelas' => 1,
            'valor_parcelas'      => $parcelaPagamento['valor'],
            'tipo'                => $tipoCartao,
        ]);
    }

    /**
     * @param Request $request
     * @param $vendaId
     * @return array
     */
    public function gerarCarne(Request $request, $vendaId)
    {
        $venda = Venda::with(['vendaPagamentos', 'pessoaCliente.enderecos'])->find($vendaId);

        $observacao = $request->get('observacao');
        if (!$observacao) {
            $observacao = $venda->transacoes->where('venda_id', $venda->id)->first()->crediarios->first()->observacao;
        }

        $crediario = $venda->vendaPagamentos->filter(function ($e) {
            return $e['tipo_pagamento_id'] == 4;
        })->values()[0];
        $parcelas = [];
        $vencimento = Carbon::parse($crediario['vencimento'], 'America/Sao_Paulo');
        $valor = round($crediario['valor'] / $crediario['parcelas'], 2);

        for ($i = 0; $i < $crediario['parcelas']; $i++) {
            $parcelas[$i] = [
                'numero'     => $i + 1,
                'total'      => $crediario['parcelas'],
                'valor'      => $valor,
                'vencimento' => $i > 0 ? $vencimento->copy()->addMonth($i)->format('d/m/Y') : $vencimento->format('d/m/Y'),
                'observacao' => $observacao == 'null' ? '' : $observacao
            ];
        }

        return [
            'venda'    => $venda,
            'parcelas' => $parcelas
        ];
    }

    /**
     * @param $tipoProcessamento
     * @param $chequeId
     */
    public function salvarCompensacaoCheque($tipoProcessamento, $chequeId)
    {
        $transacao = $this->addTransacao();

        $cheque = Cheque::find($chequeId);
        $cheque->status = true;
        $cheque->save();

        $transacao->cheques()->attach($cheque->id, ['tipo_processamento' => $tipoProcessamento]);

        if ($tipoProcessamento == 'S') {
            Dinheiro::create([
                'transacao_id'   => $transacao->id,
                'valor_recebido' => $cheque->valor,
                'observacao'     => "Saque do cheque Nº {$cheque->numero}, Banco {$cheque->banco}, AG {$cheque->agencia}, Conta {$cheque->conta}, Data {$cheque->vencimento->format('d/m/Y')}"
            ]);
        } elseif ($tipoProcessamento == 'D') {
            $data = [
                'caixa_id'      => $transacao->caixa_id,
                'valor'         => $cheque->valor,
                'descricao'     => "Depósito do cheque Nº {$cheque->numero}, Banco {$cheque->banco}, AG {$cheque->agencia}, Conta {$cheque->conta}, Data {$cheque->vencimento->format('d/m/Y')}",
                'hora'          => date('H:i:s'),
                'delete_action' => false
            ];
            Credito::create($data);
            Retirada::create($data);
        }

        if ($cheque->predatado->cheques->where('status', false)->count() == 0) {
            $cheque->predatado->update(['status' => true]);
        }
    }

    /**
     * @param array $data
     */
    public function salvarVencimento(array $data)
    {
        $transacao = $this->addTransacao();

        $data['transacao_id'] = $transacao->id;

        $vencimento = Vencimento::create($data);

        Retirada::create([
            'caixa_id'      => $transacao->caixa_id,
            'valor'         => $vencimento->valor,
            'hora'          => $transacao->created_at->format('H:i:s'),
            'delete_action' => false,
            'descricao'     => "{$vencimento->tipo->nome} - {$vencimento->pessoa->nome}"
        ]);
    }

    /**
     * @param array $data
     * @param $id
     * @throws \Exception
     */
    public function atualizarVencimento(array $data, $id)
    {
        $vencimento = Vencimento::findOrFail($id);

        if (!$vencimento->transacao->caixa->aberto) {
            throw new \Exception('Vencimentos não podem ser editados ou removidos se o caixa de seu lançamento estiver encerrado.',
                409);
        }

        $retirada = $vencimento->transacao->caixa->retiradas
            ->where('valor', $vencimento->valor)
            ->where('hora', $vencimento->transacao->created_at->format('H:i:s'))
            ->first();

        Retirada::findOrFail($retirada->id)->update(['valor' => $data['valor']]);

        $vencimento->update($data);
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function cancelarVencimento($id)
    {
        $vencimento = Vencimento::findOrFail($id);

        if (!$vencimento->transacao->caixa->aberto) {
            throw new \Exception('Vencimentos não podem ser editados ou removidos se o caixa de seu lançamento estiver encerrado.',
                409);
        }

        $retirada = $vencimento->transacao->caixa->retiradas
            ->where('valor', $vencimento->valor)
            ->where('hora', $vencimento->transacao->created_at->format('H:i:s'))
            ->first();

        Retirada::findOrFail($retirada->id)->delete();

        $transacao = Transacao::findOrFail($vencimento->transacao->id);

        $vencimento->delete();

        $transacao->delete();
    }
}
