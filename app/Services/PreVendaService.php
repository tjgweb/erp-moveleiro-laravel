<?php

namespace App\Services;

use App\Models\ProdutoVenda;
use App\Models\Venda;
use App\Models\VendaPagamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PreVendaService
{
    public function validaDados(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'vendedor_id' => 'required',
            'estatus_venda_id' => 'required',
            'tipo_venda_id' => 'required',
            'cliente_id' => 'required'
        ]);
        $validator->after(function ($validator) use ($data) {
            if (count($data['produto_vendas']) == 0 && count($data['combinado_vendas']) == 0) {
                $validator->errors()->add('carrinho', 'Adicione ao menos um produto no carrinho');
            }
            if (count($data['venda_pagamentos']) == 0) {
                $validator->errors()->add('pagamento', 'Adicione uma opção de pagamento');
            }
        });

        return $validator;
    }

    public function updateProdutoVenda(Venda $venda, array $data)
    {
        if (count($data['produto_vendas']) > 0) {
            foreach ($data['produto_vendas'] as $item) {
                ProdutoVenda::updateOrCreate(
                    ['venda_id' => $venda->id, 'produto_id' => $item['produto_id'], 'produto_combinado' => $item['produto_combinado'], 'combinado_id' => $item['combinado_id']],
                    ['quantidade' => $item['quantidade'], 'valor_unitario' => $item['valor_unitario'], 'desconto' => $item['desconto']]
                );
            }
        }

        $remove = [];
        foreach ($venda->produtoVendas as $old) {
            if (!$old->produto_combinado) {
                $confirm = true;
                foreach ($data['produto_vendas'] as $new) {
                    if ($old->produto_id == $new['produto_id']): $confirm = false;endif;
                }
                if ($confirm): $remove[] = $old->id;endif;
            }
        }
        if (count($remove) > 0): ProdutoVenda::destroy($remove);endif;

    }

    public function updateCombinadoVenda(Venda $venda, array $data)
    {
        if (count($data['combinado_vendas']) > 0) {
            foreach ($data['combinado_vendas'] as $combinado) {
                foreach ($combinado['combinado']['produtos'] as $produto) {
                    ProdutoVenda::updateOrCreate(
                        ['venda_id' => $venda->id, 'produto_id' => $produto['id'], 'produto_combinado' => $combinado['produto_combinado'], 'combinado_id' => $combinado['combinado_id']],
                        ['quantidade' => $produto['pivot']['quantidade'] * $combinado['quantidade'], 'valor_unitario' => $produto['pivot']['valor'], 'desconto' => $combinado['desconto']]
                    );
                }
            }
        }

        $remove = [];
        foreach ($venda->produtoVendas as $old) {
            if ($old->produto_combinado) {
                $confirm = true;
                foreach ($data['combinado_vendas'] as $combinado) {
                    if ($old->combinado_id == $combinado['combinado_id']) {
                        foreach ($combinado['combinado']['produtos'] as $produto) {
                            if ($old->produto_id == $produto['id']): $confirm = false;endif;
                        }
                    }
                }
                if ($confirm): $remove[] = $old->id;endif;
            }
        }
        if (count($remove) > 0): ProdutoVenda::destroy($remove);endif;

    }

    public function updatePagamento(Venda $venda, array $data)
    {
        foreach ($venda->vendaPagamentos as $item) {
            $item->delete();
        }

        foreach ($data['venda_pagamentos'] as $item) {
            $item['venda_id'] = $venda->id;
            VendaPagamento::create($item);
        }
    }
}
