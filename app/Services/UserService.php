<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserService
{
    public function createRules()
    {
        return [
            'apelido' => 'min:4|max:45|unique:users',
            'email' => 'email|unique:users',
            'password' => 'min:6|confirmed',
            'grupo_id' => 'required|numeric',
            'status' => 'required|numeric'
        ];
    }

    public function updateRules(Request $request, $id)
    {
        $rules = [
            'apelido' => ['min:4', 'max:45', Rule::unique('users')->ignore($id)],
            'email' => ['email', Rule::unique('users')->ignore($id)],
            'grupo_id' => 'required|numeric',
            'status' => 'required|numeric'
        ];

        $password = $request->get('password');
        if (isset($password)) {
            $rules['password'] = 'min:6|confirmed';
        }

        return $rules;
    }

    public function dataToUpdate(Request $request)
    {
        $data = [
            'apelido' => $request->get('apelido'),
            'email' => $request->get('email'),
            'grupo_id' => $request->get('grupo_id'),
            'pessoa_id' => $request->get('pessoa_id'),
            'status' => $request->get('status'),
        ];

        $password = $request->get('password');
        if (isset($password)) {
            $data['password'] = Hash::make($password);
        }

        return $data;
    }

    public function updateProfileRules(Request $request, $id)
    {
        $rules = [
            'apelido' => ['min:4', 'max:45', Rule::unique('users')->ignore($id)],
            'email' => ['required', 'email', Rule::unique('users')->ignore($id)]
        ];

        $password = $request->get('password');
        if (isset($password)) {
            $rules['password'] = 'min:6|confirmed';
        }

        return $rules;
    }

    public function dataToProfileUpdate(Request $request)
    {
        $data = [
            'apelido' => $request->get('apelido'),
            'email' => $request->get('email')
        ];

        $password = $request->get('password');
        if (isset($password)) {
            $data['password'] = Hash::make($password);
        }

        return $data;
    }
}
