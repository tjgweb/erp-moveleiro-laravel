<?php

namespace App\Services;

use App\Models\Custo;
use App\Models\NotaFiscal;

class EntradaService
{

    public function atualizaCusto($qtdProdutosEstoque, array $movimentoProduto, $unidadeMedida)
    {
        if (!$custo = Custo::find($movimentoProduto['produto_id'])) {
            $custo = Custo::create(['produto_id' => $movimentoProduto['produto_id']]);
        }

        $valor_total = $movimentoProduto['valor_unitario'] - $movimentoProduto['desconto'] / $movimentoProduto['quantidade'];
        if ($unidadeMedida->agrupado) {
            $valor_total = $valor_total / $movimentoProduto['quantidade_por_caixa'];
        }
        $valor_icms = $valor_total * $movimentoProduto['aliquota_icms'] / 100;
        $valor_ipi = $valor_total * $movimentoProduto['aliquota_ipi'] / 100;
        $valor_icms_sub = $movimentoProduto['valor_icms_substituicao'] / $movimentoProduto['quantidade'];
        if ($unidadeMedida->agrupado) {
            $valor_icms_sub = $valor_icms_sub / $movimentoProduto['quantidade_por_caixa'];
        }
        $custo_atual = $valor_total + $valor_icms + $valor_ipi + $valor_icms_sub;

        $custo->custo_medio = $qtdProdutosEstoque > 0 ? ($custo->custo_medio + $custo_atual) / 2 : $custo_atual;
        $custo->custo_atual = $custo_atual;
        $custo->data_atualizacao = date('Y-m-d');
        $custo->save();
    }

    public function countProdutosEstoque($produtoId)
    {
        $notasProduto = NotaFiscal::with('movimentoProdutos')
            ->whereHas('movimentoProdutos', function ($query) use ($produtoId) {
                $query->where('produto_id', $produtoId);
            })->get();

        $qtdEntrada = 0;
        $qtdSaida = 0;

        foreach ($notasProduto->where('movimento_tipo_id', '=', 2) as $entrada) {
            if ($entrada->agrupado) {
                $qtdEntrada += $entrada->quantidade * $entrada->quantidade_por_caixa;
            } else {
                $qtdEntrada += $entrada->quantidade;
            }
        }

        foreach ($notasProduto->where('movimento_tipo_id', '=', 1) as $saida) {
            $qtdSaida += $saida->quantidade;
        }

        return $qtdEntrada - $qtdSaida;
    }
}
