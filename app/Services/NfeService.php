<?php

namespace App\Services;


use App\Models\Configuracao;
use App\Models\LoteNfe;
use App\Models\MovimentoProduto;
use App\Models\NotaFiscal;
use Illuminate\Support\Carbon;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;
use NFePHP\NFe\Make;
use NFePHP\NFe\Tools;
use stdClass;


class NfeService
{
    /** @var Configuracao $configuracao */
    private $configuracao;

    private $configNfe;

    /** @var Make $nfe */
    private $nfe;

    /** @var Tools $tools */
    private $tools;

    /** @var LoteNfe $lote */
    private $lote;

    /** @var NotaFiscal $nota */
    private $nota;

    private $path;

    private $XML_transmissao;


    /**
     * NfeService constructor.
     * @param NotaFiscal $nota
     * @throws \Exception
     */
    public function __construct(NotaFiscal $nota)
    {
        $this->nota = $nota;

        $this->configuracao = Configuracao::findOrFail(1);

        $this->configNfe = [
            "atualizacao" => date('Y-m-d h:i:s', strtotime($this->configuracao->atualizacao)),
            "tpAmb"       => (int)$this->configuracao->tpAmb,
            "razaosocial" => $this->nota->lojaEmissor->razao_social,
            "cnpj"        => $this->nota->lojaEmissor->cnpj,
            "siglaUF"     => strtoupper($this->nota->lojaEmissor->cidade->estado->uf),
            "schemes"     => $this->configuracao->schemes,
            "versao"      => $this->configuracao->versao,
            "tokenIBPT"   => $this->nota->lojaEmissor->token_ibpt,
            "CSC"         => $this->nota->lojaEmissor->token_sefaz,
            "CSCid"       => $this->nota->lojaEmissor->token_sefaz_id
        ];

        $this->path = "uploads/nfe/" . date('Y') . "/" . date('m');

        if ($this->nota->loteNfe) {
            $this->lote = $this->nota->loteNfe;
            if (file_exists(public_path($this->nota->loteNfe->xmlTransmissao))) {
                $this->XML_transmissao = file_get_contents(public_path($this->nota->loteNfe->xmlTransmissao));
            }
        }

        $certificadoDigital = file_get_contents(storage_path("app/{$this->nota->lojaEmissor->certificado}"));
        $this->tools = new Tools(
            $this->configNfeToJson(),
            Certificate::readPfx($certificadoDigital, $this->nota->lojaEmissor->senha_certificado)
        );
    }

    /**
     *
     * @throws \Exception
     */
    public function makeNfe()
    {
        try {
            $this->lote = LoteNfe::create();
            $this->nota->lote_nfe_id = $this->lote->id;
            $this->nota->numero = formatCodNota($this->nota->id);

            $this->nfe = new Make();
//            $this->montarXML();
//            exit;
            $this->gerarXmlTransmissao();
            $this->nota->chave_acesso = $this->nfe->getChave();

            if ($this->enviaLote()) {
                $cStat = $this->consultaRecibo();
            } else {
                $this->nota->lote_nfe_id = null;
                $this->lote->delete();
                $cStat = 999;
            };

            $this->nota->save();

            return $cStat;

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * @return false|string
     */
    private function configNfeToJson()
    {
        return json_encode($this->configNfe);
    }

    /**
     * Gerar o xml para transmissão
     *
     * @return void
     * @throws \Exception
     */
    private function gerarXmlTransmissao()
    {
        try {
            $this->XML_transmissao = $this->assinarXML($this->montarXML());
            $filePath = "{$this->path}/nota_{$this->nota->id}_transmissao.xml";
            file_put_contents(public_path($filePath), $this->XML_transmissao);
            $this->lote->xmlTransmissao = $filePath;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function gerarNfeContigencia()
    {

    }

    /**
     * Retorna o status da nota
     *
     * @return mixed
     * @throws \Exception
     */
    private function enviaLoteSincrono()
    {
        try {
            $xmlResp = $this->tools->sefazEnviaLote([$this->XML_transmissao], $this->lote, 1);

            $std = (new Standardize())->toStd($xmlResp);


            return true;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Retorna o número do recibo
     *
     * @return mixed
     * @throws \Exception
     */
    private function enviaLote()
    {
        try {
            $xmlResp = $this->tools->sefazEnviaLote([$this->XML_transmissao], $this->lote);

            $std = (new Standardize())->toStd($xmlResp);

            if ($std->cStat != 103) {
                return false;
            }

            $this->lote->nRec = $std->infRec->nRec;
            $this->lote->cStat = $std->cStat;
            $this->lote->xMotivo = $std->xMotivo;
            $this->lote->dhRecbto = Carbon::createFromTimeString($std->dhRecbto)->toDayDateTimeString();

            $this->lote->save();

            return true;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Retorna
     * @return int
     * @throws \Exception
     */
    public function consultaRecibo()
    {
        try {
            $xmlResp = $this->tools->sefazConsultaRecibo($this->lote->nRec, $this->configuracao->tpAmb);

            $std = (new Standardize())->toStd($xmlResp);

            $this->lote->cStat = $std->cStat;
            $this->lote->xMotivo = $std->xMotivo;
            $this->lote->dhRecbto = Carbon::createFromTimeString($std->dhRecbto)->toDayDateTimeString();
            $filePath = "{$this->path}/nota_{$this->nota->id}_protocolo.xml";
            file_put_contents(public_path($filePath), $xmlResp);
            $this->lote->xmlProtocolo = $filePath;

            $this->lote->save();

            if ($std->cStat == '103') { //Lote enviado
                return 103;
            }
            if ($std->cStat == '105') { //Lote em processamento
                return 105;
            }
            if ($std->cStat == '104') { //lote processado (tudo ok)

                if ($std->protNFe->infProt->cStat == '100') { //Autorizado o uso da NF-e

                    $xmlFinal = Complements::toAuthorize($this->XML_transmissao, $xmlResp);
                    $filePath = "{$this->path}/nota_{$this->nota->id}.xml";
                    file_put_contents(public_path($filePath), $xmlFinal);
                    $this->nota->xml = $filePath;
                    $this->nota->status = $std->protNFe->infProt->cStat;

                    $this->nota->save();

                    return 100;
                } else { //denegada ou não autorizada (rejeição)
                    return 302;
                }

            } else { //outros erros possíveis
                return 302;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Faz assinatura do xml da nota fiscal e retorna um novo xml incluindo a tag com assinatura.
     *
     * @param $xml
     * @return string
     * @throws \Exception
     */
    private function assinarXML($xml)
    {
        try {
            return $this->tools->signNFe($xml);
        } catch (\Exception $e) {
            throw new \Exception("Erro ao assinar XML da nota!\n\n{$e->getMessage()}");
        }
    }

    /**
     * Monta o XML da nota fiscal
     *
     * @return string
     * @throws \Exception
     */
    private function montarXML()
    {
        $nfe = new Make();

        $nfe->taginfNFe($this->infNFe());
        $nfe->tagide($this->ide());
        $nfe->tagemit($this->emit());
        $nfe->tagenderEmit($this->enderEmit());
        $nfe->tagdest($this->dest());
        $nfe->tagenderDest($this->enderDest());
        $nfe->tagautXML($this->autXML($this->nota->lojaEmissor->cnpj));
        $nfe->tagautXML($this->autXML($this->configuracao->cnpj_contador));
        $item = 1;
        foreach ($this->nota->movimentoProdutos as $mp) {
            $nfe->tagprod($this->prod($item, $mp));
            $nfe->tagimposto($this->imposto($item, $mp));
            $nfe->tagICMSSN($this->ICMSSN($item, $mp));
            $nfe->tagIPI($this->IPI($item, $mp));
            $nfe->tagPIS($this->PIS($item, $mp));
            $nfe->tagCOFINS($this->COFINS($item, $mp));
            $item++;
        }
        $nfe->tagICMSTot($this->ICMSTot());
        $nfe->tagtransp($this->transp());
        $nfe->tagtransporta($this->transporta());
        $nfe->tagvol($this->vol());
        $nfe->tagpag($this->pag());
        $nfe->tagdetPag($this->detPag());
        $nfe->taginfAdic($this->infAdic());

        if ($nfe->monta()) {
            header('Content-type: text/xml; charset=UTF-8');
            echo $nfe->getXML();
            exit;
//            return $nfe->getXML();
        } else {
            throw new \Exception("Erro ao gerar NF-e");
        }
    }

    /**
     * @return stdClass
     */
    private function infNFe()
    {
        $std = new stdClass();
        $std->versao = $this->configuracao->versao; //versão do layout
        $std->Id = null;//se o Id de 44 digitos não for passado será gerado automaticamente
        $std->pk_nItem = null; //deixe essa variavel sempre como NULL

        return $std;
    }

    /**
     * @return stdClass
     * @throws \Exception
     */
    private function ide($finNFe = 1, $indFinal = 0, $indPres = 9)
    {
        $std = new stdClass();
        $std->cUF = $this->nota->lojaEmissor->cidade->estado->codigo_ibge; // Código IBGE do estado
        $std->cNF = formatCodNota($this->nota->id); // gerar o número da nota
        $std->natOp = $this->nota->naturezaOperacao->descricao;
        //$std->indPag = 0; //NÃO EXISTE MAIS NA VERSÃO 4.00
        $std->mod = (int)$this->nota->modelo;
        $std->serie = (int)$this->nota->serie;
        $std->nNF = $this->nota->id;
        $std->dhEmi = date('Y-m-d\TH:i:sP');
        $this->nota->data_emissao = date('Y-m-d H:i:s', strtotime($std->dhEmi));
        if ($this->nota->modelo == 55) {
            $std->dhSaiEnt = Carbon::createFromTimestamp(strtotime($this->nota->data_saida))
                ->addHours(explode(':', $this->nota->hora_saida)[0])
                ->addMinutes(explode(':', $this->nota->hora_saida)[1])
                ->toW3cString();
        } else {
            $std->dhSaiEnt = $std->dhEmi;
            $this->nota->hora_saida = date('H:i:s', strtotime($std->dhEmi));
        }
        $std->tpNF = $this->nota->movimento_tipo_id == 1 ? 1 : 0; // 0 - entrada ou 1 - saida
        $std->idDest = $this->nota->lojaEmissor->cidade->estado_id == $this->nota->enderecoDestinatario->estado_id ? 1 : 2; // 1 - dentro ou 2 - fora do estado - 3 exterior
        $std->cMunFG = $this->nota->lojaEmissor->cidade->codigo_ibge;
        $std->tpImp = 1; // 1 - retrato / 2 - paisagem
        $std->tpEmis = 1; // Tipo de Emissão da NF-e
        $std->cDV = null; // dígito verificador o id
        $std->tpAmb = $this->configuracao->tpAmb; // 1 = ambiente produção / 2 - ambiente homologação
        $std->finNFe = $finNFe; // 1 - NF-e normal / 2 - complementar / 3 - ajuste
        $std->indFinal = $indFinal; // consumidor final
        $std->indPres = $indPres;
        $std->procEmi = 0;
        $std->verProc = '1.0.0'; // versão do nosso sitema
        $std->dhCont = null; // Data e hora contigencia (sefaz não responde)
        $std->xJust = null; // Sefaz local não responde

        return $std;
    }

    /**
     * @return stdClass
     */
    private function emit()
    {
        $std = new stdClass();
        $std->xNome = $this->nota->lojaEmissor->razao_social;
        $std->xFant = $this->nota->lojaEmissor->razao_social;
        $std->IE = $this->nota->lojaEmissor->inscricao_estadual;
        $std->IEST = null;
        $std->IM = $this->nota->lojaEmissor->inscricao_municipal;
        $std->CNAE = null;
        $std->CRT = 1; // Simples nacional;
        $std->CNPJ = $this->nota->lojaEmissor->cnpj;
        $std->CPF = null;

        return $std;
    }

    private function enderEmit()
    {
        $std = new stdClass();
        $std->xLgr = $this->nota->lojaEmissor->logradouro;
        $std->nro = $this->nota->lojaEmissor->numero;
        $std->xCpl = $this->nota->lojaEmissor->complemento;
        $std->xBairro = $this->nota->lojaEmissor->bairro;
        $std->cMun = $this->nota->lojaEmissor->cidade->codigo_ibge;
        $std->xMun = $this->nota->lojaEmissor->cidade->nome;
        $std->UF = $this->nota->lojaEmissor->cidade->estado->uf;
        $std->CEP = $this->nota->lojaEmissor->cep;
        $std->cPais = $this->nota->lojaEmissor->cidade->estado->pais->codigo_bacen;
        $std->xPais = $this->nota->lojaEmissor->cidade->estado->pais->nome;
        $std->fone = $this->nota->lojaEmissor->telefone;

        return $std;
    }

    private function dest()
    {
        $std = new stdClass();
        $std->xNome = $this->nota->pessoaDestinatario->juridica->razao_social ?? $this->nota->pessoaDestinatario->nome;
        $std->indIEDest = 9;
        $std->IE = null;
        $std->ISUF = null;
        $std->IM = null;
        $std->email = null;
        $std->CNPJ = $this->nota->pessoaDestinatario->juridica->cnpj ?? null;
        $std->CPF = $this->nota->pessoaDestinatario->fisica->cpf ?? null;
        $std->idEstrangeiro = null;

        return $std;
    }

    private function enderDest()
    {
        $std = new stdClass();
        $std->xLgr = $this->nota->enderecoDestinatario->logradouro;
        $std->nro = $this->nota->enderecoDestinatario->numero;
        $std->xCpl = $this->nota->enderecoDestinatario->complemento;
        $std->xBairro = $this->nota->enderecoDestinatario->bairro;
        $std->cMun = $this->nota->enderecoDestinatario->cidade->codigo_ibge;
        $std->xMun = $this->nota->enderecoDestinatario->cidade->nome;
        $std->UF = $this->nota->enderecoDestinatario->estado->uf;
        $std->CEP = str_replace([' ', '.', '-'], ['', '', ''], $this->nota->enderecoDestinatario->cep);
        $std->cPais = $this->nota->enderecoDestinatario->estado->pais->codigo_bacen;
        $std->xPais = $this->nota->enderecoDestinatario->estado->pais->nome;
        $fone = null;
        if ($this->nota->pessoaDestinatario->contatos->count()) {
            $fone = $this->nota->pessoaDestinatario->contatos->where('principal', true)->first()->numero ??
                $this->nota->pessoaDestinatario->contatos->first()->numero ?? null;
            $fone = str_replace(['(', ')', ' ', '-'], ['', '', '', ''], $fone);
        }
        $std->fone = $fone;

        return $std;
    }

    /**
     * Quem está autorizado a baixar esta nota. Geralmente o contador ou a própria empresa
     *
     * @param $documento
     * @param bool $cnpj
     * @return stdClass
     */
    private function autXML($documento, $cnpj = true)
    {
        $std = new stdClass();
        if ($cnpj) {
            $std->CNPJ = $documento; //indicar um CNPJ ou CPF
        } else {
            $std->CPF = $documento;
        }

        return $std;
    }

    /**
     * Usar laço de repetição para adicionar produtos
     *
     * @param $item | número do item na nota fiscal (usar contador iniciando com 1)
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function prod($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->cProd = formatCodProduto($mp->produto->id);//str_replace(['.', ' '], ['', ''],$mp->naturezaOperacao->cfop);
        $std->cEAN = null;
        $std->xProd = $mp->produto->descricao_completa;
        $std->NCM = substr($mp->produto->mercadoria->categoria->numero_ncm, 0, 8);

        $std->cBenef = null; //incluido no layout 4.00

        $std->EXTIPI = null; // Contador
        $std->CFOP = str_replace(['.', ' '], ['', ''], $mp->naturezaOperacao->cfop);
        $std->uCom = $mp->unidadeMedida->unidade;
        $std->qCom = $mp->quantidade;
        $std->vUnCom = number_format($mp->valor_unitario, 2, '.', '');
        $std->vProd = number_format($mp->valor_total, 2, '.', '');
        //$std->cEANTrib = null;
        $std->uTrib = $mp->unidadeMedida->unidade;
        $std->qTrib = $mp->quantidade;
        $std->vUnTrib = number_format($mp->valor_unitario, 2, '.', '');
        $std->vFrete = null;
        $std->vSeg = null;
        $std->vDesc = $mp->desconto > 0 ? number_format($mp->desconto, 2, '.', '') : null;
        $std->vOutro = null;
        $std->indTot = 1;
        $std->xPed = null;
        $std->nItemPed = null;
        $std->nFCI = null;

        return $std;
    }

    /**
     * Imposto para cada item - Gerar laço
     *
     * @param $item | número do item
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function imposto($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //número do item da NFe
        $std->vTotTrib = 0;

        return $std;
    }

    /**
     * ICMS Simples Nacional
     * Tag para cada item de produto
     *
     * @param $item | número do item
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function ICMSSN($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->orig = 0;
        $std->CSOSN = '102';
        $std->pCredSN = null; //2.00;
        $std->vCredICMSSN = null; //2.00;
        $std->modBCST = null;
        $std->pMVAST = null;
        $std->pRedBCST = null;
        $std->vBCST = null;
        $std->pICMSST = null;
        $std->vICMSST = null;
        $std->vBCFCPST = null; //incluso no layout 4.00
        $std->pFCPST = null; //incluso no layout 4.00
        $std->vFCPST = null; //incluso no layout 4.00
        $std->vBCSTRet = null;
        $std->pST = null;
        $std->vICMSSTRet = null;
        $std->vBCFCPSTRet = null; //incluso no layout 4.00
        $std->pFCPSTRet = null; //incluso no layout 4.00
        $std->vFCPSTRet = null; //incluso no layout 4.00
        $std->modBC = null;
        $std->vBC = null;
        $std->pRedBC = null;
        $std->pICMS = null;
        $std->vICMS = null;
        $std->pRedBCEfet = null;
        $std->vBCEfet = null;
        $std->pICMSEfet = null;
        $std->vICMSEfet = null;
        $std->vICMSSubstituto = null;

        return $std;
    }

    /**
     * IPI para cada item da nota
     *
     * @param $item | número do item
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function IPI($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->clEnq = null;
        $std->CNPJProd = null;
        $std->cSelo = null;
        $std->qSelo = null;
        $std->cEnq = '999';
        $std->CST = '99';
        $std->vIPI = number_format($mp->valor_ipi, 2, '.', '');
        $std->vBC = number_format($mp->valor_total, 2, '.', '');
        $std->pIPI = $mp->aliquota_ipi;
        $std->qUnid = null;
        $std->vUnid = null;

        return $std;
    }

    /**
     * PIS para cada item da nota
     *
     * @param $item | número do item
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function PIS($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->CST = '99';
        $std->vBC = null;
        $std->pPIS = null;
        $std->vPIS = null;
        $std->qBCProd = null;
        $std->vAliqProd = null;

        return $std;
    }

    /**
     * COFINS para cada item da nota
     *
     * @param $item | número do item
     * @param MovimentoProduto $mp
     * @return stdClass
     */
    private function COFINS($item, MovimentoProduto $mp)
    {
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->CST = '99';
        $std->vBC = null;
        $std->pCOFINS = null;
        $std->vCOFINS = null;
        $std->qBCProd = null;
        $std->vAliqProd = null;

        return $std;
    }

    /**
     *
     * @param bool $calculoAutomatico
     * @return stdClass
     */
    private function ICMSTot(bool $calculoAutomatico = true)
    {
        if ($calculoAutomatico) {
            $std = new stdClass();
            $std->vBC = null;
            $std->vICMS = null;
            $std->vICMSDeson = null;
            $std->vFCP = null; //incluso no layout 4.00
            $std->vBCST = null;
            $std->vST = null;
            $std->vFCPST = null; //incluso no layout 4.00
            $std->vFCPSTRet = null; //incluso no layout 4.00
            $std->vProd = null;
            $std->vFrete = null;
            $std->vSeg = null;
            $std->vDesc = null;
            $std->vII = null;
            $std->vIPI = null;
            $std->vIPIDevol = null; //incluso no layout 4.00
            $std->vPIS = null;
            $std->vCOFINS = null;
            $std->vOutro = null;
            $std->vNF = null;
            $std->vTotTrib = null;

            return $std;
        } else {
            $std = new stdClass();
            $std->vBC = 1000.00;
            $std->vICMS = 1000.00;
            $std->vICMSDeson = 1000.00;
            $std->vFCP = 1000.00; //incluso no layout 4.00
            $std->vBCST = 1000.00;
            $std->vST = 1000.00;
            $std->vFCPST = 1000.00; //incluso no layout 4.00
            $std->vFCPSTRet = 1000.00; //incluso no layout 4.00
            $std->vProd = 1000.00;
            $std->vFrete = 1000.00;
            $std->vSeg = 1000.00;
            $std->vDesc = 1000.00;
            $std->vII = 1000.00;
            $std->vIPI = 1000.00;
            $std->vIPIDevol = 1000.00; //incluso no layout 4.00
            $std->vPIS = 1000.00;
            $std->vCOFINS = 1000.00;
            $std->vOutro = 1000.00;
            $std->vNF = 1000.00;
            $std->vTotTrib = 1000.00;

            return $std;
        }
    }

    private function transp()
    {
        $std = new stdClass();
        $std->modFrete = $this->nota->carga->frete_pagador;

        return $std;
    }

    private function transporta()
    {
        $std = new stdClass();
        $std->xNome = $this->nota->carga->transportador->pessoa->juridica ?
            $this->nota->carga->transportador->pessoa->juridica->razao_social :
            $this->nota->carga->transportador->pessoa->nome;
        $std->IE = $this->nota->carga->transportador->pessoa->juridica->inscricao_estadual ?? null;
        $std->xEnder = $this->nota->carga->transportador->pessoa->enderecos->where('principal', true)
                ->first()->logradouro_completo ?? $this->nota->carga->transportador->pessoa->enderecos
                ->first()->logradouro_completo ?? null;
        $std->xMun = $this->nota->carga->transportador->pessoa->enderecos->where('principal', true)
                ->first()->cidade->nome ?? $this->nota->carga->transportador->pessoa->enderecos
                ->first()->cidade->nome ?? null;
        $std->UF = $this->nota->carga->transportador->pessoa->enderecos->where('principal', true)
                ->first()->estado->uf ?? $this->nota->carga->transportador->pessoa->enderecos
                ->first()->estado->uf ?? null;
        $std->CNPJ = $this->nota->carga->transportador->pessoa->juridica->cnpj ?? null;
        $std->CPF = $this->nota->carga->transportador->pessoa->fisica->cpf ?? null;

        return $std;
    }

    private function vol()
    {
        $std = new stdClass();
        $std->item = 1; //indicativo do numero do volume
        $std->qVol = $this->nota->carga->quantidade;
        $std->esp = $this->nota->carga->especie ?? null;
        $std->marca = $this->nota->carga->marca ?? null;
        $std->nVol = $this->nota->carga->numeracao ?? null;
        $std->pesoL = number_format($this->nota->carga->peso_liquido, 3, '.', '.');
        $std->pesoB = number_format($this->nota->carga->peso_bruto, 3, '.', '.');

        return $std;
    }

    private function pag()
    {
        $std = new stdClass();
        $std->vTroco = 0; //incluso no layout 4.00, obrigatório informar para NFCe (65)

        return $std;
    }

    private function detPag()
    {
        /*
           NOTA: para NFe (modelo 55), temos ...
            vPag=0.00 mas pode ter valor se a venda for a vista

            tPag é usualmente:

            14 = Duplicata Mercantil
            15 = Boleto Bancário
            90 = Sem pagamento
            99 = Outros
            Porém podem haver casos que os outros nodes e valores tenha de ser usados.
          */
        $std = new stdClass();
        $std->tPag = '99';
        $std->vPag = number_format($this->nota->valor_total_nota, 2, '.',
            ''); //Obs: deve ser informado o valor pago pelo cliente
        $std->CNPJ = null;
        $std->tBand = null;
        $std->cAut = null;
        $std->tpIntegra = null; //incluso na NT 2015/002
        $std->indPag = null; //0= Pagamento à Vista 1= Pagamento à Prazo

        return $std;
    }

    private function infAdic()
    {
        $std = new stdClass();
        $infAdFisco = null;
        $infCpl = null;

        if ($this->nota->modelo == 55) {
            $infCpl = "DOCUMENTO EMITIDO POR ME OU PP OPTANTE PELO SIMPLES NACIONAL;\n";
            $infCpl .= "NÃO GERA DIREITO A CRÉDITO FISCAL DE ICMS, DE ISS E DE IPI;";
        }

        $std->infAdFisco = $infAdFisco;
        $std->infCpl = $infCpl;

        return $std;
    }
}
