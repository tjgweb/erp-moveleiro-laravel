<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CaixaSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('caixa_id')) {

            if ($request->ajax()) {

                return response()->json(['Você deve escolher qual caixa irá realizar o atendimento.'], 422);
            }

            toastr()->warning('Você deve escolher qual caixa irá realizar esta operação.', 'Abertura de Caixa');

            return redirect()->route('caixa.index');
        }

        return $next($request);
    }
}
