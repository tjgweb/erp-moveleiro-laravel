<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->status === 0) {
            auth()->logout();
            $request->session()->invalidate();

            if ($request->ajax()) {
                return response()->json('Unauthorized', 401);
            } else {
                toastr()->warning('Contate o administrador do sistema', 'Usuário Bloqueado');
                return redirect()->route('login');
            }
        }

        return $next($request);
    }
}
