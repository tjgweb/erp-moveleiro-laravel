<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class Acl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (auth()->guest()) {
            return $response;
        }

        $currentRouteAction = Route::getCurrentRoute()->action['uses'];
        $routesActionName = getRoutesActionName();
        $user = auth()->user();

        foreach ($routesActionName as $routeActionName) {
            if ($routeActionName == $currentRouteAction) {
                foreach ($user->grupo->permissoes as $permission) {
                    if ($permission->action == $currentRouteAction) {
                        return $response;
                    }
                }
                if ($request->ajax()) {
                    return response()->json('Forbidden', 403);
                } else {
                    toastr()->warning('Você não tem permissão de acesso');
                    return redirect(redirectUserType($user));
                }
            }
        }

        return $response;

    }
}
