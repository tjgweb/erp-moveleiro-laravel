<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Caixa;
use App\MOdels\EstatusVenda;
use App\Models\Venda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Gestão') */
class RelatoriosController extends Controller
{
    /** @permissionName('Relatórios diários de caixa') */
    public function caixaDiario(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $form = [
            'start' => $start ?? Carbon::create()->firstOfMonth()->format('Y-m-d'),
            'end' => $end ?? Carbon::create()->format('Y-m-d')
        ];

        $caixas = Caixa::whereDate('created_at', '>=', $form['start'])
            ->whereDate('created_at', '<=', $form['end'])
            ->paginate();

        return view('gestao.caixa.relatorios-diarios', compact(['form', 'caixas']));
    }

    /** @permissionName('Relatórios mensais de caixa') */
    public function caixaMensal(Request $request)
    {
        $ano = $request->get('ano') ?? Carbon::create()->format('Y');

        $meses = Caixa::whereYear('created_at', $ano)
            ->orderBy('created_at')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('m');
            });

        return view('gestao.caixa.relatorios-mensais', compact(['ano', 'meses']));
    }

    public function vendas(Request $request)
    {
        $status = $request->get('status');
        $start = $request->get('start');
        $end = $request->get('end');

        $form = [
            'status' => $status ?? 0,
            'start' => $start ?? Carbon::create()->firstOfMonth()->format('Y-m-d'),
            'end' => $end ?? Carbon::create()->format('Y-m-d')
        ];

        $statusVenda = EstatusVenda::all();

        if ($form['status'] == 0) {
            $vendas = Venda::whereDate('updated_at', '>=', $form['start'])
                ->whereDate('updated_at', '<=', $form['end'])
                ->orderBy('updated_at')
                ->orderBy('vendedor_id')
                ->get();
        } elseif (in_array($form['status'], [2, 6, 7, 8])) {
            $vendas = Venda::where('estatus_venda_id', $form['status'])
                ->whereDate('data_venda', '>=', $form['start'])
                ->whereDate('data_venda', '<=', $form['end'])
                ->orderBy('data_venda')
                ->orderBy('vendedor_id')
                ->get();
        } else {
            $vendas = Venda::where('estatus_venda_id', $form['status'])
                ->whereDate('updated_at', '>=', $form['start'])
                ->whereDate('updated_at', '<=', $form['end'])
                ->orderBy('updated_at')
                ->orderBy('vendedor_id')
                ->get();
        }

        return view('gestao.vendas.relatorios', compact(['form', 'statusVenda', 'vendas']));
    }
}
