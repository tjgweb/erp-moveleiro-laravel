<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Cidade;
use App\Models\Endereco;
use App\Models\Estado;
use App\Models\Regiao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Pessoas') */
class EnderecoController extends Controller
{

    /** @permissionName('Listar endereços') */
    public function lista($pessoaId)
    {
        $data = Endereco::with(['cidade', 'estado'])->where('pessoa_id', $pessoaId)->get();
        return response()->json($data);
    }

    /** @permissionName('Criar novo endereço') */
    public function create(Request $request)
    {
        $request->validate([
            'pessoa_id' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            'titulo' => 'required',
            'logradouro' => 'required',
            'bairro' => 'required',
            'numero' => 'required',
            'cep' => 'required|min:8|max:9'
        ]);

        $data = Endereco::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar endereços') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pessoa_id' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            'titulo' => 'required',
            'logradouro' => 'required',
            'bairro' => 'required',
            'numero' => 'required',
            'cep' => 'required|min:8|max:9'
        ]);

        $data = Endereco::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover endereços') */
    public function destroy($id)
    {
        return response()->json(Endereco::find($id)->delete());
    }

    public function regioes()
    {
        return response()->json(Regiao::all());
    }

    public function estados()
    {
        return response()->json(Estado::all());
    }

    public function cidades($estadoId)
    {
        if ($estadoId) {
            $data = Cidade::where('estado_id', $estadoId)->get();
            return response()->json($data);
        }

        return response()->json(Cidade::all());
    }
}
