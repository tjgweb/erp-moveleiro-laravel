<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Financeira;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Configurações') */
class FinanceirasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionName('Financeiras - Listar')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financeiras = Financeira::orderBy('nome')->paginate();
        return view('gestao.financeiras.index', compact('financeiras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = 'add';
        $title = 'Adicionar nova Financeira | ';
        return view('gestao.financeiras.form', compact(['form', 'title']));
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Financeiras - Criar nova')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'banco_credito' => 'required'
        ]);

        Financeira::create($request->all());
        toastr()->success('Criado com sucesso');

        return redirect()->route('gestao.financeiras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeira = Financeira::find($id);
        $form = 'remove';
        $title = 'Remover Financeira | ';
        return view('gestao.financeiras.form', compact(['financeira', 'form', 'title']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeira = Financeira::find($id);
        $form = 'edit';
        $title = 'Editar Financeira | ';
        return view('gestao.financeiras.form', compact(['financeira', 'form', 'title']));
    }

    /**
     * Update the specified resource in storage.
     * @permissionName('Financeiras - Atualizar')
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' => 'required',
            'banco_credito' => 'required'
        ]);

        Financeira::find($id)->update($request->all());
        toastr()->success('Atualizado com sucesso');

        return redirect()->route('gestao.financeiras.index');
    }

    /**
     * Remove the specified resource from storage.
     * @permissionName('Financeiras - Remover')
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $financeira = Financeira::find($id);

        if ($financeira->creditos()->count() > 0) {
            toastr()->info('Não pode excluir quando existe registro de pagamentos associados a esta Financeira', 'Operação não realizada');
            return redirect()->route('gestao.financeiras.show', ['id' => $id]);
        }
        $financeira->delete();
        toastr()->success('Removido com sucesso.');
        return redirect()->route('gestao.financeiras.index');
    }
}
