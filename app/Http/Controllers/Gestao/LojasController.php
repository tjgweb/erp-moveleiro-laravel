<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Loja;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/** @permissionGroup('Configurações') */
class LojasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionName('Diversas - Listar')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Loja::orderBy('descricao')->paginate();
        return view('gestao.lojas.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            'type'         => 'add',
            'method'       => 'POST',
            'action'       => route('gestao.lojas.store'),
            'title'        => 'Adicionar nova Loja / Depósito',
            'submit-class' => 'btn-primary',
            'submit-title' => 'Salvar'
        ];
        return view('gestao.lojas.form', compact(['form']));
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Lojas / Depósitos - Criar nova')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'descricao'           => 'required|max:100',
            'razao_social'        => 'required|max:255',
            'cnpj'                => 'required',
            'inscricao_estadual'  => 'required|max:20',
            'inscricao_minucipal' => 'max:20',
            'token_ibpt'          => 'required|max:255',
            'token_sefaz'         => 'required|max:255',
            'token_sefaz_id'      => 'required|max:16',
            'certificado'         => 'required|file',
            'senha_certificado'   => 'required|max:16',
            'cep'                 => 'required',
            'logradouro'          => 'required|max:255',
            'numero'              => 'required|max:10',
            'complemento'         => 'max:20',
            'bairro'              => 'required|max:100',
            'cidade_id'           => 'required',
            'estado_id'           => 'required',
        ]);

        try {
            DB::beginTransaction();
            $data = $request->all();
            unset($data['certificado']);
            $loja = Loja::create($data);

            if ($request->hasFile('certificado')) {
                $file = $request->file('certificado');
                Storage::disk('local')->putFileAs('/', $file,
                    "certificado_digital_{$loja->id}.{$file->getClientOriginalExtension()}");
                $loja->certificado = "certificado_digital_{$loja->id}.{$file->getClientOriginalExtension()}";
                $loja->save();
            }

            toastr()->success('Criado com sucesso');

            DB::commit();

            return redirect()->route('gestao.lojas.index');
        } catch (\Exception $e) {
            DB::rollBack();

            toastr()->error($e->getMessage(), 'Um erro aconteceu!');

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loja = Loja::find($id);
        $certificado = Storage::disk('local')->exists($loja->certificado);
        $form = [
            'type'         => 'remove',
            'method'       => 'DELETE',
            'action'       => route('gestao.lojas.destroy', ['id' => $id]),
            'title'        => 'Remover Loja / Depósito',
            'submit-class' => 'btn-danger',
            'submit-title' => 'Remover'
        ];
        return view('gestao.lojas.form', compact(['loja', 'form', 'certificado']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loja = Loja::find($id);
        $certificado = Storage::disk('local')->exists($loja->certificado);
        $form = [
            'type'         => 'edit',
            'method'       => 'PUT',
            'action'       => route('gestao.lojas.update', ['id' => $id]),
            'title'        => 'Editar Loja / Depósito',
            'submit-class' => 'btn-warning',
            'submit-title' => 'Atualizar'
        ];
        return view('gestao.lojas.form', compact(['loja', 'form', 'certificado']));
    }

    /**
     * Update the specified resource in storage.
     * @permissionName('Lojas / Depósitos - Atualizar')
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'descricao'           => 'required|max:100',
            'razao_social'        => 'required|max:255',
            'cnpj'                => 'required',
            'inscricao_estadual'  => 'required|max:20',
            'inscricao_minucipal' => 'max:20',
            'token_ibpt'          => 'required|max:255',
            'token_sefaz'         => 'required|max:255',
            'token_sefaz_id'      => 'required|max:16',
            'senha_certificado'   => 'required|max:16',
            'cep'                 => 'required',
            'logradouro'          => 'required|max:255',
            'numero'              => 'required|max:10',
            'complemento'         => 'max:20',
            'bairro'              => 'required|max:100',
            'cidade_id'           => 'required',
            'estado_id'           => 'required',
        ]);

        try {
            DB::beginTransaction();

            $data = $request->all();

            $loja = Loja::find($id);

            if ($request->hasFile('certificado')) {
                if (Storage::disk('local')->exists($loja->certificado)) {
                    Storage::disk('local')->delete($loja->certificado);
                }

                $file = $request->file('certificado');

                Storage::disk('local')->putFileAs('/', $file,
                    "certificado_digital_{$id}.{$file->getClientOriginalExtension()}");
                $data['certificado'] = "certificado_digital_{$id}.{$file->getClientOriginalExtension()}";
            } else {
                unset($data['certificado']);
            }

            $loja->update($data);

            toastr()->success('Atualizado com sucesso');

            DB::commit();

            return redirect()->route('gestao.lojas.index');
        } catch (\Exception $e) {
            DB::rollBack();

            toastr()->error($e->getMessage(), 'Um erro aconteceu!');

            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @permissionName('Lojas / Depósitos - Remover')
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $loja = Loja::find($id);

        if ($loja->saidas()->count() > 0 || $loja->entradas()->count() > 0) {
            toastr()->info('Não pode excluir Loja ou Depósito que contenha histórico de movimento.',
                'Operação não realizada');
            return redirect()->route('gestao.lojas.show', ['id' => $id]);
        }

        try {
            DB::beginTransaction();
            if (Storage::disk('local')->exists($loja->certificado)) {
                Storage::disk('local')->delete($loja->certificado);
            }

            $loja->delete();
            toastr()->success('Removido com sucesso.');
            DB::commit();
            return redirect()->route('gestao.lojas.index');
        } catch (\Exception $e) {
            DB::rollBack();
            toastr()->error($e->getMessage(), 'Um erro aconteceu!');

            return back();
        }

    }
}
