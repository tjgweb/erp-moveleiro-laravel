<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Configuracao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Configurações') */
class ConfiguracaoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionName('Diversas - Listar')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Configuracao::find(1);

        return view('gestao.configuracoes.index', compact(['data']));
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Diversas - Atualizar')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'multa'     => 'required',
            'juros_dia' => 'required',
            'tpAmb'     => 'required',
            'schemes'   => 'required',
            'versao'    => 'required'
        ]);

        try {
            DB::beginTransaction();

            Configuracao::findOrFail(1)->update($request->all());

            DB::commit();

            toastr()->success("Salvo com sucesso!");

            return redirect()->route('gestao.configuracoes.index');

        } catch (\Exception $e) {

            DB::rollBack();

            toastr()->error($e->getMessage(), 'Ocorreu um erro!');

            return back();
        }

    }

}
