<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Caixa;
use App\Models\Pessoa;
use App\Models\Venda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Gestão') */
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionDashboard('Gestão')
     * @permissionName('Painel Administrativo')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $concluidas = Venda::where('estatus_venda_id', 2)
            ->whereBetween('updated_at', [Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()])
            ->count();
        $autorizacao = Venda::where('estatus_venda_id', 3)
            ->whereBetween('updated_at', [Carbon::now()->subDays(3), Carbon::now()])
            ->count();
        $cancelada = Venda::where('estatus_venda_id', 5)
            ->whereBetween('updated_at', [Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()])
            ->count();
        $caixas = Caixa::where('aberto', true)->get();
        $pessoas = Pessoa::all();

        return view('gestao.dashboard', [
            'concluidas' => $concluidas,
            'autorizacao' => $autorizacao,
            'cancelada' => $cancelada,
            'caixas' => $caixas,
            /*'clientes' => $pessoas->padrao()->where('id', 1)->count(),
            'funcionarios' => $pessoas->padrao()->where('id',2)->count(),
            'fornecedores' => $pessoas->padrao()->where('id',3)->count(),
            'transportadoras' => $pessoas->padrao()->where('id',4)->count(),
            'prestadorServicos' => $pessoas->padrao()->where('id',5)->count(),*/
        ]);
    }

}
