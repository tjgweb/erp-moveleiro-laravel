<?php

namespace App\Http\Controllers\Gestao\Seguranca;

use App\Models\Grupo;
use App\Models\Pessoa;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

/** @permissionGroup('Usuários do sistema') */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /** @permissionName('Menu de acesso') */
    public function index()
    {
        return view('gestao.seguranca.usuarios-index');
    }

    /** @permissionName('Listar usuários') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = User::with(['grupo', 'pessoa'])
                ->where('apelido', 'like', "%{$search}%")
                ->orWhere('email', 'like', "%{$search}%")
                ->orWhereHas('pessoa', function ($query) use ($search) {
                    $query->where('nome', 'like', "%{$search}%");
                })
                ->paginate($pages);
        } else {
            $data = User::with(['grupo', 'pessoa'])->paginate($pages);
        }
        return response()->json($data);
    }

    public function listaGrupos()
    {
        return response()->json(Grupo::all());
    }

    public function listaFuncionarios()
    {
        return response()->json(Pessoa::whereHas('padrao', function($query){
            $query->where('padrao_id', 2);
        })->get());
    }

    /** @permissionName('Criar usuários') */
    public function store(Request $request)
    {
        $request->validate($this->service->createRules());
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);

        return response()->json($user);
    }

    /** @permissionName('Atualizar usuários') */
    public function update(Request $request, $id)
    {
        $request->validate($this->service->updateRules($request, $id));
        $user = User::with('grupo')->find($id);
        $user->update($this->service->dataToUpdate($request));

        return response()->json($user);
    }

    /** @permissionName('Remover usuários') */
    public function destroy($id)
    {
        return response()->json(User::find($id)->delete());
    }
}
