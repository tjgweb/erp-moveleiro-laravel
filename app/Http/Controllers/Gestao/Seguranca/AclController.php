<?php

namespace App\Http\Controllers\Gestao\Seguranca;

use App\Models\Grupo;
use App\Models\Permissao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Níveis de acesso') */
class AclController extends Controller
{

    /** @permissionName('Acesso ao Menu') */
    public function index()
    {
        DB::beginTransaction();
        try {
            $mapPermissions = getMapPermissions();

            /* Busca e remove permissões no banco de dados que não são mais mapeadas pelo sistema */
            $permissoesParaRemover = Permissao::all()->filter(function ($permissao) use ($mapPermissions) {
                $equals = false;
                foreach ($mapPermissions as $mapPermission) {
                    if ($permissao->action == $mapPermission['action']) {
                        $equals = true;
                    }
                }
                return !$equals;
            });

            if ($permissoesParaRemover->count()) {
                foreach ($permissoesParaRemover as $permissao) {
                    if ($permissao->grupos->count()) {
                        $permissao->grupos()->detach();
                    }
                    $permissao->delete();
                }
            }

            /* Atualiza ou Cria permissões no banco de dados de acordo com o mapeamento do sistema */
            foreach ($mapPermissions as $permission) {
                Permissao::updateOrCreate(
                    ['action' => $permission['action']],
                    ['method' => $permission['method'], 'controller' => $permission['controller']]
                );
            }

            DB::commit();
        } catch (\Exception $e) {
            toastr()->error('Erro ao sincronizar permissões');
            DB::rollBack();
        }

        return view('gestao.seguranca.acl-index');
    }

    /** @permissionName('Listar grupos') */
    public function listaGrupos()
    {
        $grupos = Grupo::with(['users', 'permissoes'])->get();
        $mapDashboards = getMapDashboards()->toArray();

        return response()->json(['grupos' => $grupos, 'mapDashboards' => $mapDashboards]);
    }

    public function listaPermissoes()
    {
        $permissoes = Permissao::with('grupos')
            ->orderBy('controller')
            ->orderBy('method')
            ->get()
            ->groupBy('controller');

        return response()->json($permissoes);
    }

    /** @permissionName('Criar grupo') */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'min:3|max:50',
            'descricao' => 'min:3|max:255',
            'dashboard' => 'required'
        ], [
            'dashboard.required' => 'Selecione uma opção'
        ]);

        return response()->json(Grupo::create($request->all()));
    }

    /** @permissionName('Atualizar grupo') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' => 'min:3|max:50',
            'descricao' => 'min:3|max:255',
            'dashboard' => 'required'
        ], [
            'dashboard.required' => 'Selecione uma opção'
        ]);

        return response()->json(Grupo::find($id)->update($request->all()));
    }

    /** @permissionName('Atribuir permissões aos grupos') */
    public function syncGrupoPermissoes(Request $request, $grupoId)
    {
        $grupo = Grupo::find($grupoId);
        $grupo->permissoes()->sync($request->all());

        return response()->json($grupo);
    }

    /** @permissionName('Remover Grupo') */
    public function destroy($id)
    {
        $data = Grupo::find($id);

        if ($data->users()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir Grupos associadas a um usuário do sistema.'
            ], 409);
        }

        if ($data->permissoes()->count() > 0) {
            $data->permissoes()->detach($data->permissoes->map(function ($permissao, $key) {
                return $permissao->id;
            }));
        }

        return response()->json($data->delete());
    }
}
