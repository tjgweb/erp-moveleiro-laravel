<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Fisica;
use App\Models\Funcionario;
use App\Models\Juridica;
use App\Models\Padrao;
use App\Models\Pessoa;
use App\Models\TipoIdentidade;
use App\Models\Transportador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/** @permissionGroup('Pessoas') */
class PessoaController extends Controller
{
    /** @permissionName('Acesso ao Menu no painel de Gestão') */
    public function index()
    {
        return view('gestao.pessoas');
    }

    /** @permissionName('Acesso ao Menu no painel de Estoque') */
    public function menuEstoque()
    {
        return view('estoque.pessoas');
    }

    /** @permissionName('Acesso ao Menu no painel de Vendas') */
    public function menuVendas()
    {
        return view('vendas.pessoas');
    }

    /** @permissionName('Acesso ao Menu no painel de Caixa') */
    public function menuCaixa()
    {
        return view('caixa.pessoas');
    }

    /** @permissionName('Listar Pessoas') */
    public function lista(Request $request, $pages = 15)
    {
        $search = str_replace(['.', '-', '/'], ['', '', ''], $request->get('search'));
        if (isset($search)) {
            $data = Pessoa::with(['fisica', 'juridica', 'transportador', 'padrao', 'funcionario'])
                ->whereHas('fisica', function ($query) use ($search) {
                    $query->where('cpf', 'like', "{$search}%");
                })
                ->orWhereHas('juridica', function ($query) use ($search) {
                    $query->where('cnpj', 'like', "{$search}%")
                        ->orWhere('razao_social', 'like', "%{$search}%");
                })
                ->orWhere('nome', 'like', "%{$search}%")
                ->paginate($pages);
        } else {
            $data = Pessoa::with(['fisica', 'juridica', 'transportador', 'padrao', 'funcionario'])
                ->paginate($pages);
        }

        return response()->json($data);
    }

    /** @permissionName('Criar nova Pessoa') */
    public function create(Request $request, $tipoPessoaId)
    {
        $dados = $request->all();
        if ($tipoPessoaId == 1) {
            $validator = Validator::make($dados, [
                'nome' => 'required|max:100',
                'tipo_pessoa_id' => 'required',
                'padrao_id' => 'required',
                'status' => 'required',
                'cpf' => 'min:11|unique:fisica,cpf',
            ]);
            /*$validator->after(function ($validator) use ($dados) {
                if ($dados['cpf'] && !(validaCpf($dados['cpf']))) {
                    $validator->errors()->add('cpf', 'Número de CPF inválido');
                }
            });*/
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->toArray()], 422);
            }

            DB::beginTransaction();
            try {
                $pessoa = Pessoa::create($dados);
                $pessoa->padrao()->sync($dados['padrao_id']);
                $dados['pessoa_id'] = $pessoa->id;
                Fisica::create($dados);
                if (in_array(4, $dados['padrao_id'])) {
                    Transportador::create($dados);
                }
                if (in_array(2, $dados['padrao_id'])) {
                    Funcionario::create($dados);
                }
                DB::commit();
                return response()->json($pessoa);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 409);
            }
        } else {
            $request->validate([
                'nome' => 'required|max:100',
                'tipo_pessoa_id' => 'required',
                'padrao_id' => 'required',
                'status' => 'required',
                'cnpj' => 'min:14|unique:juridica,cnpj',
            ]);
            DB::beginTransaction();
            try {
                $pessoa = Pessoa::create($dados);
                $pessoa->padrao()->sync($dados['padrao_id']);
                $dados['pessoa_id'] = $pessoa->id;
                Juridica::create($dados);
                if (in_array(4, $dados['padrao_id'])) {
                    Transportador::create($dados);
                }
                DB::commit();
                return response()->json($pessoa);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 409);
            }
        }
    }

    /** @permissionName('Atualizar Pessoa') */
    public function update(Request $request, $tipoPessoaId, $pessoaId)
    {
        $dados = $request->all();
        if ($tipoPessoaId == 1) {
            $validator = Validator::make($dados, [
                'nome' => 'required|max:100',
                'tipo_pessoa_id' => 'required',
                'padrao_id' => 'required',
                'status' => 'required',
                'cpf' => ['min:11', Rule::unique('fisica', 'cpf')->ignore($pessoaId, 'pessoa_id')],
            ]);
            /*$validator->after(function ($validator) use ($dados) {
                if ($dados['cpf'] && !(validaCpf($dados['cpf']))) {
                    $validator->errors()->add('cpf', 'Número de CPF inválido');
                }
            });*/
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->toArray()], 422);
            }
            DB::beginTransaction();
            try {
                $pessoa = Pessoa::find($pessoaId);
                $pessoa->update($dados);
                $pessoa->padrao()->sync($dados['padrao_id']);
                $pessoa->fisica->update($dados);
                $dados['pessoa_id'] = $pessoaId;
                if (in_array(4, $dados['padrao_id'])) {
                    $transportador = Transportador::find($pessoaId);
                    $transportador ? $transportador->update($dados) : Transportador::create($dados);
                }
                $funcionario = Funcionario::find($pessoaId);
                if (in_array(2, $dados['padrao_id'])) {
                    $funcionario ? $funcionario->update($dados) : Funcionario::create($dados);
                } elseif ($funcionario) {
                    $funcionario->update(['status' => false]);
                }
                DB::commit();
                return response()->json($pessoa);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 409);
            }
        } else {
            $request->validate([
                'nome' => 'required|max:100',
                'padrao_id' => 'required',
                'tipo_pessoa_id' => 'required',
                'status' => 'required',
                'cnpj' => ['min:14', Rule::unique('juridica', 'cnpj')->ignore($pessoaId, 'pessoa_id')],
            ]);
            DB::beginTransaction();
            try {
                $dados = $request->all();
                $pessoa = Pessoa::find($pessoaId);
                $dados['pessoa_id'] = $pessoaId;
                $pessoa->update($dados);
                $pessoa->padrao()->sync($dados['padrao_id']);
                $pessoa->juridica->update($dados);
                if (in_array(4, $dados['padrao_id'])) {
                    $transportador = Transportador::find($pessoaId);
                    isset($transportador->pessoa_id) ? $transportador->update($dados) : Transportador::create($dados);
                }
                DB::commit();
                return response()->json($pessoa);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 409);
            }
        }
    }

    public function padroes()
    {
        return response()->json(Padrao::all());
    }

    public function tipoIdentidades()
    {
        return response()->json(TipoIdentidade::all());
    }
}
