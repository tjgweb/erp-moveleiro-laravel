<?php

namespace App\Http\Controllers\Gestao;

use App\Models\Contato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Pessoas') */
class ContatoController extends Controller
{
    /** @permissionName('Listar contatos') */
    public function lista($pessoaId)
    {
        $data = Contato::where('pessoa_id', $pessoaId)->get();
        return response()->json($data);
    }

    /** @permissionName('Criar novo contato') */
    public function create(Request $request)
    {
        $request->validate([
            'pessoa_id' => 'required',
            'descricao' => 'required',
            'responsavel' => 'required|max:60',
            'numero' => 'min:10|max:20',
            'principal' => 'required'
        ]);

        $data = Contato::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar contato') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pessoa_id' => 'required',
            'descricao' => 'required',
            'responsavel' => 'required|max:60',
            'numero' => 'min:10|max:20',
            'principal' => 'required'
        ]);

        $data = Contato::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover contato') */
    public function destroy($id)
    {
        return response()->json(Contato::find($id)->delete());
    }
}
