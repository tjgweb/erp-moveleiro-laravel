<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/** @permissionGroup('Usuários do sistema') */
class ProfileController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /** @permissionName('Altualizar perfil') */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validator = Validator::make($request->all(), $this->service->updateProfileRules($request, $id));
        $validator->after(function ($validator) use ($request, $user) {
            if (!Hash::check($request->get('senha_atual'), $user->password)) {
                $validator->errors()->add('senha_atual', 'Senha atual não confere.');
            }
        });
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user->update($this->service->dataToProfileUpdate($request));
        toastr()->success('Seus dados foram salvos!');

        return response()->json($user);
    }

}
