<?php

namespace App\Http\Controllers;

use App\Models\AliquotaIcms;
use App\Models\Bandeira;
use App\Models\Financeira;
use App\Models\Loja;
use App\Models\MovimentoProduto;
use App\Models\NaturezaOperacao;
use App\Models\Pessoa;
use App\Models\Produto;
use App\Models\TipoPagamento;
use App\Models\UnidadeMedida;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SharedController extends Controller
{
    public function naturezaOperacao()
    {
        return response()->json(NaturezaOperacao::all());
    }

    public function emissores()
    {
        $dados = Pessoa::with(['fisica', 'juridica', 'padrao', 'enderecos.estado', 'enderecos.cidade'])
            ->whereHas('padrao', function ($query) {
                $query->where('id', 3);
            })
            ->get();
        return response()->json($dados);
    }

    public function transportadores()
    {
        $dados = Pessoa::with(['fisica', 'juridica', 'padrao'])
            ->whereHas('padrao', function ($query) {
                $query->where('id', 4);
            })
            ->get();
        return response()->json($dados);
    }

    public function vendedores()
    {
        $data = Pessoa::with(['fisica', 'juridica'])
            ->whereHas('padrao', function ($query) {
                $query->where('padrao_id', 2);
            })
            ->whereHas('funcionario', function ($query) {
                $query->where('status', true);
            })
            ->whereHas('user', function ($query) {
                $query->where('grupo_id', 2);
            })->get();

        return response()->json($data);
    }

    public function clientes(Request $request)
    {
        $search = str_replace(['.', '-', '/'], ['', '', ''], $request->get('search'));
        $data = Pessoa::with(['fisica', 'juridica', 'enderecos', 'padrao'])
            ->where('nome', 'like', "%{$search}%")
            ->orWhereHas('fisica', function ($query) use ($search) {
                $query->where('cpf', 'like', "%{$search}%")
                    ->orWhere('identidade', 'like', "%{$search}%");
            })
            ->orWhereHas('juridica', function ($query) use ($search) {
                $query->where('cnpj', 'like', "%{$search}%")
                    ->orWhere('razao_social', 'like', "%{$search}%");
            })
            ->limit(20)->get();

        return response()->json($data);
    }

    public function aliquotaIcms($estadoOrigemId, $estadoDestinoId)
    {
        $aliquota = AliquotaIcms::where('origem_id', $estadoOrigemId)->where('destino_id', $estadoDestinoId)->first();
        $aliquota = $aliquota ?? [];
        return response()->json($aliquota);
    }

    public function unidadesMedida()
    {
        return response()->json(UnidadeMedida::all());
    }

    public function lojas()
    {
        return response()->json(Loja::all());
    }

    public function bandeiras()
    {
        $data = Bandeira::all();
        return response()->json($data);
    }

    public function financeiras()
    {
        $data = Financeira::all();
        return response()->json($data);
    }

    public function tipoPagamentos()
    {
        $data = TipoPagamento::where('ativo', true)->get();
        return response()->json($data);
    }

    public function autorizarDesconto(Request $request)
    {
        if (!$user = User::where('email', $request->get('email'))->first()) {
            return response()->json([], 401);
        } elseif (!Hash::check($request->get('password'), $user->password)) {
            return response()->json([], 401);
        } elseif (!$user->hasPermission('App\Http\Controllers\Gestao\DashboardController@index')) {
            return response()->json([], 403);
        }

        return response()->json(['autorizar' => true]);
    }

    public function buscaProdutos(Request $request)
    {
        $search = $request->get('search');

        $data = Produto::with([
            'mercadoria.categoria',
            'marca',
            'imagens',
            'produtoVendas.venda.estatusVenda',
            'movimentoProdutos.notaFiscal',
            'movimentoProdutos.unidadeMedida'
        ])
            ->whereHas('mercadoria', function ($query) use ($search) {
                $query->where('descricao', 'like', "%{$search}%");
            })
            ->orWhereHas('mercadoria.categoria', function ($query) use ($search) {
                $query->where('descricao', 'like', "%{$search}%");
            })
            ->orWhereHas('marca', function ($query) use ($search) {
                $query->where('nome', 'like', "%{$search}%");
            })
            ->orWhere('descricao', 'like', "%{$search}%")
            ->orWhere('codigo_fabrica', 'like', "%{$search}%")
            ->orWhere('id', 'like', "%{$search}%")
            ->limit(30)
            ->get()
            ->sortBy('mercadoria.descricao');

        return response()->json($data);
    }

    public function estoqueProdutoPorLoja($produtoId)
    {
        $entradas = MovimentoProduto::where('produto_id', $produtoId)
            ->whereHas('notaFiscal', function ($q) {
                $q->where('movimento_tipo_id', 2);
            })
            ->get()
            ->groupBy('loja_entrada_id');
        $loja_entrada = [];
        foreach ($entradas as $loja_id => $mProdutos) {
            $loja_entrada[$loja_id] = [
                'loja_id' => $loja_id,
                'descricao' => $mProdutos[0]->lojaEntrada->descricao,
                'quantidade' => 0
            ];
            foreach ($mProdutos as $mp) {
                if ($mp->unidadeMedida->agrupado == 1) {
                    $loja_entrada[$loja_id]['quantidade'] += $mp['quantidade'] * $mp['quantidade_por_caixa'];
                } else {
                    $loja_entrada[$loja_id]['quantidade'] += $mp['quantidade'];
                }
            }
        }

        $saidas = MovimentoProduto::where('produto_id', $produtoId)
            ->whereHas('notaFiscal', function ($q) {
                $q->where('movimento_tipo_id', 1);
            })
            ->get()
            ->groupBy('loja_saida_id');
        $loja_saida = [];
        foreach ($saidas as $loja_id => $mProdutos) {
            $loja_saida[$loja_id] = [
                'loja_id' => $loja_id,
                'descricao' => $mProdutos[0]->lojaEntrada->descricao,
                'quantidade' => 0
            ];
            foreach ($mProdutos as $mp) {
                if ($mp->unidadeMedida->agrupado == 1) {
                    $loja_saida[$loja_id]['quantidade'] += $mp['quantidade'] * $mp['quantidade_por_caixa'];
                } else {
                    $loja_saida[$loja_id]['quantidade'] += $mp['quantidade'];
                }
            }
        }

        $data = [];
        foreach ($loja_entrada as $keyEntrada => $valueEntrada) {
            $data[$keyEntrada] = $valueEntrada;
            if (isset($loja_saida[$keyEntrada])) {
                $data[$keyEntrada] -= $loja_saida[$keyEntrada];
            }
        }

        return response()->json($data);
    }
}
