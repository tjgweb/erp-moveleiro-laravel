<?php

namespace App\Http\Controllers\Vendas;

use App\Http\Controllers\Controller;
use App\Models\Pessoa;
use App\Models\User;
use App\Models\Venda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/** @permissionGroup('Vendas') */
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionDashboard('Vendas')
     * @permissionName('Painel Administrativo')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendedores = Pessoa::with(['fisica', 'juridica'])
            ->whereHas('padrao', function ($query) {
                $query->where('padrao_id', 2);
            })
            ->whereHas('funcionario', function ($query) {
                $query->where('status', true);
            })
            ->whereHas('user', function ($query) {
                $query->where('grupo_id', 2);
            })->get();

        $mesAtual = Carbon::now()->month;

        return view('vendas.dashboard.index', compact(['vendedores', 'mesAtual']));
    }

    /**
     * @param Request $request
     * @permissionName('Busca vendas por vendedor')
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $dados = $request->all();

        $validator = Validator::make($dados, [
            'mes' => 'required',
            'ano' => 'required',
            'vendedor' => 'required',
            'senha' => 'required'
        ]);
        $validator->after(function ($validator) use ($dados) {
            if (!$user = User::where('pessoa_id', $dados['vendedor'])->first()) {
                $validator->errors()->add('vendedor', 'Vendedor não associado a um usuário do sistema.');
            } elseif (!Hash::check($dados['senha'], $user->password)) {
                $validator->errors()->add('senha', 'Senha incorreta');
            }
        });
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $data = Carbon::createFromDate($dados['ano'], $dados['mes'], '01');

        $vendas = Venda::where('vendedor_id', $dados['vendedor'])
            ->whereBetween('data_venda', [$data, $data->copy()->lastOfMonth()])
            ->orderBy('data_venda')
            ->orderBy('updated_at')
            ->get();

        $vendedores = Pessoa::with(['fisica', 'juridica'])
            ->whereHas('padrao', function ($query) {
                $query->where('padrao_id', 2);
            })
            ->whereHas('funcionario', function ($query) {
                $query->where('status', true);
            })
            ->whereHas('user', function ($query) {
                $query->where('grupo_id', 2);
            })->get();

        $mesAtual = Carbon::now()->month;

        return view('vendas.dashboard.index', compact(['vendas', 'vendedores', 'mesAtual', 'dados']));
    }

}
