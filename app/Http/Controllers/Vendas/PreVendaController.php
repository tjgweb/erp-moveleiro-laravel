<?php

namespace App\Http\Controllers\Vendas;

use App\Http\Controllers\Controller;
use App\Models\Combinado;
use App\Models\Produto;
use App\Models\ProdutoVenda;
use App\Models\Venda;
use App\Models\VendaPagamento;
use App\Services\PreVendaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Vendas') */
class PreVendaController extends Controller
{
    /**
     * @var PreVendaService
     */
    private $preVendaService;

    /**
     * PreVendaController constructor.
     * @param PreVendaService $preVendaService
     */
    public function __construct(PreVendaService $preVendaService)
    {
        $this->preVendaService = $preVendaService;
    }

    /** @permissionName('Acesso Menu Pré-vendas registradas') */
    public function index()
    {
        return view('vendas.pre-venda-lista');
    }

    /** @permissionName('Listar pré-vendas') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Venda::with([
                'produtoVendas.produto',
                'pessoaVendedor',
                'pessoaCliente',
                'estatusVenda',
                'tipoVenda'
            ])
                ->where(function ($query) use ($search) {
                    $query->whereIn('estatus_venda_id', [1, 3, 4, 5])
                        ->whereHas('pessoaVendedor', function ($query) use ($search) {
                            $query->where('nome', 'like', "%{$search}%");
                        });
                })
                ->orWhere(function ($query) use ($search) {
                    $query->whereIn('estatus_venda_id', [1, 3, 4, 5])
                        ->whereHas('pessoaCliente', function ($query) use ($search) {
                            $query->where('nome', 'like', "%{$search}%");
                        });
                })
                ->orderBy('estatus_venda_id')
                ->orderByDesc('updated_at')
                ->paginate($pages);
        } else {
            $data = Venda::with([
                'produtoVendas.produto',
                'pessoaVendedor',
                'pessoaCliente',
                'estatusVenda',
                'tipoVenda'
            ])
                ->whereIn('estatus_venda_id', [1, 3, 4, 5])
                ->orderBy('estatus_venda_id')
                ->orderByDesc('updated_at')
                ->paginate($pages);
        }

        return response()->json($data);
    }

    /** @permissionName('Acesso Menu Registrar pré-venda') */
    public function form($action, $vendaId = null)
    {
        if ($vendaId) {
            $venda = Venda::findOrFail($vendaId);

            if (in_array($venda->estatus_venda_id, [2, 6, 7, 8])) {

                toastr()->error('Esta pré-venda já foi processada no caixa', 'Não pode editar');

                return redirect()->route('vendas.pre-venda.index');

            }
        }

        return view('vendas.pre-venda-form', compact(['action', 'vendaId']));
    }

    /** @permissionName('Salvar pré-venda') */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->preVendaService->validaDados($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->toArray()], 422);
        }

        DB::beginTransaction();
        try {
            $venda = Venda::create($data);
            foreach ($data['produto_vendas'] as $pv) {
                $pv['venda_id'] = $venda->id;
                ProdutoVenda::create($pv);
            }
            foreach ($data['combinado_vendas'] as $cv) {
                foreach ($cv['combinado']['produtos'] as $produto) {
                    $pv = [];
                    $pv['venda_id'] = $venda->id;
                    $pv['produto_id'] = $produto['id'];
                    $pv['quantidade'] = $produto['pivot']['quantidade'] * $cv['quantidade'];
                    $pv['valor_unitario'] = $produto['pivot']['valor'];
                    $pv['desconto'] = $cv['desconto'];
                    $pv['produto_combinado'] = true;
                    $pv['combinado_id'] = $cv['combinado_id'];
                    ProdutoVenda::create($pv);
                }
            }
            foreach ($data['venda_pagamentos'] as $item) {
                $item['venda_id'] = $venda->id;
                VendaPagamento::create($item);
            }
            DB::commit();
            toastr()->success('Salvo com sucesso');
            return response()->json([]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    public function edit($id)
    {
        $data = Venda::with([
            'vendaPagamentos',
            'pessoaCliente.padrao',
            'pessoaCliente.enderecos',
            'pessoaCliente.fisica',
            'pessoaCliente.juridica',
            'estatusVenda',

            'produtoVendas.produto.imagens',
            'produtoVendas.produto.mercadoria.categoria',
            'produtoVendas.produto.marca',
            'produtoVendas.produto.movimentoProdutos.notaFiscal',
            'produtoVendas.produto.movimentoProdutos.unidadeMedida',
            'produtoVendas.produto.produtoVendas.venda.estatusVenda',

            'produtoVendas.combinado.produtos.movimentoProdutos.notaFiscal',
            'produtoVendas.combinado.produtos.movimentoProdutos.unidadeMedida',
            'produtoVendas.combinado.produtos.imagens',
            'produtoVendas.combinado.produtos.mercadoria.categoria',
            'produtoVendas.combinado.produtos.marca',
            'produtoVendas.combinado.produtos.produtoVendas.venda.estatusVenda',
        ])
            ->where('id', $id)
            ->first();

        return response()->json($data);
    }

    /** @permissionName('Editar pré-venda') */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = $this->preVendaService->validaDados($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->toArray()], 422);
        }

        DB::beginTransaction();
        try {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $venda = Venda::find($id);
            $venda->update($data);
            $this->preVendaService->updateProdutoVenda($venda, $data);
            $this->preVendaService->updateCombinadoVenda($venda, $data);
            $this->preVendaService->updatePagamento($venda, $data);
            DB::commit();
            toastr()->success('Salvo com sucesso');
            return response()->json([]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    public function cancelar(Request $request, $id)
    {
        $venda = Venda::findOrFail($id);

        if (in_array($venda->estatus_venda_id, [2, 6, 7, 8])) {

            toastr()->error('Esta pré-venda já foi processada no caixa', 'Não pode cancelar');

        } else {

            $venda->update($request->all());

            toastr()->success('Cancelado com sucesso');
        }

        return response()->json([], 204);
    }

    public function produtos(Request $request)
    {
        $search = $request->get('search');
        $produtos = Produto::with([
            'mercadoria.categoria',
            'marca',
            'imagens',
            'produtoVendas.venda.estatusVenda',
            'movimentoProdutos.notaFiscal',
            'movimentoProdutos.unidadeMedida',
        ])
            ->whereHas('mercadoria', function ($query) use ($search) {
                $query->where('descricao', 'like', "%{$search}%");
            })
            ->orWhereHas('mercadoria.categoria', function ($query) use ($search) {
                $query->where('descricao', 'like', "%{$search}%");
            })
            ->orWhereHas('marca', function ($query) use ($search) {
                $query->where('nome', 'like', "%{$search}%");
            })
            ->orWhere('descricao', 'like', "%{$search}%")
            ->orWhere('codigo_fabrica', 'like', "%{$search}%")
            ->orWhere('valor_atual', 'like', "%{$search}%")
            ->orWhere('id', 'like', "%{$search}%")
            ->limit(30)->get(); //->sortBy('mercadoria.descricao');

        $combinados = Combinado::with([
            'produtos.mercadoria.categoria',
            'produtos.marca',
            'produtos.imagens',
            'produtos.produtoVendas.venda.estatusVenda',
            'produtos.movimentoProdutos.notaFiscal',
            'produtos.movimentoProdutos.unidadeMedida',
        ])
            ->where('descricao', 'like', "%{$search}%")
            ->limit(20)->get();

        return response()->json(compact(['produtos', 'combinados']));
    }
}
