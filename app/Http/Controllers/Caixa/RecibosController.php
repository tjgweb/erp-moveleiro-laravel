<?php

namespace App\Http\Controllers\Caixa;

use App\Models\Transacao;
use App\Pdf\CaixaReciboCrediario;
use App\Pdf\CaixaReciboVenda;
use App\Http\Controllers\Controller;

/** @permissionGroup('Caixa') */
class RecibosController extends Controller
{

    /**
     * @permissionName('Recibo - Gerar/Imprimir')
     * @param $transacaoId
     */
    public function index($transacaoId)
    {
        $transacao = Transacao::find($transacaoId);

        if ($transacao->venda_id) {
            (new CaixaReciboVenda($transacao))
                ->Output('I', "venda-recibo-{$transacaoId}.pdf", true);
        } else {
            (new CaixaReciboCrediario($transacao))
                ->Output('I', "crediario-recibo-{$transacaoId}.pdf", true);
        }

        exit;
    }
}
