<?php

namespace App\Http\Controllers\Caixa;

use App\Pdf\CaixaCarne;
use App\Http\Controllers\Controller;
use App\Services\CaixaService;
use Illuminate\Http\Request;

/** @permissionGroup('Caixa') */
class CarneController extends Controller
{
    /**
     * @var CaixaService
     */
    private $caixaService;

    /**
     * VendasController constructor.
     * @param CaixaService $caixaService
     */
    public function __construct(CaixaService $caixaService)
    {
        $this->caixaService = $caixaService;
    }

    /**
     * @permissionName('Carnê - Gerar/imprimir')
     * @param Request $request
     * @param $vendaId
     */
    public function index(Request $request, $vendaId)
    {
        $dados = $this->caixaService->gerarCarne($request, $vendaId);

        (new CaixaCarne($dados))
            ->Output('I', "carne-{$vendaId}.pdf", true);

        exit;
    }
}
