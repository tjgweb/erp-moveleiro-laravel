<?php

namespace App\Http\Controllers\Caixa;

use App\Http\Controllers\Controller;
use App\Models\Venda;
use App\Services\CaixaService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/** @permissionGroup('Caixa') */
class VendasController extends Controller
{
    /**
     * @var CaixaService
     */
    private $caixaService;

    /**
     * VendasController constructor.
     * @param CaixaService $caixaService
     */
    public function __construct(CaixaService $caixaService)
    {
        $this->middleware('caixa-session')->except(['index']);
        $this->caixaService = $caixaService;
    }

    /**
     * Display a listing of the resource.
     * @permissionName('Vendas a receber - Listar')
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Venda::with(['produtoVendas.produto', 'pessoaVendedor', 'pessoaCliente', 'estatusVenda', 'tipoVenda'])
                ->where('estatus_venda_id', 1)
                ->whereBetween('updated_at', [Carbon::now()->subDays(1), Carbon::now()])
                ->get();

            return response()->json($data);
        }

        return view('caixa.vendas.index');
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Vendas a receber - Finalizar recebimento')
     * @param  \Illuminate\Http\Request $request
     * @return string
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $transacao = $this->caixaService->salvarPagamentoVendas($request->all());
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }

        DB::commit();
        return response()->json($transacao);
    }

    /**
     * Display the specified resource.
     * @permissionName('Vendas a receber - Iniciar recebimento')
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $caixa_id = Session::get('caixa_id');
        $data = Venda::with([
            'produtoVendas.produto.imagens', 'produtoVendas.produto.marca', 'produtoVendas.produto.mercadoria',
            'produtoVendas.produto.combinados',

            'pessoaVendedor', 'pessoaCliente.fisica', 'pessoaCliente.juridica', 'pessoaCliente.enderecos', 'estatusVenda',
            'tipoVenda', 'vendaPagamentos.tipoPagamento', 'vendaPagamentos.bandeira', 'vendaPagamentos.financeira'
        ])->find($id);

        return view('caixa.vendas.recebimento', compact(['data', 'caixa_id']));
    }
}
