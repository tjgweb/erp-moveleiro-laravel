<?php

namespace App\Http\Controllers\Caixa;

use App\Http\Controllers\Controller;
use App\Models\Bandeira;
use App\Models\Configuracao;
use App\Models\Crediario;
use App\Models\TipoPagamento;
use App\Services\CaixaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/** @permissionGroup('Caixa') */
class CrediarioController extends Controller
{
    /**
     * @var CaixaService
     */
    private $caixaService;

    /**
     * CrediarioController constructor.
     * @param CaixaService $caixaService
     */
    public function __construct(CaixaService $caixaService)
    {
        $this->middleware('caixa-session')->except(['index']);
        $this->caixaService = $caixaService;
    }

    /**
     * Display a listing of the resource.
     * @permissionName('Crediário - Listar')
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $form = [
            'search' => str_replace(['.', '-', '/'], ['', '', ''], $request->get('search')),
            'status' => $request->get('status') ?? 'A'
        ];

        if ($request->get('search')) {
            $crediarios = Crediario::with('transacao.venda.pessoaCliente')
                ->where('status', $form['status'])
                ->whereHas('transacao.venda.pessoaCliente', function ($query) use ($form) {
                    $query->where('nome', 'like', "%{$form['search']}%")->orWhere('id', $form['search']);
                })
                ->orWhereHas('transacao.venda.pessoaCliente.fisica', function ($query) use ($form) {
                    $query->where('cpf', $form['search']);
                })
                ->orWhereHas('transacao.venda.pessoaCliente.juridica', function ($query) use ($form) {
                    $query->where('cnpj', $form['search']);
                })
                ->paginate();
            $crediarios = $crediarios->setCollection($crediarios->getCollection()->groupBy('transacao.venda.pessoaCliente.id'));
        } else {
            $crediarios = Crediario::with('transacao.venda.pessoaCliente')
                ->where('status', $form['status'])
                ->paginate();
            $crediarios = $crediarios->setCollection($crediarios->getCollection()->groupBy('transacao.venda.pessoaCliente.id'));
        }

        $form['search'] = $request->get('search');

        return view('caixa.crediario.index', compact(['crediarios', 'form']));
    }

    /**
     * @permissionName('Crediário - Receber')
     * @param $pessoaId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($pessoaId)
    {
        $crediarios = Crediario::with([
            'parcelas.transacoes.caixa',
            'transacao.venda.produtoVendas.produto.imagens',
            'transacao.venda.produtoVendas.produto.mercadoria',
            'transacao.venda.produtoVendas.produto.marca',
            'transacao.venda.pessoaCliente.fisica',
            'transacao.venda.pessoaCliente.juridica',
            'transacao.venda.pessoaCliente.contatos',
            'transacao.venda.vendaPagamentos'
        ])
            ->where('status', 'A')
            ->whereHas('transacao.venda.pessoaCliente', function ($query) use ($pessoaId) {
                $query->where('id', $pessoaId);
            })->get();

        $tipoPagamentos = TipoPagamento::where('ativo', true)->get();

        $bandeiras = Bandeira::all();

        $configuracoes = Configuracao::firstOrFail();

        return view('caixa.crediario.show', compact(['crediarios', 'tipoPagamentos', 'bandeiras', 'configuracoes']));
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Crediário - Finalizar recebimento')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $data = $request->all();

        DB::beginTransaction();

        try {

            $transacao = $this->caixaService->salvarPagamentoParcela($data);

        } catch (\Exception $e) {

            DB::rollBack();

            return response()->json($e->getMessage(), 409);
        }

        DB::commit();

        return response()->json($transacao);
    }
}
