<?php

namespace App\Http\Controllers\Caixa;

use App\Models\Pessoa;
use App\Models\Vencimento;
use App\Services\CaixaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Caixa') */
class ValeController extends Controller
{
    /**
     * @var CaixaService
     */
    private $service;

    /**
     * ValeController constructor.
     * @param CaixaService $service
     */
    public function __construct(CaixaService $service)
    {
        $this->middleware('caixa-session')->except(['index']);
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     * @permissionName('Vales - listar')
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $form = [
            'start' => $request->get('start') ?
                Carbon::createFromTimestamp(strtotime($request->get('start')))->format('Y-m-d') :
                Carbon::now()->addMonth(-1)->format('Y-m-d'),
            'end' => $request->get('end') ?
                Carbon::createFromTimestamp(strtotime($request->get('end')))->format('Y-m-d') :
                Carbon::now()->format('Y-m-d')
        ];

        $vencimentos = Vencimento::where('vencimento_tipo_id', 3)
            ->whereHas('transacao', function ($query) use ($form) {
                $query->whereDate('created_at', '>=', $form['start'])
                    ->whereDate('created_at', '<=', $form['end']);
            })
            ->paginate();

        $vencimentos->appends(['start' => $form['start'], 'end' => $form['end']]);

        return view('caixa.vencimentos.vales.index', compact(['vencimentos', 'form']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            'type' => 'create',
            'action' => route('caixa.vencimentos.vales.store'),
            'method' => 'POST',
            'vencimentoTipoId' => 3
        ];

        $funcionarios = Pessoa::whereHas('padrao', function ($query) {
            $query->whereIn('id', [2, 5]);
        })->get();

        return view('caixa.vencimentos.form', compact(['form', 'funcionarios']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @permissionName('Vales - criar')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'vencimento_tipo_id' => 'required',
            'pessoa_id' => 'required',
            'mes' => 'required',
            'ano' => 'required',
            'valor' => 'required'
        ]);

        DB::beginTransaction();

        try {

            $this->service->salvarVencimento($request->all());

        } catch (\Exception $e) {

            DB::rollBack();

            dd($e->getMessage());

            toastr()->error($e->getMessage(), 'Não foi possível salvar.');

            return redirect()->route('caixa.vencimentos.vales.create')->withInput();
        }

        DB::commit();

        toastr()->success('Salvo com sucesso!');

        return redirect()->route('caixa.vencimentos.vales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = [
            'type' => 'remove',
            'action' => route('caixa.vencimentos.vales.destroy', ['id' => $id]),
            'method' => 'DELETE',
            'vencimentoTipoId' => 3
        ];

        $funcionarios = Pessoa::whereHas('padrao', function ($query) {
            $query->whereIn('id', [2, 5]);
        })->get();

        $vencimento = Vencimento::findOrFail($id);

        return view('caixa.vencimentos.form', compact(['form', 'funcionarios', 'vencimento']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = [
            'type' => 'edit',
            'action' => route('caixa.vencimentos.vales.update', ['id' => $id]),
            'method' => 'PUT',
            'vencimentoTipoId' => 3
        ];

        $funcionarios = Pessoa::whereHas('padrao', function ($query) {
            $query->whereIn('id', [2, 5]);
        })->get();

        $vencimento = Vencimento::findOrFail($id);

        return view('caixa.vencimentos.form', compact(['form', 'funcionarios', 'vencimento']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @permissionName('Vales - atualizar')
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'vencimento_tipo_id' => 'required',
            'mes' => 'required',
            'ano' => 'required',
            'valor' => 'required'
        ]);

        DB::beginTransaction();

        try {

            $this->service->atualizarVencimento($request->all(), $id);

        } catch (\Exception $e) {

            DB::rollBack();

            toastr()->error($e->getMessage(), 'Não foi possível salvar.');

            return back()->withInput();
        }

        DB::commit();

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('caixa.vencimentos.vales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @permissionName('Vales - remover')
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {

            $this->service->cancelarVencimento($id);

        } catch (\Exception $e) {

            DB::rollBack();

            toastr()->error($e->getMessage(), 'Não foi possível remover.');

            return back()->withInput();
        }

        DB::commit();

        toastr()->success('Removido com sucesso!');

        return redirect()->route('caixa.vencimentos.vales.index');
    }
}
