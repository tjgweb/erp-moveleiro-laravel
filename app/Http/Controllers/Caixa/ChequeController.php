<?php

namespace App\Http\Controllers\Caixa;

use App\Models\Cheque;
use App\Models\Predatado;
use App\Services\CaixaService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/** @permissionGroup('Caixa') */
class ChequeController extends Controller
{
    /**
     * @var CaixaService
     */
    private $service;

    /**
     * ChequeController constructor
     * @param CaixaService $service
     */
    public function __construct(CaixaService $service)
    {
        $this->middleware('caixa-session')->only(['update']);
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     * @permissionName('Cheques à processar - Listar')
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $form = [
            'start' => $request->get('start') ?
                Carbon::createFromTimestamp(strtotime($request->get('start')))->format('Y-m-d') :
                Carbon::now()->addDays(-15)->format('Y-m-d'),
            'end' => $request->get('end') ?
                Carbon::createFromTimestamp(strtotime($request->get('end')))->format('Y-m-d') :
                Carbon::now()->addMonth(3)->format('Y-m-d')
        ];

        if ($request->ajax()) {

            $predatados = Predatado::with([
                'cheques',
                'transacao.venda.pessoaVendedor',
                'transacao.venda.pessoaCliente.contatos'
            ])
                ->where('status', false)
                ->whereHas('cheques', function ($query) use ($form) {
                    $query->whereDate('vencimento', '>=', $form['start'])
                        ->whereDate('vencimento', '<=', $form['end'])
                        ->orderByDesc('vencimento');
                })
                ->get()
                ->sortByDesc('transacao.created_at');

            return response()->json(['predatados' => $predatados, 'form' => $form]);
        }

        $predatados = Predatado::where('status', false)
            ->whereHas('cheques', function ($query) use ($form) {
                $query->where('status', false)
                    ->whereDate('vencimento', '>=', $form['start'])
                    ->whereDate('vencimento', '<=', $form['end'])
                    ->orderByDesc('vencimento');
            })
            ->paginate();

        $predatados->appends(['start' => $form['start'], 'end' => $form['end']]);

        return view('caixa.cheques.index', compact(['predatados', 'form']));
    }

    /**
     * Update the specified resource in storage.
     * @permissionName('Cheques à processar - Dar baixa')
     * @param  \Illuminate\Http\Request $request
     * @param $chequeId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $chequeId)
    {
        $request->validate([
            'tipo_processamento' => 'required'
        ]);

        DB::beginTransaction();

        try {
            $this->service->salvarCompensacaoCheque($request->get('tipo_processamento'), $chequeId);
        } catch (\Exception $e) {
            DB::rollBack();

            if ($request->ajax()) {
                return response()->json($e->getMessage(), 409);
            }

            toastr()->error($e->getMessage(), 'Ocorreu um erro!');

            return redirect()->route('caixa.cheques.index');
        }

        DB::commit();

        if ($request->ajax()) {
            return response()->json([], 203);
        }

        toastr()->success('Salvo com sucesso!');

        return redirect()->route('caixa.cheques.index');
    }

    /**
     *  @permissionName('Cheques processados - Listar')
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function processados(Request $request)
    {
        $form = [
            'start' => $request->get('start') ?
                Carbon::createFromTimestamp(strtotime($request->get('start')))->format('Y-m-d') :
                Carbon::now()->addMonth(-2)->format('Y-m-d'),
            'end' => $request->get('end') ?
                Carbon::createFromTimestamp(strtotime($request->get('end')))->format('Y-m-d') :
                Carbon::now()->format('Y-m-d')
        ];

        $cheques = Cheque::with([
            'transacoes' => function ($query) use ($form) {
                $query->whereDate('created_at', '>=', $form['start'])
                    ->whereDate('created_at', '<=', $form['end'])
                    ->orderByDesc('created_at');
            }
        ])
            ->where('status', true)
            ->paginate();

        $cheques->appends(['start' => $form['start'], 'end' => $form['end']]);

        return view('caixa.cheques.processados', compact(['cheques', 'form']));
    }

}
