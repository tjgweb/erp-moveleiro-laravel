<?php

namespace App\Http\Controllers\Caixa;

use App\Models\Caixa;
use App\Models\Credito;
use App\Models\Retirada;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/** @permissionGroup('Caixa') */
class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     * @permissionDashboard('Caixa')
     * @permissionName('Caixa - Painel')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abertos = Caixa::where('aberto', true)->get();

        $caixas = $abertos->where('operador_id', '=', auth()->user()->id);//->first() ?? null;
        $outrosCaixas = $abertos->where('operador_id', '!=', auth()->user()->id);

        return view('caixa.painel.index', compact(['caixas', 'outrosCaixas']));
    }

    /** @permissionName('Caixa - Abertura') */
    public function abrirCaixa(Request $request)
    {
        $data = $request->all();
        $data['troco_inicial'] = str_replace(',', '.', $data['troco_inicial']);
        $data['troco_inicial'] = number_format($data['troco_inicial'], 2, '.', '');

        $caixa = Caixa::create($data);
        Session::put('caixa_id', $caixa->id);

        return redirect()->route('caixa.index');
    }

    public function selecionarCaixa(Request $request)
    {
        Session::put('caixa_id', $request->get('caixa_id'));
        toastr()->info('Você está usando este caixa no momento.', 'Caixa - ' . $request->get('caixa_apelido'));

        return redirect()->route('caixa.index');
    }

    /** @permissionName('Caixa - Fechamento') */
    public function fecharCaixa(Request $request)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();

            $caixa = Caixa::findOrFail($data['caixa_id']);
            $caixa->update(['aberto' => 0]);

            if ($data['em_caixa'] > 0) {
                Retirada::create([
                    'caixa_id' => $data['caixa_id'],
                    'valor' => $data['em_caixa'],
                    'descricao' => 'Fundo de caixa para o dia seguinte',
                    'hora' => now()->format('H:i:s'),
                    'delete_action' => 0
                ]);
            }

            Session::put('caixa_id');

            toastr()->success("Operador: {$caixa->operador->apelido}", 'Encerramento de Caixa');

            DB::commit();

            return redirect()->route('caixa.index');

        } catch (\Exception $e) {
            DB::rollBack();

            toastr()->error("Operador: {$caixa->operador->apelido}", 'Erro no encerramento');

            return redirect()->route('caixa.index');
        }
    }

    /** @permissionName('Crédito - Cadastrar') */
    public function postCredito(Request $request)
    {
        $dados = $request->all();
        $validator = Validator::make($dados, [
            'caixa_id' => 'required',
            'valor' => 'required',
            'descricao' => 'required|min:5|max:255',
            'hora' => 'required'
        ]);

        if ($validator->fails()) {
            toastr()->error('Corrija os campos do formulário', 'Não inserido!');
            return redirect(route('caixa.index'))->withErrors($validator)->withInput();
        }

        Credito::create($dados);
        toastr()->success('Registrado com sucesso');

        return redirect()->route('caixa.index');
    }

    /** @permissionName('Crédito - Cancelar') */
    public function destroyCredito($id)
    {
        Credito::find($id)->delete();
        toastr()->success('Excluído com sucesso');

        return redirect()->route('caixa.index');
    }

    /** @permissionName('Retirada - Cadastrar') */
    public function postRetirada(Request $request)
    {
        $dados = $request->all();
        $validator = Validator::make($dados, [
            'caixa_id' => 'required',
            'valor' => 'required',
            'descricao' => 'required|min:5|max:255',
            'hora' => 'required'
        ]);
        $validator->after(function ($validator) use ($dados) {
            $caixa = Caixa::findOrFail($dados['caixa_id']);
            $saldo = $caixa->troco_inicial + $caixa->total_dinheiro + $caixa->total_credito;
            if ($caixa->em_caixa - $dados['valor'] < 0) {
                $validator->errors()->add('valor', 'Saldo insuficiente.');
            }
        });
        if ($validator->fails()) {
            toastr()->error('Corrija os campos do formulário', 'Não inserido!');
            return redirect(route('caixa.index'))->withErrors($validator)->withInput();
        }

        Retirada::create($dados);
        toastr()->success('Registrado com sucesso');

        return redirect()->route('caixa.index');
    }

    /** @permissionName('Retirada - Cancelar') */
    public function destroyRetirada($id)
    {
        Retirada::find($id)->delete();
        toastr()->success('Excluído com sucesso');

        return redirect()->route('caixa.index');
    }

}
