<?php

namespace App\Http\Controllers\Caixa;

use App\Models\Caixa;
use App\Models\Cartao;
use App\Models\Financiamento;
use App\Pdf\CaixaRelatorio;
use App\Pdf\CaixaRelatorioMensal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Caixa') */
class RelatoriosController extends Controller
{
    /** @permissionName('Relatório diário - Listar') */
    public function diarioIndex(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $form = [
            'start' => $start ?? Carbon::create()->subDays(7)->format('Y-m-d'),
            'end' => $end ?? Carbon::create()->format('Y-m-d')
        ];

        $caixas = Caixa::where('operador_id', auth()->user()->id)
            ->whereDate('created_at', '>=', $form['start'])
            ->whereDate('created_at', '<=', $form['end'])
            ->orderByDesc('created_at')
            ->paginate();

        return view('caixa.relatorio-caixas.diario', compact(['form', 'caixas']));
    }

    /** @permissionName('Relatório diário - Ver/Imprimir') */
    public function diarioShow($caixaId)
    {
        $caixa = Caixa::find($caixaId);

        if (!$caixa) {
            toastr()->info('Código do caixa informado não encontrado');
            return redirect()->back();
        }

        $cartoes = Cartao::whereHas('transacao', function ($query) use ($caixa) {
            $query->where('caixa_id', $caixa->id);
        })
            ->get();//->where('tipo', 'C')->groupBy('bandeira_id');

        $financiamentos = Financiamento::whereHas('transacao', function ($query) use ($caixa) {
            $query->where('caixa_id', $caixa->id);
        })
            ->get();//->where('tipo', 'C')->groupBy('bandeira_id');

        (new CaixaRelatorio($caixa, $cartoes, $financiamentos))->Output('I', "relatorio-caixa-{$caixaId}.pdf", true);
        exit;
    }

    /** @permissionName('Relatório mensal - Listar')
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mensalIndex(Request $request)
    {
        $ano = $request->get('ano') ?? Carbon::create()->format('Y');

        $meses = Caixa::where('operador_id', auth()->user()->id)
            ->whereYear('created_at', $ano)
            ->orderBy('created_at')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('m');
            });

        return view('caixa.relatorio-caixas.mensal', compact(['ano', 'meses']));
    }

    /** @permissionName('Relatório mensal - Ver/Imprimir')
     * @param $mes
     * @param $ano
     */
    public function mensalShow($mes, $ano)
    {
        // verificar user->hasPermission
        $caixas = Caixa::whereMonth('created_at', $mes)
            ->whereYear('created_at', $ano)
            ->get();

        (new CaixaRelatorioMensal($caixas))->Output('I', "relatorio-mensal-caixa-{$mes}-{$ano}.pdf", true);
        exit;
    }
}
