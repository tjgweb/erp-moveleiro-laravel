<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Marca;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Marca') */
class MarcaController extends Controller
{

    /** @permissionName('Acesso Menu Marcas') */
    public function index(Request $request)
    {
        return view('estoque.marcas');
    }

    /** @permissionName('Listar Marcas') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Marca::with('produtos')
                ->where('nome', 'like', "%{$search}%")
                ->paginate($pages);
        } else {
            $data = Marca::with('produtos')->paginate($pages);
        }
        return response()->json($data);
    }

    /** @permissionName('Criar Marcas') */
    public function store(Request $request)
    {
        $request->validate(['nome' => 'required|max:50']);

        $data = Marca::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar Marcas') */
    public function update(Request $request, $id)
    {
        $request->validate(['nome' => 'required|max:50']);

        $data = Marca::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover Marcas') */
    public function destroy($id)
    {
        $data = Marca::find($id);
        if ($data->produtos()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir marcas associadas a um produto.'
            ], 409);
        }

        return response()->json($data->delete());
    }
}
