<?php

namespace App\Http\Controllers\Estoque;

use App\Models\MovimentoProduto;
use App\Models\Pessoa;
use App\Models\PosVenda;
use App\Models\Venda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RetiradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        if ($search) {
            $vendas = Venda::where('estatus_venda_id', 7)
                ->whereHas('pessoaCliente', function ($query) use ($search) {
                    $query->where('nome', 'like', "{$search}%")
                        ->orWhereHas('fisica', function ($query) use ($search) {
                            $query->where('cpf', $search);
                        })
                        ->orWhereHas('juridica', function ($query) use ($search) {
                            $query->where('cnpj', $search);
                        });
                })
                ->orderByDesc('previsao_entrega')
                ->paginate();

            $posVendas = PosVenda::where('tipo_pos_venda_id', 2)
                ->whereHas('equipe', function ($query) use ($search) {
                    $query->where('nome', 'like', "{$search}%")
                        ->orWhereHas('fisica', function ($query) use ($search) {
                            $query->where('cpf', $search);
                        })
                        ->orWhereHas('juridica', function ($query) use ($search) {
                            $query->where('cnpj', $search);
                        });
                })
                ->orderByDesc('data')
                ->paginate();
        } else {
            $vendas = Venda::where('estatus_venda_id', 7)
                ->orderByDesc('previsao_entrega')
                ->paginate();

            $posVendas = PosVenda::where('tipo_pos_venda_id', 2)
                ->orderByDesc('data')
                ->paginate();
        }

        return view('estoque.pos-venda.retiradas.index', compact(['vendas', 'posVendas', 'search']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'tipo_pos_venda_id' => 'required',
            'data' => 'required',
            'hora' => 'required',
            'equipe' => 'required',
            'produtos' => 'required',
            'qtd' => 'required'
        ]);
        if ($validator->fails()) {
            toastr()->error('Corrija os erros no formulário.');

            return redirect(route('estoque.pos-venda.retiradas.show', ['id' => $data['venda_id']]))
                ->withErrors($validator)
                ->withInput();
        }

        DB::beginTransaction();
        try {
            $venda = Venda::find($data['venda_id']);


            //$posVenda = PosVenda::create($data);
            //$posVenda->equipe()->attach($data['equipe']);
        } catch (\Exception $e) {
            DB::rollBack();
        }

        DB::rollBack();
        toastr()->success('Salvo com sucesso');

        return redirect()->route('estoque.pos-venda.retiradas.show', ['id' => $data['venda_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movimentoProdutos = MovimentoProduto::where('venda_id', $id)->get();

        $equipe = Pessoa::whereHas('padrao', function ($query) {
            $query->where('id', 2)->orWhere('id', 5);
        })
            ->get();

        return view('estoque.pos-venda.retiradas.show', compact(['movimentoProdutos', 'equipe']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
