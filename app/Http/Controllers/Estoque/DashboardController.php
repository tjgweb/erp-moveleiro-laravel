<?php

namespace App\Http\Controllers\Estoque;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Estoque') */
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionDashboard('Estoque')
     * @permissionName('Painel Administrativo')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estoque.dashboard');
    }

}
