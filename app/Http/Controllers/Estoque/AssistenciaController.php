<?php

namespace App\Http\Controllers\Estoque;

use App\Models\MovimentoProduto;
use App\Models\Pessoa;
use App\Models\PosVenda;
use App\Models\Venda;
use App\Pdf\EstoqueAssistencias;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AssistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $vendaId = $request->get('venda');

            $posVendas = PosVenda::with(['movimentoProdutos.venda.pessoaCliente', 'equipe'])
                ->whereHas('movimentoProdutos', function ($query) use ($vendaId) {
                    $query->where('venda_id', $vendaId);
                })
                ->get();

            return response()->json($posVendas);
        }

        return view('estoque.pos-venda.assistencias.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request, $pages = 15)
    {
        $search = str_replace(['.', '-', '/'], ['', '', ''], $request->get('search'));

        $vendas = Venda::with(['movimentoProdutos','pessoaCliente.fisica', 'pessoaCliente.juridica', 'estatusVenda'])
            ->whereIn('estatus_venda_id', [2, 7, 8])
            ->whereHas('pessoaCliente', function ($query) use ($search) {
                $query->where('nome', 'like', "{$search}%")
                    ->orWhereHas('fisica', function ($query) use ($search) {
                        $query->where('cpf', $search);
                    })
                    ->orWhereHas('juridica', function ($query) use ($search) {
                        $query->where('cnpj', $search);
                    });
            })
            ->orderByDesc('data_venda')
            ->limit(100)
            ->paginate($pages);

        return response()->json($vendas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendaId = $request->get('venda');

        if (!$vendaId) {
            toastr()->error('Parâmetro de rota incorreto');
            return back();
        }

        $movimentoProdutos = MovimentoProduto::where('venda_id', '=', 2)->get();

        $equipe = Pessoa::whereHas('padrao', function ($query) {
            $query->where('id', 2)->orWhere('id', 5);
        })->get();

        return view('estoque.pos-venda.assistencias.create', compact(['movimentoProdutos', 'equipe']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'tipo_pos_venda_id' => 'not_in:0',
            'data' => 'required',
            'hora' => 'required',
            'equipe' => 'required',
            'm_produtos' => 'required',
            'qtd' => 'required'
        ], [
            'tipo_pos_venda_id.not_in' => 'Escolha o tipo de assistência.',
            'm_produtos.required' => 'Selecione os produtos para assistência.'
        ]);

        DB::beginTransaction();

        if ($validator->fails()) {
            toastr()->error('Corrija os erros no formulário.');

            return redirect(route('estoque.pos-venda.assistencias.create', ['venda' => $data['venda_id']]))
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $posVenda = PosVenda::create($data);
            $posVenda->equipe()->attach($data['equipe']);
            for ($i = 0; $i < count($data['m_produtos']); $i++) {
                $posVenda->movimentoProdutos()->attach($data['m_produtos'][$i], ['quantidade' => $data['qtd'][$i]]);
            }
        } catch (\Exception $e) {
            DB::rollBack();

            toastr()->error($e->getMessage());

            return redirect(route('estoque.pos-venda.assistencias.create', ['venda' => $data['venda_id']]))
                ->withInput();
        }

        DB::commit();
        toastr()->success('Assistência criada com sucesso.');

        return redirect()->route('estoque.pos-venda.assistencias.show', ['posVendaId' => $posVenda->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $posVendaId
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $posVendaId)
    {
        $posVenda = PosVenda::with([
            'movimentoProdutos.venda.pessoaCliente',
            'movimentoProdutos.produto'
        ])
            ->find($posVendaId);

        return view('estoque.pos-venda.assistencias.show', compact(['posVenda']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $posVendaId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($posVendaId)
    {
        $posVenda = PosVenda::find($posVendaId);

        $vendaId = $posVenda->movimentoProdutos->first()->venda_id;

        $movimentoProdutos = MovimentoProduto::where('venda_id', $vendaId)->get();

        $equipe = Pessoa::whereHas('padrao', function ($query) {
            $query->where('id', 2)->orWhere('id', 5);
        })->get();

        return view('estoque.pos-venda.assistencias.edit', compact(['posVenda', 'movimentoProdutos', 'equipe']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $posVendaId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $posVendaId)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'tipo_pos_venda_id' => 'not_in:0',
            'data' => 'required',
            'hora' => 'required',
            'equipe' => 'required',
            'm_produtos' => 'required',
            'qtd' => 'required',
            'finalizado_em' => 'required_if:status,1'
        ], [
            'tipo_pos_venda_id.not_in' => 'Escolha o tipo de assistência.',
            'm_produtos.required' => 'Selecione os produtos para assistência.',
            'finalizado_em.required_if' => 'Informe a data do encerramento da assistência.'
        ]);

        DB::beginTransaction();

        if ($validator->fails()) {
            toastr()->error('Corrija os erros no formulário.');

            return redirect(route('estoque.pos-venda.assistencias.edit', ['posVendaId' => $posVendaId]))
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $posVenda = PosVenda::find($posVendaId);
            if ($data['status'] == 0) {
                $data['finalizado_em'] = null;
            }
            $posVenda->update($data);
            $posVenda->equipe()->sync($data['equipe']);
            $posVenda->movimentoProdutos()->detach();
            for ($i = 0; $i < count($data['m_produtos']); $i++) {
                $posVenda->movimentoProdutos()->attach($data['m_produtos'][$i], ['quantidade' => $data['qtd'][$i]]);
            }
        } catch (\Exception $e) {
            DB::rollBack();

            toastr()->error($e->getMessage());

            return redirect(route('estoque.pos-venda.assistencias.edit', ['posVendaId' => $posVendaId]))
                ->withInput();
        }

        DB::commit();
        toastr()->success('Assistência atualizada com sucesso.');

        return redirect()->route('estoque.pos-venda.assistencias.show', ['posVendaId' => $posVendaId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print($posVendaId)
    {
        $posVenda = PosVenda::find($posVendaId);

        (new EstoqueAssistencias($posVenda))->Output('I', "assistencia-{$posVendaId}.pdf", true);
        exit;
    }
}
