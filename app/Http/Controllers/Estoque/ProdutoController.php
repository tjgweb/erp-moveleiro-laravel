<?php

namespace App\Http\Controllers\Estoque;

use App\Http\Controllers\Controller;
use App\Models\Marca;
use App\Models\Mercadoria;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Produto') */
class ProdutoController extends Controller
{

    /** @permissionName('Acesso Menu Produtos') */
    public function index()
    {
        return view('estoque.produtos');
    }

    /** @permissionName('Listar Produtos') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Produto::with([
                'mercadoria.categoria', 'marca', 'imagens', 'produtoVendas.venda.estatusVenda',
                'movimentoProdutos.notaFiscal', 'movimentoProdutos.unidadeMedida',
            ])
                ->whereHas('mercadoria', function ($query) use ($search) {
                    $query->where('descricao', 'like', "%{$search}%");
                })
                ->orWhereHas('mercadoria.categoria', function ($query) use ($search) {
                    $query->where('descricao', 'like', "%{$search}%");
                })
                ->orWhereHas('marca', function ($query) use ($search) {
                    $query->where('nome', 'like', "%{$search}%");
                })
                ->orWhere('descricao', 'like', "%{$search}%")
                ->orWhere('codigo_fabrica', 'like', "%{$search}%")
                ->orWhere('valor_atual', 'like', "%{$search}%")
                ->orWhere('id', 'like', "%{$search}%")
                ->paginate($pages);
        } else {
            $data = Produto::with([
                'mercadoria.categoria', 'marca', 'imagens', 'produtoVendas.venda.estatusVenda',
                'movimentoProdutos.notaFiscal', 'movimentoProdutos.unidadeMedida',
            ])
                ->orderBy('descricao')
                ->paginate($pages);
        }

        return response()->json($data);
    }

    /** @permissionName('Criar Produtos') */
    public function store(Request $request)
    {
        $request->validate([
            'mercadoria_id' => 'required',
            'marca_id' => 'required',
            'descricao' => 'required',
            'codigo_fabrica' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $produto = Produto::create($request->all());

            DB::commit();
            return response()->json($produto);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => 'Erro ao gravar os dados.',
            ], 409);
        }
    }

    /** @permissionName('Atualizar Produtos') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'mercadoria_id' => 'required',
            'marca_id' => 'required',
            'descricao' => 'required',
            'codigo_fabrica' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $produto = Produto::find($id);
            $produto->update($request->all());

            DB::commit();
            return response()->json($produto);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => 'Erro ao gravar os dados.',
            ], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @permissionName('Remover Produtos')
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        return response()->json(Produto::find($id)->delete());
    }

    public function listarMercadorias()
    {
        return response()->json(Mercadoria::with('categoria')->get());
    }

    public function listarMarcas()
    {
        return response()->json(Marca::all());
    }
}
