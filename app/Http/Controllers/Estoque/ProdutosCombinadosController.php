<?php

namespace App\Http\Controllers\Estoque;

use App\Http\Controllers\Controller;
use App\Models\Combinado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Runner\Exception;
use TJGazel\Toastr\Toastr;

/** @permissionGroup('Produtos Combinados') */
class ProdutosCombinadosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionName('Acesso Menu Produtos Combinados')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estoque.produtos-combinados');
    }

    /** @permissionName('Listar Produtos Combinados') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Combinado::with('produtos')
                ->where('descricao', 'like', "%{$search}%")
                ->orderBy('descricao')
                ->paginate($pages);
        } else {
            $data = Combinado::with('produtos')
                ->orderBy('descricao')
                ->paginate($pages);
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'add';
        return view('estoque.produtos-combinados-form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Criar Produtos Combinados')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'descricao' => 'required',
            'margem' => 'required',
            'valor_total' => 'required',
            'produtos' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 422);
        }

        DB::beginTransaction();
        try {
            $combinado = Combinado::create($data);
            foreach ($data['produtos'] as $produto) {
                $combinado->produtos()->attach($produto['id'], ['quantidade' => $produto['pivot']['quantidade'], 'valor' => $produto['pivot']['valor']]);
            }
            DB::commit();
            toastr()->success('Criado com sucesso.');
            return response()->json($combinado);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Erro ao gravar os dados.', $e->getMessage()], 409);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $combinado = Combinado::with(['produtos.mercadoria.categoria', 'produtos.marca', 'produtos.imagens'])->find($id);
        $action = 'edit';
        return view('estoque.produtos-combinados-form', compact(['combinado', 'action']));
    }

    /**
     * Update the specified resource in storage.
     * @permissionName('Atualizar Produtos Combinados')
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'descricao' => 'required',
            'margem' => 'required',
            'valor_total' => 'required',
            'produtos' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 422);
        }

        DB::beginTransaction();
        try {
            $combinado = Combinado::find($id);
            $combinado->update($data);
            $combinado->produtos()->detach();
            foreach ($data['produtos'] as $produto) {
                $combinado->produtos()->attach($produto['id'], ['quantidade' => $produto['pivot']['quantidade'], 'valor' => $produto['pivot']['valor']]);
            }
            DB::commit();
            toastr()->success('Atualizado com sucesso.');
            return response()->json($combinado);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Erro ao gravar os dados.', $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @permissionName('Remover Produtos Combinados')
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $combinado = Combinado::find($id)->delete();
            DB::commit();
            toastr()->success('Removido com sucesso.');
            return response()->json($combinado);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([$e->getMessage()], 409);
        }
    }
}