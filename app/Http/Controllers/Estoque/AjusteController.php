<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Ajuste;
use App\Models\NotaFiscal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Produto') */
class AjusteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @permissionName('Acesso Menu Ajuste')
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estoque.ajuste');
    }

    public function search(Request $request)
    {
        $data = NotaFiscal::with([
            'movimentoProdutos.produto.custo', 'movimentoProdutos.produto.ajuste', 'movimentoProdutos.produto.marca',
            'movimentoProdutos.produto.mercadoria'
        ])
            ->where('numero', $request->get('search'))
            ->first();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     * @permissionName('Ajuste de preços')
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $form = $request->all();

        try {
            DB::beginTransaction();
            for ($i = 0; $i < count($form); $i++) {
                $ajuste = Ajuste::updateOrCreate(
                    ['produto_id' => $form[$i]['movimento_produto_id'], 'nota_fiscal_id' => $form[$i]['nota_fiscal_id']],
                    ['frete' => $form[$i]['frete'], 'despesa_extra' => $form[$i]['despesa_extra']]
                );
                $ajuste->produto->valor_atual = $form[$i]['valor_atual'];
                $ajuste->produto->margem = $form[$i]['margem'];
                $ajuste->produto->save();
            }
            DB::commit();
            return response()->json([]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 409);
            DB::rollBack();
        }
    }

}
