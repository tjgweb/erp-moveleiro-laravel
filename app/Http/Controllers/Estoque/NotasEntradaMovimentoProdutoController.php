<?php

namespace App\Http\Controllers\Estoque;

use App\Http\Controllers\Controller;
use App\Models\Loja;
use App\Models\MovimentoProduto;
use App\Models\Produto;
use App\Models\UnidadeMedida;
use App\Services\EntradaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/** @permissionGroup('Nota Fiscal Entrada') */
class NotasEntradaMovimentoProdutoController extends Controller
{
    /**
     * @var EntradaService
     */
    private $entradaService;

    /**
     * NotasEntradaMovimentoProdutoController constructor.
     * @param EntradaService $entradaService
     */
    public function __construct(EntradaService $entradaService)
    {
        $this->entradaService = $entradaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $notaId
     * @return \Illuminate\Http\Response
     */
    public function lista($notaId)
    {
        $data = MovimentoProduto::with([
            'produto.mercadoria.categoria', 'produto.marca', 'naturezaOperacao:id,cfop',
            'unidadeMedida:id,unidade', 'notaFiscal:id,base_calculo_icms',
        ])
            ->where('nota_fiscal_id', $notaId)
            ->get();

        return response()->json($data);
    }

    /** @permissionName('Adicionar produtos à Nota')
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $mp = $request->all();
        $request->validate([
            'produto_id' => 'required',
            'nota_fiscal_id' => 'required',
            'unidade_medida_id' => 'required',
            'cfop_id' => 'required',
            'id_na_nota' => 'required',
            'quantidade' => 'required|numeric',
            'quantidade_por_caixa' => 'required_if:unidade_medida_id,5',
            'valor_unitario' => 'required|numeric',
            'desconto' => 'required|numeric',
            'aliquota_icms' => 'required|numeric',
            'aliquota_ipi' => 'required|numeric',
            'mva' => 'required',
            'loja_entrada_id' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $unidadeMedida = UnidadeMedida::find($mp['unidade_medida_id']);
            $qtdProdutosEstoque = $this->entradaService->countProdutosEstoque($mp['produto_id']);
            $data = MovimentoProduto::create($mp);
            $this->entradaService->atualizaCusto($qtdProdutosEstoque, $mp, $unidadeMedida);
            DB::commit();
            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    /** @permissionName('Atualizar produtos da Nota')
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $mp = $request->all();
        $request->validate([
            'produto_id' => 'required',
            'nota_fiscal_id' => 'required',
            'unidade_medida_id' => 'required',
            'cfop_id' => 'required',
            'id_na_nota' => 'required',
            'quantidade' => 'required|numeric',
            'quantidade_por_caixa' => 'required_if:unidade_medida_id,5',
            'valor_unitario' => 'required|numeric',
            'desconto' => 'required|numeric',
            'aliquota_icms' => 'required|numeric',
            'aliquota_ipi' => 'required|numeric',
            'mva' => 'required',
            'loja_entrada_id' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $unidadeMedida = UnidadeMedida::find($mp['unidade_medida_id']);
            $qtdProdutosEstoque = $this->entradaService->countProdutosEstoque($mp['produto_id']);
            $data = MovimentoProduto::find($id);
            $data->update($mp);
            $this->entradaService->atualizaCusto($qtdProdutosEstoque, $mp, $unidadeMedida);
            DB::commit();
            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    /** @permissionName('Remover produtos da Nota')
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        return response()->json(MovimentoProduto::find($id)->delete());
    }
}
