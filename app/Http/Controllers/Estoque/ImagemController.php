<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Imagem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/** @permissionGroup('Produto') */
class ImagemController extends Controller
{

    public function lista($produtoId)
    {
        $data = Imagem::whereHas('produtos', function ($query) use ($produtoId) {
            $query->where('id', $produtoId);
        })->orderBy('ordem')->get();

        return response()->json($data);
    }

    /** @permissionName('Adicionar Imagem') */
    public function store(Request $request)
    {
        $request->validate(['src' => 'required'], ['src.required' => 'Selecione uma imagem e recorte.']);
        $data = $request->all();
        $img = base64_decode(str_replace('data:image/png;base64,', '', $data['src']));
        $file = '/produtos/' . date('Y') . '/' . date('m') . '/' . time() . '.png';

        DB::beginTransaction();
        try {
            Storage::put($file, $img);
            $data['src'] = Storage::url($file);

            $imagem = Imagem::create($data);
            $imagem->produtos()->attach($data['produtoId']);

            DB::commit();
            return response()->json($imagem);
        } catch (\Exception $e) {
            DB::rollBack();
            if (Storage::exists($file)) {
                Storage::delete($file);
            }
            return response()->json(['error' => $e->getMessage()], 409);
        }

    }

    /** @permissionName('Atualizar imagem') */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        DB::beginTransaction();
        try {
            $imagem = Imagem::find($id);

            if (strstr($data['src'], 'data:image/png;base64,')) {
                $img = base64_decode(str_replace('data:image/png;base64,', '', $data['src']));
                $file = '/produtos/' . date('Y') . '/' . date('m') . '/' . time() . '.png';
                Storage::put($file, $img);
                $data['src'] = Storage::url($file);
                $oldFile = str_replace(config('app.url') . '/uploads/', '', $imagem->src);
                if (Storage::exists($oldFile)) {
                    Storage::delete($oldFile);
                }
            }
            $imagem->update($data);
            DB::commit();

            return response()->json($imagem);
        } catch (\Exception $e) {
            DB::rollBack();
            if (isset($file) && Storage::exists($file)) {
                Storage::delete($file);
            }
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    /** @permissionName('Remover imagem') */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $imagem = Imagem::find($id);
            $imagem->produtos()->detach();
            $oldFile = str_replace(config('app.url') . '/uploads/', '', $imagem->src);
            if (Storage::exists($oldFile)) {
                Storage::delete($oldFile);
            }
            $data = $imagem->delete();
            DB::commit();

            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 409);
        }

    }
}
