<?php

namespace App\Http\Controllers\Estoque;

use App\Models\NotaFiscal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotasTransferenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $search = $request->get('search');
            $pages = $request->get('per_page') ?? 15;
            if (isset($search)) {
                $data = NotaFiscal::with(['lojaEmissor', 'pessoaDestinatario', 'enderecoDestinatario'])
//                    ->where('movimento_tipo_id', 1)
                    ->where('status', '!=', null)
                    ->whereHas('movimentoProdutos', function ($query) {
                        $query->where('venda_id', null);
                    })
                    ->where('numero', 'like', "%{$search}%")
                    ->orWhereHas('lojaEmissor', function ($query) use ($search) {
                        $query->where('descricao', 'like', "%{$search}%")
                            ->orWhere('razao_social', 'like', "%{$search}%")
                            ->orWhere('cnpj', str_replace([' ', '.', '-', '/'], ['', '', '', ''], $search));
                    })
                    ->orWhereHas('pessoaDestinatario', function ($query) use ($search) {
                        $query->where('nome', 'like', "%{$search}%")
                            ->orWhereHas('juridica', function ($query) use ($search) {
                                $query->where('razao_social', 'like', "%{$search}%")
                                    ->orWhere('cnpj', str_replace([' ', '.', '-', '/'], ['', '', '', ''], $search));
                            });
                    })
                    ->orderByDesc('data_emissao', 'status')
                    ->paginate($pages);
            } else {
                $data = NotaFiscal::with(['lojaEmissor', 'pessoaDestinatario', 'enderecoDestinatario'])
//                    ->where('movimento_tipo_id', 1)
                    ->where('status', '!=', null)
                    ->whereHas('movimentoProdutos', function ($query) {
                        $query->where('venda_id', null);
                    })
                    ->orderByDesc('data_emissao', 'status')
                    ->paginate($pages);
            }

            return response()->json($data);
        } else {
            return view('estoque.notas.transferencia.index');
        }
    }

    /*public function emissao(Request $request)
    {
        $search = $request->get('search');
        $pages = $request->get('per_page') ?? 15;
        if (isset($search)) {
            $data = NotaFiscal::with(['lojaEmissor', 'pessoaDestinatario', 'enderecoDestinatario'])
                ->where('movimento_tipo_id', 1)
                ->where('status', '!=', null)
                ->where('status', '!=', 100)
                ->whereHas('movimentoProdutos', function ($query) {
                    $query->where('venda_id', null);
                })
                ->where('numero', 'like', "%{$search}%")
                ->orWhereHas('lojaEmissor', function ($query) use ($search) {
                    $query->where('descricao', 'like', "%{$search}%")
                        ->orWhere('razao_social', 'like', "%{$search}%")
                        ->orWhere('cnpj', str_replace([' ', '.', '-', '/'], ['', '', '', ''], $search));
                })
                ->orWhereHas('pessoaDestinatario', function ($query) use ($search) {
                    $query->where('nome', 'like', "%{$search}%")
                        ->orWhereHas('juridica', function ($query) use ($search) {
                            $query->where('razao_social', 'like', "%{$search}%")
                                ->orWhere('cnpj', str_replace([' ', '.', '-', '/'], ['', '', '', ''], $search));
                        });
                })
                ->orderByDesc('data_emissao')
                ->paginate($pages);
        } else {
            $data = NotaFiscal::with(['lojaEmissor', 'pessoaDestinatario', 'enderecoDestinatario'])
                ->where('movimento_tipo_id', 1)
                ->where('status', '!=', null)
                ->where('status', '!=', 100)
                ->whereHas('movimentoProdutos', function ($query) {
                    $query->where('venda_id', null);
                })
                ->orderByDesc('data_emissao')
                ->paginate($pages);
        }

        return response()->json($data);
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('estoque.notas.transferencia.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
