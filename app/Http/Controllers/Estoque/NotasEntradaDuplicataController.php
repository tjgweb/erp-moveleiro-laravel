<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Duplicata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NotasEntradaDuplicataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nota_fiscal_id = $request->get('nota_fiscal_id');

        if ($request->ajax() && $nota_fiscal_id) {

            $duplicatas = Duplicata::where('nota_fiscal_id', $nota_fiscal_id)->get();

            return response()->json($duplicatas);
        }

        if ($nota_fiscal_id) {
            $duplicatas = Duplicata::where('nota_fiscal_id', $nota_fiscal_id)->paginate();
        } else {
            $duplicatas = Duplicata::paginate();
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'nota_fiscal_id' => 'required',
            'numero'         => 'required',
            'vencimento'     => 'required',
            'valor'          => 'required'
        ]);

        DB::beginTransaction();

        try {

            Duplicata::create($request->all());

            DB::commit();

            if ($request->ajax()) {

                return response()->json([], 201);

            } else {

                toastr()->success('Salvo com sucesso!');

                return redirect()->route('estoque.notas-entrada.duplicatas.index', 201);
            }

        } catch (\Exception $e) {

            DB::rollBack();

            if ($request->ajax()) {

                return response()->json($e->getMessage(), 409);

            } else {

                toastr()->error($e->getMessage());

                return back()->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nota_fiscal_id' => 'required',
            'numero'         => 'required',
            'vencimento'     => 'required',
            'valor'          => 'required'
        ]);

        DB::beginTransaction();

        try {

            Duplicata::findOrFail($id)->update($request->all());

            DB::commit();

            if ($request->ajax()) {

                return response()->json([], 202);

            } else {

                toastr()->success('Atualizado com sucesso!');

                return redirect()->route('estoque.notas-entrada.duplicatas.index', 202);
            }

        } catch (\Exception $e) {

            DB::rollBack();

            if ($request->ajax()) {

                return response()->json($e->getMessage(), 409);

            } else {

                toastr()->error($e->getMessage());

                return back()->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, $id)
    {
        Duplicata::findOrFail($id)->delete();

        if ($request->ajax()) {
            return response()->json([], 202);
        }

        toastr()->success('Excluído com sucesso');

        return redirect()->route('estoque.notas-entrada.duplicatas.index');
    }
}
