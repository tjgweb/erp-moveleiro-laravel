<?php

namespace App\Http\Controllers\Estoque;

use App\Models\PosVenda;
use App\Models\Venda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EntregaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entregas = PosVenda::where('tipo_pos_venda_id', 1)->paginate();

        return view('estoque.pos-venda.entregas.index', compact('entregas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
//        dd($data);
        $validator = Validator::make($data, [
            'tipo_pos_venda_id' => 'required',
            'data' => 'required',
            'hora' => 'required',
            'tipo_retirada' => 'required',
            'equipe' => 'required'
        ]);
        if ($validator->fails()) {
            toastr()->error('Corrija os erros no formulário da guia de entrega.', 'Guia de Entrega');

            return redirect(route('estoque.pos-venda.retiradas.show', ['id' => $data['venda_id']]))
                ->withErrors($validator)
                ->withInput();
        }

        DB::beginTransaction();
        try {
            $venda = Venda::find($data['venda_id']);


            $posVenda = PosVenda::create($data);
            $posVenda->equipe()->attach($data['equipe']);
        } catch (\Exception $e) {
            DB::rollBack();
        }

        DB::rollBack();
        toastr()->success('Salvo com sucesso');

        return redirect()->route('estoque.pos-venda.retiradas.show', ['id' => $data['venda_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
