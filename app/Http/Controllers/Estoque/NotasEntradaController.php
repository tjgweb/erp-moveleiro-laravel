<?php

namespace App\Http\Controllers\Estoque;

use App\Http\Controllers\Controller;
use App\Models\AliquotaIcms;
use App\Models\Carga;
use App\Models\Duplicata;
use App\Models\NaturezaOperacao;
use App\Models\NotaFiscal;
use App\Models\Pessoa;
use App\Pdf\EstoqueNotaFiscal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Picqer\Barcode\BarcodeGeneratorPNG;

/** @permissionGroup('Nota Fiscal Entrada') */
class NotasEntradaController extends Controller
{
    /** @permissionName('Acesso Menu N.F. Entrada') */
    public function index()
    {
        return view('estoque.notas.compra.index');
    }

    /** @permissionName('Listar Notas') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = NotaFiscal::with(['pessoaEmissor'])
                ->where('movimento_tipo_id', 2)
                ->where('numero', 'like', "%{$search}%")
                ->orWhereHas('pessoaEmissor', function ($query) use ($search) {
                    $query->where('nome', 'like', "%{$search}%");
                })
                ->orderByDesc('data_emissao')
                ->paginate($pages);
        } else {
            $data = NotaFiscal::with(['pessoaEmissor'])
                ->where('movimento_tipo_id', 2)
                ->orderByDesc('data_emissao')->paginate($pages);
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function pdf($id)
    {
        $nota = NotaFiscal::findOrFail($id);

        (new EstoqueNotaFiscal($nota))->Output('I', "nota-fiscal-{$nota->numero}.pdf", true);
        exit;
    }

    public function create()
    {
        return view('estoque.notas.compra.form');
    }

    /** @permissionName('Salvar Nota') */
    public function store(Request $request)
    {
        $data = $request->all();
        $errors = [];

        $notaValidator = Validator::make($data['nota'], [
            'numero'               => 'required|max:50',
            'serie'                => 'required|max:10',
            'natureza_operacao_id' => 'required',
            'chave_acesso'         => 'required|max:50',
            'data_emissao'         => 'required',
            'data_saida'           => 'required',
            'hora_saida'           => 'required',
            'emissor_id'           => 'required',
            'endereco_emissor_id'  => 'required',
        ]);
        if ($notaValidator->fails()) {
            $errors = array_merge($notaValidator->errors()->toArray(), $errors);
        }
        $cargaValidator = Validator::make($data['carga'], [
            'transportador_id' => 'required',
            'quantidade'       => 'required',
            'peso_bruto'       => 'required',
            'peso_liquido'     => 'required',
            'placa_veiculo'    => 'required',
            'frete_pagador'    => 'required'
        ]);
        if ($cargaValidator->fails()) {
            $errors = array_merge($cargaValidator->errors()->toArray(), $errors);
        }
        if (count($errors) > 0) {
            return response()->json(['errors' => $errors], 422);
        }

        DB::beginTransaction();
        try {
            $nota = NotaFiscal::create($data['nota']);
            $data['carga']['nota_fiscal_id'] = $nota->id;
            Carga::create($data['carga']);
            DB::commit();
            return response()->json(['id' => $nota->id]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([$e->getMessage()], 409);
        }
    }

    /*
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nota = NotaFiscal::find($id);
        $carga = Carga::find($id);
        $duplicatas = Duplicata::where('nota_fiscal_id', $id)->get();

        return response()->json(compact(['nota', 'carga', 'duplicatas']));
    }

    public function edit($id)
    {
        return view('estoque.notas.compra.form', ['notaId' => $id]);
    }

    /** @permissionName('Atualizar Notas') */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $errors = [];

        $notaValidator = Validator::make($data['nota'], [
            'numero'               => 'required|max:50',
            'serie'                => 'required|max:10',
            'natureza_operacao_id' => 'required',
            'chave_acesso'         => 'required|max:50',
            'data_emissao'         => 'required',
            'data_saida'           => 'required',
            'hora_saida'           => 'required',
            'emissor_id'           => 'required',
            'endereco_emissor_id'  => 'required',
        ]);
        if ($notaValidator->fails()) {
            $errors = array_merge($notaValidator->errors()->toArray(), $errors);
        }
        $cargaValidator = Validator::make($data['carga'], [
            'transportador_id' => 'required',
            'quantidade'       => 'required',
            'peso_bruto'       => 'required',
            'peso_liquido'     => 'required',
            'placa_veiculo'    => 'required',
            'frete_pagador'    => 'required'
        ]);
        if ($cargaValidator->fails()) {
            $errors = array_merge($cargaValidator->errors()->toArray(), $errors);
        }
        if (count($errors) > 0) {
            return response()->json(['errors' => $errors], 422);
        }

        DB::beginTransaction();
        try {
            $nota = NotaFiscal::find($id)->update($data['nota']);
            Carga::find($id)->update($data['carga']);
            DB::commit();
            return response()->json($nota);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    /** @permissionName('Remover Nota') */
    public function destroy($id)
    {
        $nota = Notafiscal::find($id);

        //validar se não existe venda de produtos desta nota
        /*if($nota->movimentoProdutos->where()->count() > 0){

        }*/

        DB::beginTransaction();
        try {
            foreach ($nota->ajuste as $a) {
                $a->delete();
            }
            foreach ($nota->movimentoProdutos as $mp) {
                $mp->delete();
            }
            foreach ($nota->duplicatas as $duplicata) {
                $duplicata->delete();
            }
            $nota->carga()->delete();
            $nota->delete();
            DB::commit();
            return response()->json([]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }

    }
}
