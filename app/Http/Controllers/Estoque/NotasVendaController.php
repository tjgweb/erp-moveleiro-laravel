<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Carga;
use App\Models\Cidade;
use App\Models\MovimentoProduto;
use App\Models\NotaFiscal;
use App\Models\Venda;
use App\Pdf\EstoqueNotaFiscal;
use App\Services\Nfe;
use App\Services\NfeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class NotasVendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $search = $request->get('search');
            $pages = $request->get('per_page') ?? 15;
            if (isset($search)) {
                $data = NotaFiscal::with(['pessoaDestinatario', 'enderecoDestinatario'])
                    ->where('movimento_tipo_id', 1)
                    ->whereHas('movimentoProdutos', function ($query) {
                        $query->whereHas('venda', function ($query) {
                            $query->where('estatus_venda_id', 2);
                        });
                    })
                    ->where('numero', 'like', "%{$search}%")
                    ->orWhereHas('pessoaEmissor', function ($query) use ($search) {
                        $query->where('nome', 'like', "%{$search}%");
                    })
                    ->orderByDesc('data_emissao')
                    ->paginate($pages);
            } else {
                $data = NotaFiscal::with(['pessoaDestinatario', 'enderecoDestinatario'])
                    ->where('movimento_tipo_id', 1)
                    ->whereHas('movimentoProdutos', function ($query) {
                        $query->whereHas('venda', function ($query) {
                            $query->where('estatus_venda_id', 2);
                        });
                    })
                    ->orderByDesc('data_emissao')
                    ->paginate($pages);
            }

            return response()->json($data);
        } else {
            return view('estoque.notas.venda.index');
        }

    }

    public function emissao(Request $request)
    {
        $search = $request->get('search');
        $pages = $request->get('per_page') ?? 15;
        if ($search) {
            $data = Venda::with(['pessoaCliente.fisica', 'pessoaCliente.juridica'])
                ->whereIn('estatus_venda_id', [6, 7])
                ->whereHas('pessoaCliente', function ($query) use ($search) {
                    $query->where('nome', 'like', "%{$search}%")
                        ->orWhereHas('fisica', function ($query) use ($search) {
                            $query->where('cpf', 'like', "{$search}%");
                        })
                        ->orWhereHas('juridica', function ($query) use ($search) {
                            $query->where('cnpj', 'like', "{$search}%");
                        });
                })
                ->orderBy('previsao_entrega')
                ->paginate();
        } else {
            $data = Venda::with(['pessoaCliente.fisica', 'pessoaCliente.juridica'])
                ->whereIn('estatus_venda_id', [6, 7])
                ->orderBy('previsao_entrega')
                ->paginate($pages);
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendaId = $request->get('venda_id');
        $venda = Venda::with([
            'pessoaCliente.enderecos',
            'produtoVendas.produto.marca',
            'produtoVendas.produto.mercadoria'
        ])->find($vendaId);

        if ($venda->movimentoProdutos->count()) {
            $nota = NotaFiscal::with([
                'movimentoProdutos.produto',
                'movimentoProdutos.lojasSaida',
                'pessoaDestinatario',
                'enderecoDestinatario',
                'carga',
                'loteNfe'
            ])
                ->findOrFail($venda->movimentoProdutos->first()->notaFiscal->id);

            return view('estoque.notas.venda.form', compact(['venda', 'nota']));
        }
        return view('estoque.notas.venda.form', compact(['venda']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $errors = [];

        $notaValidator = Validator::make($data['nota'], [
            'modelo'               => 'required',
            'natureza_operacao_id' => 'required',
            'data_saida'           => 'required_if:modelo,55',
            'hora_saida'           => 'required_if:modelo,55',
            'emissor_id'           => 'required',
            'endereco_emissor_id'  => 'required',
        ]);
        if ($notaValidator->fails()) {
            $errors = array_merge($notaValidator->errors()->toArray(), $errors);
        }
        $cargaValidator = Validator::make($data['carga'], [
            'transportador_id' => 'required',
            'frete_pagador'    => 'required',
            'quantidade'       => 'required',
            'peso_bruto'       => 'required',
            'peso_liquido'     => 'required',
            'placa_veiculo'    => 'required',
        ]);
        if ($cargaValidator->fails()) {
            $errors = array_merge($cargaValidator->errors()->toArray(), $errors);
        }
        if (count($errors) > 0) {
            return response()->json(['errors' => $errors], 422);
        }

        try {
            DB::beginTransaction();
            $nota = NotaFiscal::create($data['nota']);

            $data['carga']['nota_fiscal_id'] = $nota->id;
            Carga::create($data['carga']);

            foreach ($data['nota']['movimento_produtos'] as $mp) {
                $mp['nota_fiscal_id'] = $nota->id;
                $mp['loja_saida_id'] = 1;
                MovimentoProduto::create($mp);
                // Gerar nota de transferência caso o produto esteja em outro depósito.
            }

            $venda = Venda::find($data['venda_id']);

            // Gerar nota
            $nfe = new NfeService($nota);
            $cStat = $nfe->makeNfe();
            switch ($cStat) {
                case 999:
                    $venda->estatus_venda_id = 6;
                    toastr()->warning('Nota preparada com sucesso mas não pode ser transmitida.', 'SEFAZ não responde');
                    break;
                case 100:
                    $venda->estatus_venda_id = 2;
                    toastr()->success('Nota autorizada.');
                    break;
                case 103:
                    $venda->estatus_venda_id = 7;
                    toastr()->warning('Consulte em alguns minutos.', 'Lote enviado');
                    break;
                case 105:
                    $venda->estatus_venda_id = 7;
                    toastr()->warning('Consulte em alguns minutos.', 'Lote em processamento');
                    break;
                case 302:
                    $venda->estatus_venda_id = 7;
                    toastr()->error('Nota preparada com sucesso mas não foi aceita.', 'Denegada ou rejeitada');
                    break;
            }

            $venda->save();
            DB::commit();

            return response()->json([], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return mixed | \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($id)
    {
        try {
            $nota = NotaFiscal::findOrFail($id);

            (new EstoqueNotaFiscal($nota))->Output('I', "nota-fiscal-{$nota->numero}.pdf", true);
            exit;
        } catch (\Exception $e) {
            toastr()->error($e->getMessage());
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('estoque.notas.venda.form', ['notaId' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $errors = [];

        $notaValidator = Validator::make($data['nota'], [
            'modelo'               => 'required',
            'natureza_operacao_id' => 'required',
            'data_saida'           => 'required_if:modelo,55',
            'hora_saida'           => 'required_if:modelo,55',
            'emissor_id'           => 'required',
            'endereco_emissor_id'  => 'required',
        ]);
        if ($notaValidator->fails()) {
            $errors = array_merge($notaValidator->errors()->toArray(), $errors);
        }
        $cargaValidator = Validator::make($data['carga'], [
            'transportador_id' => 'required',
            'frete_pagador'    => 'required',
            'quantidade'       => 'required',
            'peso_bruto'       => 'required',
            'peso_liquido'     => 'required',
            'placa_veiculo'    => 'required',
        ]);
        if ($cargaValidator->fails()) {
            $errors = array_merge($cargaValidator->errors()->toArray(), $errors);
        }
        if (count($errors) > 0) {
            return response()->json(['errors' => $errors], 422);
        }

        try {
            DB::beginTransaction();
            $nota = NotaFiscal::findOrFail($id);
            $nota->update($data['nota']);
            $nota->carga->update($data['carga']);
            foreach ($data['nota']['movimento_produtos'] as $mp) {
                MovimentoProduto::findOrFail($mp['nota_fiscal_id'])->update($mp);
            }
            $venda = Venda::find($data['venda_id']);

            if ($venda->estatus_venda_id == 6) {
                // Gerar nota
                $nfe = new NfeService($nota);
                $cStat = $nfe->makeNfe();
            } else {
                // Retransmitir nota
                $nfe = new NfeService($nota);
            }


            return response()->json([], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
