<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Categoria;
use App\Models\Mercadoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Mercadoria') */
class MercadoriaController extends Controller
{

    /** @permissionName('Acesso Menu Mercadorias') */
    public function index()
    {
        return view('estoque.mercadorias');
    }

    /** @permissionName('Listar Mercadorias') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Mercadoria::with(['categoria', 'produtos'])
                ->whereHas('categoria', function ($query) use ($search) {
                    $query->where('descricao', 'like', "%{$search}%");
                })
                ->orWhere('descricao', 'like', "%{$search}%")
                ->paginate($pages);
        } else {
            $data = Mercadoria::with(['categoria', 'produtos'])->paginate($pages);
        }

        return response()->json($data);
    }

    public function categorias()
    {
        return response()->json(Categoria::all());
    }

    /** @permissionName('Criar Mercadorias') */
    public function store(Request $request)
    {
        $request->validate([
            'categoria_id' => 'required',
            'descricao' => 'required|min:4|max:100'
        ]);

        $data = Mercadoria::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar Mercadorias') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'categoria_id' => 'required',
            'descricao' => 'required|min:4|max:100'
        ]);

        $data = Mercadoria::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover Mercadorias') */
    public function destroy($id)
    {
        $data = Mercadoria::find($id);
        if ($data->produtos()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir mercadorias associadas a um produto.'
            ], 409);
        }

        return response()->json($data->delete());
    }
}
