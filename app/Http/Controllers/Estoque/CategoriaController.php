<?php

namespace App\Http\Controllers\Estoque;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/** @permissionGroup('Categoria') */
class CategoriaController extends Controller
{

    /** @permissionName('Acesso Menu Categorias') */
    public function index(Request $request)
    {
        return view('estoque.categorias');
    }

    /** @permissionName('Listar Categorias') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = Categoria::with('mercadorias')
                ->where('descricao', 'like', "%{$search}%")
                ->orWhere('numero_ncm', 'like', "%{$search}%")
                ->paginate($pages);
        } else {
            $data = Categoria::with('mercadorias')->paginate($pages);
        }
        return response()->json($data);
    }

    /** @permissionName('Criar Categorias') */
    public function store(Request $request)
    {
        $request->validate([
            'numero_ncm' => 'required|integer',
            'descricao' => 'required|max:200'
        ]);

        $data = Categoria::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar Categorias') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'numero_ncm' => 'required|integer',
            'descricao' => 'required|max:200'
        ]);

        $data = Categoria::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover Categorias') */
    public function destroy($id)
    {
        $data = Categoria::find($id);
        if ($data->mercadorias()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir categorias associadas a uma mercadoria.'
            ], 409);
        }

        return response()->json($data->delete());
    }
}
