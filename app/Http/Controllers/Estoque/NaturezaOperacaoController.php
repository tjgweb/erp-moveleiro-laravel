<?php

namespace App\Http\Controllers\Estoque;

use App\Models\NaturezaOperacao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

/** @permissionGroup('Natureza Operação') */
class NaturezaOperacaoController extends Controller
{
    /** @permissionName('Acesso ao menu Natureza Operação') */
    public function index()
    {
        return view('estoque.natureza-operacao');
    }

    /** @permissionName('Listar Natureza Operação') */
    public function lista(Request $request, $pages = 15)
    {
        $search = $request->get('search');
        if (isset($search)) {
            $data = NaturezaOperacao::with(['notaFiscais:id', 'movimentoProdutos:id'])
                ->where('descricao', 'like', "%{$search}%")
                ->orWhere('cfop', 'like', "{$search}%")
                ->paginate($pages);
        } else {
            $data = NaturezaOperacao::with(['notaFiscais:id', 'movimentoProdutos:id'])->orderBy('descricao')->paginate($pages);
        }

        return response()->json($data);
    }

    /** @permissionName('Criar Natureza Operação') */
    public function store(Request $request)
    {
        $request->validate([
            'cfop' => 'required|min:4|max:8|unique:natureza_operacao,cfop',
            'descricao' => 'required|min:4'
        ]);
        $data = NaturezaOperacao::create($request->all());

        return response()->json($data);
    }

    /** @permissionName('Atualizar Natureza Operação') */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cfop' => ['required', 'min:4', 'max:8', Rule::unique('natureza_operacao', 'cfop')->ignore($id)],
            'descricao' => 'required|min:4'
        ]);
        $data = NaturezaOperacao::find($id)->update($request->all());

        return response()->json($data);
    }

    /** @permissionName('Remover Natureza Operação') */
    public function destroy($id)
    {
        $data = NaturezaOperacao::find($id);
        if ($data->movimentoProdutos()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir Natureza de Operação que está relacionada com movimento de produtos.'
            ], 409);
        }
        if ($data->notaFiscais()->count() > 0) {
            return response()->json([
                'error' => 'Você não pode excluir Natureza de Operação que está relacionada com uma Nota Fiscal.'
            ], 409);
        }
        $data->delete();
        return response()->json([]);
    }
}
