<?php

namespace App\Pdf;


use Carbon\Carbon;
use TJGazel\LaraFpdf\LaraFpdf;

class EstoqueAssistencias extends LaraFpdf
{
    use PdfTrait;

    public $posVenda;

    public function __construct($posVenda)
    {
        $this->posVenda = $posVenda;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Assistência Nº ' . $this->posVenda->id, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage();
        $this->Body();
    }

    public function Header()
    {
        $altura = 6;
        $this->SetDarkFill();

        $this->SetFont('Arial', 'B', '14');
        $this->Cell($this->CelX(70), $altura, 'Brasão Center Móveis', 'LT', 0, 'C', true);
        $this->SetFont('Arial', '', '11');
        $this->Cell($this->CelX(30), $altura, "Assistência Nº: {$this->posVenda->id}", 'TR', 1, 'L', true);

        $this->SetFont('Arial', '', '9');
        $this->Cell($this->CelX(70), $altura, "Rua Engenheiro Argolo, 247 - Centro - Teófilo Otoni - MG - 39802-034",
            '', 0, 'C', true);
        $this->SetFont('Arial', '', '11');
        $this->Cell($this->CelX(30), $altura, "Data: " . date('d/m/Y', strtotime($this->posVenda->data)), 'R', 1, 'L',
            true);

        $this->Cell($this->CelX(70), $altura, "Telefone: (33) 3522-3199", 'LB', 0, 'C', true);
        $this->Cell($this->CelX(30), $altura, "Hora: " . date('H:i', strtotime($this->posVenda->hora)), 'RB', 1, 'L',
            true);

        $this->SetDefaultFill();
        $this->Cell($this->CelX(100), 3, '', '', 1, 'C');
    }

    public function Body()
    {
        $this->dadosCliente($this->posVenda->movimentoProdutos->first()->venda);
        $this->Cell($this->CelX(100), 3, '', '', 1, 'C');
        $this->assistencia();
    }

    public function Footer()
    {
        $altura = 8;
        $fonte = 10;
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(100), 30, '', '', 1, 'C');
        $this->Cell($this->CelX(100), 3, '-----------------------------------------------------------------------------------', '', 1, 'C');
        $this->Cell($this->CelX(100), $altura, 'Assinatura do Cliente', '', 1, 'C');
    }

    public function assistencia()
    {
        $altura = 8;
        $fonte = 10;
        $fonteTitulo = $fonte * 130 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, 'Dados da Assistência', 'B', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->Cell($this->CelX(10), $altura, 'Tipo', 'LB', 0, 'R');
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(90), $altura, $this->posVenda->tipoPosVenda->descricao, 'LRB', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->Cell($this->CelX(10), $altura, 'Equipe', 'LB', 0, 'R');
        $this->SetFont('Arial', '', $fonte);
        $equipe = '';
        foreach ($this->posVenda->equipe as $membro) {
            $equipe .= $membro->nome . '; ';
        }
        $this->Cell($this->CelX(90), $altura, substr($equipe, 0, -2), 'LRB', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->Cell($this->CelX(10), $altura * 2, 'Descrição', 'LR', 0, 'R');
        $x = $this->GetX();
        $y = $this->GetY();
        $this->SetFont('Arial', '', $fonte);
        $this->MultiCell($this->CelX(90), $altura / 1.6, $this->posVenda->descricao, '', 'L');
        $this->SetXY($x + $this->CelX(90), $y);
        $this->Cell(0, $altura * 2, '', 'R', 1, 'L');
        $this->Cell($this->CelX(100), 0, '', 'B', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->SetDarkFill(true);
        $this->Cell($this->CelX(100), $altura, 'Produtos relacionados à assistência', 'LRBT', 1, 'C', true);
        $this->SetDefaultFill();
        $this->Cell($this->CelX(10), $altura, 'Qtd', 'LB', 0, 'C');
        $this->Cell($this->CelX(90), $altura, 'Descrição', 'LBR', 1, 'L');
        $this->SetFont('Arial', '', $fonte);
        foreach ($this->posVenda->movimentoProdutos as $mp) {
            $this->Cell($this->CelX(10), $altura, $mp->pivot->quantidade, 'LB', 0, 'C');
            $this->Cell($this->CelX(90), $altura, $mp->produto->descricao_completa, 'LBR', 1, 'L');
        }

        $this->Cell($this->CelX(100), 3, '', '', 1, 'C');

    }

}
