<?php

namespace App\Pdf;

use App\Models\Caixa;
use App\Models\Cartao;
use TJGazel\LaraFpdf\LaraFpdf;

class CaixaRelatorio extends LaraFpdf
{

    private $caixa;
    private $cartoes;
    private $financiamentos;

    private $finalCol1Y;
    private $finalCol2Y;
    private $finalCol3Y;

    /**
     * ReciboPagamento constructor.
     * @param LaraFpdf $pdf
     */
    public function __construct(Caixa $caixa, $cartoes, $financiamentos)
    {
        $this->caixa = $caixa;
        $this->cartoes = $cartoes;
        $this->financiamentos = $financiamentos;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Relatório Caixa Nº ' . $this->caixa->id, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage('L');
        $this->Body();
    }


    public function Header()
    {
        $altura = 8;
        $this->SetDarkFill();
        $this->SetFont('Arial', 'B', '16');
        $this->Cell($this->CelX(60), $altura, 'Brasão Center Móveis', 'LT', 0, 'R', true);
        $this->SetFont('Arial', 'B', '13');
        $this->Cell($this->CelX(40), $altura, "Relatório Caixa Nº: {$this->caixa->id}", 'TR', 1, 'R', true);
        $this->SetFont('Arial', 'B', '11');
        $this->Cell($this->CelX(42), $altura, 'Operador: ' . $this->caixa->operador->apelido, 'LB', 0, 'L', true);
        $this->Cell($this->CelX(28), $altura, 'Abertura: ' . date('d/m/Y H:i', strtotime($this->caixa->created_at)),
            'B', 0, 'C', true);
        $this->Cell($this->CelX(30), $altura, 'Atualização: ' . date('d/m/Y H:i', strtotime($this->caixa->updated_at)),
            'BR', 1, 'C', true);
        $this->SetDefaultFill();
        $altura = 8;
        $this->Cell($this->CelX(100), $altura, '', '', 1, 'C');
    }

    public function Body()
    {
        $this->resumo();
        $this->Cell($this->CelX(100), 5, '', '', 1, 'C');
        $this->resumoCartaoDebito();
        $this->resumoCartaoCredito();
        $this->resumoFinanceiras();
        $this->finalCol1Y > $this->finalCol2Y && $this->finalCol1Y > $this->finalCol3Y ? $this->SetY($this->finalCol1Y) :
            $this->finalCol2Y > $this->finalCol1Y && $this->finalCol2Y > $this->finalCol3Y ? $this->SetY($this->finalCol2Y) :
                $this->finalCol3Y > $this->finalCol1Y && $this->finalCol3Y > $this->finalCol2Y ? $this->SetY($this->finalCol3Y) : null;
        $this->Cell($this->CelX(100), 5, '', '', 1, 'C');
        $this->resumoCredito();
        $this->resumoRetirada();

        $this->AddPage('L');
        $this->SetFont('Arial', 'B', 14);
        $this->Cell($this->CelX(100), 8, 'Detalhes das Transações', 'B', 1, 'C');
        $this->Cell($this->CelX(100), 5, '', '', 1, 'C');
        $this->dinheiro();
        $this->cartao('D');
        $this->cartao('C');
        $this->financeira();
        $this->crediario();
        $this->cheques();
    }

    public function Footer()
    {

    }

    private function resumo()
    {
        $alturaTitulo = 8;
        $alturaLinha = 8;
        $fonteTitulo = 11;
        $fonteLinha = 10;

        $this->SetFont('Arial', 'B', $fonteTitulo + 3);
        $this->Cell($this->CelX(100), $alturaTitulo, 'Resumo do caixa', 'B', 1, 'C');

        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Troco inicial', '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, 'Crédito', '', 0, 'C');
        $this->Cell($this->CelX(10), $alturaLinha, 'Dinheiro', '', 0, 'C');
        $this->Cell($this->CelX(10), $alturaLinha, 'Cartão Débido', '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, 'Cartão Crédito', '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, 'Financiamento', '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, 'Crediário', '', 0, 'C');
        $this->Cell($this->CelX(10), $alturaLinha, 'Cheque', '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, 'Retirada', '', 1, 'C');

        $this->SetFont('Arial', '', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, number_format($this->caixa->troco_inicial, 2, ',', '.'), '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->total_credito, 2, ',', '.'), '', 0, 'C');
        $this->Cell($this->CelX(10), $alturaLinha, number_format($this->caixa->total_dinheiro, 2, ',', '.'), '', 0,'C');
        $this->Cell($this->CelX(10), $alturaLinha, number_format($this->caixa->total_cartao_debito, 2, ',', '.'), '', 0,'C');
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->total_cartao_credito, 2, ',', '.'), '',0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->total_financiamento, 2, ',', '.'), '', 0,'C');
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->total_crediario, 2, ',', '.'), '', 0,'C');
        $this->Cell($this->CelX(10), $alturaLinha, number_format($this->caixa->total_cheque, 2, ',', '.'), '', 0, 'C');
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->total_retirada, 2, ',', '.'), '', 1,'C');

        $this->SetFont('Arial', '', $fonteTitulo);
        $this->Cell($this->CelX(88), $alturaLinha, 'Fluxo de caixa', '', 0, 'R');
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->fluxo, 2, ',', '.'), '', 1, 'R');
        $this->SetFont('Arial', '', $fonteTitulo);
        $this->Cell($this->CelX(88), $alturaLinha, 'Em caixa', '', 0, 'R');
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(12), $alturaLinha, number_format($this->caixa->em_caixa, 2, ',', '.'), '', 1, 'R');
    }

    private function resumoCartaoDebito()
    {
        $maxCol = 32;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;
        $y = $this->GetY();

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Cartão Débito', 'LTRB', 1, 'C', true);
        $this->SetDefaultFill();

        $hasData = false;
        $total = 0;
        if ($this->cartoes->where('tipo', 'D')->count()) {
            foreach ($this->cartoes->where('tipo', 'D')->groupBy('bandeira_id') as $bandeiras) {
                $hasData = true;
                $this->SetFont('Arial', '', $fonteLinha - 2);
                $this->Cell($this->CelX(22), $alturaLinha,
                    $bandeiras[0]->bandeira->nome . ' - ' . $bandeiras[0]->bandeira->banco_credito, 'LB', 0, 'L');
                $this->SetFont('Arial', '', $fonteLinha);
                $this->Cell($this->CelX(10), $alturaLinha, number_format($bandeiras->sum('total'), 2, ',', '.'), 'LBR',
                    1, 'R');
                $total += $bandeiras->sum('total');
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX(32), $alturaLinha, 'Nenhum registro', 'LBR', 1, 'C');
        }
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(32), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');

        $this->finalCol1Y = $this->GetY();
        $this->SetY($y);
    }

    private function resumoCartaoCredito()
    {
        $maxCol = 32;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteLinha = 9;
        $fonteTitulo = 11;
        $y = $this->GetY();
        $initCol = 34;
        $this->PosX($initCol);

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Cartão Crédito', 'LTRB', 1, 'C', true);
        $this->SetDefaultFill();

        $hasData = false;
        $total = 0;
        if ($this->cartoes->where('tipo', 'C')->count()) {
            foreach ($this->cartoes->where('tipo', 'C')->groupBy('bandeira_id') as $bandeiras) {
                $hasData = true;
                $this->PosX($initCol);
                $this->SetFont('Arial', '', $fonteLinha - 2);
                $this->Cell($this->CelX(22), $alturaLinha,
                    $bandeiras[0]->bandeira->nome . ' - ' . $bandeiras[0]->bandeira->banco_credito, 'LB', 0, 'L');
                $this->SetFont('Arial', '', $fonteLinha);
                $this->Cell($this->CelX(10), $alturaLinha, number_format($bandeiras->sum('total'), 2, ',', '.'), 'LBR',
                    1, 'R');
                $total += $bandeiras->sum('total');
            }
        }
        if (!$hasData) {
            $this->PosX($initCol);
            $this->Cell($this->CelX(32), $alturaLinha, 'Nenhum registro', 'LBR', 1, 'C');
        }
        $this->PosX($initCol);
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(32), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');

        $this->finalCol2Y = $this->GetY();
        $this->SetY($y);
    }

    private function resumoFinanceiras()
    {
        $maxCol = 32;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;
        $initCol = 68;
        $this->PosX($initCol);

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Financeiras', 'LTRB', 1, 'C', true);
        $this->SetDefaultFill();

        $hasData = false;
        $total = 0;
        if ($this->financiamentos->count()) {
            foreach ($this->financiamentos->groupBy('financeira_id') as $financiamento) {
                $hasData = true;
                $this->PosX($initCol);
                $this->SetFont('Arial', '', $fonteLinha - 2);
                $this->Cell($this->CelX(22), $alturaLinha,
                    $financiamento[0]->financeira->nome . ' - ' . $financiamento[0]->financeira->banco_credito, 'LB', 0,
                    'L');
                $this->SetFont('Arial', '', $fonteLinha);
                $this->Cell($this->CelX(10), $alturaLinha, number_format($financiamento->sum('total'), 2, ',', '.'),
                    'LBR', 1, 'R');
                $total += $financiamento->sum('total');
            }
        }
        if (!$hasData) {
            $this->PosX($initCol);
            $this->Cell($this->CelX(32), $alturaLinha, 'Nenhum registro', 'LBR', 1, 'C');
        }
        $this->PosX($initCol);
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(32), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');

        $this->finalCol3Y = $this->GetY();
    }

    private function resumoCredito()
    {
        $maxCol = 100;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Créditos', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(70), $alturaLinha, 'Descrição', 'LTB', 0, 'L', true);
        $this->Cell($this->CelX(20), $alturaLinha, 'Valor', 'LTBR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        foreach ($this->caixa->creditos as $credito) {
            $this->Cell($this->CelX(10), $alturaLinha, date('H:i', strtotime($credito->hora)), 'LTB', 0, 'C', true);
            $this->SetFont('Arial', '', $fonteLinha * 85 / 100);
            $this->Cell($this->CelX(70), $alturaLinha, $credito->descricao, 'LTB', 0, 'L', true);
            $this->SetFont('Arial', '', $fonteLinha);
            $this->Cell($this->CelX(20), $alturaLinha, number_format($credito->valor, 2, ',', '.'), 'LTBR', 1, 'C',
                true);
        }
        if ($this->caixa->creditos->count() == 0) {
            $this->Cell($this->CelX($maxCol), $alturaLinha, 'Nenhum registro', 'LBR', 1, 'C');
        }
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo,
            number_format($this->caixa->creditos->sum('valor'), 2, ',', '.'), '', 1, 'R');
    }

    private function resumoRetirada()
    {
        $maxCol = 100;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Retiradas', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(70), $alturaLinha, 'Descrição', 'LTB', 0, 'L', true);
        $this->Cell($this->CelX(20), $alturaLinha, 'Valor', 'LTBR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        foreach ($this->caixa->retiradas as $retirada) {
            $this->Cell($this->CelX(10), $alturaLinha, date('H:i', strtotime($retirada->hora)), 'LTB', 0, 'C', true);
            $this->SetFont('Arial', '', $fonteLinha * 85 / 100);
            $this->Cell($this->CelX(70), $alturaLinha, $retirada->descricao, 'LTB', 0, 'L', true);
            $this->SetFont('Arial', '', $fonteLinha);
            $this->Cell($this->CelX(20), $alturaLinha, number_format($retirada->valor, 2, ',', '.'), 'LTBR', 1, 'C',
                true);
        }
        if ($this->caixa->retiradas->count() == 0) {
            $this->Cell($this->CelX($maxCol), $alturaLinha, 'Nenhum registro', 'LBR', 1, 'C');
        }
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo,
            number_format($this->caixa->retiradas->sum('valor'), 2, ',', '.'), '', 1, 'R');
    }


    private function dinheiro()
    {
        $maxCol = 100;
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX($maxCol), $alturaTitulo, 'Dinheiro', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Transação', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(14), $alturaLinha, 'Tipo Recebimento', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(54), $alturaLinha, 'Observações', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Valor', 'LTBR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        $hasData = false;
        $total = 0;
        foreach ($this->caixa->transacoes as $transacao) {
            if ($transacao->dinheiros->count()) {
                $this->Cell($this->CelX(10), $alturaLinha, $transacao->id, 'LB', 0, 'C');
                $this->Cell($this->CelX(7), $alturaLinha, date('H:i', strtotime($transacao->created_at)), 'LB', 0, 'C');
                if ($transacao->venda_id) {
                    $tipo = 'Venda';
                } elseif ($transacao->cheques->count()) {
                    $tipo = 'Cheque';
                } elseif ($transacao->crediarios->count()) {
                    $tipo = 'Crediário';
                } else {
                    $tipo = '';
                }
                $this->Cell($this->CelX(14), $alturaLinha, $tipo, 'LB', 0, 'C');
                $this->SetFont('Arial', '', $fonteLinha * 80 / 100);
                $this->Cell($this->CelX(54), $alturaLinha, $transacao->dinheiros->first()->observacao, 'LB', 0, 'L');
                $this->SetFont('Arial', '', $fonteLinha);
                $this->Cell($this->CelX(15), $alturaLinha,
                    number_format($transacao->dinheiros->first()->valor_recebido, 2, ',', '.'), 'LBR', 1, 'R');
                $hasData = true;
                $total += $transacao->dinheiros->first()->valor_recebido;
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX($maxCol), $alturaLinha, 'Nenhum recebimento registrado', 'LBR', 1, 'C');
        }
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');

    }

    private function cartao($tipo)
    {
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaTitulo, $tipo == 'C' ? 'Cartão de Crédito' : 'Cartão de Débito', '', 1,
            'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Transação', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(10), $alturaLinha, 'Tipo Rec', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(27), $alturaLinha, 'Bandeira', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Protocolo', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(6), $alturaLinha, 'Qtd P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(10), $alturaLinha, 'Valor P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Subtotal', 'LBTR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        $hasData = false;
        $total = 0;
        foreach ($this->caixa->transacoes as $transacao) {
            if ($transacao->cartoes->where('tipo', $tipo)->count()) {
                foreach ($transacao->cartoes as $cartao) {
                    $this->Cell($this->CelX(10), $alturaLinha, $transacao->id, 'LB', 0, 'C');
                    $this->Cell($this->CelX(7), $alturaLinha, date('H:i', strtotime($transacao->created_at)), 'LB', 0,
                        'C');
                    $this->Cell($this->CelX(10), $alturaLinha, $transacao->venda_id ? 'Venda' : 'Crediário', 'LB', 0,
                        'C');
                    $this->SetFont('Arial', '', $fonteLinha * 80 / 100);
                    $this->Cell($this->CelX(27), $alturaLinha,
                        $cartao->bandeira->nome . ' - ' . $cartao->bandeira->banco_credito, 'LB', 0, 'C');
                    $this->SetFont('Arial', '', $fonteLinha);
                    $this->Cell($this->CelX(15), $alturaLinha, $cartao->protocolo, 'LB', 0, 'C');
                    $this->Cell($this->CelX(6), $alturaLinha, $cartao->quantidade_parcelas, 'LB', 0, 'C');
                    $this->Cell($this->CelX(10), $alturaLinha, number_format($cartao->valor_parcelas, 2, ',', '.'),
                        'LB', 0, 'R');
                    $this->Cell($this->CelX(15), $alturaLinha,
                        number_format($cartao->quantidade_parcelas * $cartao->valor_parcelas, 2, ',', '.'), 'LBR', 1,
                        'R');
                    $hasData = true;
                    $total += $cartao->total;
                }
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX(100), $alturaLinha, 'Nenhum recebimento registrado', 'LBR', 1, 'C');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');

    }

    private function financeira()
    {
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaTitulo, 'Financiamentos', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Transação', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(22), $alturaLinha, 'Financeira', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Contrato', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Protocolo', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(6), $alturaLinha, 'Qtd P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(10), $alturaLinha, 'Valor P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Subtotal', 'LBTR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        $hasData = false;
        $total = 0;
        foreach ($this->caixa->transacoes as $transacao) {
            if ($transacao->financiamentos->count()) {
                foreach ($transacao->financiamentos as $financiamento) {
                    $this->Cell($this->CelX(10), $alturaLinha, $transacao->id, 'LB', 0, 'C');
                    $this->Cell($this->CelX(7), $alturaLinha, date('H:i', strtotime($transacao->created_at)), 'LB', 0,
                        'C');
                    $this->SetFont('Arial', '', $fonteLinha * 80 / 100);
                    $this->Cell($this->CelX(22), $alturaLinha,
                        $financiamento->financeira->nome . ' - ' . $financiamento->financeira->banco_credito, 'LB', 0,
                        'C');
                    $this->SetFont('Arial', '', $fonteLinha);
                    $this->Cell($this->CelX(15), $alturaLinha, $financiamento->contrato, 'LB', 0, 'C');
                    $this->Cell($this->CelX(15), $alturaLinha, $financiamento->protocolo, 'LB', 0, 'C');
                    $this->Cell($this->CelX(6), $alturaLinha, $financiamento->quantidade_parcelas, 'LB', 0, 'C');
                    $this->Cell($this->CelX(10), $alturaLinha,
                        number_format($financiamento->valor_parcelas, 2, ',', '.'), 'LB', 0, 'R');
                    $this->Cell($this->CelX(15), $alturaLinha,
                        number_format($financiamento->quantidade_parcelas * $financiamento->valor_parcelas, 2, ',',
                            '.'), 'LBR', 1, 'R');
                    $hasData = true;
                    $total += $financiamento->quantidade_parcelas * $financiamento->valor_parcelas;
                }
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX(100), $alturaLinha, 'Nenhum recebimento registrado', 'LBR', 1, 'C');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');
    }

    private function crediario()
    {
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaTitulo, 'Vendas com Crediário', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Transação', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(26), $alturaLinha, 'Observações', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaLinha, '1º Venc.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaLinha, 'Último Venc.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(6), $alturaLinha, 'Qtd P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(10), $alturaLinha, 'Valor P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Subtotal', 'LBTR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        $hasData = false;
        $total = 0;
        foreach ($this->caixa->transacoes as $transacao) {
            if ($transacao->crediarios->count()) {
                foreach ($transacao->crediarios as $crediario) {
                    $this->Cell($this->CelX(10), $alturaLinha, $transacao->id, 'LB', 0, 'C');
                    $this->Cell($this->CelX(7), $alturaLinha, date('H:i', strtotime($transacao->created_at)), 'LB', 0,
                        'C');
                    $this->SetFont('Arial', '', $fonteLinha * 80 / 100);
                    $this->Cell($this->CelX(26), $alturaLinha, $crediario->observacao, 'LB', 0, 'L');
                    $this->SetFont('Arial', '', $fonteLinha);
                    $this->Cell($this->CelX(13), $alturaLinha,
                        date('d/m/Y', strtotime($crediario->parcelas->first()->vencimento)), 'LB', 0, 'C');
                    $this->Cell($this->CelX(13), $alturaLinha,
                        date('d/m/Y', strtotime($crediario->parcelas->last()->vencimento)), 'LB', 0, 'C');
                    $this->Cell($this->CelX(6), $alturaLinha, $crediario->parcelas->count(), 'LB', 0, 'C');
                    $this->Cell($this->CelX(10), $alturaLinha,
                        number_format($crediario->parcelas->first()->valor, 2, ',', '.'), 'LB', 0, 'R');
                    $subtotal = $crediario->parcelas->sum('valor');
                    $this->Cell($this->CelX(15), $alturaLinha, number_format($subtotal, 2, ',', '.'), 'LBR', 1, 'R');
                    $hasData = true;
                    $total += $subtotal;
                }
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX(100), $alturaLinha, 'Nenhum recebimento registrado', 'LBR', 1, 'C');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');
    }

    private function cheques()
    {
        $alturaTitulo = 8;
        $alturaLinha = 6;
        $fonteTitulo = 11;
        $fonteLinha = 9;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaTitulo, 'Vendas/Pagamentos com Cheques', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont('Arial', 'B', $fonteLinha);
        $this->Cell($this->CelX(10), $alturaLinha, 'Transação', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaLinha, 'Hora', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(26), $alturaLinha, 'Observações', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaLinha, '1º Venc.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaLinha, 'Último Venc.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(6), $alturaLinha, 'Qtd P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(10), $alturaLinha, 'Valor P.', 'LTB', 0, 'C', true);
        $this->Cell($this->CelX(15), $alturaLinha, 'Subtotal', 'LBTR', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonteLinha);
        $hasData = false;
        $total = 0;
        foreach ($this->caixa->transacoes as $transacao) {
            if ($transacao->predatados->count()) {
                foreach ($transacao->predatados as $predatado) {
                    $this->Cell($this->CelX(10), $alturaLinha, $transacao->id, 'LB', 0, 'C');
                    $this->Cell($this->CelX(7), $alturaLinha, date('H:i', strtotime($transacao->created_at)), 'LB', 0,
                        'C');
                    $this->SetFont('Arial', '', $fonteLinha * 80 / 100);
                    $this->Cell($this->CelX(26), $alturaLinha, $predatado->observacao, 'LB', 0, 'L');
                    $this->SetFont('Arial', '', $fonteLinha);
                    $this->Cell($this->CelX(13), $alturaLinha,
                        date('d/m/Y', strtotime($predatado->cheques->first()->vencimento)), 'LB', 0, 'C');
                    $this->Cell($this->CelX(13), $alturaLinha,
                        date('d/m/Y', strtotime($predatado->cheques->last()->vencimento)), 'LB', 0, 'C');
                    $this->Cell($this->CelX(6), $alturaLinha, $predatado->cheques->count(), 'LB', 0, 'C');
                    $this->Cell($this->CelX(10), $alturaLinha,
                        number_format($predatado->cheques->first()->valor, 2, ',', '.'), 'LB', 0, 'R');
                    $subtotal = $predatado->cheques->sum('valor');
                    $this->Cell($this->CelX(15), $alturaLinha, number_format($subtotal, 2, ',', '.'), 'LBR', 1, 'R');
                    $hasData = true;
                    $total += $subtotal;
                }
            }
        }
        if (!$hasData) {
            $this->Cell($this->CelX(100), $alturaLinha, 'Nenhum recebimento registrado', 'LBR', 1, 'C');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaLinha, number_format($total, 2, ',', '.'), '', 1, 'R');
    }

}
