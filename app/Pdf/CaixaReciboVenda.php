<?php

namespace App\Pdf;


use App\Models\Transacao;
use TJGazel\LaraFpdf\LaraFpdf;

class CaixaReciboVenda extends LaraFpdf
{

    use CaixaReciboTrait, PdfTrait;

    /** @var Transacao */
    public $transacao;

    /**
     * CaixaReciboCrediario constructor.
     * @param Transacao $transacao
     */
    public function __construct($transacao)
    {
        $this->transacao = $transacao;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Recibo de Pagamento Nº ' . $this->transacao->id, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage();
        $this->Body();
    }

    public function Header()
    {
        $this->cabecalho($this->transacao->created_at);
        $this->dadosCliente($this->transacao->venda);
    }

    public function Body()
    {
        $this->pagamentos();
        $this->podutos();
    }

    public function Footer()
    {

    }

    private function podutos()
    {
        $alturaHeader = 8;
        $altura = 8;
        $fonte = 10;
        $fonteTitulo = $fonte * 130 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $alturaHeader, 'Produtos', 'B', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->SetDarkFill(true);
        $this->Cell($this->CelX(10), $alturaHeader, 'Código', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(35), $alturaHeader, 'Descrição', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(7), $alturaHeader, 'Qtd.', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(11), $alturaHeader, 'Valor Unit.', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaHeader, 'Valor Total', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(11), $alturaHeader, 'Desconto', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(13), $alturaHeader, 'Subtotal', 'LBRT', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonte);
        foreach ($this->transacao->venda->produtoVendas as $pv) {
            $this->SetFont('Arial', '', $fonte * 75 / 100);
            $this->Cell($this->CelX(10), $altura, $pv->produto->codigo_fabrica, 'LBR', 0, 'C');

            $y = $this->GetY();
            $x = $this->GetX();
            $this->MultiCell($this->CelX(35), $altura / 2, "{$pv->produto->mercadoria->descricao} {$pv->produto->marca->nome} {$pv->produto->descricao}", 'T', 'L');
            $this->SetXY($x + $this->CelX(35), $y);

            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(7), $altura, $pv->quantidade, 'LB', 0, 'C');
            $this->Cell($this->CelX(11), $altura, number_format($pv->valor_unitario, 2, ',', '.'), 'LB', 0, 'R');
            $this->Cell($this->CelX(13), $altura, number_format($pv->valor_total, 2, ',', '.'), 'LB', 0, 'R');
            $this->Cell($this->CelX(11), $altura, number_format($pv->valor_desconto, 2, ',', '.'), 'LB', 0, 'R');
            $this->Cell($this->CelX(13), $altura, number_format($pv->valor_pago, 2, ',', '.'), 'LBR', 1, 'R');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(10), $alturaHeader, '', '', 0, 'C');
        $this->Cell($this->CelX(35), $alturaHeader, '', 'T', 0, 'C');
        $this->Cell($this->CelX(55), $alturaHeader, number_format($this->transacao->venda->valor_pago, 2, ',', '.'), '', 1, 'R');
    }

    private function pagamentos()
    {
        $altura = 8;
        $fonte = 10;
        $fonteTitulo = $fonte * 130 / 100;
        $fonteDescricao = $fonte * 80 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, 'Formas de pagamento', '', 1, 'L');
        $this->SetFont('Arial', 'B', $fonte);
        $this->SetDarkFill(true);
        $this->Cell($this->CelX(45), $altura, 'Observações', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(25), $altura, 'Tipo', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(15), $altura, 'Condições', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(15), $altura, 'Subtotal', 'LBRT', 1, 'C', true);
        $this->SetDefaultFill();

        foreach ($this->transacao->dinheiros as $dinheiro) {
            $this->SetFont('Arial', '', $fonteDescricao);
            $this->Cell($this->CelX(45), $altura, $dinheiro->observacao, 'LBT', 0, 'L');
            $this->Cell($this->CelX(25), $altura, 'Dinheiro', 'LBT', 0, 'C');
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, '', 'LBT', 0, 'C');
            $this->Cell($this->CelX(15), $altura, number_format($dinheiro->valor_recebido, 2, ',', '.'), 'LBRT', 1, 'R');
        }

        foreach ($this->transacao->cartoes as $cartao) {
            $this->SetFont('Arial', '', $fonteDescricao);
            $this->Cell($this->CelX(45), $altura, '', 'LBT', 0, 'L');
            $tipo = $cartao->tipo == 'C' ? 'C. Crédito - ' : 'C. Débito - ';
            $this->Cell($this->CelX(25), $altura, "{$tipo} {$cartao->bandeira->nome} {$cartao->bandeira->banco_credito}", 'LBT', 0, 'C');
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, $cartao->quantidade_parcelas . ' x ' . number_format($cartao->valor_parcelas, 2, ',', '.'), 'LBT', 0, 'C');
            $this->Cell($this->CelX(15), $altura, number_format($cartao->valor_parcelas * $cartao->quantidade_parcelas, 2, ',', '.'), 'LBRT', 1, 'R');
        }

        foreach ($this->transacao->financiamentos as $financiamento) {
            $this->SetFont('Arial', '', $fonteDescricao);
            $this->Cell($this->CelX(45), $altura, '', 'LBT', 0, 'L');
            $this->Cell($this->CelX(25), $altura, "Financiamento {$financiamento->financeira->nome}", 'LBT', 0, 'C');

            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, $financiamento->quantidade_parcelas . ' x ' . number_format($financiamento->valor_parcelas, 2, ',', '.'), 'LBT', 0, 'C');
            $this->Cell($this->CelX(15), $altura, number_format($financiamento->valor_parcelas * $financiamento->quantidade_parcelas, 2, ',', '.'), 'LBRT', 1, 'R');
        }

        foreach ($this->transacao->crediarios as $crediario) {
            $this->SetFont('Arial', '', $fonteDescricao);
            $this->Cell($this->CelX(45), $altura, $crediario->observacao, 'LBT', 0, 'L');
            $this->Cell($this->CelX(25), $altura, 'Crediário', 'LBT', 0, 'C');
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, $crediario->parcelas->count() . ' x ' . number_format($crediario->parcelas->first()->valor,2,',','.'), 'LBT', 0, 'C');
            $this->Cell($this->CelX(15), $altura, number_format($crediario->parcelas->sum('valor'), 2, ',', '.'), 'LBRT', 1, 'R');
        }

        foreach ($this->transacao->predatados as $predatado) {
            $this->SetFont('Arial', '', $fonteDescricao);
            $this->Cell($this->CelX(45), $altura, $predatado->observacao, 'LBT', 0, 'L');
            $this->Cell($this->CelX(25), $altura, 'Cheque', 'LBT', 0, 'C');
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, $predatado->cheques->count() . ' x ' . number_format($predatado->cheques->first()->valor,2,',','.'), 'LBT', 0, 'C');
            $this->Cell($this->CelX(15), $altura, number_format($predatado->cheques->sum('valor'), 2, ',', '.'), 'LBRT', 1, 'R');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, number_format($this->transacao->venda->valor_pago, 2, ',', '.'), '', 1, 'R');

    }

}
