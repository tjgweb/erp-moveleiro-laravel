<?php

namespace App\Pdf;


use App\Models\Transacao;
use TJGazel\LaraFpdf\LaraFpdf;

class CaixaReciboCrediario extends LaraFpdf
{
    use CaixaReciboTrait, PdfTrait;

    /** @var Transacao */
    public $transacao;

    /**
     * CaixaReciboCrediario constructor.
     * @param Transacao $transacao
     */
    public function __construct($transacao)
    {
        $this->transacao = $transacao;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Recibo de Pagamento Nº ' . $this->transacao->id, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage();
        $this->Body();
    }

    public function Header()
    {
        $this->cabecalho($this->transacao->created_at);
        $this->dadosCliente($this->transacao->parcelas[0]->crediario->transacao->venda);
    }

    public function Body()
    {
        $this->pagamentos();
        $this->parcelas();
    }

    public function Footer()
    {

    }

    private function parcelas()
    {
        $altura = 8;
        $fonte = 10;
        $fonteTitulo = $fonte * 130 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, 'Parcelas pagas', '', 1, 'L');

        $this->SetFont('Arial', 'B', $fonte);
        $this->SetDarkFill(true);
        $this->Cell($this->CelX(9), $altura, 'Crediário', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(8), $altura, 'Parcela', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(13), $altura, 'Data Compra', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(12), $altura, 'Vencimento', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(11), $altura, 'Pag. Parcial', 'LBT', 0, 'C', true);
        $this->Cell($this->CelX(32), $altura, 'Observações', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(15), $altura, 'Valor Pago', 'LBRT', 1, 'C', true);
        $this->SetDefaultFill();

        $total = 0;
        foreach ($this->transacao->parcelas as $parcela) {
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(9), $altura, $parcela->crediario_id, 'LB', 0, 'C');
            $this->Cell($this->CelX(8), $altura, $parcela->sequencia . '/' . $parcela->crediario->parcelas->count(), 'LB', 0, 'C');
            $this->Cell($this->CelX(13), $altura, date('d/m/Y', strtotime($parcela->crediario->transacao->venda->data_venda)), 'LB', 0, 'C');
            $this->Cell($this->CelX(12), $altura, date('d/m/Y', strtotime($parcela->vencimento)), 'LB', 0, 'C');
            $this->Cell($this->CelX(11), $altura, $parcela->pivot->abatimento ? 'Sim' : 'Não', 'LBR', 0, 'C');
            $this->SetFont('Arial', '', $fonte * 75 / 100);
            $y = $this->GetY();
            $x = $this->getX();
            $this->MultiCell($this->CelX(32), $altura / 2, $parcela->pivot->observacao, 'T', 'L');
            $this->SetXY($x + $this->CelX(32), $y);
            $this->SetFont('Arial', '', $fonte);
            $this->Cell($this->CelX(15), $altura, number_format($parcela->pivot->valor, 2, ',', '.'), 'LBR', 1, 'R');
            $total += $parcela->pivot->valor;
        }
        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(53), $altura, '', '',0, 'R');
        $this->Cell($this->CelX(47), $altura, number_format($total, 2, ',', '.'), 'T', 1, 'R');
    }

    private function pagamentos()
    {
        $altura = 8;
        $fonte = 10;
        $fonteTitulo = $fonte * 130 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, 'Formas de pagamento', '', 1, 'L');
        $this->SetFont('Arial', 'B', $fonte);
        $this->SetDarkFill(true);
        $this->Cell($this->CelX(80), $altura, 'Descrição', 'LBT', 0, 'L', true);
        $this->Cell($this->CelX(20), $altura, 'Valor', 'LBRT', 1, 'C', true);
        $this->SetDefaultFill();

        $this->SetFont('Arial', '', $fonte);
        foreach ($this->transacao->parcelaPagamentos as $pagamento) {
            $descricao = '';
            if ($pagamento->tipo_pagamento_id == 1) {
                $descricao = $pagamento->tipoPagamento->descricao;
            } elseif ($pagamento->tipo_pagamento_id == 2) {
                $descricao = $pagamento->tipocartao == 'C' ? 'Cartão Crédito' : 'Cartão Débito' . ' ' .
                    $pagamento->bandeira->nome . ' ' . $pagamento->bandeira->banco_credito;
            }

            $this->Cell($this->CelX(80), $altura, $descricao, 'LB', 0, 'L');
            $this->Cell($this->CelX(20), $altura, number_format($pagamento->valor, 2, ',', '.'), 'LBR', 1, 'R');
        }

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, number_format($this->transacao->parcelaPagamentos->sum('valor'), 2, ',', '.'), '', 1, 'R');
    }
}
