<?php

namespace App\Pdf;

trait CaixaReciboTrait
{
    public function cabecalho($dataPagamento, $darkInverse = false)
    {
        $altura = 8;
        $fonte = 10;
        $this->SetDarkFill($darkInverse);
        $this->SetFont('Arial', 'B', $fonte * 160 / 100);
        $this->Cell($this->CelX(65), $altura, 'Brasão Center Móveis', 'LT', 0, 'C', true);
        $this->SetFont('Arial', 'B', $fonte * 130 / 100);
        $this->Cell($this->CelX(35), $altura, "Recibo Nº: {$this->transacao->id}", 'LTR', 1, 'R', true);
        $this->SetFont('Arial', '', $fonte);

        $altura = 5;
        $this->Cell($this->CelX(65), $altura, "Rua Engenheiro Argolo, 247 - Centro - Teófilo Otoni - MG - 39802-034",
            'L', 0, 'C', true);
        $this->Cell($this->CelX(35), $altura, 'Pagamento: ' . date('d/m/Y - H:i', strtotime($dataPagamento)), 'LR', 1,
            'R', true);

        $this->Cell($this->CelX(65), $altura, "Telefone: (33) 3522-3199", 'LB', 0, 'C', true);
        $this->Cell($this->CelX(35), $altura, 'Emissão: ' . date('d/m/Y - H:i'), 'LRB', 1, 'R', true);

        $this->SetDefaultFill();

        $altura = 3;
        $this->Cell($this->CelX(100), $altura, '', '', 1, 'C');
    }
}
