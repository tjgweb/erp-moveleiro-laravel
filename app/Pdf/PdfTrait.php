<?php

namespace App\Pdf;


use App\Models\Venda;

trait PdfTrait
{
    /**
     * Passar a Coleção de venda
     * @param Venda $venda
     */
    public function dadosCliente(Venda $venda)
    {
        $altura = 6;
        $alturaLabel = 5;
        $fonte = 10;
        $fonteLabel = $fonte * 80 / 100;
        $fonteTitulo = $fonte * 130 / 100;

        $this->SetFont('Arial', 'B', $fonteTitulo);
        $this->Cell($this->CelX(100), $altura, 'Dados do Cliente', 'B', 1, 'L');

        $this->SetFont('Arial', 'B', $fonteLabel);
        $this->Cell($this->CelX(75), $alturaLabel, 'Nome/Razão Social', 'L', 0, 'L');
        $this->Cell($this->CelX(25), $alturaLabel, 'CPF/CNPJ', 'LR', 1, 'L');
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(75), $altura, $venda->pessoaCliente->nome, 'LB', 0, 'L');
        $this->Cell($this->CelX(25), $altura, $venda->pessoaCliente->tipo_pessoa_id == 1 ? formatar('cpf', $venda->pessoaCliente->fisica->cpf) : formatar('cnpj', $venda->pessoaCliente->juridica->cnpj), 'LRB', 1, 'L');

        $this->SetFont('Arial', 'B', $fonteLabel);
        $this->Cell($this->CelX(75), $alturaLabel, 'Endereço', 'L', 0, 'L');
        $this->Cell($this->CelX(25), $alturaLabel, 'Bairro', 'LR', 1, 'L');
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(75), $altura, $venda->enderecoEntrega->logradouroCompleto ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->logradouroCompleto ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(25), $altura, $venda->enderecoEntrega->bairro ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->bairro ?? '', 'LRB', 1, 'L');

        $this->SetFont('Arial', 'B', $fonteLabel);
        $this->Cell($this->CelX(100), $alturaLabel, 'Referência', 'LR', 1, 'L');
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(100), $altura, $venda->enderecoEntrega->referencia ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->referencia ?? '', 'LBR', 1, 'L');

        $this->SetFont('Arial', 'B', $fonteLabel);
        $this->Cell($this->CelX(35), $alturaLabel, 'Cidade', 'L', 0, 'L');
        $this->Cell($this->CelX(10), $alturaLabel, 'UF', 'L', 0, 'L');
        $this->Cell($this->CelX(15), $alturaLabel, 'CEP', 'L', 0, 'L');
        $this->Cell($this->CelX(40), $alturaLabel, 'Telefones', 'LR', 1, 'L');
        $this->SetFont('Arial', '', $fonte);
        $this->Cell($this->CelX(35), $altura, $venda->enderecoEntrega->cidade->nome ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->cidade->nome ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(10), $altura, $venda->enderecoEntrega->estado->uf ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->estado->uf ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(15), $altura, $venda->enderecoEntrega->cep ?? $venda->pessoaCliente->enderecos()->where('principal', '=', true)->first()->cep ?? '', 'LB', 0, 'L');

        $telefones = "";
        for ($i = 0; $i < $venda->pessoaCliente->contatos->count() && $i <= 3; $i++) {
            $telefones .= "{$venda->pessoaCliente->contatos[$i]->numero} ";
        }
        $this->Cell($this->CelX(40), $altura, $telefones, 'LRB', 1, 'L');

        $altura = 3;
        $this->Cell($this->CelX(100), $altura, '', '', 1, 'C');
    }
}
