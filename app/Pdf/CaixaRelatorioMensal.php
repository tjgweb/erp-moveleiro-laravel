<?php

namespace App\Pdf;


use Carbon\Carbon;
use TJGazel\LaraFpdf\LaraFpdf;

class CaixaRelatorioMensal extends LaraFpdf
{

    public $caixas;

    public function __construct($caixas)
    {
        $this->caixas = $caixas;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Relatório Caixa Mês ', true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage('L');
        $this->Body();
    }

    public function Header()
    {
        $altura = 8;
        $this->SetDarkFill();
        $this->SetFont('Arial', 'B', '14');
        $this->Cell($this->CelX(100), $altura, 'Brasão Center Móveis', 'LTR', 1, 'C', true);
        $this->SetFont('Arial', '', '11');
        $this->Cell($this->CelX(100), $altura, "Relatório dos caixas no mês de " . ucfirst($this->caixas[0]->created_at->localeMonth) . ' ' . $this->caixas[0]->created_at->format('Y'), 'LBR', 1, 'C', true);
        $this->SetDefaultFill();
        $altura = 8;
        $this->Cell($this->CelX(100), $altura, '', '', 1, 'C');
    }

    public function Body()
    {
        $this->resumo();
    }

    public function Footer()
    {

    }

    private function resumo()
    {
        $alturaTitulo = 8;
        $alturaLinha = 8;
        $fonteTitulo = 12;
        $fonteLinha = 10;

        foreach($this->caixas as $caixa) {
            /** @var $data Carbon*/
            $data = $caixa->created_at;
            $this->SetFont('Arial', 'B', $fonteTitulo);
            $this->Cell($this->CelX(40), $alturaTitulo, ucfirst($data->localeDayOfWeek) . ' dia ' . $data->day, 'B', 0, 'L');
            $this->Cell($this->CelX(13), $alturaTitulo, 'Operador: ', 'B', 0, 'L');
            $this->Cell($this->CelX(47), $alturaTitulo, $caixa->operador->apelido, 'B', 1, 'L');

            $this->SetFont('Arial', 'B', $fonteLinha);
            $this->Cell($this->CelX(10), $alturaLinha, 'Troco inicial', '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, 'Crédito', '', 0, 'C');
            $this->Cell($this->CelX(10), $alturaLinha, 'Dinheiro', '', 0, 'C');
            $this->Cell($this->CelX(10), $alturaLinha, 'Cartão Débido', '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, 'Cartão Crédito', '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, 'Financiamento', '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, 'Crediário', '', 0, 'C');
            $this->Cell($this->CelX(10), $alturaLinha, 'Cheque', '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, 'Retirada', '', 1, 'C');

            $this->SetFont('Arial', '', $fonteLinha);
            $this->Cell($this->CelX(10), $alturaLinha, number_format($caixa->troco_inicial, 2, ',', '.'), '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->total_credito, 2, ',', '.'), '', 0, 'C');
            $this->Cell($this->CelX(10), $alturaLinha, number_format($caixa->total_dinheiro, 2, ',', '.'), '', 0,'C');
            $this->Cell($this->CelX(10), $alturaLinha, number_format($caixa->total_cartao_debito, 2, ',', '.'), '', 0,'C');
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->total_cartao_credito, 2, ',', '.'), '',0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->total_financiamento, 2, ',', '.'), '', 0,'C');
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->total_crediario, 2, ',', '.'), '', 0,'C');
            $this->Cell($this->CelX(10), $alturaLinha, number_format($caixa->total_cheque, 2, ',', '.'), '', 0, 'C');
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->total_retirada, 2, ',', '.'), '', 1,'C');

            $this->SetFont('Arial', '', $fonteTitulo);
            $this->Cell($this->CelX(88), $alturaLinha, 'Fluxo de caixa', '', 0, 'R');
            $this->SetFont('Arial', 'B', $fonteTitulo);
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->fluxo, 2, ',', '.'), '', 1, 'R');
            $this->SetFont('Arial', '', $fonteTitulo);
            $this->Cell($this->CelX(88), $alturaLinha, 'Em caixa', '', 0, 'R');
            $this->SetFont('Arial', 'B', $fonteTitulo);
            $this->Cell($this->CelX(12), $alturaLinha, number_format($caixa->em_caixa, 2, ',', '.'), '', 1, 'R');
        }
    }

}
