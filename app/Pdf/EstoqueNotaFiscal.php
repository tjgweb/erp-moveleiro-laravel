<?php

namespace App\Pdf;


use App\Models\NotaFiscal;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorPNG;
use TJGazel\LaraFpdf\LaraFpdf;

class EstoqueNotaFiscal extends LaraFpdf
{

    /**
     * @var NotaFiscal
     */
    private $nota;

    private $fontFamily = 'times';
    private $font = 8;
    private $fontSmall = 6;
    private $fontLabel = 5;
    private $altura = 4;
    private $alturaLabel = 3.25;
    private $alturaProdutos = 5;
    private $yCabecalhoFinal;

    /**
     * EstoqueNotaVenda constructor.
     * @param NotaFiscal $nota
     */
    public function __construct(NotaFiscal $nota)
    {
        $this->nota = $nota;
        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTopMargin(4);
        $this->SetLeftMargin(3);
        $this->SetRightMargin(3);
        $this->SetTitle('Nota Fiscal Nº ' . $this->nota->numero, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage();
        $this->Body();
    }

    private function getCodigoBarra($x = null, $y = null, $w = 0, $h = 0)
    {
        $generator = new BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode($this->nota->chave_acesso, $generator::TYPE_CODE_128_C, 1, 40);
        $this->Image('data:image/png;base64,' . base64_encode($barcode), $x, $y, $w, $h, 'png');
    }

    private function formatarChaveAcesso($chave)
    {
        if (strlen($chave) != 44) {
            return '';
        }

        $string = substr($chave, 0, 4);

        for ($i = 4; $i <= 44; $i += 4) {
            $string .= ' ' . substr($chave, $i, 4);
        }

        return $string;
    }

    public function formatarFretePagador($frete)
    {
        switch ($frete){
            case '1': return '1 - Emitente';
            case '2': return '2 - Dest/Rem';
            case '3': return '3 - Terceiros';
            case '9': return '9 - Sem Frete';
            default: return '';
        }
    }

    public function Header()
    {
        if ($this->nota->movimento_tipo_id == 1) {
            $this->topo();
        }
        $this->cabecalho();
    }

    public function Body()
    {
        $this->emissor();
        $this->destinatario();
        $this->dadosFatura();
        $this->imposto();
        $this->transportador();
        $this->produtos();
    }

    public function Footer()
    {
        $this->issqn();
        $this->dadosAdicionais();
    }

    private function topo()
    {
        $this->SetFont($this->fontFamily, '', $this->fontSmall);
        $this->Cell($this->CelX(75), $this->altura,
            'RECEBEMOS DE ' . mb_strtoupper($this->nota->pessoaEmissor->juridica->razao_social ?? $this->nota->pessoaEmissor->nome) . ' OS PRODUTOS DA NOTA FISCAL INDICADA AO LADO.',
            'LT', 0, 'L');
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cell($this->CelX(25), $this->altura, 'NF-e', 'LTR', 1, 'C');

        $this->SetFont($this->fontFamily, '', $this->fontSmall);
        $this->Cell($this->CelX(75), $this->altura, 'EMISSÃO: ' .
            date('d/m/Y', strtotime($this->nota->data_emissao)) .
            '     DESTINATÁRIO: ' . $this->nota->pessoaDestinatario->nome .
            '     VALOR DA NOTA: ' . number_format($this->nota->valor_total_nota, 2, ',', '.'), 'L', 0, 'L');
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cell($this->CelX(25), $this->altura, 'Nº ' . $this->nota->numero, 'LR', 1, 'C');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(25), $this->alturaLabel, 'DATA DE RECEBIMENTO', 'LT', 0, 'L');
        $this->Cell($this->CelX(50), $this->alturaLabel, 'IDENTIFICAÇÃO E ASSINATURA DO RECEBEDOR', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, '', 'LR', 1, 'L');

        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cell($this->CelX(25), $this->altura, '', 'LB', 0, 'C');
        $this->Cell($this->CelX(50), $this->altura, '', 'LB', 0, 'C');
        $this->Cell($this->CelX(25), $this->altura, 'SÉRIE ' . $this->nota->serie, 'LBR', 1, 'C');

        $this->SetFont($this->fontFamily, '', 16);
        $this->Cell($this->CelX(100), $this->altura + 2,
            '--------------------------------------------------------------------------------------------------------------',
            '', 1, 'C');
    }

    private function cabecalho()
    {
        $this->SetFont($this->fontFamily, '', 1);
        $this->Cel(100, 15, '', 'TR', 0, 'C');
        $this->Cel(0, 0, '', '', 1, 'C');
        $x = $this->GetX();
        $y = $this->GetY();
        $this->SetFont($this->fontFamily, 'B', $this->font + 3);
        $this->MultiCell($this->CelX(43), $this->altura +1,
            mb_strtoupper($this->nota->pessoaEmissor->juridica->razao_social ?? $this->nota->pessoaEmissor->nome), 'L',
            'L');
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->SetX($x);
        $this->Cel(43, $this->altura, '', 'L', 1, 'L');
        $this->SetX($x);
        $this->Cel(43, $this->altura, $this->nota->enderecoEmissor->logradouro_completo, 'L', 1, 'L');
        $this->SetX($x);
        $this->Cel(43, $this->altura, $this->nota->enderecoEmissor->bairro, 'L', 1, 'L');
        $this->SetX($x);
        $this->Cel(43, $this->altura,
            $this->nota->enderecoEmissor->cidade->nome . ' - ' . $this->nota->enderecoEmissor->estado->uf, 'L', 1, 'L');
        $this->SetX($x);
        $contato = $this->nota->pessoaEmissor->contatos->where('principal', true)->first()->numero ??
            $this->nota->pessoaEmissor->contatos->first()->numero ?? '';
        $this->Cel(43, $this->altura, $this->nota->enderecoEmissor->cep . ' - ' . $contato, 'L', 1, 'L');

        $this->SetX($x);
        $this->SetXY($this->GetX() + $this->CelX(43), $y);
        $x = $this->GetX();
        $this->SetFont($this->fontFamily, 'B', $this->font + 4);
        $this->Cel(17, $this->altura + 1, 'DANFE', 'LR', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, '', $this->font - 1);
        $this->Cel(17, $this->alturaLabel, 'DOCUMENTO AUXILIAR DA', 'LR', 1, 'C');
        $this->SetX($x);
        $this->Cel(17, $this->alturaLabel, 'NOTA FISCAL ELETRÔNICA', 'LR', 1, 'C');
        $this->SetX($x);
        $this->Cel(17, $this->alturaLabel - 2, '', 'LR', 1, 'C');
        $this->SetX($x);
        $yEntrada = $this->GetY();
        $this->Cel(10, $this->alturaLabel, '0 - ENTRADA', 'L', 1, 'L');
        $this->SetX($x);
        $this->Cel(10, $this->alturaLabel, '1 - SAÍDA', 'L', 1, 'L');
        $this->SetX($x);
        $this->SetXY($this->GetX() + $this->CelX(12), $yEntrada);
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cel(4, $this->alturaLabel * 2, $this->nota->movimento_tipo_id == 1 ? '1' : '0', 'LTRB', 0, 'C');
        $this->SetFont($this->fontFamily, 'B', $this->fontSmall);
        $this->Cel(1, $this->alturaLabel * 2, '', 'R', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cel(17, $this->altura, 'Nº ' . $this->nota->numero, 'LR', 1, 'C');
        $this->SetX($x);
        $this->Cel(17, $this->alturaLabel, 'SÉRIE ' . $this->nota->serie, 'LR', 1, 'C');
        $this->SetX($x);
        $this->Cel(17, $this->alturaLabel, 'FOLHA ' . $this->PageNo() . ' / 1', 'LRB', 1, 'C');
        $this->yCabecalhoFinal = $this->GetY();

        $this->SetX($x);
        $this->SetXY($this->GetX() + $this->CelX(17), $y + 1);
        $x = $this->GetX();
        $this->getCodigoBarra($x + 4);
        $this->SetX($x);
        $this->Cel(40, 1.25, '', 'R', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cel(40, $this->alturaLabel, 'CHAVE DE ACESSO', 'LTR', 1, 'L');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cel(40, $this->alturaLabel, $this->formatarChaveAcesso($this->nota->chave_acesso), 'LR', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, '', $this->fontSmall);
        $this->Cel(40, $this->alturaLabel + 0.75, 'CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL DA NF-e', 'LTR', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, 'B', $this->fontSmall);
        $this->Cel(40, $this->alturaLabel, 'www.nfe.fazenda.gov.br/portal', 'LR', 1, 'C');
        $this->SetX($x);
        $this->SetFont($this->fontFamily, '', $this->fontSmall);
        $this->Cel(40, $this->alturaLabel, 'OU NO SITE DA SEFAZ AUTORIZADORA.', 'LBR', 1, 'C');

        $this->SetXY(3, $this->yCabecalhoFinal + 2);
    }

    private function emissor()
    {
//        $this->SetXY(3, $this->yCabecalhoFinal + 2);
        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cel(70, $this->alturaLabel, 'NATUREZA DA OPERAÇÃO', 'LT', 0, 'L');
        $this->Cel(30, $this->alturaLabel, 'PROTOCOLO DE AUTORIZAÇÃO DE USO', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font - 1);
        $this->Cell($this->CelX(70), $this->altura, $this->nota->naturezaOperacao->descricao, 'L', 0, 'L');
        $this->SetFont($this->fontFamily, 'B', $this->font);
        $this->Cell($this->CelX(30), $this->altura, '', 'LR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(33), $this->alturaLabel, 'INSCRIÇÃO ESTADUAL', 'LT', 0, 'L');
        $this->Cell($this->CelX(34), $this->alturaLabel, 'INSC. ESTADUAL DO SUBST. TRIBUTÁRIO', 'LT', 0, 'L');
        $this->Cell($this->CelX(33), $this->alturaLabel, 'CNPJ', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(33), $this->altura, $this->nota->pessoaEmissor->juridica->inscricao_estadual ?? '',
            'LB', 0,
            'L');
        $this->Cell($this->CelX(34), $this->altura, '', 'LB', 0, 'L');
        $this->Cell($this->CelX(33), $this->altura, formatar('cnpj', $this->nota->pessoaEmissor->juridica->cnpj ?? ''),
            'LRB',
            1, 'L');
    }

    private function destinatario()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'DESTINATÁRIO / REMETENTE', '', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(65), $this->alturaLabel, 'NOME / RAZÃO SOCIAL', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'CNPJ / CPF', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'DATA DA EMISSÃO', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(65), $this->altura, $this->nota->pessoaDestinatario->nome, 'L', 0, 'L');
        $this->Cell($this->CelX(20), $this->altura, $this->nota->pessoaDestinatario->tipo_pessoa_id == 1 ?
            formatar('cpf', $this->nota->pessoaDestinatario->fisica->cpf) :
            formatar('cnpj', $this->nota->pessoaDestinatario->juridica->cnpj), 'L', 0, 'L');
        $this->Cell($this->CelX(15), $this->altura, date('d/m/Y', strtotime($this->nota->data_emissao)), 'LR', 1, 'C');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(45), $this->alturaLabel, 'ENDEREÇO', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'BAIRRO / DISTRITO', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'CEP', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'DATA DA SAÍDA / ENTRADA', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(45), $this->altura, $this->nota->enderecoDestinatario->logradouroCompleto, 'L', 0, 'L');
        $this->Cell($this->CelX(25), $this->altura, $this->nota->enderecoDestinatario->bairro, 'L', 0, 'L');
        $this->Cell($this->CelX(15), $this->altura, $this->nota->enderecoDestinatario->cep, 'LR', 0, 'L');
        $this->Cell($this->CelX(15), $this->altura, date('d/m/Y', strtotime($this->nota->data_saida)), 'LR', 1, 'C');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(35), $this->alturaLabel, 'MUNICÍPIO', 'LT', 0, 'L');
        $this->Cell($this->CelX(5), $this->alturaLabel, 'UF', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'FONE / FAX', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'INSCRIÇÃO ESTADUAL', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'HORA SAÍDA', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(35), $this->altura, $this->nota->enderecoDestinatario->cidade->nome, 'LB', 0, 'L');
        $this->Cell($this->CelX(5), $this->altura, $this->nota->enderecoDestinatario->estado->uf, 'LB', 0, 'C');
        $this->Cell($this->CelX(25), $this->altura,
            $this->nota->pessoaDestinatario->contatos->where('principal', true)->first()->numero ??
            $this->nota->pessoaDestinatario->contatos->first()->numero ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(20), $this->altura, $this->nota->pessoaDestinatario->tipo_pessoa_id == 2 ?
            $this->nota->pessoaDestinatario->juridica->inscricao_estadual : '', 'LB', 0, 'L');
        $this->Cell($this->CelX(15), $this->altura, $this->nota->hora_saida, 'LRB', 1, 'C');
    }

    private function dadosFatura()
    {
        if ($this->nota->duplicatas->count()) {
            $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
            $this->Cell($this->CelX(100), $this->alturaLabel, 'FATURA / DUPLICATAS', '', 1, 'L');

            $this->Cell($this->CelX(10), $this->alturaLabel, 'NÚMERO', 'LT', 0, 'L');
            $this->Cell($this->CelX(10), $this->alturaLabel, 'VENCIMENTO', 'T', 0, 'L');
            $this->Cell($this->CelX(10), $this->alturaLabel, 'VALOR', 'T', 0, 'L');
            $this->Cell($this->CelX(70), $this->alturaLabel, '', 'TR', 1, 'L');

            $this->SetFont($this->fontFamily, '', $this->fontSmall);
            foreach ($this->nota->duplicatas as $duplicata) {
                $this->Cell($this->CelX(10), $this->alturaLabel, $duplicata->numero, 'L', 0, 'L');
                $this->Cell($this->CelX(10), $this->alturaLabel, $duplicata->vencimento->format('d/m/Y'), '', 0, 'L');
                $this->Cell($this->CelX(10), $this->alturaLabel, number_format($duplicata->valor, 2, ',', '.'), '', 0,
                    'L');
                $this->Cell($this->CelX(70), $this->alturaLabel, '', 'R', 1, 'L');
            }
            $this->Cell($this->CelX(100), 0, '', 'LRB', 1, 'L');
        }
    }

    private function imposto()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'CÁLCULO DO IMPOSTO', '', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(20), $this->alturaLabel, 'BASE DE CÁLCULO DO ICMS', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'VALOR ICMS', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'BASE DE CÁLCULO ICMS S.T.', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'VALOR ICMS S.T.', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'VALOR TOTAL PRODUTOS', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->base_calculo_icms, 2, ',', '.'), 'L', 0,
            'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->valor_icms, 2, ',', '.'), 'L', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura,
            number_format($this->nota->base_calculo_icms_substituicao, 2, ',', '.'), 'L', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->valor_icms_substituicao, 2, ',', '.'),
            'L', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->valor_total_produtos, 2, ',', '.'), 'LR',
            1, 'R');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(13), $this->alturaLabel, 'VALOR FRETE', 'LT', 0, 'L');
        $this->Cell($this->CelX(13), $this->alturaLabel, 'VALOR SEGURO', 'LT', 0, 'L');
        $this->Cell($this->CelX(14), $this->alturaLabel, 'DESCONTO', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'OUTRAS DESP. ACESSÓRIAS', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'VALOR DO IPI', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'VALOR TOTAL DA NOTA', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(13), $this->altura, number_format($this->nota->valor_frete, 2, ',', '.'), 'LB', 0, 'R');
        $this->Cell($this->CelX(13), $this->altura, number_format($this->nota->valor_seguro, 2, ',', '.'), 'LB', 0,
            'R');
        $this->Cell($this->CelX(14), $this->altura, number_format($this->nota->desconto, 2, ',', '.'), 'LB', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->outras_despesas_acessorias, 2, ',', '.'),
            'LB', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->valor_ipi, 2, ',', '.'), 'LB', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, number_format($this->nota->valor_total_nota, 2, ',', '.'), 'LRB', 1,
            'R');
    }

    private function transportador()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'TRANSPORTADOR / VOLUMES TRANSPORTADOS', '', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(42), $this->alturaLabel, 'NOME / RAZÃO SOCIAL', 'LT', 0, 'L');
        $this->Cell($this->CelX(12), $this->alturaLabel, 'FRETE POR CONTA', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'CÓDIGO ANTT', 'LT', 0, 'L');
        $this->Cell($this->CelX(10), $this->alturaLabel, 'PLACA VEÍCULO', 'LT', 0, 'L');
        $this->Cell($this->CelX(5), $this->alturaLabel, 'UF', 'LT', 0, 'L');
        $this->Cell($this->CelX(16), $this->alturaLabel, 'CNPJ / CPF', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font - 1);
        $this->Cell($this->CelX(42), $this->altura, $this->nota->carga->transportador->pessoa->tipo_pessoa_id == 2 ?
            $this->nota->carga->transportador->pessoa->juridica->razao_social :
            $this->nota->carga->transportador->pessoa->nome, 'LB', 0, 'L');
//        $this->SetFont($this->fontFamily, '', $this->fontSmall);
        $this->Cell($this->CelX(12), $this->altura, $this->formatarFretePagador($this->nota->carga->frete_pagador) , 'LB', 0, 'C');
        $this->SetFont($this->fontFamily, '', $this->font - 1);
        $this->Cell($this->CelX(15), $this->altura, $this->nota->carga->transportador->codigo_antt, 'LB', 0, 'L');
        $this->Cell($this->CelX(10), $this->altura, $this->nota->carga->placa_veiculo, 'LB', 0, 'C');
        $this->Cell($this->CelX(5), $this->altura, '', 'LB', 0, 'C');
        $this->Cell($this->CelX(16), $this->altura, $this->nota->carga->transportador->pessoa->tipo_pessoa_id == 2 ?
            formatar('cnpj', $this->nota->carga->transportador->pessoa->juridica->cnpj ?? '') :
            formatar('cpf', $this->nota->carga->transportador->pessoa->fisica->cpf ?? ''), 'LRB', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(50), $this->alturaLabel, 'ENDEREÇO', 'LT', 0, 'L');
        $this->Cell($this->CelX(29), $this->alturaLabel, 'MUNICÍPIO', 'LT', 0, 'L');
        $this->Cell($this->CelX(5), $this->alturaLabel, 'UF', 'LT', 0, 'L');
        $this->Cell($this->CelX(16), $this->alturaLabel, 'INSCRIÇÃO ESTADUAL', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font - 1);
        $this->Cell($this->CelX(50), $this->altura,
            $this->nota->carga->transportador->pessoa->enderecos->where('principal',
                true)->first()->logradouroCompleto ??
            $this->nota->carga->transportador->pessoa->enderecos->first()->logradouroCompleto ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(29), $this->altura,
            $this->nota->carga->transportador->pessoa->enderecos->where('principal', true)->first()->cidade->nome ??
            $this->nota->carga->transportador->pessoa->enderecos->first()->cidade->nome ?? '', 'LB', 0, 'L');
        $this->Cell($this->CelX(5), $this->altura,
            $this->nota->carga->transportador->pessoa->enderecos->where('principal', true)->first()->estado->uf ??
            $this->nota->carga->transportador->pessoa->enderecos->first()->estado->uf ?? '', 'LB', 0, 'C');
        $this->Cell($this->CelX(16), $this->altura,
            $this->nota->carga->transportador->pessoa->juridica->inscricao_estadual ?? '', 'LRB', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(10), $this->alturaLabel, 'QUANTIDADE', 'LT', 0, 'L');
        $this->Cell($this->CelX(20), $this->alturaLabel, 'ESPÉCIE', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'MARCA', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'NUMERAÇÃO', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'PESO BRUTO', 'LT', 0, 'L');
        $this->Cell($this->CelX(15), $this->alturaLabel, 'PESO LÍQUIDO', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(10), $this->altura, $this->nota->carga->quantidade, 'LB', 0, 'R');
        $this->Cell($this->CelX(20), $this->altura, $this->nota->carga->especie, 'LB', 0, 'L');
        $this->Cell($this->CelX(25), $this->altura, $this->nota->carga->marca, 'LB', 0, 'L');
        $this->Cell($this->CelX(15), $this->altura, $this->nota->carga->numeracao, 'LB', 0, 'R');
        $this->Cell($this->CelX(15), $this->altura, $this->nota->carga->peso_bruto, 'LB', 0, 'R');
        $this->Cell($this->CelX(15), $this->altura, $this->nota->carga->peso_liquido, 'LRB', 1, 'R');
    }

    private function produtos()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'DADOS DOS PRODUTOS / SERVIÇOS', '', 1, 'L');

        $this->SetDarkFill(true);
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(8), $this->alturaLabel, 'CÓD.', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(34), $this->alturaLabel, 'DESCRIÇÃO DO PRODUTO / SERVIÇO', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(6), $this->alturaLabel, 'NCM', 'LT', 0, 'C', true);
//        $this->Cell($this->CelX(5), $this->alturaLabel, 'CSO', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(5), $this->alturaLabel, 'CFOP', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(2), $this->alturaLabel, 'UN.', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(4), $this->alturaLabel, 'QTD', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(7), $this->alturaLabel, 'V.UNITÁRIO', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(7), $this->alturaLabel, 'V.TOTAL', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(7), $this->alturaLabel, 'B.C.ICMS', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(7), $this->alturaLabel, 'V.ICMS', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(6.50), $this->alturaLabel, 'V.IPI', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(3.50), $this->alturaLabel, 'A.ICMS', 'LT', 0, 'C', true);
        $this->Cell($this->CelX(3), $this->alturaLabel, 'A.IPI', 'LTR', 1, 'C', true);
        $this->SetDefaultFill();

        foreach ($this->nota->movimentoProdutos as $mp) {
            $this->SetFont($this->fontFamily, '', $this->fontLabel);
            $this->Cell($this->CelX(8), $this->alturaProdutos,
                $this->nota->movimento_tipo_id == 1 ? $mp->produto_id : $mp->produto->codigo_fabrica, 'LTR', 0, 'C');
            if (strlen($mp->produto->descricao_completa) < 68) {
                $this->Cell($this->CelX(34), $this->alturaProdutos, $mp->produto->descricao_completa, 'T', 0, 'L');
            } else {
                $x = $this->GetX();
                $y = $this->GetY();
                $this->MultiCell($this->CelX(34), $this->alturaProdutos / 2, $mp->produto->descricao_completa, 'T',
                    'L');
                $this->SetXY($x + $this->CelX(34), $y);
            }
            $this->Cell($this->CelX(6), $this->alturaProdutos, $mp->produto->mercadoria->categoria->numero_ncm, 'LT', 0,
                'C');
//            $this->Cell($this->CelX(5), $this->alturaProdutos, '', 'LT', 0, 'C');
            $this->Cell($this->CelX(5), $this->alturaProdutos, $mp->naturezaOperacao->cfop, 'LT', 0, 'C');
            $this->Cell($this->CelX(2), $this->alturaProdutos, $mp->unidadeMedida->unidade, 'LT', 0, 'C');
            $this->Cell($this->CelX(4), $this->alturaProdutos, $mp->quantidade, 'LT', 0, 'C');
            $this->Cell($this->CelX(7), $this->alturaProdutos, number_format($mp->valor_unitario, 2, ',', '.'), 'LT', 0,
                'R');
            $this->Cell($this->CelX(7), $this->alturaProdutos, number_format($mp->valor_total, 2, ',', '.'), 'LT', 0,
                'R');
            $bcicms = $this->nota->movimento_tipo_id == 1 ? 0 : $mp->valor_total;
            $this->Cell($this->CelX(7), $this->alturaProdutos, number_format($bcicms, 2, ',', '.'), 'LT', 0, 'R');
            $vicms = $this->nota->movimento_tipo_id == 1 ? 0 : $mp->valor_icms;
            $this->Cell($this->CelX(7), $this->alturaProdutos, number_format($vicms, 2, ',', '.'), 'LT', 0, 'R');
            $vipi = $this->nota->movimento_tipo_id == 1 ? 0 : $mp->valor_ipi;
            $this->Cell($this->CelX(6.50), $this->alturaProdutos, number_format($vipi, 2, ',', '.'), 'LT', 0, 'R');
            $aicms = $this->nota->movimento_tipo_id == 1 ? 0 : $mp->aliquota_icms;
            $this->Cell($this->CelX(3.50), $this->alturaProdutos, $aicms, 'LT', 0, 'C');
            $aipi = $this->nota->movimento_tipo_id == 1 ? 0 : $mp->aliquota_ipi;
            $this->Cell($this->CelX(3), $this->alturaProdutos, $aipi, 'LTR', 1, 'C');
        }

        $this->Cell($this->CelX(8), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(34), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(6), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(5), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(5), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(2), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(4), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(7), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(7), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(7), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(7), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(6.50), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(3.50), 0, '', 'LT', 0, 'C');
        $this->Cell($this->CelX(3), 0, '', 'LTR', 1, 'C');
    }

    private function issqn()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'CÁLCULO DO ISSQN', '', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        $this->Cell($this->CelX(25), $this->alturaLabel, 'INSCRIÇÃO MUNICIPAL', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'VALOR DOS SERVIÇOS', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'BASE DE CÁLCULO DO ISSQN', 'LT', 0, 'L');
        $this->Cell($this->CelX(25), $this->alturaLabel, 'VALOR DO ISSQN', 'LTR', 1, 'L');

        $this->SetFont($this->fontFamily, '', $this->font);
        $this->Cell($this->CelX(25), $this->altura, '', 'LB', 0, 'L');
        $this->Cell($this->CelX(25), $this->altura, '0,00', 'LB', 0, 'R');
        $this->Cell($this->CelX(25), $this->altura, '0,00', 'LB', 0, 'R');
        $this->Cell($this->CelX(25), $this->altura, '0,00', 'LRB', 1, 'R');
    }

    private function dadosAdicionais()
    {
        $this->SetFont($this->fontFamily, 'B', $this->fontLabel);
        $this->Cell($this->CelX(100), $this->alturaLabel, 'DADOS ADICIONAIS', '', 1, 'L');

        $x = $this->GetX();
        $y = $this->GetY();
        $this->SetFont($this->fontFamily, '', $this->fontLabel);
        if (strlen($this->nota->dados_adicionais) < 2) {
            $altura = 15;
        } else {
            $altura = $this->alturaLabel - 0.75;
        }
        $this->MultiCell($this->CelX(70), $altura, $this->nota->dados_adicionais, 'LTRB', 'L');
        $yFinal = $this->GetY();

        $this->SetXY($x + $this->CelX(70), $y);
        $x = $this->GetX();
        $this->Cell($this->CelX(30), $this->alturaLabel, 'RESERVADO AO FISCO', 'LTR', 1, 'L');
        $this->SetX($x);
        $this->Cell($this->CelX(30), $yFinal - $y - $this->alturaLabel, '', 'LBR', 1, 'C');
    }
}
