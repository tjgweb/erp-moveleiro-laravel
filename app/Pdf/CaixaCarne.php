<?php

namespace App\Pdf;


use App\Models\Venda;
use Illuminate\Support\Collection;
use TJGazel\LaraFpdf\LaraFpdf;

class CaixaCarne extends LaraFpdf
{
    /** @var Venda */
    public $venda;

    /** @var Collection */
    public $parcelas;

    /**
     * CaixaReciboCrediario constructor.
     * @param array $dados
     */
    public function __construct(array $dados)
    {
        $this->venda = $dados['venda'];
        $this->parcelas = collect($dados['parcelas']);

        parent::__construct('P', 'mm', 'A4');
        $this->SetA4();
        $this->SetTitle('Carnê Nº ' . $this->venda->id, true);
        $this->SetAuthor('MidiaUai', true);
        $this->AddPage();
        $this->Body();
    }

    public function Header()
    {

    }

    public function Body()
    {
        $altura = 6;
        $font = 10;
        $maxCol1 = 34;
        $maxCol2 = 64;
        $space = 2;

        $i = 1;
        foreach ($this->parcelas as $parcela) {
            $this->SetDarkFill(true);
            $this->SetFont('Arial', 'B', $font);
            $this->Cell($this->CelX($maxCol1), $altura - 1, 'Brasão Center Móveis', 'LTR', 0, 'C', true);
            $this->Cell($this->CelX($space), $altura - 1, '', '', 0, 'C', false);
            $this->Cell($this->CelX($maxCol2), $altura - 1, 'Brasão Center Móveis', 'LTR', 1, 'C', true);
            $this->SetFont('Arial', '', $font * 75 / 100);
            $this->Cell($this->CelX($maxCol1), $altura - 3, 'Após vencimento multa de 2% e juros 0,20% ao dia.', 'LBR', 0, 'C', true);
            $this->Cell($this->CelX($space), $altura - 3, '', '', 0, 'C', false);
            $this->Cell($this->CelX($maxCol2), $altura - 3, 'Após vencimento multa de 2% mais juros de 0,20% ao dia.', 'LBR', 1, 'C', true);
            $this->SetDefaultFill();

            $this->SetFont('Arial', '', $font);
            $this->Cell($this->CelX($maxCol1), $altura, 'Código Venda: ' . $this->venda->id, 'LR', 0, 'L');
            $this->Cell($this->CelX($space), $altura, '', '', 0, 'C');
            $this->Cell($this->CelX($maxCol2 / 2), $altura, 'Código Venda: ' . $this->venda->id, 'L', 0, 'L');
            $this->Cell($this->CelX($maxCol2 / 2), $altura, 'Vencimento: ' . $parcela['vencimento'], 'R', 1, 'R');

            $this->Cell($this->CelX($maxCol1), $altura, 'Parcela: ' . $parcela['numero'] . '/' . $this->parcelas->count(), 'LR', 0, 'L');
            $this->Cell($this->CelX($space), $altura, '', '', 0, 'C');
            $this->Cell($this->CelX($maxCol2 / 2), $altura, 'Parcela: ' . $parcela['numero'] . '/' . $this->parcelas->count(), 'L', 0, 'L');
            $this->Cell($this->CelX($maxCol2 / 2), $altura, 'Valor: R$' . number_format($parcela['valor'], 2, ',', '.'), 'R', 1, 'R');

            $this->Cell($this->CelX($maxCol1), $altura, 'Vencimento: ' . $parcela['vencimento'], 'LR', 0, 'L');
            $this->Cell($this->CelX($space), $altura, '', '', 0, 'C');
            $this->Cell($this->CelX($maxCol2), $altura, "Cliente[{$this->venda->pessoaCliente->id}]: {$this->venda->pessoaCliente->nome}", 'LR', 1, 'L');

            $this->Cell($this->CelX($maxCol1), $altura + 2, 'Valor: R$' . number_format($parcela['valor'], 2, ',', '.'), 'LR', 0, 'L');
            $this->Cell($this->CelX($space), $altura + 2, '', 'R', 0, 'C');
            $x = $this->GetX();
            $y = $this->GetY();
            $end = $this->venda->enderecoEntrega->full ?? $this->venda->pessoaCliente->enderecos->where('principal', true)->first()->full ??
                $this->venda->pessoaCliente->enderecos->first()->full ?? '';
            $this->SetFont('Arial', '', $font * 85 / 100);
            $this->MultiCell($this->CelX($maxCol2), ($altura + 2) / 2, $end, '', 'L');
            $this->SetXY($x + $this->PosX($maxCol2), $y);
            $this->Cell(0, $altura + 2, '', 'R', 1, 'R');
            $this->SetFont('Arial', '', $font);

            $this->Cell($this->CelX($maxCol1), $altura + 4, 'Pago em: ___/___/______', 'LR', 0, 'L');
            $this->Cell($this->CelX($space), $altura + 4, '', 'R', 0, 'C');
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetFont('Arial', '', $font * 85 / 100);
            $this->MultiCell($this->CelX($maxCol2), ($altura + 4) / 2, "Observação: {$parcela['observacao']}", '', 'L');
            $this->SetXY($x + $this->PosX($maxCol2), $y);
            $this->Cell(0, $altura + 4, '', 'R', 1, 'R');
            $this->SetFont('Arial', '', $font);

            $this->Cell($this->CelX($maxCol1), 0, '', 'B', 0, 'C');
            $this->Cell($this->CelX($space), 0, '', '', 0, 'C');
            $this->Cell($this->CelX($maxCol2), 0, '', 'B', 1, 'C');

            if ($i % 5 == 0) {
                $this->AddPage();
            } else {
                $this->Cell($this->CelX(100), $altura + 2, '------------------------------------------------------------------------------------------------------------------------------------------------------------------', '', 1, 'C');
            }
            $i++;
        }
    }

    public function Footer()
    {

    }
}
