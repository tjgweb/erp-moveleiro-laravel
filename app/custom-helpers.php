<?php

if (!function_exists('redirectUserType')) {
    /**
     * Retorna a rota do dashboard de acordo com o tipo de usuário
     * @param $user
     * @return string
     */
    function redirectUserType($user)
    {
        foreach (getRoutes() as $route) {
            if ($user->grupo->dashboard == $route->action['uses']) {
                return $route->uri;
            }
        }

        \Illuminate\Support\Facades\Auth::logout();
        toastr()->warning('Você não tem permissão de acesso');
        return route('login');
    }
}

if (!function_exists('getRoutes')) {
    /**
     * Retorna array com todas as rotas registradas que tem controlladores.
     * @return \Illuminate\Support\Collection
     */
    function getRoutes()
    {
        return collect(\Illuminate\Support\Facades\Route::getRoutes()->getIterator());
    }
}

if (!function_exists('getRoutesActionName')) {
    /**
     * Retorna array com todas as rotas registradas que tem controlladores.
     * @return \Illuminate\Support\Collection
     */
    function getRoutesActionName()
    {
        return getRoutes()->map(function (\Illuminate\Routing\Route $route) {
            return $route->getActionName();
        })->filter(function ($actionName) {
            if (strstr($actionName, '@')) {
                $controllerMethods = explode('@', $actionName);
                $rc = new \ReflectionClass($controllerMethods[0]);
                return strstr($rc->getMethod($controllerMethods[1])->getDocComment(), "@permissionName('");
            }
            return false;
        });
    }
}

if (!function_exists('getMapPermissions')) {
    /**
     * Retorna array com as permissões mapeadas pelo sistema através dos comentários nos controladores
     * @return \Illuminate\Support\Collection
     */
    function getMapPermissions()
    {
        return getRoutesActionName()->map(function ($routeActionName) {
            $controllerMethods = explode('@', $routeActionName);

            $rc = new \ReflectionClass($controllerMethods[0]);

            $method = strstr($rc->getMethod($controllerMethods[1])->getDocComment(), "@permissionName('");
            $method = strstr($method, "')", true);
            $method = (explode("('", $method))[1] ?? $controllerMethods[1];

            $controller = strstr($rc->getDocComment(), "@permissionGroup('");
            $controller = strstr($controller, "')", true);
            $controller = (explode("('", $controller))[1] ?? $controllerMethods[0];

            if (!$dashboard = strstr($rc->getMethod($controllerMethods[1])->getDocComment(),
                "@permissionDashboard('")) {
                $dashboard = null;
            } else {
                $dashboard = strstr($dashboard, "')", true);
                $dashboard = (explode("('", $dashboard))[1] ?? $controllerMethods[1];
            }

            return [
                'action'     => $routeActionName,
                'method'     => $method,
                'controller' => $controller,
                'dashboard'  => $dashboard
            ];
        });
    }
}

if (!function_exists('getMapDashboards')) {

    /**
     * @return \Illuminate\Support\Collection
     */
    function getMapDashboards()
    {
        return getMapPermissions()->filter(function ($permission, $key) {
            if ($permission['dashboard'] != null) {
                return true;
            }
            return false;
        });
    }
}

if (!function_exists('constraintViolationMessage')) {
    /**
     * Retorna mensagem padrão contraint violation
     * @return array
     */
    function constraintViolationMessage()
    {
        return ['error' => 'Você não pode excluir dados vinculados ao outra entidade.'];
    }
}

if (!function_exists('validaCpf')) {
    /**
     * @param String $cpf
     * @return bool
     */
    function validaCpf(String $cpf)
    {
        if (empty($cpf)) {
            return false;
        }
        $cpf = str_replace(['.', '-', '_', ' '], ['', '', '', ''], $cpf);
        $cpf = preg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) {
            return false;
        }

        if ($cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' ||
            $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' ||
            $cpf == '88888888888' || $cpf == '99999999999') {
            return false;
        }

        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

if (!function_exists('formatar')) {
    function formatar($tipo, $string, $size = 10)
    {
        $string = preg_replace("[^0-9]", "", $string);

        switch ($tipo) {
            case 'fone':
                if ($size === 10) {
                    $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 4)
                        . '-' . substr($tipo, 6);
                } else {
                    if ($size === 11) {
                        $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 5)
                            . '-' . substr($tipo, 7);
                    }
                }
                break;
            case 'cep':
                $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
                break;
            case 'cpf':
                $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) .
                    '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
                break;
            case 'cnpj':
                $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
                    '.' . substr($string, 5, 3) . '/' .
                    substr($string, 8, 4) . '-' . substr($string, 12, 2);
                break;
            case 'rg':
                $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
                    '.' . substr($string, 5, 3);
                break;
            default:
                $string = 'Definir um tipo válido (fone, cep, cpf, cnpj, rg)';
                break;
        }
        return $string;
    }
}

if (!function_exists('formatCodProduto')) {
    function formatCodProduto($cod)
    {
        return str_pad($cod, 6, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('formatCodNota')) {
    function formatCodNota($cod)
    {
        return str_pad($cod, 6, '0', STR_PAD_LEFT);
    }
}
