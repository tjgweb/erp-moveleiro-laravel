<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Credito
 *
 * @property-read \App\Models\Caixa $caixa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $caixa_id
 * @property float $valor
 * @property string $descricao
 * @property string $hora
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito whereCaixaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Credito whereValor($value)
 */
class Credito extends Model
{
    protected $table = 'credito';

    public $timestamps = false;

    protected $fillable = ['caixa_id', 'valor', 'descricao', 'hora', 'delete_action'];

    public function caixa()
    {
        return $this->belongsTo(Caixa::class, 'caixa_id', 'id');
    }

}
