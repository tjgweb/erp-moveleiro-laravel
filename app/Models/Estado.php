<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Estado
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AliquotaIcms[] $aliquotaIcmsDestino
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AliquotaIcms[] $aliquotaIcmsOrigem
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cidade[] $cidades
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Endereco[] $enderecos
 * @property-read \App\Models\Pais $pais
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $pais_id
 * @property string $nome
 * @property string $uf
 * @property float|null $aliquota_icms_entrada float
 * @property float|null $aliquota_icms_saida
 * @property float|null $aliquota_icms_entrada_cpf
 * @property float|null $aliquota_icms_saida_cpf
 * @property string|null $inscricao_substituicao_tributaria
 * @property int|null $uf_ibge
 * @property float|null $aliquota_pobreza
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereAliquotaIcmsEntrada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereAliquotaIcmsEntradaCpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereAliquotaIcmsSaida($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereAliquotaIcmsSaidaCpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereAliquotaPobreza($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereInscricaoSubstituicaoTributaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado wherePaisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereUf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Estado whereUfIbge($value)
 */
class Estado extends Model
{
    protected $table = 'estado';

    public $timestamps = false;

    protected $fillable = ['nome', 'uf', 'aliquota_icms_entrada', 'aliquota_icms_saida',
        'aliquota_icms_entrada_cpf', 'aliquota_icms_saida_cpf', 'inscricao_substituicao_tributaria', 'codigo_ibge',
        'aliquota_pobreza', 'pais_id'];

    public function pais()
    {
        return $this->belongsTo(Pais::class, 'pais_id', 'id');
    }

    public function cidades()
    {
        return $this->hasMany(Cidade::class, 'estado_id', 'id');
    }

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'estado_id', 'id');
    }

    public function aliquotaIcmsOrigem()
    {
        return $this->hasMany(AliquotaIcms::class, 'origem_id', 'id');
    }

    public function aliquotaIcmsDestino()
    {
        return $this->hasMany(AliquotaIcms::class, 'destino_id', 'id');
    }
}
