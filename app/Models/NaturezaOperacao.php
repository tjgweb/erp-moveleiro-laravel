<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\NaturezaOperacao
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notaFiscais
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $cfop
 * @property string $descricao Tipo
 * @property string|null $ind_nfe + acrescenta estoque
 * @property string|null $ind_comunica
 * @property string|null $ind_transp
 * @property string|null $ind_devol
 * @property string|null $altera_estoque
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereAlteraEstoque($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereCfop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereIndComunica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereIndDevol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereIndNfe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NaturezaOperacao whereIndTransp($value)
 */
class NaturezaOperacao extends Model
{
    protected $table = 'natureza_operacao';

    public $timestamps = false;

    protected $fillable = ['cfop', 'descricao', 'ind_nfe', 'ind_comunica', 'ind_transp', 'ind_devol',
        'altera_estoque'];

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'cfop_id', 'id');
    }

    public function notaFiscais()
    {
        return $this->hasMany(NotaFiscal::class, 'natureza_operacao_id', 'id');
    }
}
