<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Juridica
 *
 * @property-read \App\Models\Pessoa $pessoa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica query()
 * @mixin \Eloquent
 * @property int $pessoa_id
 * @property string|null $razao_social
 * @property string $cnpj
 * @property string|null $inscricao_estadual
 * @property string|null $inscricao_municipal
 * @property string|null $nome_contato
 * @property string|null $endereco_eletronico
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereCnpj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereEnderecoEletronico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereInscricaoEstadual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereInscricaoMunicipal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereNomeContato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Juridica whereRazaoSocial($value)
 */
class Juridica extends Model
{
    protected $table = 'juridica';

    public $primaryKey = 'pessoa_id';

    public $timestamps = false;

    protected $fillable = ['razao_social', 'cnpj', 'inscricao_estadual', 'inscricao_municipal', 'nome_contato',
        'endereco_eletronico', 'pessoa_id'];

   /* public function getCnpjAttribute($value)
    {
        return formatar('cnpj', $value, strlen($value));
    }*/

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }
}
