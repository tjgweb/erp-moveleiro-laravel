<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\AliquotaIcms
 *
 * @property-read \App\Models\Estado $estadoDestino
 * @property-read \App\Models\Estado $estadoOrigem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $origem_id
 * @property int $destino_id
 * @property float $aliquota
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms whereAliquota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms whereDestinoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AliquotaIcms whereOrigemId($value)
 */
class AliquotaIcms extends Model
{
    protected $table = 'aliquota_icms';

    public $timestamps = false;

    protected $fillable = ['origem_id', 'destino_id', 'aliquota'];

    public function estadoOrigem()
    {
        return $this->belongsTo(Estado::class, 'origem_id', 'id');
    }

    public function estadoDestino()
    {
        return $this->belongsTo(Estado::class, 'destino_id', 'id');
    }

}
