<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Custo
 *
 * @property-read \App\Models\Produto $produto
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo query()
 * @mixin \Eloquent
 * @property int $produto_id
 * @property float|null $custo_medio
 * @property float|null $custo_atual
 * @property \Illuminate\Support\Carbon|null $data_atualizacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo whereCustoAtual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo whereCustoMedio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo whereDataAtualizacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Custo whereProdutoId($value)
 */
class Custo extends Model
{
    protected $table = 'custo';

    public $primaryKey = 'produto_id';

    public $timestamps = false;

    public $incrementing = false;

    public function getDates()
    {
        return ['data_atualizacao'];
    }

    protected $fillable = ['produto_id', 'custo_medio', 'custo_atual', 'data_atualizacao'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }
}
