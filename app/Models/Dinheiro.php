<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Dinheiro
 *
 * @property-read \App\Models\Transacao $transacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $transacao_id
 * @property float $valor_recebido
 * @property string|null $observacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro whereObservacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro whereTransacaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dinheiro whereValorRecebido($value)
 */
class Dinheiro extends Model
{
    protected $table = 'dinheiro';

    public $timestamps = false;

    protected $fillable = ['observacao', 'valor_recebido', 'transacao_id'];

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }
}
