<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Loja
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $entradas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $saidas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Loja newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Loja newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Loja query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $descricao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Loja whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Loja whereId($value)
 */
class Loja extends Model
{

    protected $table = 'loja';

    public $timestamps = false;

    protected $fillable = [
        'descricao', 'razao_social', 'cnpj', 'inscricao_estadual', 'inscricao_municipal', 'token_ibpt', 'token_sefaz',
        'token_sefaz_id', 'certificado', 'senha_certificado', 'cep', 'logradouro', 'numero', 'complemento', 'bairro',
        'cidade_id', 'telefone'
    ];

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = str_replace([' ', '.', '-', '/'], ['', '', '', ''], $value);
    }

    public function setInscricaoEstadualAttribute($value)
    {
        $this->attributes['inscricao_estadual'] = str_replace([' ', '.', '-', '/'], ['', '', '', ''], $value);
    }

    public function setInscricaoMunicipalAttribute($value)
    {
        $this->attributes['inscricao_municipal'] = str_replace([' ', '.', '-', '/'], ['', '', '', ''], $value);
    }

    public function setCepAttribute($value)
    {
        $this->attributes['cep'] = str_replace([' ', '.', '-', '/'], ['', '', '', ''], $value);
    }

    public function entradas()
    {
        return $this->hasMany(MovimentoProduto::class, 'loja_entrada_id', 'id');
    }

    public function saidas()
    {
        return $this->hasMany(MovimentoProduto::class, 'loja_saida_id', 'id');
    }

    public function notasEmitidas()
    {
        return $this->hasMany(NotaFiscal::class, 'loja_emissor_id');
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class, 'cidade_id');
    }
}
