<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Categoria
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Mercadoria[] $mercadorias
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria query()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $numero_ncm
 * @property string $descricao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereNumeroNcm($value)
 */
class Categoria extends Model
{
    protected $table = 'categoria';

    public $timestamps = false;

    protected $fillable = ['numero_ncm', 'descricao'];

    public function mercadorias()
    {
        return $this->hasMany(Mercadoria::class, 'categoria_id', 'id');
    }
}
