<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Contato
 *
 * @property-read \App\Models\Pessoa $pessoa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $pessoa_id
 * @property string|null $descricao Descrição do telefone quanto a celular, operadora, residencial, recado... etc. Ou setores de Empresas jurídicas vendas, financeiro, jurudico etc.
 * @property string|null $responsavel Quando pessoa jurídica responsável pelo setor da descrição.
 * @property string|null $numero
 * @property int|null $ramal
 * @property int|null $principal Marca 1 para o telefone principal
 * @property int|null $status Marcar se o responsável ainda responde pelo seto
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato wherePrincipal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereRamal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereResponsavel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contato whereStatus($value)
 */
class Contato extends Model
{
    protected $table = 'contato';

    public $timestamps = false;

    protected $fillable = ['descricao', 'responsavel', 'numero', 'ramal', 'principal', 'status', 'pessoa_id'];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }
}
