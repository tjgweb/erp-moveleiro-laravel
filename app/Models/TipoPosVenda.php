<?php

namespace App\MOdels;

use Illuminate\Database\Eloquent\Model;


/**
 * App\MOdels\TipoPosVenda
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MOdels\PosVenda[] $vendas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoPosVenda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoPosVenda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoPosVenda query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoPosVenda whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoPosVenda whereId($value)
 */
class TipoPosVenda extends Model
{
    protected $table = 'tipo_pos_venda';

    public $timestamps = false;

    protected $fillable = ['descricao'];

    public function vendas()
    {
        return $this->hasMany(PosVenda::class, 'tipo_pos_venda_id', 'id');
    }

}
