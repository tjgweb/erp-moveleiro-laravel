<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Cidade
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Endereco[] $enderecos
 * @property-read \App\Models\Estado $estado
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property int $estado_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade whereEstadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cidade whereNome($value)
 */
class Cidade extends Model
{
    protected $table = 'cidade';

    public $timestamps = false;

    protected $fillable = ['nome', 'codigo_ibge'];

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id', 'id');
    }

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'cidade_id', 'id');
    }
}
