<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Permissao
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grupo[] $grupos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $action
 * @property string $method
 * @property string $controller
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao whereController($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permissao whereMethod($value)
 */
class Permissao extends Model
{
    protected $table = 'permissao';

    public $timestamps = false;

    protected $fillable = ['action', 'method', 'controller'];

    public function grupos()
    {
        return $this->belongsToMany(Grupo::class, 'permissao_grupo', 'permissao_id', 'grupo_id');
    }
}
