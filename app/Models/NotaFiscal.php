<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\NotaFiscal
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ajuste[] $ajuste
 * @property-read \App\Models\Carga $carga
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Duplicata[] $duplicatas
 * @property-read \App\Models\Endereco $enderecoDestinatario
 * @property-read \App\Models\Endereco $enderecoEmissor
 * @property mixed $data_emissao
 * @property-read mixed $data_saida
 * @property mixed $hora_saida
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @property-read \App\Models\MovimentoTipo $movimentoTipo
 * @property-read \App\Models\NaturezaOperacao $naturezaOperacao
 * @property-read \App\Models\Pessoa $pessoaDestinatario
 * @property-read \App\Models\Pessoa $pessoaEmissor
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NotaFiscal onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NotaFiscal withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NotaFiscal withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $natureza_operacao_id
 * @property int $movimento_tipo_id
 * @property int $emissor_id
 * @property int $endereco_emissor_id
 * @property int $destinatario_id
 * @property int $endereco_destinatario_id
 * @property string|null $numero
 * @property string|null $serie
 * @property string|null $chave_acesso
 * @property string|null $dados_adicionais
 * @property float|null $base_calculo_icms
 * @property float|null $valor_icms
 * @property float|null $base_calculo_icms_substituicao
 * @property float|null $valor_icms_substituicao
 * @property float|null $valor_pis
 * @property float|null $valor_cofins
 * @property float|null $valor_total_produtos
 * @property float|null $valor_frete
 * @property float|null $valor_seguro
 * @property float|null $desconto
 * @property float|null $outras_despesas_acessorias
 * @property float|null $valor_ipi
 * @property float|null $valor_total_nota
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereBaseCalculoIcms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereBaseCalculoIcmsSubstituicao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereChaveAcesso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDadosAdicionais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDataEmissao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDataSaida($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDesconto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereDestinatarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereEmissorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereEnderecoDestinatarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereEnderecoEmissorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereHoraSaida($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereMovimentoTipoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereNaturezaOperacaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereOutrasDespesasAcessorias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereSerie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorCofins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorFrete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorIcms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorIcmsSubstituicao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorIpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorPis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorSeguro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorTotalNota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotaFiscal whereValorTotalProdutos($value)
 */
class NotaFiscal extends Model
{
    use SoftDeletes;

    protected $table = 'nota_fiscal';

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    protected $fillable = [
        'numero', 'serie', 'chave_acesso', 'dados_adicionais', 'data_emissao', 'data_saida', 'modelo',
        'hora_saida', 'base_calculo_icms', 'valor_icms', 'base_calculo_icms_substituicao', 'valor_icms_substituicao',
        'valor_pis', 'valor_cofins', 'valor_total_produtos', 'valor_frete', 'valor_seguro', 'desconto',
        'outras_despesas_acessorias', 'valor_ipi', 'valor_total_nota', 'endereco_emissor_id', 'endereco_destinatario_id',
        'movimento_tipo_id', 'natureza_operacao_id', 'loja_emissor_id', 'emissor_id', 'destinatario_id', 'lote_nfe_id',
        'status', 'xml'
    ];

    public function setHoraSaidaAttribute($value)
    {
        if ($value == '' || is_null($value)) {
            $this->attributes['hora_saida'] = date('H:i');
        }
        $this->attributes['hora_saida'] = date('H:i', strtotime($value));
    }

    public function getHoraSaidaAttribute($value)
    {
        return date('H:i', strtotime($value));
    }

    public function getDataEmissaoAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function getDataSaidaAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function enderecoEmissor()
    {
        return $this->belongsTo(Endereco::class, 'endereco_emissor_id', 'id');
    }

    public function enderecoDestinatario()
    {
        return $this->belongsTo(Endereco::class, 'endereco_destinatario_id', 'id');
    }

    public function movimentoTipo()
    {
        return $this->belongsTo(MovimentoTipo::class, 'movimento_tipo_id', 'id');
    }

    public function naturezaOperacao()
    {
        return $this->belongsTo(NaturezaOperacao::class, 'natureza_operacao_id', 'id');
    }

    public function lojaEmissor()
    {
        return $this->belongsTo(Loja::class, 'loja_emissor_id', 'id');
    }

    public function pessoaEmissor()
    {
        return $this->belongsTo(Pessoa::class, 'emissor_id', 'id');
    }

    public function pessoaDestinatario()
    {
        return $this->belongsTo(Pessoa::class, 'destinatario_id', 'id');
    }

    public function carga()
    {
        return $this->hasOne(Carga::class, 'nota_fiscal_id', 'id');
    }

    public function duplicatas()
    {
        return $this->hasMany(Duplicata::class, 'nota_fiscal_id', 'id');
    }

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'nota_fiscal_id', 'id');
    }

    public function ajuste()
    {
        return $this->hasMany(Ajuste::class, 'nota_fiscal_id', 'id');
    }

    public function loteNfe()
    {
        return $this->belongsTo(LoteNfe::class, 'lote_nfe_id');
    }
}
