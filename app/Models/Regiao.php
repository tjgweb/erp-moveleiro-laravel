<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Regiao
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Endereco[] $enderecos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regiao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regiao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regiao query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $regiao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regiao whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regiao whereRegiao($value)
 */
class Regiao extends Model
{
    protected $table = 'regiao';

    public $timestamps = false;

    protected $fillable = ['regiao'];

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'regiao_id', 'id');
    }
}
