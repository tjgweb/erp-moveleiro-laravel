<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Marca
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Produto[] $produtos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereNome($value)
 */
class Marca extends Model
{
    protected $table = 'marca';

    public $timestamps = false;

    protected $fillable = ['nome'];

    public function produtos()
    {
        return $this->hasMany(Produto::class, 'marca_id', 'id');
    }
}
