<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Grupo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permissao[] $permissoes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $dashboard
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo whereDashboard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Grupo whereNome($value)
 */
class Grupo extends Model
{
    protected $table = 'grupo';

    public $timestamps = false;

    protected $fillable = ['nome', 'descricao', 'dashboard'];

    public function users()
    {
        return $this->hasMany(User::class, 'grupo_id', 'id');
    }

    public function permissoes()
    {
        return $this->belongsToMany(Permissao::class, 'permissao_grupo', 'grupo_id', 'permissao_id');
    }
}
