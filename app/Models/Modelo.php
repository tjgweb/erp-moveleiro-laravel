<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Modelo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modelo[] $derivados
 * @property-read \App\Models\Modelo $pai
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property string|null $portas
 * @property string|null $gavetas
 * @property int|null $modelo_pai_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereGavetas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereModeloPaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo wherePortas($value)
 */
class Modelo extends Model
{
    protected $table = 'modelo';

    public $timestamps = false;

    protected $fillable = ['nome', 'portas', 'gavetas', 'modelo_pai_id'];

    public function pai()
    {
        return $this->belongsTo(Modelo::class, 'modelo_pai_id', 'id');
    }

    public function derivados()
    {
        return $this->hasMany(Modelo::class, 'modelo_pai_id', 'id');
    }
}
