<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transacao
 *
 * @property-read \App\Models\Caixa $caixa
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cartao[] $cartoes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Crediario[] $crediarios
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dinheiro[] $dinheiros
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Financiamento[] $financiamentos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParcelaPagamento[] $parcelaPagamentos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Parcela[] $parcelas
 * @property-read \App\Models\Venda $venda
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $caixa_id
 * @property int|null $venda_id
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao whereCaixaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transacao whereVendaId($value)
 */
class Transacao extends Model
{
    protected $table = 'transacao';

    public const UPDATED_AT = null;

    protected $dates = ['created_at'];

    protected $fillable = ['caixa_id', 'venda_id', 'vencimento_id', 'created_at'];

    public function caixa()
    {
        return $this->belongsTo(Caixa::class, 'caixa_id', 'id');
    }

    public function venda()
    {
        return $this->belongsTo(Venda::class, 'venda_id', 'id');
    }

    public function dinheiros()
    {
        return $this->hasMany(Dinheiro::class, 'transacao_id', 'id');
    }

    public function cartoes()
    {
        return $this->hasMany(Cartao::class, 'transacao_id', 'id');
    }

    public function financiamentos()
    {
        return $this->hasMany(Financiamento::class, 'transacao_id', 'id');
    }

    public function crediarios()
    {
        return $this->hasMany(Crediario::class, 'transacao_id', 'id');
    }

    public function parcelas()
    {
        return $this->belongsToMany(Parcela::class, 'parcela_transacao', 'transacao_id', 'parcela_id')
            ->withPivot(['abatimento', 'valor', 'observacao']);
    }

    public function parcelaPagamentos()
    {
        return $this->hasMany(ParcelaPagamento::class, 'transacao_id', 'id');
    }

    public function predatados()
    {
        return $this->hasMany(Predatado::class);
    }

    public function cheques()
    {
        return $this->belongsToMany(Cheque::class, 'cheques_transacao', 'transacao_id', 'cheques_id')
            ->withPivot(['tipo_processamento']);
    }

    public function vencimento()
    {
        return $this->hasOne(Vencimento::class, 'transacao_id', 'id');
    }
}
