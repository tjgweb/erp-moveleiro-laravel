<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Carga
 *
 * @property-read \App\Models\NotaFiscal $notaFiscal
 * @property-read \App\Models\Transportador $transportador
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Carga onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Carga withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Carga withoutTrashed()
 * @mixin \Eloquent
 * @property int $nota_fiscal_id
 * @property int $transportador_id
 * @property int $quantidade
 * @property string|null $especie
 * @property string|null $marca
 * @property string|null $numeracao
 * @property float $peso_bruto
 * @property float $peso_liquido
 * @property string $placa_veiculo
 * @property string|null $frete_pagador
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereEspecie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereFretePagador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereMarca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereNotaFiscalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereNumeracao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga wherePesoBruto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga wherePesoLiquido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga wherePlacaVeiculo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereQuantidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carga whereTransportadorId($value)
 */
class Carga extends Model
{
    use SoftDeletes;

    protected $table = 'carga';

    public $primaryKey = 'nota_fiscal_id';

    public $timestamps = false;

    public $incrementing = false;

    protected $dates = ['deleted_at'];

    protected $fillable = ['nota_fiscal_id', 'transportador_id', 'quantidade', 'especie', 'marca', 'numeracao', 'peso_bruto', 'peso_liquido',
        'placa_veiculo', 'frete_pagador'];

    public function notaFiscal()
    {
        return $this->belongsTo(NotaFiscal::class, 'nota_fiscal_id', 'id');
    }

    public function transportador()
    {
        return $this->belongsTo(Transportador::class, 'transportador_id', 'pessoa_id');
    }
}
