<?php

namespace App\MOdels;

use Illuminate\Database\Eloquent\Model;


/**
 * App\MOdels\TipoVenda
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venda[] $vendas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoVenda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoVenda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoVenda query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoVenda whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\TipoVenda whereId($value)
 */
class TipoVenda extends Model
{
    protected $table = 'tipo_venda';

    public $timestamps = false;

    protected $fillable = ['descricao'];

    public function vendas()
    {
        return $this->hasMany(Venda::class, 'tipo_venda_id', 'id');
    }
}
