<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Financiamento
 *
 * @property-read \App\Models\Financeira $financeira
 * @property-read mixed $total
 * @property-read \App\Models\Transacao $transacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $transacao_id
 * @property int $financeira_id
 * @property int $quantidade_parcelas
 * @property float $valor_parcelas
 * @property string|null $protocolo
 * @property string|null $contrato
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereFinanceiraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereProtocolo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereQuantidadeParcelas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereTransacaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financiamento whereValorParcelas($value)
 */
class Financiamento extends Model
{
    protected $table = 'financiamento';

    public $timestamps = false;

    protected $fillable = ['transacao_id', 'financeira_id', 'quantidade_parcelas', 'valor_parcelas', 'protocolo', 'contrato'];

    protected $appends = ['total'];

    public function getTotalAttribute()
    {
        $this->attributes['total'] = round($this->valor_parcelas * $this->quantidade_parcelas, 2);
        return round($this->attributes['total'], 2);
    }

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }

    public function financeira()
    {
        return $this->belongsTo(Financeira::class, 'financeira_id', 'id');
    }
}
