<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Predatado extends Model
{
    protected $table = 'predatado';

    public $timestamps = false;

    protected $fillable = ['transacao_id', 'observacao', 'status'];

    public function transacao()
    {
        return $this->belongsTo(Transacao::class);
    }

    public function cheques()
    {
        return $this->hasMany(Cheque::class);
    }
}
