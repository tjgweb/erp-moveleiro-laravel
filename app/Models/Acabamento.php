<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Acabamento
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $nome
 * @property string|null $sigla
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Acabamento whereSigla($value)
 */
class Acabamento extends Model
{
    protected $table = 'acabamento';

    public $timestamps = false;

    protected $fillable = ['nome', 'sigla'];


}
