<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Fisica
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Caixa[] $caixas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Caixa[] $gerentes
 * @property mixed $data_admissao
 * @property mixed $nascimento
 * @property-read \App\Models\Pessoa $pessoa
 * @property-read \App\Models\TipoIdentidade $tipoIdentidade
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica query()
 * @mixin \Eloquent
 * @property int $pessoa_id
 * @property string $cpf
 * @property string|null $sexo
 * @property string|null $identidade
 * @property int|null $tipo_identidade_id
 * @property string|null $nacionalidade B: Brasileiro, E: estrangeiro, N: naturalizado
 * @property float|null $renda_mensal
 * @property string|null $estado_civil C - casado, S solteiro
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereCpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereDataAdmissao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereEstadoCivil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereIdentidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereNacionalidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereNascimento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereRendaMensal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereSexo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fisica whereTipoIdentidadeId($value)
 */
class Fisica extends Model
{
    protected $table = 'fisica';

    public $primaryKey = 'pessoa_id';

    public $timestamps = false;

    public $incrementing = false;

    public function getDates()
    {
        return ['nascimento', 'data_admissao'];
    }

    protected $fillable = ['pessoa_id', 'cpf', 'sexo', 'identidade', 'nascimento', 'nacionalidade', 'data_admissao',
        'renda_mensal', 'estado_civil', 'pessoa_id', 'tipo_identidade_id'
    ];

    public function getNascimentoAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function setNascimentoAttribute($value)
    {
        if ($value == '' || is_null($value)) {
            $this->attributes['nascimento'] = null;
        }
        $this->attributes['nascimento'] = date('Y-m-d', strtotime($value));
    }

    public function getDataAdmissaoAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function setDataAdmissaoAttribute($value)
    {
        if ($value == '' || is_null($value)) {
            $this->attributes['data_admissao'] = null;
        }

        $this->attributes['data_admissao'] = date('Y-m-d', strtotime($value));
    }

    /*public function getCpfAttribute($value)
    {
        return formatar('cpf', $value, strlen($value));
    }*/

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }

    public function tipoIdentidade()
    {
        return $this->belongsTo(TipoIdentidade::class, 'tipo_identidade_id', 'id');
    }

    public function caixas()
    {
        return $this->hasMany(Caixa::class, 'caixa_id', 'pessoa_id');
    }

    public function gerentes()
    {
        return $this->hasMany(Caixa::class, 'gerente_id', 'pessoa_id');
    }

}
