<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Cor
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cor[] $derivados
 * @property-read \App\Models\Cor $pai
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property int|null $cor_pai_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor whereCorPaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cor whereNome($value)
 */
class Cor extends Model
{
    protected $table = 'cor';

    public $timestamps = false;

    protected $fillable = ['nome', 'cor_pai_id'];

    public function pai()
    {
        return $this->belongsTo(Cor::class, 'cor_pai_id', 'id');
    }

    public function derivados()
    {
        return $this->hasMany(Cor::class, 'cor_pai_id', 'id');
    }
}
