<?php

namespace App\Models;

use App\Notifications\ResetarSenha;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * App\Models\User
 *
 * @property-read mixed $gravatar
 * @property-read \App\Models\Grupo $grupo
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\Pessoa $pessoa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $grupo_id
 * @property int|null $pessoa_id
 * @property string $apelido
 * @property string $email
 * @property string $password
 * @property int $status Se esta pessoa está ativa para o sistema,
 * @property string|null $remember_token
 * @property string|null $email_verified_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereApelido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereGrupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    public $timestamps = false;

    protected $fillable = [
        'apelido', 'password', 'email', 'status', 'remember_token', 'email_verified_at', 'grupo_id', 'pessoa_id'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $appends = ['gravatar'];

    public function getGravatarAttribute()
    {
        return $this->attributes['gravatar'] = 'https://www.gravatar.com/avatar/' . md5($this->email);
    }

    /* Mensagem de E-mail personalizada para recuperar senha */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetarSenha($token));
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class, 'grupo_id', 'id');
    }

    public function hasPermission($permissionAction)
    {
        foreach ($this->grupo->permissoes as $permissao){
            if($permissao->action == $permissionAction){
                return true;
            }
        }

        return false;
    }
}
