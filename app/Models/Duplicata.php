<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Duplicata
 *
 * @property-read \App\Models\NotaFiscal $notaFiscal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $numero
 * @property \Illuminate\Support\Carbon $vencimento
 * @property float $valor
 * @property int $nota_fiscal_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata whereNotaFiscalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata whereValor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Duplicata whereVencimento($value)
 */
class Duplicata extends Model
{
    use SoftDeletes;

    protected $table = 'duplicata';

    public $timestamps = false;

    protected $dates = ['deleted_at', 'vencimento'];

    /*public function getDates()
    {
        return ['vencimento'];
    }*/

    protected $fillable = ['numero', 'vencimento', 'valor', 'nota_fiscal_id', 'liquidada'];

    public function notaFiscal()
    {
        return $this->belongsTo(NotaFiscal::class, 'nota_fiscal_id', 'id');
    }
}
