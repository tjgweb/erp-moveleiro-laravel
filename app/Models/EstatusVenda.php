<?php

namespace App\MOdels;

use Illuminate\Database\Eloquent\Model;


/**
 * App\MOdels\EstatusVenda
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venda[] $vendas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao A venda pode estar efetivada, não autorizada, entre outros estados
 * @property int $produto_reservado reserva o produto no estoque para uma pre-venda ou aguardando aprovação.
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MOdels\EstatusVenda whereProdutoReservado($value)
 */
class EstatusVenda extends Model
{
    protected $table = 'estatus_venda';

    public $timestamps = false;

    protected $fillable = ['descricao'];

    public function vendas()
    {
        return $this->hasMany(Venda::class, 'estatus_venda_id', 'id');
    }
}
