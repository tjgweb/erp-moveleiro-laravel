<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Padrao
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pessoa[] $pessoas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Padrao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Padrao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Padrao query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao identificação da pessoa, quanto ao relacionamento dela na empresa, funcionário, fornecedor, etc.
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Padrao whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Padrao whereId($value)
 */
class Padrao extends Model
{
    protected $table = 'padrao';

    public $timestamps = false;

    protected $fillable = ['descricao'];

    public function pessoas()
    {
        return $this->belongsToMany(Pessoa::class, 'pessoa_padrao', 'padrao_id', 'pessoa_id');
    }
}
