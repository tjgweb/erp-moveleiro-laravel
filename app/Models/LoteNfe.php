<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoteNfe extends Model
{
    protected $table = 'lote_nfe';

    public $timestamps = null;

    protected $fillable = ['nRec', 'cStat', 'xMotivo', 'xmlProtocolo', 'xmlTransmissao'];

    public function notasFiscais()
    {
        return $this->hasMany(NotaFiscal::class, 'lote_nfe_id');
    }
}
