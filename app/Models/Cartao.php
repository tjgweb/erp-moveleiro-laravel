<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cartao
 *
 * @property-read \App\Models\Bandeira $bandeira
 * @property-read mixed $total
 * @property-read \App\Models\Transacao $transacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $transacao_id
 * @property int $bandeira_id
 * @property string|null $protocolo
 * @property int $quantidade_parcelas
 * @property float $valor_parcelas
 * @property string $tipo C crédito, D débito
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereBandeiraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereProtocolo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereQuantidadeParcelas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereTransacaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cartao whereValorParcelas($value)
 */
class Cartao extends Model
{
    protected $table = 'cartao';

    public $timestamps = false;

    protected $fillable = ['transacao_id', 'bandeira_id', 'protocolo', 'quantidade_parcelas', 'valor_parcelas', 'tipo'];

    protected $appends = ['total'];

    public function getTotalAttribute()
    {
        $this->attributes['total'] = round($this->valor_parcelas * $this->quantidade_parcelas, 2);
        return round($this->attributes['total'], 2);
    }

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }

    public function bandeira()
    {
        return $this->belongsTo(Bandeira::class, 'bandeira_id', 'id');
    }
}
