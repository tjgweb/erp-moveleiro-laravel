<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;


/**
 * App\Models\Produto
 *
 * @property-read \App\Models\Ajuste $ajuste
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Combinado[] $combinados
 * @property-read \App\Models\Custo $custo
 * @property-read mixed $descricao_completa
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Imagem[] $imagens
 * @property-read \App\Models\Marca $marca
 * @property-read \App\Models\Mercadoria $mercadoria
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProdutoVenda[] $produtoVendas
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produto onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produto withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $mercadoria_id
 * @property int $marca_id
 * @property string|null $codigo_fabrica
 * @property string|null $altura
 * @property string|null $largura
 * @property string|null $profundidade
 * @property float|null $valor_atual
 * @property float|null $margem Máximo de desconto
 * @property string|null $descricao
 * @property string|null $informacao_adicional
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereAltura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereCodigoFabrica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereInformacaoAdicional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereLargura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereMarcaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereMargem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereMercadoriaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereProfundidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produto whereValorAtual($value)
 */
class Produto extends Model
{
    use SoftDeletes;

    protected $table = 'produto';

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = ['altura', 'largura', 'profundidade', 'valor_atual', 'codigo_fabrica', 'descricao',
        'informacao_adicional', 'marca_id', 'mercadoria_id'
    ];

    protected $appends = ['descricao_completa', 'estoque_por_loja'];

    public function getDescricaoCompletaAttribute()
    {
        $this->attributes['descricao_completa'] = "{$this->mercadoria->categoria->descricao} - {$this->mercadoria->descricao} - {$this->marca->nome} - {$this->descricao}";
        return $this->attributes['descricao_completa'];
    }

    public function getEstoquePorLojaAttribute()
    {
        $entradas = $this->movimentoProdutos
            ->where('loja_entrada_id', '!=', null)
            ->groupBy('loja_entrada_id');

        $loja_entrada = [];
        foreach ($entradas as $loja_id => $mProdutos) {
            $loja_entrada[$loja_id] = [
                'loja_id' => $loja_id,
                'descricao' => $mProdutos[0]->lojaEntrada->descricao ?? '',
                'quantidade' => 0
            ];
            foreach ($mProdutos as $mp) {
                if ($mp->unidadeMedida->agrupado == 1) {
                    $loja_entrada[$loja_id]['quantidade'] += $mp['quantidade'] * $mp['quantidade_por_caixa'];
                } else {
                    $loja_entrada[$loja_id]['quantidade'] += $mp['quantidade'];
                }
            }
        }

        $saidas = $this->movimentoProdutos
            ->where('loja_saida_id', '!=', null)
            ->groupBy('loja_saida_id');

        $loja_saida = [];
        foreach ($saidas as $loja_id => $mProdutos) {
            $loja_saida[$loja_id] = [
                'loja_id' => $loja_id,
                'descricao' => $mProdutos[0]->lojaEntrada->descricao ?? '',
                'quantidade' => 0
            ];
            foreach ($mProdutos as $mp) {
                if ($mp->unidadeMedida->agrupado == 1) {
                    $loja_saida[$loja_id]['quantidade'] += $mp['quantidade'] * $mp['quantidade_por_caixa'];
                } else {
                    $loja_saida[$loja_id]['quantidade'] += $mp['quantidade'];
                }
            }
        }

        $data = [];
        foreach ($loja_entrada as $keyEntrada => $valueEntrada) {
            $calcular[$keyEntrada] = $valueEntrada;
            if (isset($loja_saida[$keyEntrada])) {
                $calcular[$keyEntrada]['quantidade'] -= $loja_saida[$keyEntrada]['quantidade'];
            }
            $data[] = $calcular[$keyEntrada];
        }

        $this->attributes['estoque_por_loja'] = Collection::make($data);

        return $this->attributes['estoque_por_loja'];
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class, 'marca_id', 'id');
    }

    public function mercadoria()
    {
        return $this->belongsTo(Mercadoria::class, 'mercadoria_id', 'id');
    }

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'produto_id', 'id');
    }

    public function custo()
    {
        return $this->hasOne(Custo::class, 'produto_id', 'id');
    }

    public function ajuste()
    {
        return $this->hasOne(Ajuste::class, 'produto_id', 'id');
    }

    public function produtoVendas()
    {
        return $this->hasMany(ProdutoVenda::class, 'produto_id', 'id');
    }

    public function imagens()
    {
        return $this->belongsToMany(Imagem::class, 'imagem_produto', 'produto_id', 'imagem_id');
    }

    public function combinados()
    {
        return $this->belongsToMany(Combinado::class, 'produto_combinado', 'produto_id', 'combinado_id')
            ->withPivot('quantidade');
    }
}
