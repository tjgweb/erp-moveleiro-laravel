<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Venda
 *
 * @property-read \App\Models\Endereco $enderecoEntrega
 * @property-read \App\MOdels\EstatusVenda $estatusVenda
 * @property-read mixed $valor_pago
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @property-read \App\Models\Pessoa $pessoaCliente
 * @property-read \App\Models\Pessoa $pessoaVendedor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProdutoVenda[] $produtoVendas
 * @property-read \App\MOdels\TipoVenda $tipoVenda
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transacao[] $transacoes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VendaPagamento[] $vendaPagamentos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $tipo_venda_id
 * @property int $estatus_venda_id
 * @property int $vendedor_id
 * @property int|null $cliente_id
 * @property int|null $endereco_entrega_id
 * @property string|null $data_venda
 * @property string|null $previsao_entrega
 * @property string|null $observacao
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereClienteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereDataVenda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereEnderecoEntregaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereEstatusVendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereObservacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda wherePrevisaoEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereTipoVendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venda whereVendedorId($value)
 */
class Venda extends Model
{
    protected $table = 'venda';

//    protected $dates = ['data_venda', 'updated_at'];

    protected $fillable = [
        'vendedor_id', 'cliente_id', 'endereco_entrega_id', 'estatus_venda_id', 'tipo_venda_id',
        'data_venda', 'previsao_entrega', 'observacao', 'created_at', 'updated_at'
    ];

    protected $appends = ['valor_pago'];

    public function getValorPagoAttribute()
    {
        $soma = 0;
        foreach ($this->produtoVendas as $pv) {
            $soma += $pv->valor_pago;
        }
        $this->attributes['valor_pago'] = round($soma, 2);
        return $this->attributes['valor_pago'];
    }

    public function tipoVenda()
    {
        return $this->belongsTo(TipoVenda::class, 'tipo_venda_id', 'id');
    }

    public function estatusVenda()
    {
        return $this->belongsTo(EstatusVenda::class, 'estatus_venda_id', 'id');
    }

    public function pessoaVendedor()
    {
        return $this->belongsTo(Pessoa::class, 'vendedor_id', 'id');
    }

    public function pessoaCliente()
    {
        return $this->belongsTo(Pessoa::class, 'cliente_id', 'id');
    }

    public function enderecoEntrega()
    {
        return $this->belongsTo(Endereco::class, 'endereco_entrega_id', 'id');
    }

    public function transacoes()
    {
        return $this->hasMany(Transacao::class, 'venda_id', 'id');
    }

    public function vendaPagamentos()
    {
        return $this->hasMany(VendaPagamento::class, 'venda_id', 'id');
    }

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'venda_id', 'id');
    }

    public function produtoVendas()
    {
        return $this->hasMany(ProdutoVenda::class, 'venda_id', 'id');
    }
}
