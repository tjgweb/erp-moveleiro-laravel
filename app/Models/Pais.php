<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Pais
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Estado[] $estados
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property string $sigla
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereSigla($value)
 */
class Pais extends Model
{
    protected $table = 'pais';

    public $timestamps = false;

    protected $fillable = ['nome', 'sigla'];

    public function estados()
    {
        return $this->hasMany(Estado::class, 'pais_id', 'id');
    }
}
