<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VendaPagamento
 *
 * @property-read \App\Models\Bandeira $bandeira
 * @property-read \App\Models\Financeira $financeira
 * @property-read \App\Models\TipoPagamento $tipoPagamento
 * @property-read \App\Models\Venda $venda
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $tipo_pagamento_id
 * @property int $venda_id
 * @property int|null $bandeira_id
 * @property int|null $financeira_id
 * @property float $valor
 * @property int|null $parcelas
 * @property float|null $valor_parcelas
 * @property string|null $vencimento
 * @property string|null $observacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereBandeiraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereFinanceiraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereObservacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereParcelas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereTipoCartao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereTipoPagamentoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereValor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereValorParcelas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereVencimento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VendaPagamento whereVendaId($value)
 */
class VendaPagamento extends Model
{
    protected $table = 'venda_pagamento';

    public $timestamps = false;

    protected $fillable = ['tipo_pagamento_id', 'venda_id', 'bandeira_id', 'financeira_id', 'valor',
        'parcelas', 'valor_parcelas', 'vencimento', 'observacao'];

    public function tipoPagamento()
    {
        return $this->belongsTo(TipoPagamento::class, 'tipo_pagamento_id', 'id');
    }

    public function bandeira()
    {
        return $this->belongsTo(Bandeira::class, 'bandeira_id', 'id');
    }

    public function venda()
    {
        return $this->belongsTo(Venda::class, 'venda_id', 'id');
    }

    public function financeira()
    {
        return $this->belongsTo(Financeira::class, 'financeira_id', 'id');
    }
}
