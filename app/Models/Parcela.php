<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Parcela
 *
 * @property-read \App\Models\Crediario $crediario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transacao[] $transacoes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $crediario_id
 * @property string $vencimento
 * @property float $valor
 * @property float|null $desconto
 * @property float|null $juros
 * @property float|null $multa
 * @property int $sequencia
 * @property string $status A - Aberto
 * P - Pago
 * X - Pagamento Parcial
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereCrediarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereDesconto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereJuros($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereMulta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereSequencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereValor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Parcela whereVencimento($value)
 */
class Parcela extends Model
{
    protected $table = 'parcela';

    public $timestamps = false;

    protected $fillable = [
        'vencimento', 'valor', 'desconto', 'juros', 'multa', 'sequencia', 'status', 'crediario_id',
    ];

/*    protected $appends = ['abatimento'];

public function getAbatimentoAttribute()
{
$soma = 0;
if ($this->abatimentos->count()) {
foreach ($this->abatimentos as $abatimento) {
$soma += $abatimento->valor;
}
}
$this->attributes['abatimento'] = round($soma, 2);
return round($soma, 2);
}*/

    public function crediario()
    {
        return $this->belongsTo(Crediario::class, 'crediario_id', 'id');
    }

    public function transacoes()
    {
        return $this->belongsToMany(Transacao::class, 'parcela_transacao', 'parcela_id', 'transacao_id')
            ->withPivot(['abatimento', 'valor', 'observacao']);
    }
}