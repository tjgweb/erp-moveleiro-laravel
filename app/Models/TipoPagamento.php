<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TipoPagamento
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParcelaPagamento[] $parcelaPagamentos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VendaPagamento[] $vendaPagamentos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPagamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPagamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPagamento query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao
 * @property string $codigo
 * @property string $ativo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPagamento whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPagamento whereId($value)
 */
class TipoPagamento extends Model
{
    protected $table = 'tipo_pagamento';

    public $timestamps = false;

    protected $fillable = ['descricao', 'codigo', 'ativo'];

    public function vendaPagamentos()
    {
        return $this->hasMany(VendaPagamento::class, 'tipo_pagamento_id', 'id');
    }

    public function parcelaPagamentos()
    {
        return $this->hasMany(ParcelaPagamento::class, 'tipo_pagamento_id', 'id');
    }
}
