<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Combinado
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProdutoVenda[] $produtoVendas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Produto[] $produtos
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Combinado onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Combinado withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Combinado withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao
 * @property float $valor_total
 * @property float|null $margem
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado whereMargem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Combinado whereValorTotal($value)
 */
class Combinado extends Model
{
    use SoftDeletes;

    protected $table = 'combinado';

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = ['descricao', 'valor_total', 'margem'];

    public function produtos()
    {
        return $this->belongsToMany(Produto::class, 'produto_combinado', 'combinado_id', 'produto_id')
            ->withPivot('quantidade', 'valor');
    }

    public function produtoVendas()
    {
        return $this->hasMany(ProdutoVenda::class, 'combinado_id', 'id');
    }
}