<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\UnidadeMedida
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $unidade
 * @property string $descricao
 * @property int $agrupado se produto é subdividido em outras unidades.
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida whereAgrupado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnidadeMedida whereUnidade($value)
 */
class UnidadeMedida extends Model
{
    protected $table = 'unidade_medida';

    public $timestamps = false;

    protected $fillable = ['unidade', 'descricao'];

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'unidade_medida_id', 'id');
    }
}
