<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\TipoIdentidade
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fisica[] $fisicas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $titulo
 * @property string $sigla_identidade
 * @property string $emissor
 * @property string $sigla
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade whereEmissor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade whereSigla($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade whereSiglaIdentidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoIdentidade whereTitulo($value)
 */
class TipoIdentidade extends Model
{
    protected $table = 'tipo_identidade';

    public $timestamps = false;

    protected $fillable = ['titulo', 'sigla_identidade', 'emissor', 'sigla'];

    public function fisicas()
    {
        return $this->hasMany(Fisica::class, 'tipo_identidade_id', 'id');
    }
}
