<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Funcionario
 *
 * @property-read \App\Models\Pessoa $pessoa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario query()
 * @mixin \Eloquent
 * @property int $pessoa_id
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereStatus($value)
 */
class Funcionario extends Model
{
    protected $table = 'funcionario';

    public $primaryKey = 'pessoa_id';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = ['pessoa_id', 'status'];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }
}
