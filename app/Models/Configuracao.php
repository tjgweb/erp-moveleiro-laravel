<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    protected $table = 'configuracoes';

    public const CREATED_AT = null;

    public const UPDATED_AT = 'atualizacao';

    protected $dates = ['atualizacao'];

    protected $fillable = [
        'tpAmb',
        'schemes',
        'versao',
        'atualizacao',
        'multa',
        'juros_dia',
        'cnpj_contador'
    ];

    public function setCnpjContadorAttribute($value)
    {
        $this->attributes['cnpj_contador'] = str_replace([' ', '.', '-', '/'], ['', '', '', ''], $value);
    }

}
