<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Mercadoria
 *
 * @property-read \App\Models\Categoria $categoria
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Produto[] $produtos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $descricao
 * @property int $categoria_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria whereCategoriaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mercadoria whereId($value)
 */
class Mercadoria extends Model
{
    protected $table = 'mercadoria';

    public $timestamps = false;

    protected $fillable = ['descricao', 'categoria_id'];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id', 'id');
    }

    public function produtos()
    {
        return $this->hasMany(Produto::class, 'mercadoria_id', 'id');
    }
}
