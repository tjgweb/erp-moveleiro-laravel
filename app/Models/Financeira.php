<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Financeira
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Financiamento[] $creditos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VendaPagamento[] $vendaPagamentos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @property string|null $banco_credito
 * @property string|null $telefone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira whereBancoCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Financeira whereTelefone($value)
 */
class Financeira extends Model
{
    protected $table = 'financeira';

    public $timestamps = false;

    protected $fillable = ['nome', 'banco_credito', 'telefone'];

    public function creditos()
    {
        return $this->hasMany(Financiamento::class, 'financeira_id', 'id');
    }

    public function vendaPagamentos()
    {
        return $this->hasMany(VendaPagamento::class, 'financeira_id', 'id');
    }
}
