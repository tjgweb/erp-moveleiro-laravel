<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Imagem
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Produto[] $produtos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $src
 * @property string|null $legenda
 * @property string|null $ordem 1 para principal 0 para secundarias
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem whereLegenda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem whereOrdem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagem whereSrc($value)
 */
class Imagem extends Model
{
    protected $table = 'imagem';

    public $timestamps = false;

    protected $fillable = ['src', 'legenda', 'ordem'];

    public function produtos()
    {
        return $this->belongsToMany(Produto::class, 'imagem_produto', 'imagem_id', 'produto_id');
    }
}
