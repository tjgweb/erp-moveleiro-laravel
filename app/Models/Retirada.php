<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Retirada
 *
 * @property-read \App\Models\Caixa $caixa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $caixa_id
 * @property float $valor
 * @property string $descricao propósito da retirada.
 * @property string $hora
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada whereCaixaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retirada whereValor($value)
 */
class Retirada extends Model
{
    protected $table = 'retirada';

    public $timestamps = false;

    protected $fillable = ['caixa_id', 'valor', 'descricao', 'hora', 'delete_action'];

    public function caixa()
    {
        return $this->belongsTo(Caixa::class, 'caixa_id', 'id');
    }

}
