<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Vencimento extends Model
{
    protected $table = 'vencimento';

    public $timestamps = false;

    protected $fillable = ['vencimento_tipo_id', 'pessoa_id', 'transacao_id', 'mes', 'ano', 'valor'];

    protected $appends = ['referencia'];

    public function getReferenciaAttribute()
    {
        $this->attributes['referencia'] = ucfirst(Carbon::create($this->ano, $this->mes, '01')->localeMonth) . ' / ' . $this->ano;

        return $this->attributes['referencia'];
    }

    public function tipo()
    {
        return $this->belongsTo(VencimentoTipo::class, 'vencimento_tipo_id', 'id');
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }
}
