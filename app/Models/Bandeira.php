<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Bandeira
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cartao[] $cartoes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParcelaPagamento[] $parcelaPagamentos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VendaPagamento[] $vendaPagamentos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao
 * @property string $codigo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira whereBancoCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bandeira whereNome($value)
 */
class Bandeira extends Model
{
    protected $table = 'bandeira';

    public $timestamps = false;

    protected $fillable = ['descricao', 'codigo'];

    public function cartoes()
    {
        return $this->hasMany(Cartao::class, 'bandeira_id', 'id');
    }

    public function vendaPagamentos()
    {
        return $this->hasMany(VendaPagamento::class, 'bandeira_id', 'id');
    }

    public function parcelaPagamentos()
    {
        return $this->hasMany(ParcelaPagamento::class, 'bandeira_id', 'id');
    }
}
