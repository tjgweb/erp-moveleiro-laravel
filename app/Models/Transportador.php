<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Transportador
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Carga[] $cargas
 * @property-read \App\Models\Pessoa $pessoa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador query()
 * @mixin \Eloquent
 * @property int $pessoa_id
 * @property string|null $codigo_antt
 * @property string|null $cadastro_nacional
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador whereCadastroNacional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador whereCodigoAntt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transportador wherePessoaId($value)
 */
class Transportador extends Model
{
    protected $table = 'transportador';

    public $primaryKey = 'pessoa_id';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = ['pessoa_id', 'codigo_antt', 'cadastro_nacional'];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }

    public function cargas()
    {
        return $this->hasMany(Carga::class, 'transportador_id', 'pessoa_id');
    }
}
