<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Lote
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote query()
 * @mixin \Eloquent
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereId($value)
 */
class Lote extends Model
{
    protected $table = 'lote';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [];

    public function movimentoProdutos()
    {
        return $this->hasMany(MovimentoProduto::class, 'lote_id', 'id');
    }
}
