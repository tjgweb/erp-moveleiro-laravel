<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\PosVenda
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pessoa[] $equipe
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MovimentoProduto[] $movimentoProdutos
 * @property-read \App\Models\TipoPosVenda $tipoPosVenda
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $tipo_pos_venda_id
 * @property string|null $descricao
 * @property string $data
 * @property string $hora
 * @property int $status terminado ou não a assistencia
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosVenda whereTipoPosVendaId($value)
 */
class PosVenda extends Model
{
    protected $table = 'pos_venda';

    public $timestamps = false;

    protected $fillable = ['tipo_pos_venda_id', 'descricao', 'data', 'hora', 'status', 'finalizado_em'];

    public function tipoPosVenda()
    {
        return $this->belongsTo(TipoPosVenda::class, 'tipo_pos_venda_id', 'id');
    }

    public function equipe()
    {
        return $this->belongsToMany(Pessoa::class, 'pos_venda_pessoa', 'pos_venda_id', 'pessoa_id');
    }

    public function movimentoProdutos()
    {
        return $this->belongsToMany(MovimentoProduto::class, 'pos_venda_movimento_produto', 'pos_venda_id', 'movimento_produto_id')
            ->withPivot(['quantidade']);
    }
}
