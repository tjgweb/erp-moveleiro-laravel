<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProdutoVenda
 *
 * @property-read \App\Models\Combinado $combinado
 * @property-read mixed $valor_desconto
 * @property-read mixed $valor_pago
 * @property-read mixed $valor_total
 * @property-read \App\Models\Produto $produto
 * @property-read \App\Models\Venda $venda
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $venda_id
 * @property int $produto_id
 * @property int|null $combinado_id
 * @property int $quantidade
 * @property float $valor_unitario
 * @property float|null $desconto Percentual
 * @property int $produto_combinado
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereCombinadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereDesconto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereProdutoCombinado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereProdutoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereQuantidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereValorUnitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProdutoVenda whereVendaId($value)
 */
class ProdutoVenda extends Model
{
    protected $table = 'produto_venda';

    public $timestamps = false;

    protected $fillable = ['quantidade', 'desconto', 'produto_id', 'venda_id', 'valor_unitario', 'produto_combinado', 'combinado_id'];

    protected $appends = ['valor_total', 'valor_desconto', 'valor_pago'];

    public function getValorTotalAttribute()
    {
        $this->attributes['valor_total'] = round($this->valor_unitario * $this->quantidade,2);
        return $this->attributes['valor_total'];
    }

    public function getValorDescontoAttribute()
    {
        $this->attributes['valor_desconto'] = round(($this->valor_unitario * $this->quantidade) * $this->desconto / 100,2);
        return $this->attributes['valor_desconto'];
    }

    public function getValorPagoAttribute()
    {
        $this->attributes['valor_pago'] = round($this->valor_total - $this->valor_desconto, 2);
        return $this->attributes['valor_pago'];
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }

    public function venda()
    {
        return $this->belongsTo(Venda::class, 'venda_id', 'id');
    }

    public function combinado()
    {
        return $this->belongsTo(Combinado::class, 'combinado_id', 'id');
    }
}
