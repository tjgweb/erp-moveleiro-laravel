<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\MovimentoProduto
 *
 * @property-read mixed $base_calculo_icms_substituicao
 * @property-read mixed $valor_icms
 * @property-read mixed $valor_icms_substituicao
 * @property-read mixed $valor_ipi
 * @property-read mixed $valor_minimo
 * @property-read mixed $valor_total
 * @property-read mixed $valor_unitario
 * @property-read \App\Models\Loja $lojaEntrada
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Loja[] $lojasSaida
 * @property-read \App\Models\Lote $lote
 * @property-read \App\Models\NaturezaOperacao $naturezaOperacao
 * @property-read \App\Models\NotaFiscal $notaFiscal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MOdels\PosVenda[] $posVendas
 * @property-read \App\Models\Produto $produto
 * @property-read \App\Models\UnidadeMedida $unidadeMedida
 * @property-read \App\Models\Venda $venda
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MovimentoProduto onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MovimentoProduto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MovimentoProduto withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $nota_fiscal_id
 * @property int|null $loja_entrada_id validar via aplicação. Obrigatório para nota de Entrada
 * @property int|null $venda_id
 * @property int $produto_id
 * @property int $unidade_medida_id
 * @property int $cfop_id
 * @property int|null $lote_id
 * @property string $id_na_nota identificação do produto na nota de entrada para cadastros
 * @property float $quantidade
 * @property float|null $quantidade_por_caixa
 * @property float $desconto valor do desconto
 * @property string|null $observacao
 * @property float $aliquota_icms
 * @property float $aliquota_ipi
 * @property float $mva
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereAliquotaIcms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereAliquotaIpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereCfopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereDesconto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereIdNaNota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereLojaEntradaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereLoteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereMva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereNotaFiscalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereObservacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereProdutoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereQuantidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereQuantidadePorCaixa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereUnidadeMedidaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereValorUnitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoProduto whereVendaId($value)
 */
class MovimentoProduto extends Model
{
    use SoftDeletes;

    protected $table = 'movimento_produto';

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    protected $fillable = [
        'id_na_nota', 'quantidade', 'quantidade_por_caixa', 'valor_unitario', 'desconto', 'observacao', 'aliquota_icms',
        'aliquota_ipi', 'valor_minimo', 'lote_id', 'cfop_id', 'nota_fiscal_id', 'produto_id', 'unidade_medida_id',
        'venda_id', 'mva', 'loja_entrada_id', 'loja_saida_id'
    ];

    /* adiciona atributos em tempo de execução para a entidade/model */
    protected $appends = ['valor_total', 'valor_icms', 'valor_ipi', 'valor_icms_substituicao', 'base_calculo_icms_substituicao'];

    public function getValorTotalAttribute()
    {
        $this->attributes['valor_total'] = ($this->valor_unitario * $this->quantidade) - ($this->desconto * $this->quantidade);
        return round($this->attributes['valor_total'], 2);
    }

    public function getValorIcmsAttribute()
    {
        $valorTotal = ($this->valor_unitario * $this->quantidade) - ($this->desconto * $this->quantidade);
        $this->attributes['valor_icms'] = ($valorTotal * $this->aliquota_icms) / 100;
        return round($this->attributes['valor_icms'], 2);
    }

    public function getValorIpiAttribute()
    {
        $valorTotal = ($this->valor_unitario * $this->quantidade) - ($this->desconto * $this->quantidade);
        $this->attributes['valor_ipi'] = ($valorTotal * $this->aliquota_ipi) / 100;
        return round($this->attributes['valor_ipi'], 2);
    }

    public function getBaseCalculoIcmsSubstituicaoAttribute()
    {
        /* if ($this->mva != 0) {
        $valorMva = $this->valor_total * $this->mva / 100;
        $basecalculo = $this->valor_total + $valorMva - $this->valor_icms;
        $this->attributes['base_calculo_icms_substituicao'] = $basecalculo / (1 - (18 / 100));
        return round($this->attributes['base_calculo_icms_substituicao'], 2);
        } */
        if ($this->mva != 0) {
            $valorMva = $this->valor_total * $this->mva / 100;
            $basecalculo = $this->valor_total + $valorMva;
            $this->attributes['base_calculo_icms_substituicao'] = ($basecalculo * 18) / 100;
            return round($this->attributes['base_calculo_icms_substituicao'], 2);
        }

        $this->attributes['base_calculo_icms_substituicao'] = 0;
        return $this->attributes['base_calculo_icms_substituicao'];
    }

    public function getValorIcmsSubstituicaoAttribute()
    {
        /* if ($this->mva != 0) {
        $valorIcmsSub = $this->base_calculo_icms_substituicao * 18 / 100;
        $this->attributes['valor_icms_substituicao'] = $valorIcmsSub - $this->valor_icms;
        return round($this->attributes['valor_icms_substituicao'], 2);
        } */
        if ($this->mva != 0) {
            $valorIcmsSub = $this->base_calculo_icms_substituicao - $this->valor_icms;
            $this->attributes['valor_icms_substituicao'] = $valorIcmsSub;
            return round($this->attributes['valor_icms_substituicao'], 2);
        }

        $this->attributes['valor_icms_substituicao'] = 0;
        return $this->attributes['valor_icms_substituicao'];
    }

    /* Modifica o atributo valor_unitario para apresentação na view.*/
    public function getValorUnitarioAttribute($value)
    {
        return round($value, 2);
    }

    /* Modifica o atributo valor_minimo para apresentação na view.*/
    public function getValorMinimoAttribute($value)
    {
        return round($value, 2);
    }

    public function lote()
    {
        return $this->belongsTo(Lote::class, 'lote_id', 'id');
    }

    public function naturezaOperacao()
    {
        return $this->belongsTo(NaturezaOperacao::class, 'cfop_id', 'id');
    }

    public function notaFiscal()
    {
        return $this->belongsTo(NotaFiscal::class, 'nota_fiscal_id', 'id');
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }

    public function unidadeMedida()
    {
        return $this->belongsTo(UnidadeMedida::class, 'unidade_medida_id', 'id');
    }

    public function venda()
    {
        return $this->belongsTo(Venda::class, 'venda_id', 'id');
    }

    public function lojaEntrada()
    {
        return $this->belongsTo(Loja::class, 'loja_entrada_id', 'id');
    }

    public function lojasSaida()
    {
        return $this->belongsTo(Loja::class, 'loja_saida_id', 'id');
    }

    public function posVendas()
    {
        return $this->belongsToMany(PosVenda::class, 'pos_venda_movimento_produto', 'movimento_produto_id', 'pos_venda_id')
            ->withPivot(['quantidade']);
    }
}
