<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Crediario
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Parcela[] $parcelas
 * @property-read \App\Models\Transacao $transacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $transacao_id
 * @property string|null $observacao
 * @property string $status O estado do contrato:
 * F - finalizado
 * A - aberto
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario whereObservacao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crediario whereTransacaoId($value)
 */
class Crediario extends Model
{
    protected $table = 'crediario';

    public $timestamps = false;

    protected $fillable = ['transacao_id', 'observacao', 'status'];

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }

    public function parcelas()
    {
        return $this->hasMany(Parcela::class, 'crediario_id', 'id');
    }
}