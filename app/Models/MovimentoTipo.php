<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\MovimentoTipo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notaFiscais
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoTipo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoTipo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoTipo query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descricao Entrada ou saída?
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoTipo whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovimentoTipo whereId($value)
 */
class MovimentoTipo extends Model
{
    protected $table = 'movimento_tipo';

    public $timestamps = false;

    protected $fillable = ['descricao'];

    public function notaFiscais()
    {
        return $this->hasMany(NotaFiscal::class, 'movimento_tipo_id', 'id');
    }
}
