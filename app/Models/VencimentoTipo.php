<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VencimentoTipo extends Model
{
    protected $table = 'vencimento_tipo';

    public $timestamps = false;

    protected $fillable = ['nome'];

    public function vencimentos()
    {
        return $this->hasMany(Vencimento::class, 'vencimento_tipo_id', 'id');
    }
}
