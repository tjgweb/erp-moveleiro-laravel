<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Caixa
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Credito[] $creditos
 * @property-read \App\Models\User $gerente
 * @property-read mixed $fluxo_total
 * @property-read mixed $total_cartao_credito
 * @property-read mixed $total_cartao_debito
 * @property-read mixed $total_crediario
 * @property-read mixed $total_credito
 * @property-read mixed $total_dinheiro
 * @property-read mixed $total_financiamento
 * @property-read mixed $total_liquido
 * @property-read mixed $total_retirada
 * @property-read \App\Models\User $operador
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Retirada[] $retiradas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transacao[] $transacoes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $operador_id
 * @property int|null $gerente_id
 * @property float $troco_inicial
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $aberto indica se o caixa está aberto true ou encerrado false
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereAberto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereGerenteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereOperadorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereTrocoInicial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caixa whereUpdatedAt($value)
 */
class Caixa extends Model
{
    protected $table = 'caixa';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['troco_inicial', 'operador_id', 'gerente_id', 'created_at', 'updated_at', 'aberto'];

    protected $appends = [
        'total_dinheiro', 'total_credito', 'total_cartao_debito', 'total_cartao_credito', 'total_financiamento',
        'total_crediario', 'total_cheque', 'total_retirada', 'fluxo', 'em_caixa'
    ];

    public function getTotalDinheiroAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->dinheiros->count()) {
                    $soma += $transacao->dinheiros->sum('valor_recebido');
                }
            }
        }
        $this->attributes['total_dinheiro'] = $soma;
        return round($this->attributes['total_dinheiro'], 2);
    }

    public function getTotalCreditoAttribute()
    {
        $this->attributes['total_credito'] = $this->creditos->sum('valor');
        return round($this->attributes['total_credito'], 2);
    }



    public function getTotalCartaoDebitoAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->cartoes->where('tipo', 'D')->count()) {
                    foreach ($transacao->cartoes->where('tipo', 'D') as $cartao) {
                        $soma += $cartao->quantidade_parcelas * $cartao->valor_parcelas;
                    }
                }
            }
        }
        $this->attributes['total_cartao_debito'] = round($soma, 2);
        return round($this->attributes['total_cartao_debito'], 2);
    }

    public function getTotalCartaoCreditoAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->cartoes->where('tipo', 'C')->count()) {
                    foreach ($transacao->cartoes->where('tipo', 'C') as $cartao) {
                        $soma += $cartao->quantidade_parcelas * $cartao->valor_parcelas;
                    }
                }
            }
        }
        $this->attributes['total_cartao_credito'] = $soma;
        return round($this->attributes['total_cartao_credito'], 2);
    }

    public function getTotalFinanciamentoAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->financiamentos->count()) {
                    foreach ($transacao->financiamentos as $financiamento) {
                        $soma += $financiamento->quantidade_parcelas * $financiamento->valor_parcelas;
                    }
                }
            }
        }
        $this->attributes['total_financiamento'] = $soma;
        return round($this->attributes['total_financiamento'], 2);
    }

    public function getTotalCrediarioAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->crediarios->count()) {
                    foreach ($transacao->crediarios as $crediario) {
                        foreach ($crediario->parcelas as $parcela) {
                            $soma += $parcela->valor;
                        }
                    }
                }
            }
        }
        $this->attributes['total_crediario'] = $soma;
        return round($this->attributes['total_crediario'], 2);
    }

    public function getTotalChequeAttribute()
    {
        $soma = 0;
        if ($this->transacoes->count()) {
            foreach ($this->transacoes as $transacao) {
                if ($transacao->predatados->count()) {
                    foreach($transacao->predatados as $predatado){
                        $soma += $predatado->cheques->sum('valor');
                    }
                }
            }
        }
        $this->attributes['total_cheque'] = $soma;
        return round($this->attributes['total_cheque'], 2);
    }

    public function getTotalRetiradaAttribute()
    {
        $this->attributes['total_retirada'] = $this->retiradas->sum('valor');
        return round($this->attributes['total_retirada'], 2);
    }

    public function getFluxoAttribute()
    {
        $this->attributes['fluxo'] = ($this->troco_inicial + $this->total_credito + $this->total_dinheiro +
            $this->total_cartao_debito + $this->total_cartao_credito + $this->total_financiamento +
            $this->total_crediario + $this->total_cheque - $this->total_retirada);
        return round($this->attributes['fluxo'], 2);
    }

    public function getEmCaixaAttribute()
    {
        $this->attributes['em_caixa'] = ($this->troco_inicial + $this->total_credito + $this->total_dinheiro - $this->total_retirada);
        return round($this->attributes['em_caixa'], 2);
    }

    public function operador()
    {
        return $this->belongsTo(User::class, 'operador_id', 'id');
    }

    public function gerente()
    {
        return $this->belongsTo(User::class, 'gerente_id', 'id');
    }

    public function transacoes()
    {
        return $this->hasMany(Transacao::class, 'caixa_id', 'id');
    }

    public function retiradas()
    {
        return $this->hasMany(Retirada::class, 'caixa_id', 'id');
    }

    public function creditos()
    {
        return $this->hasMany(Credito::class, 'caixa_id', 'id');
    }
}
