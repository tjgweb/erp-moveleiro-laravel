<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Ajuste
 *
 * @property-read \App\Models\NotaFiscal $notaFiscal
 * @property-read \App\Models\Produto $produto
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ajuste onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ajuste withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ajuste withoutTrashed()
 * @mixin \Eloquent
 * @property int $produto_id
 * @property int $nota_fiscal_id
 * @property float|null $frete
 * @property float|null $despesa_extra
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste whereDespesaExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste whereFrete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste whereNotaFiscalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ajuste whereProdutoId($value)
 */
class Ajuste extends Model
{
    use SoftDeletes;

    protected $table = 'ajuste';

    public $primaryKey = 'produto_id';

    public $timestamps = false;

    public $incrementing = false;

    protected $dates = ['deleted_at'];

    protected $fillable = ['produto_id', 'nota_fiscal_id', 'frete', 'despesa_extra'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }

    public function notaFiscal()
    {
        return $this->belongsTo(NotaFiscal::class, 'nota_fiscal_id', 'id');
    }
}
