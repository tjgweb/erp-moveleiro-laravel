<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Endereco
 *
 * @property-read \App\Models\Cidade $cidade
 * @property-read \App\Models\Estado $estado
 * @property-read mixed $full
 * @property-read mixed $logradouro_completo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notaFiscalDestinatario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notaFiscalEmissor
 * @property-read \App\Models\Pessoa $pessoa
 * @property-read \App\Models\Regiao $regiao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $pessoa_id
 * @property int $estado_id
 * @property int $cidade_id
 * @property int|null $regiao_id
 * @property string $titulo Título do endereço, residencial, entrega, etc...
 * @property string $logradouro Rua, avenida, beco... Etc.
 * @property string $bairro
 * @property string $numero
 * @property string|null $complemento
 * @property string $cep
 * @property string|null $referencia
 * @property int|null $principal 1 endereço principal, 0 endereços secundários
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereBairro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereCep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereCidadeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereComplemento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereEstadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereLogradouro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco wherePessoaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco wherePrincipal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereReferencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereRegiaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Endereco whereTitulo($value)
 */
class Endereco extends Model
{
    protected $table = 'endereco';

    public $timestamps = false;

    protected $fillable = ['titulo', 'logradouro', 'bairro', 'numero', 'complemento', 'cep', 'referencia',
        'principal', 'cidade_id', 'estado_id', 'pessoa_id', 'regiao_id'
    ];

    /* adiciona atributos em tempo de execução para a entidade/model */
    protected $appends = ['logradouroCompleto', 'full'];

    public function getLogradouroCompletoAttribute()
    {
        if (is_null($this->logradouro) || strlen($this->logradouro) == 0) {
            return '';
        }
        return "{$this->logradouro}, {$this->numero} {$this->complemento}";
    }

    public function getFullAttribute()
    {
        if (is_null($this->logradouro) || strlen($this->logradouro) == 0) {
            return '';
        }
        return "{$this->logradouro}, {$this->numero} {$this->complemento} - {$this->bairro} - {$this->cidade->nome} - {$this->estado->uf}";
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class, 'cidade_id', 'id');
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id', 'id');
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id', 'id');
    }

    public function regiao()
    {
        return $this->belongsTo(Regiao::class, 'regiao_id', 'id');
    }

    public function notaFiscalEmissor()
    {
        return $this->hasMany(NotaFiscal::class, 'endereco_emissor_id', 'id');
    }

    public function notaFiscalDestinatario()
    {
        return $this->hasMany(NotaFiscal::class, 'endereco_destinatario_id', 'id');
    }
}
