<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected $table = 'cheques';

    public $timestamps = false;

    protected $dates = ['vencimento'];

    protected $fillable = ['predatado_id', 'numero', 'banco', 'agencia', 'conta', 'valor', 'vencimento', 'status'];

    public function predatado()
    {
        return $this->belongsTo(Predatado::class);
    }

    public function transacoes()
    {
        return $this->belongsToMany(Transacao::class, 'cheques_transacao', 'cheques_id', 'transacao_id')
            ->withPivot(['tipo_processamento']);
    }
}
