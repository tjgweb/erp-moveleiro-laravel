<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParcelaPagamento
 *
 * @property-read \App\Models\Bandeira $bandeira
 * @property-read \App\Models\TipoPagamento $tipoPagamento
 * @property-read \App\Models\Transacao $transacao
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $transacao_id
 * @property int $tipo_pagamento_id
 * @property int|null $bandeira_id
 * @property string|null $tipo_cartao
 * @property string|null $protocolo
 * @property float $valor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereBandeiraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereProtocolo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereTipoCartao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereTipoPagamentoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereTransacaoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParcelaPagamento whereValor($value)
 */
class ParcelaPagamento extends Model
{
    protected $table = 'parcela_pagamento';

    public $timestamps = false;

    protected $fillable = ['transacao_id', 'tipo_pagamento_id', 'bandeira_id', 'tipo_cartao', 'protocolo', 'valor'];

    public function transacao()
    {
        return $this->belongsTo(Transacao::class, 'transacao_id', 'id');
    }

    public function tipoPagamento()
    {
        return $this->belongsTo(TipoPagamento::class, 'tipo_pagamento_id', 'id');
    }

    public function bandeira()
    {
        return $this->belongsTo(Bandeira::class, 'bandeira_id', 'id');
    }

}
