<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Pessoa
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contato[] $contatos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Endereco[] $enderecos
 * @property-read \App\Models\Fisica $fisica
 * @property-read \App\Models\Funcionario $funcionario
 * @property-read \App\Models\Juridica $juridica
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notasFiscaisDestinatario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotaFiscal[] $notasFiscaisEmissor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Padrao[] $padrao
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MOdels\PosVenda[] $posVendas
 * @property-read \App\Models\TipoPessoa $tipoPessoa
 * @property-read \App\Models\Transportador $transportador
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venda[] $vendasCliente
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venda[] $vendasVendedor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $tipo_pessoa_id
 * @property string $nome
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pessoa whereTipoPessoaId($value)
 */
class Pessoa extends Model
{
    protected $table = 'pessoa';

    public $timestamps = false;

    protected $fillable = ['nome', 'tipo_pessoa_id'];

    protected $appends = ['contatos_string'];

    public function getContatosStringAttribute()
    {
        $string = '';

        if ($this->contatos()->count()) {
            $contatos = $this->contatos->map(function ($contato) {
                return $contato->numero;
            });
            $string = $contatos->implode(' | ');
        }

        $this->attributes['contatos_string'] = $string;

        return $this->attributes['contatos_string'];
    }

    public function padrao()
    {
        return $this->belongsToMany(Padrao::class, 'pessoa_padrao', 'pessoa_id', 'padrao_id');
    }

    public function tipoPessoa()
    {
        return $this->belongsTo(TipoPessoa::class, 'tipo_pessoa_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'pessoa_id', 'id');
    }

    public function contatos()
    {
        return $this->hasMany(Contato::class, 'pessoa_id', 'id');
    }

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'pessoa_id', 'id');
    }

    public function fisica()
    {
        return $this->hasOne(Fisica::class, 'pessoa_id', 'id');
    }

    public function juridica()
    {
        return $this->hasOne(Juridica::class, 'pessoa_id', 'id');
    }

    public function notasFiscaisEmissor()
    {
        return $this->hasMany(NotaFiscal::class, 'emissor_id', 'id');
    }

    public function notasFiscaisDestinatario()
    {
        return $this->hasMany(NotaFiscal::class, 'destinatario_id', 'id');
    }

    public function transportador()
    {
        return $this->hasOne(Transportador::class, 'pessoa_id', 'id');
    }

    public function funcionario()
    {
        return $this->hasOne(Funcionario::class, 'pessoa_id', 'id');
    }

    public function vendasCliente()
    {
        return $this->hasMany(Venda::class, 'cliente_id', 'id');
    }

    public function vendasVendedor()
    {
        return $this->hasMany(Venda::class, 'vendedor_id', 'id');
    }

    public function posVendas()
    {
        return $this->belongsToMany(PosVenda::class, 'pos_venda_pessoa', 'pessoa_id', 'pos_venda_id');
    }

    public function vencimentos()
    {
        return $this->hasMany(Vencimento::class, 'pessoa_id', 'id');
    }
}
