<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\TipoPessoa
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pessoa[] $pessoas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPessoa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPessoa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPessoa query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nome
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPessoa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoPessoa whereNome($value)
 */
class TipoPessoa extends Model
{
    protected $table = 'tipo_pessoa';

    public $timestamps = false;

    protected $fillable = ['nome'];

    public function pessoas()
    {
        return $this->hasMany(Pessoa::class, 'tipo_pessoa_id', 'id');
    }
}
