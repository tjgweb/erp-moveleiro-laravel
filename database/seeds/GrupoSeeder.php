<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupo')->truncate();

        DB::table('grupo')->insert([
            'nome' => 'Gerente',
            'descricao' => 'Tem acesso a todas as funcionalidades',
            'dashboard' => 'App\Http\Controllers\Gestao\DashboardController@index'
        ]);

        DB::table('grupo')->insert([
            'nome' => 'Vendedor',
            'descricao' => 'Vendedor loja',
            'dashboard' => 'App\Http\Controllers\Vendas\DashboardController@index'
        ]);

        DB::table('grupo')->insert([
            'nome' => 'Caixa',
            'descricao' => 'Caixa loja',
            'dashboard' => 'App\Http\Controllers\Caixa\DashboardController@index'
        ]);

        DB::table('grupo')->insert([
            'nome' => 'Almoxarife',
            'descricao' => 'Responsável pelo estoque, emissão de nota fiscal e entrega.',
            'dashboard' => 'App\Http\Controllers\Estoque\DashboardController@index'
        ]);

        DB::table('grupo')->insert([
            'nome' => 'Auxiliar Administrativo',
            'descricao' => 'Auxilia a Gerencia.',
            'dashboard' => 'App\Http\Controllers\Gestao\DashboardController@index'
        ]);
    }
}
