<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_venda')->truncate();

        DB::table('tipo_venda')->insert([
            'descricao' => 'Normal' // 1
        ]);
        DB::table('tipo_venda')->insert([
            'descricao' => 'Consumo interno' // 2
        ]);
        DB::table('tipo_venda')->insert([
            'descricao' => 'Doação' // 3
        ]);
    }
}
