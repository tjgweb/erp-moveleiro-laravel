<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'grupo_id' => 1,
            'apelido' => 'MidiaUai',
            'email' => 'contato@midiauai.com.br',
            'password' => bcrypt('123456'),
            'status' => 1
        ]);

        DB::table('users')->insert([
            'grupo_id' => 1,
            'apelido' => 'Gerente',
            'email' => 'brasaoloja@yahoo.com.br',
            'password' => bcrypt('123456'),
            'status' => 1
        ]);

        DB::table('users')->insert([
            'grupo_id' => 2,
            'pessoa_id' => 1,
            'apelido' => 'Vendedores',
            'email' => 'vendas@brasaomoveis.com.br',
            'password' => bcrypt('123456'),
            'status' => 1
        ]);
    }
}
