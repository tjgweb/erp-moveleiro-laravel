<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovimentoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movimento_tipo')->truncate();

        DB::table('movimento_tipo')->insert([
            'descricao' => 'Saída'
        ]);
        DB::table('movimento_tipo')->insert([
            'descricao' => 'Entrada'
        ]);
    }
}
