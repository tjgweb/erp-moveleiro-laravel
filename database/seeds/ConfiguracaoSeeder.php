<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuracoes')->truncate();

        DB::table('configuracoes')->insert([
            'id' => 1,
            'multa' => 2,
            'juros_dia' => 0.02,
            'tpAmb' => 2,
            'schemes' => 'PL_009_V4',
            'versao' => '4.00',
            'atualizacao' => date('Y-m-d H:i:s'),
            'cnpj_contador' => '27140332000128'
        ]);
    }
}
