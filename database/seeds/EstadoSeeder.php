<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('estado')->truncate();

        DB::statement("
            INSERT INTO `estado` (`id`, `pais_id`, `nome`, `uf`, `aliquota_icms_entrada`, `aliquota_icms_saida`, `aliquota_icms_entrada_cpf`, `aliquota_icms_saida_cpf`, `inscricao_substituicao_tributaria`, `codigo_ibge`, `aliquota_pobreza`) VALUES 
                (1, 1, 'Acre', 'AC', 0, 0, 0, 0, '0', 12, 0),
                (2, 1, 'Alagoas', 'AL', 0, 0, 0, 0, '0', 27, 0),
                (3, 1, 'Amazonas', 'AM', 0, 0, 0, 0, '0', 16, 0),
                (4, 1, 'Amapá', 'AP', 0, 0, 0, 0, '0', 13, 0),
                (5, 1, 'Bahia', 'BA', 0, 0, 0, 0, '0', 29, 0),
                (6, 1, 'Ceará', 'CE', 0, 0, 0, 0, '0', 23, 0),
                (7, 1, 'Distrito Federal', 'DF', 0, 0, 0, 0, '0', 53, 0),
                (8, 1, 'Espírito Santo', 'ES', 0, 0, 0, 0, '0', 32, 0),
                (9, 1, 'Goiás', 'GO', 0, 0, 0, 0, '0', 52, 0),
                (10, 1, 'Maranhão', 'MA', 0, 0, 0, 0, '0', 21, 0),
                (11, 1, 'Minas Gerais', 'MG', 0, 0, 0, 0, '0', 51, 0),
                (12, 1, 'Mato Grosso do Sul', 'MS', 0, 0, 0, 0, '0', 50, 0),
                (13, 1, 'Mato Grosso', 'MT', 0, 0, 0, 0, '0', 31, 0),
                (14, 1, 'Pará', 'PA', 0, 0, 0, 0, '0', 15, 0),
                (15, 1, 'Paraíba', 'PB', 0, 0, 0, 0, '0', 25, 0),
                (16, 1, 'Pernambuco', 'PE', 0, 0, 0, 0, '0', 41, 0),
                (17, 1, 'Piauí', 'PI', 0, 0, 0, 0, '0', 26, 0),
                (18, 1, 'Paraná', 'PR', 0, 0, 0, 0, '0', 22, 0),
                (19, 1, 'Rio de Janeiro', 'RJ', 0, 0, 0, 0, '0', 33, 0),
                (20, 1, 'Rio Grande do Norte', 'RN', 0, 0, 0, 0, '0', 24, 0),
                (21, 1, 'Rondônia', 'RO', 0, 0, 0, 0, '0', 43, 0),
                (22, 1, 'Roraima', 'RR', 0, 0, 0, 0, '0', 11, 0),
                (23, 1, 'Rio Grande do Sul', 'RS', 0, 0, 0, 0, '0', 14, 0),
                (24, 1, 'Santa Catarina', 'SC', 0, 0, 0, 0, '0', 42, 0),
                (25, 1, 'Sergipe', 'SE', 0, 0, 0, 0, '0', 35, 0),
                (26, 1, 'São Paulo', 'SP', 0, 0, 0, 0, '0', 28, 0),
                (27, 1, 'Tocantins', 'TO', 0, 0, 0, 0, '0', 17, 0);
        ");

    }
}
