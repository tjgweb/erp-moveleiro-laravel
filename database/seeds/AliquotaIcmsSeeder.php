<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AliquotaIcmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aliquota_icms')->truncate();
        DB::statement('
        INSERT INTO `aliquota_icms` (`id`, `origem_id`, `destino_id`, `aliquota`) VALUES
            (1, 1, 1, 17),
            (2, 1, 2, 12),
            (3, 1, 3, 12),
            (4, 1, 4, 12),
            (5, 1, 5, 12),
            (6, 1, 6, 12),
            (7, 1, 7, 12),
            (8, 1, 8, 12),
            (9, 1, 9, 12),
            (10, 1, 10, 12),
            (11, 1, 11, 12),
            (12, 1, 12, 12),
            (13, 1, 13, 12),
            (14, 1, 14, 12),
            (15, 1, 15, 12),
            (16, 1, 16, 12),
            (17, 1, 17, 12),
            (18, 1, 18, 12),
            (19, 1, 19, 12),
            (20, 1, 20, 12),
            (21, 1, 21, 12),
            (22, 1, 22, 12),
            (23, 1, 23, 12),
            (24, 1, 24, 12),
            (25, 1, 25, 12),
            (26, 1, 26, 12),
            (27, 1, 27, 12),
            (28, 2, 1, 12),
            (29, 2, 2, 17),
            (30, 2, 3, 12),
            (31, 2, 4, 12),
            (32, 2, 5, 12),
            (33, 2, 6, 12),
            (34, 2, 7, 12),
            (35, 2, 8, 12),
            (36, 2, 9, 12),
            (37, 2, 10, 12),
            (38, 2, 11, 12),
            (39, 2, 12, 12),
            (40, 2, 13, 12),
            (41, 2, 14, 12),
            (42, 2, 15, 12),
            (43, 2, 16, 12),
            (44, 2, 17, 12),
            (45, 2, 18, 12),
            (46, 2, 19, 12),
            (47, 2, 20, 12),
            (48, 2, 21, 12),
            (49, 2, 22, 12),
            (50, 2, 23, 12),
            (51, 2, 24, 12),
            (52, 2, 25, 12),
            (53, 2, 26, 12),
            (54, 2, 27, 12),
            (55, 3, 1, 12),
            (56, 3, 2, 12),
            (57, 3, 3, 17),
            (58, 3, 4, 12),
            (59, 3, 5, 12),
            (60, 3, 6, 12),
            (61, 3, 7, 12),
            (62, 3, 8, 12),
            (63, 3, 9, 12),
            (64, 3, 10, 12),
            (65, 3, 11, 12),
            (66, 3, 12, 12),
            (67, 3, 13, 12),
            (68, 3, 14, 12),
            (69, 3, 15, 12),
            (70, 3, 16, 12),
            (71, 3, 17, 12),
            (72, 3, 18, 12),
            (73, 3, 19, 12),
            (74, 3, 20, 12),
            (75, 3, 21, 12),
            (76, 3, 22, 12),
            (77, 3, 23, 12),
            (78, 3, 24, 12),
            (79, 3, 25, 12),
            (80, 3, 26, 12),
            (81, 3, 27, 12),
            (82, 4, 1, 12),
            (83, 4, 2, 12),
            (84, 4, 3, 12),
            (85, 4, 4, 17),
            (86, 4, 5, 12),
            (87, 4, 6, 12),
            (88, 4, 7, 12),
            (89, 4, 8, 12),
            (90, 4, 9, 12),
            (91, 4, 10, 12),
            (92, 4, 11, 12),
            (93, 4, 12, 12),
            (94, 4, 13, 12),
            (95, 4, 14, 12),
            (96, 4, 15, 12),
            (97, 4, 16, 12),
            (98, 4, 17, 12),
            (99, 4, 18, 12),
            (100, 4, 19, 12),
            (101, 4, 20, 12),
            (102, 4, 21, 12),
            (103, 4, 22, 12),
            (104, 4, 23, 12),
            (105, 4, 24, 12),
            (106, 4, 25, 12),
            (107, 4, 26, 12),
            (108, 4, 27, 12),
            (109, 5, 1, 12),
            (110, 5, 2, 12),
            (111, 5, 3, 12),
            (112, 5, 4, 12),
            (113, 5, 5, 17),
            (114, 5, 6, 12),
            (115, 5, 7, 12),
            (116, 5, 8, 12),
            (117, 5, 9, 12),
            (118, 5, 10, 12),
            (119, 5, 11, 12),
            (120, 5, 12, 12),
            (121, 5, 13, 12),
            (122, 5, 14, 12),
            (123, 5, 15, 12),
            (124, 5, 16, 12),
            (125, 5, 17, 12),
            (126, 5, 18, 12),
            (127, 5, 19, 12),
            (128, 5, 20, 12),
            (129, 5, 21, 12),
            (130, 5, 22, 12),
            (131, 5, 23, 12),
            (132, 5, 24, 12),
            (133, 5, 25, 12),
            (134, 5, 26, 12),
            (135, 5, 27, 12),
            (136, 6, 1, 12),
            (137, 6, 2, 12),
            (138, 6, 3, 12),
            (139, 6, 4, 12),
            (140, 6, 5, 12),
            (141, 6, 6, 17),
            (142, 6, 7, 12),
            (143, 6, 8, 12),
            (144, 6, 9, 12),
            (145, 6, 10, 12),
            (146, 6, 11, 12),
            (147, 6, 12, 12),
            (148, 6, 13, 12),
            (149, 6, 14, 12),
            (150, 6, 15, 12),
            (151, 6, 16, 12),
            (152, 6, 17, 12),
            (153, 6, 18, 12),
            (154, 6, 19, 12),
            (155, 6, 20, 12),
            (156, 6, 21, 12),
            (157, 6, 22, 12),
            (158, 6, 23, 12),
            (159, 6, 24, 12),
            (160, 6, 25, 12),
            (161, 6, 26, 12),
            (162, 6, 27, 12),
            (163, 7, 1, 12),
            (164, 7, 2, 12),
            (165, 7, 3, 12),
            (166, 7, 4, 12),
            (167, 7, 5, 12),
            (168, 7, 6, 12),
            (169, 7, 7, 17),
            (170, 7, 8, 12),
            (171, 7, 9, 12),
            (172, 7, 10, 12),
            (173, 7, 11, 12),
            (174, 7, 12, 12),
            (175, 7, 13, 12),
            (176, 7, 14, 12),
            (177, 7, 15, 12),
            (178, 7, 16, 12),
            (179, 7, 17, 12),
            (180, 7, 18, 12),
            (181, 7, 19, 12),
            (182, 7, 20, 12),
            (183, 7, 21, 12),
            (184, 7, 22, 12),
            (185, 7, 23, 12),
            (186, 7, 24, 12),
            (187, 7, 25, 12),
            (188, 7, 26, 12),
            (189, 7, 27, 12),
            (190, 8, 1, 12),
            (191, 8, 2, 12),
            (192, 8, 3, 12),
            (193, 8, 4, 12),
            (194, 8, 5, 12),
            (195, 8, 6, 12),
            (196, 8, 7, 12),
            (197, 8, 8, 17),
            (198, 8, 9, 12),
            (199, 8, 10, 12),
            (200, 8, 11, 12),
            (201, 8, 12, 12),
            (202, 8, 13, 12),
            (203, 8, 14, 12),
            (204, 8, 15, 12),
            (205, 8, 16, 12),
            (206, 8, 17, 12),
            (207, 8, 18, 12),
            (208, 8, 19, 12),
            (209, 8, 20, 12),
            (210, 8, 21, 12),
            (211, 8, 22, 12),
            (212, 8, 23, 12),
            (213, 8, 24, 12),
            (214, 8, 25, 12),
            (215, 8, 26, 12),
            (216, 8, 27, 12),
            (217, 9, 1, 12),
            (218, 9, 2, 12),
            (219, 9, 3, 12),
            (220, 9, 4, 12),
            (221, 9, 5, 12),
            (222, 9, 6, 12),
            (223, 9, 7, 12),
            (224, 9, 8, 12),
            (225, 9, 9, 17),
            (226, 9, 10, 12),
            (227, 9, 11, 12),
            (228, 9, 12, 12),
            (229, 9, 13, 12),
            (230, 9, 14, 12),
            (231, 9, 15, 12),
            (232, 9, 16, 12),
            (233, 9, 17, 12),
            (234, 9, 18, 12),
            (235, 9, 19, 12),
            (236, 9, 20, 12),
            (237, 9, 21, 12),
            (238, 9, 22, 12),
            (239, 9, 23, 12),
            (240, 9, 24, 12),
            (241, 9, 25, 12),
            (242, 9, 26, 12),
            (243, 9, 27, 12),
            (244, 10, 1, 12),
            (245, 10, 2, 12),
            (246, 10, 3, 12),
            (247, 10, 4, 12),
            (248, 10, 5, 12),
            (249, 10, 6, 12),
            (250, 10, 7, 12),
            (251, 10, 8, 12),
            (252, 10, 9, 12),
            (253, 10, 10, 17),
            (254, 10, 11, 12),
            (255, 10, 12, 12),
            (256, 10, 13, 12),
            (257, 10, 14, 12),
            (258, 10, 15, 12),
            (259, 10, 16, 12),
            (260, 10, 17, 12),
            (261, 10, 18, 12),
            (262, 10, 19, 12),
            (263, 10, 20, 12),
            (264, 10, 21, 12),
            (265, 10, 22, 12),
            (266, 10, 23, 12),
            (267, 10, 24, 12),
            (268, 10, 25, 12),
            (269, 10, 26, 12),
            (270, 10, 27, 12),
            (271, 11, 1, 7),
            (272, 11, 2, 7),
            (273, 11, 3, 7),
            (274, 11, 4, 7),
            (275, 11, 5, 7),
            (276, 11, 6, 7),
            (277, 11, 7, 7),
            (278, 11, 8, 7),
            (279, 11, 9, 7),
            (280, 11, 10, 7),
            (281, 11, 11, 18),
            (282, 11, 12, 7),
            (283, 11, 13, 7),
            (284, 11, 14, 7),
            (285, 11, 15, 7),
            (286, 11, 16, 7),
            (287, 11, 17, 7),
            (288, 11, 18, 12),
            (289, 11, 19, 12),
            (290, 11, 20, 7),
            (291, 11, 21, 7),
            (292, 11, 22, 7),
            (293, 11, 23, 12),
            (294, 11, 24, 12),
            (295, 11, 25, 7),
            (296, 11, 26, 12),
            (297, 11, 27, 7),
            (298, 12, 1, 12),
            (299, 12, 2, 12),
            (300, 12, 3, 12),
            (301, 12, 4, 12),
            (302, 12, 5, 12),
            (303, 12, 6, 12),
            (304, 12, 7, 12),
            (305, 12, 8, 12),
            (306, 12, 9, 12),
            (307, 12, 10, 12),
            (308, 12, 11, 12),
            (309, 12, 12, 17),
            (310, 12, 13, 12),
            (311, 12, 14, 12),
            (312, 12, 15, 12),
            (313, 12, 16, 12),
            (314, 12, 17, 12),
            (315, 12, 18, 12),
            (316, 12, 19, 12),
            (317, 12, 20, 12),
            (318, 12, 21, 12),
            (319, 12, 22, 12),
            (320, 12, 23, 12),
            (321, 12, 24, 12),
            (322, 12, 25, 12),
            (323, 12, 26, 12),
            (324, 12, 27, 12),
            (325, 13, 1, 12),
            (326, 13, 2, 12),
            (327, 13, 3, 12),
            (328, 13, 4, 12),
            (329, 13, 5, 12),
            (330, 13, 6, 12),
            (331, 13, 7, 12),
            (332, 13, 8, 12),
            (333, 13, 9, 12),
            (334, 13, 10, 12),
            (335, 13, 11, 12),
            (336, 13, 12, 12),
            (337, 13, 13, 17),
            (338, 13, 14, 12),
            (339, 13, 15, 12),
            (340, 13, 16, 12),
            (341, 13, 17, 12),
            (342, 13, 18, 12),
            (343, 13, 19, 12),
            (344, 13, 20, 12),
            (345, 13, 21, 12),
            (346, 13, 22, 12),
            (347, 13, 23, 12),
            (348, 13, 24, 12),
            (349, 13, 25, 12),
            (350, 13, 26, 12),
            (351, 13, 27, 12),
            (352, 14, 1, 12),
            (353, 14, 2, 12),
            (354, 14, 3, 12),
            (355, 14, 4, 12),
            (356, 14, 5, 12),
            (357, 14, 6, 12),
            (358, 14, 7, 12),
            (359, 14, 8, 12),
            (360, 14, 9, 12),
            (361, 14, 10, 12),
            (362, 14, 11, 12),
            (363, 14, 12, 12),
            (364, 14, 13, 12),
            (365, 14, 14, 17),
            (366, 14, 15, 12),
            (367, 14, 16, 12),
            (368, 14, 17, 12),
            (369, 14, 18, 12),
            (370, 14, 19, 12),
            (371, 14, 20, 12),
            (372, 14, 21, 12),
            (373, 14, 22, 12),
            (374, 14, 23, 12),
            (375, 14, 24, 12),
            (376, 14, 25, 12),
            (377, 14, 26, 12),
            (378, 14, 27, 12),
            (379, 15, 1, 12),
            (380, 15, 2, 12),
            (381, 15, 3, 12),
            (382, 15, 4, 12),
            (383, 15, 5, 12),
            (384, 15, 6, 12),
            (385, 15, 7, 12),
            (386, 15, 8, 12),
            (387, 15, 9, 12),
            (388, 15, 10, 12),
            (389, 15, 11, 12),
            (390, 15, 12, 12),
            (391, 15, 13, 12),
            (392, 15, 14, 12),
            (393, 15, 15, 18),
            (394, 15, 16, 12),
            (395, 15, 17, 12),
            (396, 15, 18, 12),
            (397, 15, 19, 12),
            (398, 15, 20, 12),
            (399, 15, 21, 12),
            (400, 15, 22, 12),
            (401, 15, 23, 12),
            (402, 15, 24, 12),
            (403, 15, 25, 12),
            (404, 15, 26, 12),
            (405, 15, 27, 12),
            (406, 16, 1, 12),
            (407, 16, 2, 12),
            (408, 16, 3, 12),
            (409, 16, 4, 12),
            (410, 16, 5, 12),
            (411, 16, 6, 12),
            (412, 16, 7, 12),
            (413, 16, 8, 12),
            (414, 16, 9, 12),
            (415, 16, 10, 12),
            (416, 16, 11, 12),
            (417, 16, 12, 12),
            (418, 16, 13, 12),
            (419, 16, 14, 12),
            (420, 16, 15, 12),
            (421, 16, 16, 18),
            (422, 16, 17, 12),
            (423, 16, 18, 12),
            (424, 16, 19, 12),
            (425, 16, 20, 12),
            (426, 16, 21, 12),
            (427, 16, 22, 12),
            (428, 16, 23, 12),
            (429, 16, 24, 12),
            (430, 16, 25, 12),
            (431, 16, 26, 12),
            (432, 16, 27, 12),
            (433, 17, 1, 12),
            (434, 17, 2, 12),
            (435, 17, 3, 12),
            (436, 17, 4, 12),
            (437, 17, 5, 12),
            (438, 17, 6, 12),
            (439, 17, 7, 12),
            (440, 17, 8, 12),
            (441, 17, 9, 12),
            (442, 17, 10, 12),
            (443, 17, 11, 12),
            (444, 17, 12, 12),
            (445, 17, 13, 12),
            (446, 17, 14, 12),
            (447, 17, 15, 12),
            (448, 17, 16, 12),
            (449, 17, 17, 18),
            (450, 17, 18, 12),
            (451, 17, 19, 12),
            (452, 17, 20, 12),
            (453, 17, 21, 12),
            (454, 17, 22, 12),
            (455, 17, 23, 12),
            (456, 17, 24, 12),
            (457, 17, 25, 12),
            (458, 17, 26, 12),
            (459, 17, 27, 12),
            (460, 18, 1, 7),
            (461, 18, 2, 7),
            (462, 18, 3, 7),
            (463, 18, 4, 7),
            (464, 18, 5, 7),
            (465, 18, 6, 7),
            (466, 18, 7, 7),
            (467, 18, 8, 7),
            (468, 18, 9, 7),
            (469, 18, 10, 7),
            (470, 18, 11, 12),
            (471, 18, 12, 7),
            (472, 18, 13, 7),
            (473, 18, 14, 7),
            (474, 18, 15, 7),
            (475, 18, 16, 7),
            (476, 18, 17, 7),
            (477, 18, 18, 18),
            (478, 18, 19, 12),
            (479, 18, 20, 7),
            (480, 18, 21, 7),
            (481, 18, 22, 7),
            (482, 18, 23, 12),
            (483, 18, 24, 12),
            (484, 18, 25, 7),
            (485, 18, 26, 12),
            (486, 18, 27, 7),
            (487, 19, 1, 7),
            (488, 19, 2, 7),
            (489, 19, 3, 7),
            (490, 19, 4, 7),
            (491, 19, 5, 7),
            (492, 19, 6, 7),
            (493, 19, 7, 7),
            (494, 19, 8, 7),
            (495, 19, 9, 7),
            (496, 19, 10, 7),
            (497, 19, 11, 12),
            (498, 19, 12, 7),
            (499, 19, 13, 7),
            (500, 19, 14, 7),
            (501, 19, 15, 7),
            (502, 19, 16, 7),
            (503, 19, 17, 7),
            (504, 19, 18, 12),
            (505, 19, 19, 19),
            (506, 19, 20, 7),
            (507, 19, 21, 7),
            (508, 19, 22, 7),
            (509, 19, 23, 12),
            (510, 19, 24, 12),
            (511, 19, 25, 7),
            (512, 19, 26, 12),
            (513, 19, 27, 7),
            (514, 20, 1, 12),
            (515, 20, 2, 12),
            (516, 20, 3, 12),
            (517, 20, 4, 12),
            (518, 20, 5, 12),
            (519, 20, 6, 12),
            (520, 20, 7, 12),
            (521, 20, 8, 12),
            (522, 20, 9, 12),
            (523, 20, 10, 12),
            (524, 20, 11, 12),
            (525, 20, 12, 12),
            (526, 20, 13, 12),
            (527, 20, 14, 12),
            (528, 20, 15, 12),
            (529, 20, 16, 12),
            (530, 20, 17, 12),
            (531, 20, 18, 12),
            (532, 20, 19, 12),
            (533, 20, 20, 18),
            (534, 20, 21, 12),
            (535, 20, 22, 12),
            (536, 20, 23, 12),
            (537, 20, 24, 12),
            (538, 20, 25, 12),
            (539, 20, 26, 12),
            (540, 20, 27, 12),
            (541, 21, 1, 12),
            (542, 21, 2, 12),
            (543, 21, 3, 12),
            (544, 21, 4, 12),
            (545, 21, 5, 12),
            (546, 21, 6, 12),
            (547, 21, 7, 12),
            (548, 21, 8, 12),
            (549, 21, 9, 12),
            (550, 21, 10, 12),
            (551, 21, 11, 12),
            (552, 21, 12, 12),
            (553, 21, 13, 12),
            (554, 21, 14, 12),
            (555, 21, 15, 12),
            (556, 21, 16, 12),
            (557, 21, 17, 12),
            (558, 21, 18, 12),
            (559, 21, 19, 12),
            (560, 21, 20, 12),
            (561, 21, 21, 17),
            (562, 21, 22, 12),
            (563, 21, 23, 12),
            (564, 21, 24, 12),
            (565, 21, 25, 12),
            (566, 21, 26, 12),
            (567, 21, 27, 12),
            (568, 22, 1, 12),
            (569, 22, 2, 12),
            (570, 22, 3, 12),
            (571, 22, 4, 12),
            (572, 22, 5, 12),
            (573, 22, 6, 12),
            (574, 22, 7, 12),
            (575, 22, 8, 12),
            (576, 22, 9, 12),
            (577, 22, 10, 12),
            (578, 22, 11, 12),
            (579, 22, 12, 12),
            (580, 22, 13, 12),
            (581, 22, 14, 12),
            (582, 22, 15, 12),
            (583, 22, 16, 12),
            (584, 22, 17, 12),
            (585, 22, 18, 12),
            (586, 22, 19, 12),
            (587, 22, 20, 12),
            (588, 22, 21, 12),
            (589, 22, 22, 17),
            (590, 22, 23, 12),
            (591, 22, 24, 12),
            (592, 22, 25, 12),
            (593, 22, 26, 12),
            (594, 22, 27, 12),
            (595, 23, 1, 7),
            (596, 23, 2, 7),
            (597, 23, 3, 7),
            (598, 23, 4, 7),
            (599, 23, 5, 7),
            (600, 23, 6, 7),
            (601, 23, 7, 7),
            (602, 23, 8, 7),
            (603, 23, 9, 7),
            (604, 23, 10, 7),
            (605, 23, 11, 12),
            (606, 23, 12, 7),
            (607, 23, 13, 7),
            (608, 23, 14, 7),
            (609, 23, 15, 7),
            (610, 23, 16, 7),
            (611, 23, 17, 7),
            (612, 23, 18, 12),
            (613, 23, 19, 12),
            (614, 23, 20, 7),
            (615, 23, 21, 7),
            (616, 23, 22, 7),
            (617, 23, 23, 17),
            (618, 23, 24, 12),
            (619, 23, 25, 7),
            (620, 23, 26, 12),
            (621, 23, 27, 7),
            (622, 24, 1, 7),
            (623, 24, 2, 7),
            (624, 24, 3, 7),
            (625, 24, 4, 7),
            (626, 24, 5, 7),
            (627, 24, 6, 7),
            (628, 24, 7, 7),
            (629, 24, 8, 7),
            (630, 24, 9, 7),
            (631, 24, 10, 7),
            (632, 24, 11, 12),
            (633, 24, 12, 7),
            (634, 24, 13, 7),
            (635, 24, 14, 7),
            (636, 24, 15, 7),
            (637, 24, 16, 7),
            (638, 24, 17, 7),
            (639, 24, 18, 12),
            (640, 24, 19, 12),
            (641, 24, 20, 7),
            (642, 24, 21, 7),
            (643, 24, 22, 7),
            (644, 24, 23, 12),
            (645, 24, 24, 17),
            (646, 24, 25, 7),
            (647, 24, 26, 12),
            (648, 24, 27, 7),
            (649, 25, 1, 12),
            (650, 25, 2, 12),
            (651, 25, 3, 12),
            (652, 25, 4, 12),
            (653, 25, 5, 12),
            (654, 25, 6, 12),
            (655, 25, 7, 12),
            (656, 25, 8, 12),
            (657, 25, 9, 12),
            (658, 25, 10, 12),
            (659, 25, 11, 12),
            (660, 25, 12, 12),
            (661, 25, 13, 12),
            (662, 25, 14, 12),
            (663, 25, 15, 12),
            (664, 25, 16, 12),
            (665, 25, 17, 12),
            (666, 25, 18, 12),
            (667, 25, 19, 12),
            (668, 25, 20, 12),
            (669, 25, 21, 12),
            (670, 25, 22, 12),
            (671, 25, 23, 12),
            (672, 25, 24, 12),
            (673, 25, 25, 17),
            (674, 25, 26, 12),
            (675, 25, 27, 12),
            (676, 26, 1, 7),
            (677, 26, 2, 7),
            (678, 26, 3, 7),
            (679, 26, 4, 7),
            (680, 26, 5, 7),
            (681, 26, 6, 7),
            (682, 26, 7, 7),
            (683, 26, 8, 7),
            (684, 26, 9, 7),
            (685, 26, 10, 7),
            (686, 26, 11, 12),
            (687, 26, 12, 7),
            (688, 26, 13, 7),
            (689, 26, 14, 7),
            (690, 26, 15, 7),
            (691, 26, 16, 7),
            (692, 26, 17, 7),
            (693, 26, 18, 12),
            (694, 26, 19, 12),
            (695, 26, 20, 7),
            (696, 26, 21, 7),
            (697, 26, 22, 7),
            (698, 26, 23, 12),
            (699, 26, 24, 12),
            (700, 26, 25, 7),
            (701, 26, 26, 18),
            (702, 26, 27, 7),
            (703, 27, 1, 12),
            (704, 27, 2, 12),
            (705, 27, 3, 12),
            (706, 27, 4, 12),
            (707, 27, 5, 12),
            (708, 27, 6, 12),
            (709, 27, 7, 12),
            (710, 27, 8, 12),
            (711, 27, 9, 12),
            (712, 27, 10, 12),
            (713, 27, 11, 12),
            (714, 27, 12, 12),
            (715, 27, 13, 12),
            (716, 27, 14, 12),
            (717, 27, 15, 12),
            (718, 27, 16, 12),
            (719, 27, 17, 12),
            (720, 27, 18, 12),
            (721, 27, 19, 12),
            (722, 27, 20, 12),
            (723, 27, 21, 12),
            (724, 27, 22, 12),
            (725, 27, 23, 12),
            (726, 27, 24, 12),
            (727, 27, 25, 12),
            (728, 27, 26, 12),
            (729, 27, 27, 17);
        ');
    }
}
