<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BandeiraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bandeira')->truncate();

        DB::table('bandeira')->insert([
            'codigo' => '01',
            'descricao' => 'Visa'
        ]);
        DB::table('bandeira')->insert([
            'codigo' => '02',
            'descricao' => 'Mastercard'
        ]);
        DB::table('bandeira')->insert([
            'codigo' => '03',
            'descricao' => 'American Express'
        ]);
        DB::table('bandeira')->insert([
            'codigo' => '04',
            'descricao' => 'Sorocred'
        ]);
        DB::table('bandeira')->insert([
            'codigo' => '99',
            'descricao' => 'Outros'
        ]);
    }
}
