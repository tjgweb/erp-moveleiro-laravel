<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnidadeMedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidade_medida')->truncate();

        DB::table('unidade_medida')->insert([
            'unidade' => 'KG',
            'descricao' => 'QUILOGRAMA', // 1
            'agrupado' => 0
        ]);
        DB::table('unidade_medida')->insert([
            'unidade' => 'M',
            'descricao' => 'METRO', // 2
            'agrupado' => 0
        ]);
        DB::table('unidade_medida')->insert([
            'unidade' => 'CM',
            'descricao' => 'CENTÍMETRO', // 3
            'agrupado' => 0
        ]);
        DB::table('unidade_medida')->insert([
            'unidade' => 'UN',
            'descricao' => 'UNIDADE', // 4
            'agrupado' => 0
        ]);
        DB::table('unidade_medida')->insert([
            'unidade' => 'CX',
            'descricao' => 'CAIXA', // 5
            'agrupado' => 1
        ]);
    }
}
