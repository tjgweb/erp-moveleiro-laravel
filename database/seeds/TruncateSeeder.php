<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruncateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Rodar essa seed de forma individual
         * php artisan db:seed --class=TruncateSeeder
         */

        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); // mysql e MariaDB
        //DB::statement('PRAGMA foreign_keys = OFF;'); // sqlite

        DB::table('caixa')->truncate();
        DB::table('cartao')->truncate();
        DB::table('cheques')->truncate();
        DB::table('cheques_transacao')->truncate();
        DB::table('crediario')->truncate();
        DB::table('credito')->truncate();
        DB::table('dinheiro')->truncate();
        DB::table('financiamento')->truncate();
        DB::table('parcela_pagamento')->truncate();
        DB::table('parcela_transacao')->truncate();
        DB::table('pos_venda')->truncate();
        DB::table('pos_venda_movimento_produto')->truncate();
        DB::table('pos_venda_pessoa')->truncate();
        DB::table('predatado')->truncate();
        DB::table('produto_venda')->truncate();
        DB::table('retirada')->truncate();
        DB::table('transacao')->truncate();
        DB::table('vencimento')->truncate();
        DB::table('venda')->truncate();
        DB::table('venda_pagamento')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;'); // mysql e MariaDB
        // DB::statement('PRAGMA foreign_keys = ON;'); //sqlite
    }
}
