<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estatus_venda')->truncate();


        DB::table('estatus_venda')->insert([
            'descricao' => 'Pré Venda', // 1
            'produto_reservado' => true
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Concluída', // 2
            'produto_reservado' => false
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Aguardando autorização', // 3
            'produto_reservado' => true
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Não autorizada', // 4
            'produto_reservado' => false
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Cancelada', // 5
            'produto_reservado' => false
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Paga', // 6
            'produto_reservado' => true
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Nota Fiscal Preparada', // 7
            'produto_reservado' => false
        ]);
        DB::table('estatus_venda')->insert([
            'descricao' => 'Devolução', // 8
            'produto_reservado' => false
        ]);
    }
}
