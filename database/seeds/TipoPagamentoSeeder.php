<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pagamento')->truncate();

        DB::table('tipo_pagamento')->insert([
            'id' => 1,
            'codigo' => '01',
            'descricao' => 'Dinheiro',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 2,
            'codigo' => '02',
            'descricao' => 'Cheque',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 3,
            'codigo' => '03',
            'descricao' => 'Cartão de Crédito',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 4,
            'codigo' => '04',
            'descricao' => 'Cartão de Débito',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 5,
            'codigo' => '05',
            'descricao' => 'Crédito Loja',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 6,
            'codigo' => '99',
            'descricao' => 'Financeira',
            'ativo' => 1
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 7,
            'codigo' => '10',
            'descricao' => 'Vale Alimentação',
            'ativo' => 0
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 8,
            'codigo' => '11',
            'descricao' => 'Vale Refeição',
            'ativo' => 0
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 9,
            'codigo' => '12',
            'descricao' => 'Vale Presente',
            'ativo' => 0
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 10,
            'codigo' => '13',
            'descricao' => 'Vale Combustível',
            'ativo' => 0
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 11,
            'codigo' => '99',
            'descricao' => 'Outros',
            'ativo' => 0
        ]);



        /*DB::table('tipo_pagamento')->insert([
            'id' => 1,
            'descricao' => 'Dinheiro'
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 2,
            'descricao' => 'Cartão'
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 3,
            'descricao' => 'Financeira'
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 4,
            'descricao' => 'Crediário'
        ]);
        DB::table('tipo_pagamento')->insert([
            'id' => 5,
            'descricao' => 'Cheque'
        ]);*/
    }
}
