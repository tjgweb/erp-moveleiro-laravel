<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LojaPessoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('pessoa')->truncate();

        /* Loja */
        $loja = \App\Models\Pessoa::create([
            'nome' => 'Brasão Loja Matris',
            'tipo_pessoa_id' => 2
        ]);

        $loja->padrao()->attach([1, 2, 4]);

        \App\Models\Juridica::create([
            'pessoa_id' => $loja->id,
            'cnpj' => '02472197000197',
            'razao_social' => 'Brasão Center Móveis LTDA',
            'inscricao_estadual' => '6867356730072',
            'nome_contato' => 'Leandro',
            'endereco_eletronico' => 'brasaoloja@yahoo.com.br'
        ]);

        \App\Models\Transportador::create([
            'pessoa_id' => $loja->id
        ]);

        \App\Models\Funcionario::create([
            'pessoa_id' => $loja->id,
            'status' => 1
        ]);

        \App\Models\Endereco::create([
            'pessoa_id' => $loja->id,
            'estado_id' => 11,
            'cidade_id' => 2370,
            'titulo' => 'Loja',
            'logradouro' => 'Rua Engenheiro Argolo',
            'bairro' => 'Centro',
            'numero' => '247',
            'cep' => '39802-034',
            'principal' => 1
        ]);

        \App\Models\Contato::create([
            'pessoa_id' => $loja->id,
            'descricao' => 'Comercial',
            'numero' => '(33) 3522-3199',
            'responsavel' => 'Leandro',
            'principal' => 1,
            'status' => 1
        ]);

        /* Depósito 1 */
        $deposito1 = \App\Models\Pessoa::create([
            'nome' => 'Brasão Depósito 1',
            'tipo_pessoa_id' => 2
        ]);

        $deposito1->padrao()->attach([1, 2, 4]);

        \App\Models\Juridica::create([
            'pessoa_id' => $deposito1->id,
            'cnpj' => '02472197000198',
            'razao_social' => 'Brasão Center Móveis LTDA',
            'inscricao_estadual' => '6867356730073',
            'nome_contato' => 'Leandro',
            'endereco_eletronico' => 'brasaoloja@yahoo.com.br'
        ]);

        \App\Models\Transportador::create([
            'pessoa_id' => $deposito1->id
        ]);

        \App\Models\Funcionario::create([
            'pessoa_id' => $deposito1->id,
            'status' => 1
        ]);

        \App\Models\Endereco::create([
            'pessoa_id' => $deposito1->id,
            'estado_id' => 11,
            'cidade_id' => 2370,
            'titulo' => 'Depósito 1',
            'logradouro' => 'Rua Engenheiro Argolo',
            'bairro' => 'Centro',
            'numero' => '247',
            'cep' => '39802-034',
            'principal' => 1
        ]);

        \App\Models\Contato::create([
            'pessoa_id' => $deposito1->id,
            'descricao' => 'Comercial',
            'numero' => '(33) 3522-3199',
            'responsavel' => 'Leandro',
            'principal' => 1,
            'status' => 1
        ]);

        /* Depósito 2 */
        $deposito2 = \App\Models\Pessoa::create([
            'nome' => 'Brasão Depósito 2',
            'tipo_pessoa_id' => 2
        ]);

        $deposito2->padrao()->attach([1, 2, 4]);

        \App\Models\Juridica::create([
            'pessoa_id' => $deposito2->id,
            'cnpj' => '02472197000199',
            'razao_social' => 'Brasão Center Móveis LTDA',
            'inscricao_estadual' => '6867356730074',
            'nome_contato' => 'Leandro',
            'endereco_eletronico' => 'brasaoloja@yahoo.com.br'
        ]);

        \App\Models\Transportador::create([
            'pessoa_id' => $deposito2->id
        ]);

        \App\Models\Funcionario::create([
            'pessoa_id' => $deposito2->id,
            'status' => 1
        ]);

        \App\Models\Endereco::create([
            'pessoa_id' => $deposito2->id,
            'estado_id' => 11,
            'cidade_id' => 2370,
            'titulo' => 'Depósito 2',
            'logradouro' => 'Rua Engenheiro Argolo',
            'bairro' => 'Centro',
            'numero' => '247',
            'cep' => '39802-034',
            'principal' => 1
        ]);

        \App\Models\Contato::create([
            'pessoa_id' => $deposito2->id,
            'descricao' => 'Comercial',
            'numero' => '(33) 3522-3199',
            'responsavel' => 'Leandro',
            'principal' => 1,
            'status' => 1
        ]);
    }
}
