<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPessoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tipo_pessoa')->truncate();

        DB::table('tipo_pessoa')->insert([
            'nome' => 'Física'
        ]);

        DB::table('tipo_pessoa')->insert([
            'nome' => 'Jurídica'
        ]);

    }
}
