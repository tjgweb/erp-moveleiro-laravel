<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VencimentoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vencimento_tipo')->truncate();

        DB::table('vencimento_tipo')->insert([
            'nome' => 'Salário'
        ]);
        DB::table('vencimento_tipo')->insert([
            'nome' => 'Comissão'
        ]);
        DB::table('vencimento_tipo')->insert([
            'nome' => 'Vale'
        ]);
    }
}
