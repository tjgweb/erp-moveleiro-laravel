<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoIdentidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_identidade')->truncate();

        DB::table('tipo_identidade')->insert([ // 1
            'titulo' => 'Registro Geral',
            'sigla_identidade' => 'RG',
            'emissor' => 'Secretaria de Segurança Pública',
            'sigla' => 'SSP'
        ]);
    }
}
