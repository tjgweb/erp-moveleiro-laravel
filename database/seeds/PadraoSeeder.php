<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PadraoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('padrao')->truncate();

        DB::table('padrao')->insert([
            'descricao' => 'Cliente'
        ]);
        DB::table('padrao')->insert([
            'descricao' => 'Funcionário'
        ]);
        DB::table('padrao')->insert([
            'descricao' => 'Fornecedor'
        ]);
        DB::table('padrao')->insert([
            'descricao' => 'Transportador'
        ]);
        DB::table('padrao')->insert([
            'descricao' => 'Prestador Serviço'
        ]);
    }
}
