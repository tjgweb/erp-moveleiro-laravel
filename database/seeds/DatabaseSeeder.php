<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); // mysql e MariaDB
        //DB::statement('PRAGMA foreign_keys = OFF;'); // sqlite

        $this->call(AliquotaIcmsSeeder::class);
        $this->call(BandeiraSeeder::class);
        $this->call(CidadeSeeder::class);
        $this->call(ConfiguracaoSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(EstatusVendaSeeder::class);
        $this->call(GrupoSeeder::class);
        $this->call(MovimentoTipoSeeder::class);
        $this->call(NaturezaOperacaoSeeder::class);
        $this->call(PadraoSeeder::class);
        $this->call(TipoIdentidadeSeeder::class);
        $this->call(TipoPagamentoSeeder::class);
        $this->call(TipoPessoaSeeder::class);
        $this->call(TipoPosVendaSeeder::class);
        $this->call(TipoVendaSeeder::class);
        $this->call(UnidadeMedidaSeeder::class);
        $this->call(VencimentoTipoSeeder::class);

        //$this->call(LojaPessoaSeeder::class); // cuidado reseta toda tabela pessoa
        //$this->call(UsersSeeder::class); // cuidado reseta toda tabela users

        DB::statement('SET FOREIGN_KEY_CHECKS=1;'); // mysql e MariaDB
        // DB::statement('PRAGMA foreign_keys = ON;'); //sqlite
    }
}
