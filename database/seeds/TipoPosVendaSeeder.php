<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPosVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pos_venda')->truncate();

        DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Retirada' // 1
        ]);
        DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Entrega' // 2
        ]);
        DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Troca' // 3
        ]);
        DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Montagem' // 4
        ]);
        DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Outros' // 5
        ]);
        /*DB::table('tipo_pos_venda')->insert([
            'descricao' => 'Assistência'
        ]);*/
    }
}
