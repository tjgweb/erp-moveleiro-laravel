<?php

Auth::routes();

/* Grupo de rotas protegidas por autenticação, Verificação de usuário ativo e ACL. */
Route::middleware(['auth', 'check.user.status', 'acl'])->group(function () {

    Route::name('profile.update')->put('profile/{id}', 'Auth\ProfileController@update');

    Route::name('shared.natureza-operacao')->get('shared/natureza-operacao', 'SharedController@naturezaOperacao');
    Route::name('shared.emissores')->get('shared/emissores', 'SharedController@emissores');
    Route::name('shared.transportadores')->get('shared/transportadores', 'SharedController@transportadores');
    Route::name('shared.vendedores')->get('shared/vendedores', 'SharedController@vendedores');
    Route::name('shared.clientes')->get('shared/clientes', 'SharedController@clientes');
    Route::name('shared.aliquota-icms')->get('shared/aliquota-icms/{estadoOrigemId}/{estadoDestinoId}', 'SharedController@aliquotaIcms');
    Route::name('shared.unidades-medida')->get('shared/unidades-medida', 'SharedController@unidadesMedida');
    Route::name('shared.lojas')->get('shared/lojas', 'SharedController@lojas');
    Route::name('shared.bandeiras')->get('shared/bandeiras', 'SharedController@bandeiras');
    Route::name('shared.financeiras')->get('shared/financeiras', 'SharedController@financeiras');
    Route::name('shared.tipo-pagamentos')->get('shared/tipo-pagamentos', 'SharedController@tipoPagamentos');
    Route::name('shared.autorizar-desconto')->post('shared/autorizar-desconto', 'SharedController@autorizarDesconto');
    Route::name('shared.busca-produtos')->get('shared/busca-produtos', 'SharedController@buscaProdutos');
    Route::name('shared.estoque-produto-por-loja')->get('shared/estoque-produto-por-loja/{produtoId}', 'SharedController@estoqueProdutoPorLoja');

    /* Grupo de rotas Módulo Caixa */
    require_once __DIR__ . '/modules/web/caixa.php';

    /* Grupo de rotas Módulo estoque */
    require_once __DIR__ . '/modules/web/estoque.php';

    /* Grupo de rotas Módulo Gestão */
    require_once __DIR__ . '/modules/web/gestao.php';

    /* Grupo de rotas Módulo Venda */
    require_once __DIR__ . '/modules/web/vendas.php';


    Route::get('routes', function () {
        if (config('app.env') == 'local') {
            \Artisan::call('route:list');
            return '<pre>' . \Artisan::output() . '</pre>';
        }
    });
});

Route::redirect('/', '/login', 301);
