<?php

Route::name('caixa.')->prefix('caixa')->namespace('Caixa')->group(function () {
    Route::name('index')->get('', 'DashboardController@index');

    Route::name('abrir-caixa')->post('/abrir-caixa', 'DashboardController@abrirCaixa');
    Route::name('selecionar-caixa')->post('/selecionar-caixa', 'DashboardController@selecionarCaixa');
    Route::name('fechar-caixa')->post('/fechar-caixa', 'DashboardController@fecharCaixa');
    Route::name('credito.store')->post('/credito', 'DashboardController@postCredito');
    Route::name('credito.destroy')->delete('/credito/{id}', 'DashboardController@destroyCredito');
    Route::name('retirada.store')->post('/retirada', 'DashboardController@postRetirada');
    Route::name('retirada.destroy')->delete('/retirada/{id}', 'DashboardController@destroyRetirada');

    Route::resource('/vendas', 'VendasController')->only(['index', 'store', 'show']);

    Route::resource('/crediario', 'CrediarioController')->only(['index', 'show', 'store']);

    Route::name('cheques.processados')->get('/cheques/processados', 'ChequeController@processados');
    Route::resource('/cheques', 'ChequeController')->only(['index', 'update']);

    Route::name('vencimentos')->resource('/vencimentos/vales', 'ValeController');//->only(['index', 'update']);

    Route::name('recibo')->get('/recibo/{transacaoId}', 'RecibosController@index');
    Route::name('carne')->get('/carne/{vendaId}', 'CarneController@index');

    Route::name('relatorios.diario.index')->get('relatorios/diario', 'RelatoriosController@diarioIndex');
    Route::name('relatorios.diario.show')->get('relatorios/diario/{caixaId}', 'RelatoriosController@diarioShow');
    Route::name('relatorios.mensal.index')->get('relatorios/mensal', 'RelatoriosController@mensalIndex');
    Route::name('relatorios.mensal.show')->get('relatorios/mensal/{mes}/{ano}', 'RelatoriosController@mensalShow');


    Route::name('pessoas.index')->get('/pessoas', '\App\Http\Controllers\Gestao\PessoaController@menuCaixa');
});
