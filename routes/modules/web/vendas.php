<?php

Route::name('vendas.')->prefix('vendas')->namespace('Vendas')->group(function () {
    Route::name('index')->get('', 'DashboardController@index');
    Route::name('search')->post('', 'DashboardController@search');

    Route::resource('pre-venda', 'PreVendaController')->except(['show', 'create', 'destroy']);
    Route::name('pre-venda.lista')->get('pre-venda/lista/{pages?}', 'PreVendaController@lista');
    Route::name('pre-venda.produtos')->get('pre-venda/produtos', 'PreVendaController@produtos');
    Route::name('pre-venda.form')->get('pre-venda/form/{action}/{vendaId?}', 'PreVendaController@form');
    Route::name('pre-venda.cancelar')->post('pre-venda/{vendaId}/cancelar', 'PreVendaController@cancelar');

    Route::name('pessoas.index')->get('/pessoas', '\App\Http\Controllers\Gestao\PessoaController@menuVendas');
});
