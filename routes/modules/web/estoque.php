<?php

Route::name('estoque.')->prefix('estoque')->namespace('Estoque')->group(function () {
    Route::get('', 'DashboardController@index')->name('index');

    Route::name('pos-venda')->resource('/pos-venda/retiradas', 'RetiradaController');
    Route::name('pos-venda')->resource('/pos-venda/entregas', 'EntregaController');

    Route::name('pos-venda.assistencias.print')->get('/pos-venda/assistencias/{posVendaId}/print', 'AssistenciaController@print');
    Route::name('pos-venda.assistencias.search')->get('/pos-venda/assistencias/search/{pages?}', 'AssistenciaController@search');
    Route::name('pos-venda')->resource('/pos-venda/assistencias', 'AssistenciaController');

    /* Notas de Entrada */
    Route::name('notas-entrada.lista')->get('/notas-entrada/lista/{pages?}', 'NotasEntradaController@lista');
    Route::name('notas-entrada.pdf')->get('/notas-entrada/pdf/{id}', 'NotasEntradaController@pdf');
    Route::name('notas-entrada')->resource('/notas-entrada/duplicatas', 'NotasEntradaDuplicataController');
    Route::resource('/notas-entrada', 'NotasEntradaController');
    Route::name('notas-entrada.movimento-produtos.lista')
        ->get('/notas-entrada/movimento-produtos/lista/{notaId}', 'NotasEntradaMovimentoProdutoController@lista');
    Route::name('notas-entrada')->resource('/notas-entrada/movimento-produtos', 'NotasEntradaMovimentoProdutoController')
        ->only(['store', 'update', 'destroy']);


    /* Notas de Venda */
    Route::name('notas-venda.emissao')->get('/notas-venda/emissao', 'NotasVendaController@emissao');
    Route::resource('/notas-venda', 'NotasVendaController');

    /* Notas de Transferência */
//    Route::name('notas-transferencia.emissao')->get('/notas-transferencia/emissao', 'NotasTransferenciaController@emissao');
    Route::resource('/notas-transferencia', 'NotasTransferenciaController');


    /* Ajuste */
    Route::name('ajuste.search')->post('/ajuste/search', 'AjusteController@search');
    Route::resource('/ajuste', 'AjusteController')->only(['index', 'store']);

    /* Produtos */
    route::name('produtos')->resource('/produtos/imagens', 'ImagemController')->only(['store', 'update', 'destroy']);

    route::name('produtos.lista.imagens')->get('/produtos/lista/imagens/{produtoId}', 'ImagemController@lista');
    Route::name('produtos.lista.mercadorias')->get('/produtos/lista/mercadorias', 'ProdutoController@listarMercadorias');
    Route::name('produtos.lista.marcas')->get('/produtos/lista/marcas', 'ProdutoController@listarMarcas');
    Route::name('produtos.lista')->get('/produtos/lista/{pages?}', 'ProdutoController@lista');
    Route::resource('/produtos', 'ProdutoController')->except(['create', 'show', 'edit']);
    Route::name('produtos-combinados.lista')->get('/produtos-combinados/lista/{pages?}', 'ProdutosCombinadosController@lista');
    Route::resource('/produtos-combinados', 'ProdutosCombinadosController')->except(['show']);


    Route::name('mercadorias.categorias')->get('/mercadorias/categorias', 'MercadoriaController@categorias');
    Route::name('mercadorias.lista')->get('/mercadorias/lista/{pages?}', 'MercadoriaController@lista');
    Route::resource('mercadorias', 'MercadoriaController')->except(['create', 'show', 'edit']);

    Route::name('categorias.lista')->get('/categorias/lista/{pages?}', 'CategoriaController@lista');
    Route::resource('categorias', 'CategoriaController')->except(['create', 'show', 'edit']);

    Route::name('marcas.lista')->get('/marcas/lista/{pages?}', 'MarcaController@lista');
    Route::resource('marcas', 'MarcaController')->except(['create', 'show', 'edit']);

    Route::name('pessoas.index')->get('/pessoas', '\App\Http\Controllers\Gestao\PessoaController@menuEstoque');

    Route::name('natureza-operacao.lista')->get('/natureza-operacao/lista/{pages?}', 'NaturezaOperacaoController@lista');
    Route::resource('/natureza-operacao', 'NaturezaOperacaoController')->except(['create', 'show', 'edit']);

});
