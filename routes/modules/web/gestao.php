<?php

Route::prefix('gestao')->namespace('Gestao')->group(function () {

    Route::name('gestao.index')->get('', 'DashboardController@index');

    Route::name('gestao.relatorios.caixa.diario')->get('relatorios/caixa/diario', 'RelatoriosController@caixaDiario');
    Route::name('gestao.relatorios.caixa.mensal')->get('relatorios/caixa/mensal', 'RelatoriosController@caixaMensal');
    Route::name('gestao.relatorios.vendas')->get('relatorios/vendas', 'RelatoriosController@vendas');

    Route::name('gestao')->resource('financeiras', 'FinanceirasController');

    Route::get('pessoas/padroes', 'PessoaController@Padroes');
    Route::get('pessoas/tipo-identidades', 'PessoaController@tipoIdentidades');
    Route::post('pessoas/{tipoPessoaId}', 'PessoaController@create');
    Route::put('pessoas/{tipoPessoaId}/{pessoaId}', 'PessoaController@update');
    Route::get('pessoas/lista/{pages?}', 'PessoaController@lista');
    Route::get('pessoas', 'PessoaController@index')->name('gestao.pessoas.index');

    Route::get('enderecos/regioes', 'EnderecoController@regioes');
    Route::get('enderecos/estados', 'EnderecoController@estados');
    Route::get('enderecos/cidades/{esdadoId?}', 'EnderecoController@cidades');
    Route::get('enderecos/{pessoaId}', 'EnderecoController@lista');
    Route::post('enderecos', 'EnderecoController@create');
    Route::put('enderecos/{id}', 'EnderecoController@update');
    Route::delete('enderecos/{id}', 'EnderecoController@destroy');

    Route::post('contatos', 'ContatoController@create');
    Route::put('contatos/{id}', 'ContatoController@update');
    Route::delete('contatos/{id}', 'ContatoController@destroy');
    Route::get('contatos/{pessoaId}', 'ContatoController@lista');

    Route::get('seguranca/acl/lista-grupos', 'Seguranca\AclController@listaGrupos');
    Route::get('seguranca/acl/lista-permissoes', 'Seguranca\AclController@listaPermissoes');
    Route::put('seguranca/acl/sync-grupo-permissoes/{grupoId}', 'Seguranca\AclController@syncGrupoPermissoes');
    Route::resource('seguranca/acl', 'Seguranca\AclController')->except(['create', 'show', 'edit']);

    Route::get('seguranca/usuarios/lista-grupos', 'Seguranca\UserController@listaGrupos');
    Route::get('seguranca/usuarios/lista-funcionarios', 'Seguranca\UserController@listaFuncionarios');
    Route::get('seguranca/usuarios/lista/{pages?}', 'Seguranca\UserController@lista');
    Route::resource('seguranca/usuarios', 'Seguranca\UserController')->except(['create', 'show', 'edit']);

    Route::name('gestao')->resource('configuracoes', 'ConfiguracaoController')->only(['index', 'store']);

    Route::name('gestao')->resource('/lojas', 'LojasController');

});
