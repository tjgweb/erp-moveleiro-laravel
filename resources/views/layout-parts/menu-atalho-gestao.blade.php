@if(auth()->user()->hasPermission('App\Http\Controllers\Gestao\DashboardController@index'))
  <li class="header text-center text-bold">
    <i class="fa fa-dashboard fa-lg"></i> OUTROS PAINEIS
  </li>
  <li>
    <a href="{{route('gestao.index')}}">
      <i class="fa fa-dashboard text-yellow"></i> <span>Painel Gestão</span>
    </a>
  </li>
  <li>
    <a href="{{route('caixa.index')}}">
      <i class="fa fa-dashboard text-green"></i> <span>Painel Caixa</span>
    </a>
  </li>
  <li>
    <a href="{{route('vendas.index')}}">
      <i class="fa fa-dashboard text-light-blue"></i> <span>Painel Vendas</span>
    </a>
  </li>
  <li>
    <a href="{{route('estoque.index')}}">
      <i class="fa fa-dashboard text-navy"></i> <span>Painel Estoque</span>
    </a>
  </li>
@endif

@include('layout-parts.menu-development')
