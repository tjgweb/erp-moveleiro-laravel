<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  Copyright &copy; 2017-{{ date('Y') }} <b><a href="https://midiauai.com.br" target="_blank">MidiaUai</a>.</b> All rights
  reserved.
</footer>
