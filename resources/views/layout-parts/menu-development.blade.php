@if(config('app.env') == 'local')
  <li class="header text-center text-bold">
    <i class="fa fa-wrench fa-lg"></i> DESENVOLVIMENTO
  </li>
  <li>
    <a href="{{url('/routes')}}" target="_blank">
      <i class="fa fa-road"></i> <span>Mapa de rotas da aplicação</span>
    </a>
  </li>
@endif
