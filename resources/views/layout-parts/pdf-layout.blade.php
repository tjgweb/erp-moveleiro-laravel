<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>@yield('title') | Brasão Center Móveis</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <style>
    .table {
      font-size: 0.8em;
    }

    .table .label {
      font-weight: bold
    }

    .table .label-dark {
      font-weight: 100;
    }

    .table small {
      font-size: 0.75em;
    }

    .page-break {
      page-break-after: always;
    }
  </style>
  @stack('style')
</head>
<body>
@yield('content')
</body>
</html>
