<header class="main-header">
  <!-- Logo -->
  <a href="{{$linkLogo}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>BM</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">{{ config('app.name', 'ERP - Brasão Móveis') }}</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        @if(auth()->user()->hasPermission('App\Http\Controllers\Gestao\DashboardController@index'))
          <li>
            <a href="{{route('gestao.index')}}">
              <i class="fa fa-gears"></i> Gestão
            </a>
          </li>
          <li>
            <a href="{{route('caixa.index')}}">
              <i class="fa fa-calculator"></i> Caixa
            </a>
          </li>
          <li>
            <a href="{{route('vendas.index')}}">
              <i class="fa fa-shopping-basket"></i> Vendas
            </a>
          </li>
          <li>
            <a href="{{route('estoque.index')}}">
              <i class="fa fa-cubes"></i> Estoque
            </a>
          </li>
        @endif
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ auth()->user()->gravatar }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ auth()->user()->apelido }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ auth()->user()->gravatar }}" class="img-circle" alt="Gravatar">
              <p>
                {{ auth()->user()->apelido }} - {{auth()->user()->grupo->nome}}
              </p>
            </li>

            <li class="user-footer">
              <div class="pull-left">
                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#profile-modal">
                  <i class="fa fa-pencil" aria-hidden="true"></i> Alterar dados
                </button>
              </div>
              <div class="pull-right">
                <button type="button" class="btn btn-danger btn-flat"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out" aria-hidden="true"></i> Sair
                </button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
<vue-profile-modal
  auth-user="{{auth()->user()}}"
  update-url="{{route('profile.update', ['id' => auth()->user()->id])}}">
</vue-profile-modal>
