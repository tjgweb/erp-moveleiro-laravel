<div class="modal fade" id="profile-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
          Meus dados - {{ auth()->user()->apelido }}
        </h4>
      </div>
      <div class="modal-body">
      <img height="50" src="{{ auth()->user()->gravatar }}" class="img-circle " alt="Gravatar">
        
        <p>One fine body&hellip;</p>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Sair sem salvar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
    
  </div>
</div>
