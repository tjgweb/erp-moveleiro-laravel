<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layout-parts.head')

<body class="hold-transition login-page">
<div id="app">

  <div class="login-box">
    <div class="login-logo">
      <a href="{{url('/')}}" class="h1">
        <b>{{ config('app.name', 'ERP - Brasão Móveis') }}</b>
      </a>
    </div>
    <div class="login-box-body">
      @yield('content')
    </div>
  </div>

</div>

<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
{!! toastr()->render() !!}
@yield('scripts')
</body>
</html>
