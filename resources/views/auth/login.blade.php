@extends('auth.layout')

@section('content')
  <h2 class="text-center">Login</h2>
  <p class="login-box-msg">Faça login para entrar no sistema</p>
  <form method="POST" action="{{ url('/login') }}">
    @csrf

    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
      <input type="email" name="email" class="form-control" value="{{ old('email') }}"
             placeholder="E-mail">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
      <input type="password" name="password" class="form-control"
             placeholder="Senha">
      <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>

    <div class="row">
      <div class="col-xs-8">
        <div class="checkbox icheck">
          <label>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembar-me.
          </label>
        </div>
      </div>
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">
          <i class="fa fa-sign-in" aria-hidden="true"></i> Entrar
        </button>
      </div>
    </div>
  </form>

  <div class="auth-links">
    <br>
    <a href="{{route('password.request')}}" class="text-center">Esqueceu sua senha?</a>
  </div>
@endsection
@section('scripts')
  <script type="application/javascript">
    $(document).ready(function () {
      $('input[type="checkbox"], input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });
    });
  </script>
@endsection
