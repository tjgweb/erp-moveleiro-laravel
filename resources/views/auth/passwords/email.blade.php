@extends('auth.layout')

@section('content')
  <h2 class="text-center">Recuperar Senha</h2>
  <p class="login-box-msg">Digite o seu e-mail cadastrado no sistema.</p>
  
  @if (session('status'))
    <div class="alert alert-success" role="alert">
      {{ session('status') }}
    </div>
  @endif
  
  <form method="POST" action="{{ route('password.email') }}">
    @csrf

    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
      <input type="email" name="email" class="form-control" value="{{ old('email') }}"
             placeholder="E-mail">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block btn-flat">
        <i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar link para redefinir senha.
      </button>
      <br>
      <a href="{{route('login')}}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Voltar</a>
    </div>
  </form>
@endsection
