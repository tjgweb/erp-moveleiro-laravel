@extends('auth.layout')

@section('content')
  <h2 class="text-center">Redefinir Senha</h2>
  <p class="login-box-msg">Digite o seu e-mail cadastrado no sistema e a nova senha.</p>
  
  <form method="POST" action="{{ route('password.update') }}">
    @csrf

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
      <input type="email" name="email" class="form-control" value="{{ $email ?? old('email') }}"
             placeholder="E-mail">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
      <input type="password" name="password" class="form-control"
             placeholder="Nova senha">
      <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
      <input type="password" name="password_confirmation" class="form-control"
             placeholder="Confirmar nova senha">
      <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-floppy-o"></i> Salvar
        </button>
    </div>
  </form>                
@endsection
