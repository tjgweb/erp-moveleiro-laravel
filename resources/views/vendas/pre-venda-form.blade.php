@extends('vendas.layout')
@section('title', 'Pré-Venda | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-shopping-basket"></i> Pré-Venda
      <small>Vendas</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('vendas.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-shopping-basket"></i> Pré-Venda</li>
    </ol>
  </section>

  <section class="content">
      <vue-pre-venda action="{{$action}}" venda-id="{{$vendaId}}"></vue-pre-venda>
  </section>
@endsection
