<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header text-center text-bold">
        <i class="fa fa-shopping-basket fa-lg"></i> MENU VENDAS
      </li>
      
      <li>
        <a href="{{route('vendas.index')}}">
          <i class="fa fa-dashboard"></i> <span>Painel</span>
        </a>
      </li>

      <li>
        <a href="{{route('vendas.pre-venda.form', ['action' => 'add'])}}">
          <i class="fa fa-plus text-green"></i> <span>Registrar Pré-venda</span>
        </a>
      </li>

      <li>
        <a href="{{route('vendas.pre-venda.index')}}">
          <i class="fa fa-shopping-basket text-aqua"></i> <span>Pré-vendas registradas</span>
        </a>
      </li>

      <li>
        <a href="{{route('vendas.pessoas.index')}}">
          <i class="fa fa-users"></i> <span>Pessoas</span>
        </a>
      </li>
      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
