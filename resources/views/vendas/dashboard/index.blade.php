@extends('vendas.layout')
@section('title', 'Vendas - Dashboard | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> Painel
      <small>Vendas</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('vendas.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
    </ol>
  </section>

  <section class="content">
    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Busca vendas por vendedor</h3>
      </div>
      <div class="box-body">
        @include('vendas.dashboard._form')
        @if(isset($vendas))
          @include('vendas.dashboard._lista', ['vendas' => $vendas])
        @endif
      </div>
    </div>
    
  </section>
@endsection
