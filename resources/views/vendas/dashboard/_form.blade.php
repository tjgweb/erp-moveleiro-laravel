<form action="{{route('vendas.search')}}" method="post">
  @csrf
  <div class="row">
    <div class="form-group col-md-2">
      <label>Mês</label>
      <select name="mes" class="form-control">
        <option value="01" {{old('mes') && old('mes') == '01' ? 'selected' : isset($dados) && $dados['mes'] == '01' ? 'selected' : '01' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Janeiro</option>
        <option value="02" {{old('mes') && old('mes') == '02' ? 'selected' : isset($dados) && $dados['mes'] == '02' ? 'selected' : '02' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Fevereiro</option>
        <option value="03" {{old('mes') && old('mes') == '03' ? 'selected' : isset($dados) && $dados['mes'] == '03' ? 'selected' : '03' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Março</option>
        <option value="04" {{old('mes') && old('mes') == '04' ? 'selected' : isset($dados) && $dados['mes'] == '04' ? 'selected' : '04' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Abril</option>
        <option value="05" {{old('mes') && old('mes') == '05' ? 'selected' : isset($dados) && $dados['mes'] == '05' ? 'selected' : '05' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Maio</option>
        <option value="06" {{old('mes') && old('mes') == '06' ? 'selected' : isset($dados) && $dados['mes'] == '06' ? 'selected' : '06' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Junho</option>
        <option value="07" {{old('mes') && old('mes') == '07' ? 'selected' : isset($dados) && $dados['mes'] == '07' ? 'selected' : '07' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Julho</option>
        <option value="08" {{old('mes') && old('mes') == '08' ? 'selected' : isset($dados) && $dados['mes'] == '08' ? 'selected' : '08' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Agosto</option>
        <option value="09" {{old('mes') && old('mes') == '09' ? 'selected' : isset($dados) && $dados['mes'] == '09' ? 'selected' : '09' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Setembro</option>
        <option value="10" {{old('mes') && old('mes') == '10' ? 'selected' : isset($dados) && $dados['mes'] == '10' ? 'selected' : '10' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Outubro</option>
        <option value="11" {{old('mes') && old('mes') == '11' ? 'selected' : isset($dados) && $dados['mes'] == '11' ? 'selected' : '11' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Novembro</option>
        <option value="12" {{old('mes') && old('mes') == '12' ? 'selected' : isset($dados) && $dados['mes'] == '12' ? 'selected' : '12' == $mesAtual && !isset($dados) && !old('mes') ? 'selected' : '' }}>Dezembro</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Ano</label>
      <select name="ano" class="form-control">
        @for($i = 2018; $i <= date('Y'); $i++)
          <option value="{{$i}}" {{old('ano') && old('ano') == $i ? ' selected' : isset($dados) && $dados['ano'] == $i ? ' selected' : $i == date('Y') && !isset($dados) && !old('ano') ? ' selected' : '' }}>
            {{$i}}
          </option>
        @endfor
      </select>
    </div>
    <div class="form-group col-md-4 {{$errors->has('vendedor') ? 'has-error' : ''}}">
      <label>Vendedor</label>
      <select name="vendedor" class="form-control">
        @foreach($vendedores as $vendedor)
          <option value="{{$vendedor->id}}"
            {{old('vendedor') && old('vendedor') == $vendedor->id ? 'selected' : isset($dados) && $dados['vendedor'] == $vendedor->id ? 'selected' : ''}}>
            {{$vendedor->nome}}
          </option>
        @endforeach
      </select>
      <span class="small text-danger">{{$errors->first('vendedor')}}</span>
    </div>
    <div class="col-md-3">
      <label>Senha</label>
      <div class="input-group {{$errors->has('senha') ? 'has-error' : ''}}">
        <input type="password" name="senha" class="form-control" placeholder="******">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </span>
      </div>
      <span class="small text-danger">{{$errors->first('senha')}}</span>
    </div>
  </div>
</form>
