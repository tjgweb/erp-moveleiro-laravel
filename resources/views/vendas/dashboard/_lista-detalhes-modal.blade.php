<div class="modal" id="detalhes-{{$venda->id}}" tabindex="-1" role="dialog"
     aria-labelledby="detalhes-{{$venda->id}}Label">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detalhes-{{$venda->id}}Label">Produtos da venda Nº: {{$venda->id}}</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th class="text-center">Código</th>
              <th class="text-center">Imagem</th>
              <th>Descrição</th>
              <th class="text-center">P. Combinado</th>
              <th class="text-center">Qtd</th>
              <th class="text-center">V. Unit.</th>
              <th class="text-center">V. Total</th>
              <th class="text-center">Desconto</th>
              <th class="text-center">V. Pago</th>
              {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @php $combinados = []; @endphp
            @foreach($venda->produtoVendas as $pv)
              @if($pv->produto_combinado)
                @if(!in_array($pv->combinado_id, $combinados))
                  @php
                    $qtd = $pv->combinado->produtos()->where('id', $pv->produto_id)->first()->pivot->quantidade / $pv->quantidade;
                    $vUnitario = $pv->combinado->valor_total;
                    $vTotal = $vUnitario * $qtd;
                    $vDesconto = ($vUnitario * $qtd) * $pv->desconto / 100;
                  @endphp
                  <tr>
                    <td class="text-center">{{$pv->combinado->id}}</td>
                    <td class="text-center">
                      @if($pv->produto->imagens->count())
                        <img src="{{$pv->produto->imagens[0]->src}}" alt="{{$pv->produto->imagens[0]->legenda}}"
                             height="60">
                      @else
                        <img src="/images/no-image.png" alt="No Image" height="60">
                      @endif
                    </td>
                    <td>{{$pv->combinado->descricao}}</td>
                    <td class="text-center"><i class="fa fa-cubes"></i> Sim</td>
                    <td class="text-center">{{$qtd}}</td>
                    <td class="text-right">R${{number_format($vUnitario,2,',','.')}}</td>
                    <td class="text-right">R${{number_format($vTotal,2,',','.')}}</td>
                    <td class="text-right">R${{number_format($vDesconto,2,',','.')}}</td>
                    <td class="text-right">R${{number_format($vTotal - $vDesconto,2,',','.')}}</td>
                    {{--<td class="btn-actions">
                      <button class="btn btn-primary btn-xs btn-flat"
                              data-toggle="modal" data-target="#combinado-{{$pv->combinado_id}}">
                        <i class="fa fa-search-plus"></i> detalhes
                      </button>
                      @include('vendas.dashboard._lista-combinado-detalhes-modal', ['combinado' => $pv->combinado])
                    </td>--}}
                  </tr>
                @endif
                @php $combinados[] = $pv->combinado_id; @endphp
              @else
                <tr>
                  <td class="text-center">{{$pv->produto->id}}[{{$pv->produto->codigo_fabrica}}]</td>
                  <td class="text-center">
                    @if($pv->produto->imagens->count())
                      <img src="{{$pv->produto->imagens[0]->src}}" alt="{{$pv->produto->imagens[0]->legenda}}"
                           height="60">
                    @else
                      <img src="/images/no-image.png" alt="No Image" height="60">
                    @endif
                  </td>
                  <td>{{$pv->produto->descricao_completa}}</td>
                  <td class="text-center"><i class="fa fa-cube"></i> Não</td>
                  <td class="text-center">{{$pv->quantidade}}</td>
                  <td class="text-right">R${{number_format($pv->valor_unitario,2,',','.')}}</td>
                  <td class="text-right">R${{number_format($pv->valor_total,2,',','.')}}</td>
                  <td class="text-right">R${{number_format($pv->valor_desconto,2,',','.')}}</td>
                  <td class="text-right">R${{number_format($pv->valor_pago,2,',','.')}}</td>
                  {{--<td></td>--}}
                </tr>
              @endif
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
