<div class="modal" id="combinado-{{$combinado->id}}" tabindex="-1" role="dialog"
     aria-labelledby="combinado-{{$combinado->id}}Label">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="combinado-{{$combinado->id}}Label">Relação dos produtos combinados</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th class="text-center">Código</th>
              <th class="text-center">Imagem</th>
              <th>Descrição</th>
            </tr>
            </thead>
            <tbody>
            @foreach($combinado->produtos as $p)
                <tr>
                  <td class="text-center">{{$p->id}}[{{$p->codigo_fabrica}}]</td>
                  <td class="text-center">
                    @if($p->imagens->count())
                      <img src="{{$p->imagens[0]->src}}" alt="{{$p->imagens[0]->legenda}}"
                           height="60">
                    @else
                      <img src="/images/no-image.png" alt="No Image" height="60">
                    @endif
                  </td>
                  <td>{{$p->descricao_completa}}</td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
