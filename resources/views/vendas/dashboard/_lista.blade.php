<br>

<table class="table table-middle">
  <thead>
  <tr>
    <th>Código</th>
    <th>Cliente</th>
    <th>Data Venda</th>
    <th>Última Atualização</th>
    <th>Status</th>
    <th class="text-center">Valor</th>
    <td></td>
  </tr>
  </thead>
  <tbody>
  @if($vendas->count() == 0)
    <tr class="info"><td colspan="7" class="text-center">Nada encontrado</td></tr>
  @endif
  @foreach($vendas as $venda)
    <tr>
      <td>{{$venda->id}}</td>
      <td>{{$venda->pessoaCliente->nome}}</td>
      <td>
        {{$venda->data_venda ? date('d/m/Y', strtotime($venda->data_venda)) : ''}}
      </td>
      <td>
        {{date('d/m/Y', strtotime($venda->updated_at))}}
      </td>
      <td>{{$venda->estatusVenda->descricao}}</td>
      <td class="text-right">
        {{number_format($venda->valor_pago, 2, ',', '.')}}
      </td>
      <td class="btn-actions">
        <button class="btn btn-primary btn-xs btn-flat"
                data-toggle="modal" data-target="#detalhes-{{$venda->id}}">
          <i class="fa fa-search-plus"></i> detalhes
        </button>
        @include('vendas.dashboard._lista-detalhes-modal', ['venda' => $venda])
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
