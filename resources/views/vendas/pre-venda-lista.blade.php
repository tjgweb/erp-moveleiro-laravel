@extends('vendas.layout')
@section('title', 'Pré-Vendas registradas | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-shopping-basket"></i> Pré-Vendas registradas
      <small>Vendas</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('vendas.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-shopping-basket"></i> Pré-Vendas registradas</li>
    </ol>
  </section>

  <section class="content">
    <vue-pre-venda-lista></vue-pre-venda-lista>
  </section>
@endsection
