@extends('gestao.layout')
@section('title', 'Configurações | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-cogs"></i> Configurações
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-cogs"></i> Configurações</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Configurações diversas</h3>
      </div>
      <form action="{{route('gestao.configuracoes.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="box-body">

          <div class="row">
            <h2 class="h3 col-xs-12">Crediário</h2>
            <div class="form-group col-xs-6 col-md-4 col-lg-3{{$errors->has('multa') ? ' has-error' : ''}}">
              <label>Multa atraso %</label>
              <input type="number" name="multa" step="0.01" min="0" class="form-control"
                     value="{{old('multa') ? old('multa') : (isset($data->multa)) ? $data->multa : ''}}">
              <span class="small text-danger">{{$errors->first('multa')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-4 col-lg-3{{$errors->has('juros_dia') ? ' has-error' : ''}}">
              <label>Juros ao dia %</label>
              <input type="number" name="juros_dia" step="0.01" min="0" class="form-control"
                     value="{{old('juros_dia') ? old('juros_dia') : (isset($data->juros_dia)) ? $data->juros_dia : ''}}">
              <span class="small text-danger">{{$errors->first('juros_dia')}}</span>
            </div>
          </div>

          <div class="row">
            <h2 class="h3 col-xs-12">NF-e
              <small>(Nota fiscal eletrônica)</small>
            </h2>
            <div class="form-group col-xs-6 col-md-3{{$errors->has('tpAmb') ? ' has-error' : ''}}">
              <label>Modo teste?</label>
              <select name="tpAmb" class="form-control">
                <option value="1" {{isset($data->tpAmb) && $data->tpAmb == 1 ? 'selected' : ''}}>Não</option>
                <option value="2" {{isset($data->tpAmb) && $data->tpAmb == 2 ? 'selected' : ''}}>Sim</option>
              </select>
              <span class="small text-danger">{{$errors->first('tpAmb')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-3{{$errors->has('schemes') ? ' has-error' : ''}}">
              <label>Schemes</label>
              <select name="schemes" class="form-control">
                <option value="PL_009_V4" {{isset($data->schemes) && $data->schemes == 'PL_009_V4' ? 'selected' : ''}}>
                  PL_009_V4
                </option>
              </select>
              <span class="small text-danger">{{$errors->first('schemes')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-3{{$errors->has('versao') ? ' has-error' : ''}}">
              <label>Versão</label>
              <select name="versao" class="form-control">
                <option value="4.00" {{isset($data->versao) && $data->versao == '4.00' ? 'selected' : ''}}>4.00</option>
              </select>
              <span class="small text-danger">{{$errors->first('versao')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-3{{$errors->has('cnpj_contador') ? ' has-error' : ''}}">
              <label>CNPJ Contador</label>
              <input type="text" name="cnpj_contador" class="form-control cnpj-mask"
                     value="{{old('cnpj_contador') ? old('cnpj_contador') : (isset($data->cnpj_contador)) ? $data->cnpj_contador : ''}}">
              <span class="small text-danger">{{$errors->first('cnpj_contador')}}</span>
            </div>
          </div>

        </div>{{--box-body--}}

        <div class="box-footer">
          <button type="submit" class="btn btn-warning btn-flat pull-right">
            <i class="fa fa-save"></i> Salvar
          </button>
        </div>

      </form>
    </div>
  </section>
@endsection
