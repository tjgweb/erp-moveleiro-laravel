@extends('gestao.layout')
@section('title', 'Gestão - Relatórios de vendas | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bar-chart-o text-info"></i> Relatórios de vendas
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bar-chart-o text-info"></i> Relatórios de vendas</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">

        <div class="row">
          <form action="{{route('gestao.relatorios.vendas')}}" method="get">
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Status da venda</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-shopping-bag"></i></span>
                <select name="status" class="form-control">
                  <option value="0" {{$form['status'] == 0 ? 'selected' : ''}}>Todos</option>
                  @foreach($statusVenda as $sv)
                    <option value="{{$sv->id}}" {{$form['status'] == $sv->id ? 'selected' : ''}}>
                      {{$sv->descricao}}
                    </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Data inicial</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="date" name="start" class="form-control" value="{{$form['start']}}">
              </div>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Data final</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="date" name="end" class="form-control" value="{{$form['end']}}">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </div>
          </form>
        </div>
        <br>
        <div class="table-responseive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th>Data Venda</th>
              <th>Última atualização</th>
              <th>Vendedor</th>
              <th>Cliente</th>
              <th class="text-center">Valor</th>
              <th class="text-center">Status</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @if($vendas->count() == 0)
              <tr class="info">
                <td colspan="5" class="text-center">
                  Nenhuma venda encontrada com os filtros configurados.
                </td>
              </tr>
            @endif
            @foreach($vendas as $venda)
              <tr>
                <td>{{$venda->data_venda ? date('d/m/Y', strtotime($venda->updated_at)) : ''}}</td>
                <td>{{date('d/m/Y - H:i', strtotime($venda->updated_at))}}</td>
                <td>{{$venda->pessoaVendedor->nome}}</td>
                <td>{{$venda->pessoaCliente->nome}}</td>
                <td class="text-right">{{number_format($venda->valor_pago,2,',','.')}}</td>
                <td class="text-center">{{$venda->estatusVenda->descricao}}</td>
                <td class="btn-actions">
                  <button class="btn btn-primary btn-xs btn-flat"
                          data-toggle="modal" data-target="#detalhes-{{$venda->id}}">
                    <i class="fa fa-search-plus"></i> detalhes
                  </button>
                  @include('vendas.dashboard._lista-detalhes-modal', ['venda' => $venda])
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

@endsection
