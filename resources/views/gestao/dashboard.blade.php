@extends('gestao.layout')
@section('title', 'Gestão - Dashboard | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> Painel
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <h3 class="col-xs-12">Vendas</h3>

      <div class="col-xs-12 col-md-4">
        <div class="info-box">
          <span class="info-box-icon bg-green-active"><i class="fa fa-shopping-basket"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Vendas concluídas do Mês</span>
            <span class="info-box-number">{{$concluidas}}</span>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-4">
        <div class="info-box">
          <span class="info-box-icon bg-yellow-active"><i class="fa fa-shopping-basket"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Aguardando autorização <br> de desconto</span>
            <span class="info-box-number">
              {{$autorizacao}}
              <a href="{{route('vendas.pre-venda.index')}}" class="btn btn-default btn-sm btn-flat pull-right">
                <i class="fa fa-search"></i> Ver
              </a>
            </span>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-4">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-shopping-basket"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Canceladas do Mês</span>
            <span class="info-box-number">{{$cancelada}}</span>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <h3 class="col-xs-12">Caixas abertos</h3>
      @if($caixas->count() > 0)
        @foreach($caixas as $caixa)
          <div class="col-xs-6 col-md-4 col-lg-3">
            <div class="box box-solid box-success">
              <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($caixa->created_at->localeDayOfWeek)}}, {{$caixa->created_at->day}} de {{$caixa->created_at->localeMonth}}</h3>
              </div>
              <div class="box-body">
                <p><strong>Operador: </strong>{{$caixa->operador->apelido}}</p>
              </div>
            </div>
          </div>
        @endforeach
      @else
        <p class="col-xs-12">Nenhum caixa aberto.</p>
      @endif
    </div>

    {{--<div class="row">
      <h3 class="col-xs-12">Pessoas</h3>

      <div class="col-xs-6 col-md-4 col-lg-3">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Clientes</span>
            <span class="info-box-number">{{$clientes}}</span>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-4 col-lg-3">
        <div class="info-box">
          <span class="info-box-icon bg-aqua-active"><i class="fa fa-users"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Funcionários</span>
            <span class="info-box-number">{{$funcionarios}}</span>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-4 col-lg-3">
        <div class="info-box">
          <span class="info-box-icon bg-maroon-active"><i class="fa fa-handshake-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Fornecedores</span>
            <span class="info-box-number">{{$fornecedores}}</span>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-4 col-lg-3">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-truck"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Transportadoras</span>
            <span class="info-box-number">{{$transportadoras}}</span>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-4 col-lg-3">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Prestador Servicos</span>
            <span class="info-box-number">{{$prestadorServicos}}</span>
          </div>
        </div>
      </div>
    </div>--}}

  </section>
@endsection
