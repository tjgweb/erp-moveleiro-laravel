@extends('gestao.layout')
@section('title', 'Financeiras | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> Financeiras
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bank"></i> Financeiras</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Lista de Financeiras</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th class="text-center">#</th>
              <th>Nome</th>
              <th>Banco</th>
              <th>Telefone</th>
              <th class="text-right">
                <a href="{{route('gestao.financeiras.create')}}" class="btn btn-primary btn-xs btn-flat">
                  <i class="fa fa-plus"></i> Adicionar
                </a>
              </th>
            </tr>
            </thead>
            <tbody>
            @foreach($financeiras as $financeira)
              <tr>
                <td class="text-center">{{$financeira->id}}</td>
                <td>{{$financeira->nome}}</td>
                <td>{{$financeira->banco_credito}}</td>
                <td>{{$financeira->telefone}}</td>
                <td class="btn-actions">
                  <a href="{{route('gestao.financeiras.show', ['id' => $financeira->id])}}"
                     class="btn btn-danger btn-xs btn-flat">
                    <i class="fa fa-trash"></i>
                  </a>
                  <a href="{{route('gestao.financeiras.edit', ['id' => $financeira->id])}}"
                     class="btn btn-warning btn-xs btn-flat">
                    <i class="fa fa-pencil"></i>
                  </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer text-center">
        {{$financeiras->links()}}
      </div>
    </div>
  </section>
@endsection
