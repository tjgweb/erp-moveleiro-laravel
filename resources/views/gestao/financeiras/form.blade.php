@extends('gestao.layout')
@section('title', $title)
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bank"></i> Financeiras
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bank"></i> Financeiras</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        @if($form == 'add')
          <h3 class="box-title">Novo Cadastro</h3>
        @elseif($form == 'edit')
          <h3 class="box-title">Editando Financera</h3>
        @elseif($form == 'remove')
          <h3 class="box-title">Remover Financera</h3>
        @endif
      </div>
      @if($form == 'add')
        <form action="{{route('gestao.financeiras.store')}}" method="post">
      @elseif($form == 'edit')
        <form action="{{route('gestao.financeiras.update', ['id' => $financeira->id])}}" method="post">
          @method('PUT')
      @elseif($form == 'remove')
        <form action="{{route('gestao.financeiras.destroy', ['id' => $financeira->id])}}" method="post">
          @method('DELETE')
      @endif
        <div class="box-body">
          <div class="row">
            @csrf
            <div class="form-group col-xs-12 col-md-4 {{$errors->has('nome') ? 'has-error' : ''}}">
              <label>Nome</label>
              <input type="text" name="nome" class="form-control"
                     value="{{old('nome') ? old('nome') : (isset($financeira->nome)) ? $financeira->nome : ''}}" 
                      {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('nome')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4 {{$errors->has('banco_credito') ? 'has-error' : ''}}">
              <label>Banco</label>
              <input type="text" name="banco_credito" class="form-control"
                     value="{{old('banco_credito') ? old('banco_credito') : (isset($financeira->banco_credito)) ? $financeira->banco_credito : ''}}"
                     {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('banco_credito')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4 {{$errors->has('telefone') ? 'has-error' : ''}}">
              <label>Telefone</label>
              <input type="text" name="telefone" class="form-control"
                     value="{{old('telefone') ? old('telefone') : (isset($financeira->telefone)) ? $financeira->telefone : ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('telefone')}}</span>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <a href="{{route('gestao.financeiras.index')}}" class="btn btn-default btn-flat pull-left"><i
              class="fa fa-ban"></i> Cancelar/Voltar</a>
          @if($form == 'add')
            <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-save"></i> Salvar</button>
          @elseif($form == 'edit')
            <button type="submit" class="btn btn-warning btn-flat pull-right"><i class="fa fa-save"></i> Atualizar</button>
          @elseif($form == 'remove')
            <button type="submit" class="btn btn-danger btn-flat pull-right"><i class="fa fa-trash"></i> Remover</button>
          @endif
        </div>
      </form>
    </div>
  </section>
@endsection
