@extends('gestao.layout')
@section('title')
  @if($padraoId == 1)
    Clientes |
  @elseif($padraoId == 2)
    Funcionários |
  @elseif($padraoId == 3)
    Fornecedores |
  @elseif($padraoId == 4)
    Transportadoras |
  @endif
@endsection
@section('content')
  <section class="content-header">
    <h1>
      @if($padraoId == 1)
        <i class="fa fa-users text-green"></i> Clientes
      @elseif($padraoId == 2)
        <i class="fa fa-users text-aqua"></i> Funcionarios
      @elseif($padraoId == 3)
        <i class="fa fa-handshake-o text-maroon"></i> Fornecedores
      @elseif($padraoId == 4)
        <i class="fa fa-truck text-yellow"></i> Transportadoras
      @endif
      <small>Pessoas</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-users"></i> Pessoas</li>
      @if($padraoId == 1)
        <li><i class="fa fa-users text-green"></i> Clientes</li>
      @elseif($padraoId == 2)
        <li><i class="fa fa-users text-aqua"></i> Funcionarios</li>
      @elseif($padraoId == 3)
        <li><i class="fa fa-handshake-o text-maroon"></i> Fornecedores</li>
      @elseif($padraoId == 4)
        <li><i class="fa fa-truck text-yellow"></i> Transportadoras</li>
      @endif
    </ol>
  </section>

  <section class="content">
      <vue-pessoas padrao-id="{{$padraoId}}"></vue-pessoas>
  </section>
@endsection
