@extends('gestao.layout')
@section('title', $form['title'] . ' | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-building-o"></i> Lojas / Depósitos
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-building-o"></i> Lojas / Depósitos</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border"><h3 class="box-title">{{$form['title']}}</h3></div>
      <form action="{{$form['action']}}" method="post">
        @method($form['method'])
        @csrf
        <div class="box-body">
          
          <div class="row">
            <div class="form-group col-xs-12 col-md-4{{$errors->has('descricao') ? ' has-error' : ''}}">
              <label>Nome/Descrição</label>
              <input type="text" name="descricao" class="form-control"
                     value="{{old('descricao') ?? $loja->descricao ?? ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('descricao')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4{{$errors->has('razao_social') ? ' has-error' : ''}}">
              <label>Razão Social</label>
              <input type="text" name="razao_social" class="form-control"
                     value="{{old('razao_social') ?? $loja->razao_social ?? ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('razao_social')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4{{$errors->has('telefone') ? ' has-error' : ''}}">
              <label>Telefone</label>
              <input type="text" name="telefone" class="form-control"
                     value="{{old('telefone') ?? $loja->telefone ?? ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('telefone')}}</span>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12 col-md-4{{$errors->has('cnpj') ? ' has-error' : ''}}">
              <label>CNPJ</label>
              <input type="text" name="cnpj" class="form-control cnpj-mask"
                     value="{{old('cnpj') ?? $loja->cnpj ?? ''}}">
              <span class="small text-danger">{{$errors->first('cnpj')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4{{$errors->has('inscricao_estadual') ? ' has-error' : ''}}">
              <label>Inscrição Estadual</label>
              <input type="text" name="inscricao_estadual" class="form-control inscricao-estadual-mask"
                     value="{{old('inscricao_estadual') ?? $loja->inscricao_estadual ?? ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('inscricao_estadual')}}</span>
            </div>            
            <div class="form-group col-xs-12 col-md-4{{$errors->has('inscricao_municipal') ? ' has-error' : ''}}">
              <label>Inscricao Municipal</label>
              <input type="text" name="inscricao_municipal" class="form-control inscricao-municipal-mask"
                     value="{{old('inscricao_municipal') ?? $loja->inscricao_municipal ?? ''}}"
                {{$form == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('inscricao_municipal')}}</span>
            </div>
          </div>          

          <div class="row">
            <div class="form-group col-xs-12 col-md-4 col-lg-2{{$errors->has('cep') ? ' has-error' : ''}}">
              <label>Cep</label>
              <input type="text" name="cep" class="form-control cep-mask"
                     value="{{old('cep') ?? $loja->cep ?? ''}}">
              <span class="small text-danger">{{$errors->first('cep')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-8 col-lg-6{{$errors->has('logradouro') ? ' has-error' : ''}}">
              <label>Logradouro</label>
              <input type="text" name="logradouro" class="form-control"
                     value="{{old('logradouro') ?? $loja->logradouro ?? ''}}">
              <span class="small text-danger">{{$errors->first('logradouro')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-6 col-lg-2{{$errors->has('numero') ? ' has-error' : ''}}">
              <label>Número</label>
              <input type="text" name="numero" class="form-control"
                     value="{{old('numero') ?? $loja->numero ?? ''}}">
              <span class="small text-danger">{{$errors->first('numero')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-6 col-lg-2{{$errors->has('complemento') ? ' has-error' : ''}}">
              <label>Complemento</label>
              <input type="text" name="complemento" class="form-control"
                     value="{{old('complemento') ?? $loja->complemento ?? ''}}">
              <span class="small text-danger">{{$errors->first('complemento')}}</span>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12 col-md-4{{$errors->has('bairro') ? ' has-error' : ''}}">
              <label>Bairro</label>
              <input type="text" name="bairro" class="form-control"
                     value="{{old('bairro') ?? $loja->bairro ?? ''}}">
              <span class="small text-danger">{{$errors->first('bairro')}}</span>
            </div>
            <vue-select-cidades
              div-class="col-xs-12 col-md-4"
              label="Cidade"
              input-name="cidade_id"
              input-value="{{old('cidade_id') ?? $loja->cidade_id ?? ''}}"
              errors-has="{{$errors->has('cidade_id')}}"
              errors-first="{{$errors->first('cidade_id')}}"
            ></vue-select-cidades>
            <vue-select-estados
              div-class="col-xs-12 col-md-4"
              label="Estado"
              input-name="estado_id"
              input-value="{{old('estado_id') ?? $loja->cidade->estado_id ?? ''}}"
              errors-has="{{$errors->has('estado_id')}}"
              errors-first="{{$errors->first('estado_id')}}"
            >
            </vue-select-estados>
          </div>

          <div class="row">
            <div class="form-group col-xs-6 col-md-4{{$errors->has('token_ibpt') ? ' has-error' : ''}}">
              <label>Token IBPT</label>
              <input type="text" name="token_ibpt" class="form-control"
                     value="{{old('token_ibpt') ?? $loja->token_ibpt ?? ''}}">
              <span class="small text-danger">{{$errors->first('token_ibpt')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4{{$errors->has('token_sefaz') ? ' has-error' : ''}}">
              <label>Token SEFAZ</label>
              <input type="text" name="token_sefaz" class="form-control"
                     value="{{old('token_sefaz') ?? $loja->token_sefaz ?? ''}}">
              <span class="small text-danger">{{$errors->first('token_sefaz')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-4{{$errors->has('token_sefaz_id') ? ' has-error' : ''}}">
              <label>Id Token SEFAZ</label>
              <input type="text" name="token_sefaz_id" class="form-control"
                     value="{{old('token_sefaz_id') ?? $loja->token_sefaz_id ?? ''}}">
              <span class="small text-danger">{{$errors->first('token_sefaz_id')}}</span>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12 col-md-6{{$errors->has('certificado') ? ' has-error' : ''}}">
              <label>Arquivo do certificado digital</label>
              <input type="file" name="certificado" accept=".pfx" class="form-control">
              @if(isset($certificado) && $certificado)
                <span class="small text-info"><strong>Já existe</strong> um certificado na base de dados. Se enviar um novo arquivo,
                  o atual será substituído.</span>
              @endif
              <span class="small text-danger">{{$errors->first('certificado')}}</span>
            </div>
            <div class="form-group col-xs-12 col-md-6{{$errors->has('senha_certificado') ? ' has-error' : ''}}">
              <label>Senha do certificado digital</label>
              <input type="password" name="senha_certificado" class="form-control"
                     value="{{old('senha_certificado') ?? $loja->senha_certificado ?? ''}}">
              <span class="small text-info"><strong>Cuidado</strong> ao alterar a senha!, na maioria dos casos deve-se também alter o arquivo do certificado.</span>
              <span class="small text-danger">{{$errors->first('senha_certificado')}}</span>
            </div>
          </div>
          
        </div>
        <div class="box-footer">
          <a href="{{route('gestao.lojas.index')}}" class="btn btn-default btn-flat pull-left"><i
              class="fa fa-ban"></i> Cancelar/Voltar
          </a>
          <button type="submit" class="btn btn-flat pull-right {{$form['submit-class']}}">
            <i class="fa fa-save"></i> {{$form['submit-title']}}
          </button>
        </div>
      </form>
    </div>
  </section>
@endsection
