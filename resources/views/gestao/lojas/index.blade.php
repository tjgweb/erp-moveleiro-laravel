@extends('gestao.layout')
@section('title', 'Lojas / Depósitos - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-building-o"></i> Lojas / Depósitos
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-building-o"></i> Lojas / Depósitos</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Lista de Lojas / Depósitos</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th class="text-center" style="width: 100px;">Código</th>
              <th>Nome/Descrição</th>
              <th class="text-right">
                <a href="{{route('gestao.lojas.create')}}" class="btn btn-primary btn-xs btn-flat">
                  <i class="fa fa-plus"></i> Adicionar
                </a>
              </th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
              <tr>
                <td class="text-center">{{$item->id}}</td>
                <td>{{$item->descricao}}</td>
                <td class="btn-actions">
                  <a href="{{route('gestao.lojas.show', ['id' => $item->id])}}"
                     class="btn btn-danger btn-xs btn-flat">
                    <i class="fa fa-trash"></i>
                  </a>
                  <a href="{{route('gestao.lojas.edit', ['id' => $item->id])}}"
                     class="btn btn-warning btn-xs btn-flat">
                    <i class="fa fa-pencil"></i>
                  </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer text-center">
        {{$data->links()}}
      </div>
    </div>
  </section>
@endsection
