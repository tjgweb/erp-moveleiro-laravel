@extends('gestao.layout')
@section('title', 'Níveis de acesso | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-user-secret"></i> Níveis de acesso
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-lock"></i> Segurança</li>
      <li><i class="fa fa-user-secret"></i> Níveis de acesso</li>
    </ol>
  </section>

  <section class="content">
    <vue-acl base-url="{{route('acl.index')}}"></vue-acl>
  </section>
@endsection
