@extends('gestao.layout')
@section('title', 'Usuários do Sistema | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Usuários do Sistema
      <small>Gestão</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('gestao.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-lock"></i> Segurança</li>
      <li><i class="fa fa-users"></i> Usuários do sistema</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <vue-users base-url="{{route('usuarios.index')}}"></vue-users>      
    </div>
  </section>
@endsection
