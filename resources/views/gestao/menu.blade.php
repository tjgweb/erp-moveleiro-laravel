<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header text-center text-bold">
        <i class="fa fa-gears fa-lg"></i> MENU GESTÃO
      </li>
      
      <li>
        <a href="{{route('gestao.index')}}">
          <i class="fa fa-dashboard"></i> <span>Painel</span>
        </a>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-calculator text-green"></i> <span>Caixa</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
          <li><a href="{{route('gestao.relatorios.caixa.diario')}}"><i class="fa fa-bar-chart-o"></i> Relatórios Diários</a></li>
          <li><a href="{{route('gestao.relatorios.caixa.mensal')}}"><i class="fa fa-bar-chart-o"></i> Relatórios Mensais</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-shopping-basket text-aqua"></i> <span>Vendas</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('vendas.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
          <li><a href="{{route('gestao.relatorios.vendas')}}"><i class="fa fa-bar-chart-o"></i> Relatórios</a></li>
        </ul>
      </li>

      <li>
        <a href="{{route('estoque.index')}}">
          <i class="fa fa-cubes text-purple"></i> <span>Estoque</span>
        </a>
      </li>

      <li>
        <a href="{{route('gestao.pessoas.index')}}">
          <i class="fa fa-users"></i> <span>Pessoas</span>
        </a>
      </li>
      
      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-lock text-red"></i> <span>Segurança</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('usuarios.index')}}"><i class="fa fa-users"></i> Usuários do sistema</a></li>
          <li><a href="{{route('acl.index')}}"><i class="fa fa-user-secret"></i> Níveis de acesso</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-cogs"></i> <span>Configurações</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{route('gestao.configuracoes.index')}}">
              <i class="fa fa-cog"></i> <span>Diversas</span>
            </a>
          </li>
          <li>
            <a href="{{route('gestao.lojas.index')}}">
              <i class="fa fa-building-o"></i> <span>Lojas/Depósitos</span>
            </a>
          </li>
          <li>
            <a href="{{route('gestao.financeiras.index')}}">
              <i class="fa fa-bank"></i> <span>Financeiras</span>
            </a>
          </li>
        </ul>
      </li>
      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
