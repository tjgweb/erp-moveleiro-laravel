@extends('caixa.layout')
@section('title', 'Caixa - Cheques | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bank text-light-blue"></i> Cheques
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bank text-light-blue"></i> Cheques</li>
      <li><i class="fa fa-circle-o text-yellow"></i> à processar</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title with-border">Lista de cheques à processar.</h3>
        <div class="box-tools">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
              class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <form action="{{route('caixa.cheques.index')}}" method="get">
          <div class="row">
            <div class="form-group col-md-4">
              <label>Data Inicial</label>
              <input type="date" name="start" value="{{$form['start'] ?? ''}}" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Data Final</label>
              <div class="input-group">
                <input type="date" name="end" value="{{$form['end'] ?? ''}}" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  <a href="{{route('caixa.cheques.index')}}" class="btn btn-default"><i class="fa fa-eraser"></i></a>
                </span>
              </div>
            </div>
          </div>
        </form>
        <br>
        @foreach($predatados as $predatado)
          <div class="box">
            <div class="box-header">
              <h3 class="box-title with-border">
                <strong>{{$predatado->transacao->venda->pessoaCliente->nome}}</strong>
              </h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                    class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <strong>Contatos:</strong> {{$predatado->transacao->venda->pessoaCliente->contatos_string}}
              <br>
              <strong>Observação:</strong> {{$predatado->observacao}}
              <br>
              <table class="table table-middle table-hover table-condensed">
                <thead>
                <tr>
                  <th>Vencimento</th>
                  <th>Número</th>
                  <th>Banco</th>
                  <th>Agência</th>
                  <th>Conta</th>
                  <th>Valor</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($predatado->cheques->where('status', false) as $cheque)
                  <tr>
                    <td>{{$cheque->vencimento->format('d/m/Y')}}</td>
                    <td>{{$cheque->numero}}</td>
                    <td>{{$cheque->banco}}</td>
                    <td>{{$cheque->agencia}}</td>
                    <td>{{$cheque->conta}}</td>
                    <td>R${{number_format($cheque->valor, 2, ',', '')}}</td>
                    <td class="btn-actions">
                      <button class="btn btn-primary btn-flat btn-xs" data-toggle="modal"
                              data-target="#modal{{$cheque->id}}">
                        <i class="fa fa-gear"></i> Processar
                      </button>
                      @include('caixa.cheques._modal-cheques', ['modalId' => $cheque->id, 'predatado' => $predatado, 'cheque' => $cheque])
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @endforeach

        @if(!$predatados->count())
          <p class="bg-info text-center">Nenhum cheque à processar.</p>
        @endif

      </div>

      <div class="box-footer text-center">
        {{$predatados->links()}}
      </div>
    </div>
  </section>
@endsection
