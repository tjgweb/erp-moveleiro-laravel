<div class="modal fade" id="modal{{$modalId}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$modalId}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal{{$modalId}}Label">Deseja processar este cheque?</h4>
      </div>

      <form method="post" action="{{route('caixa.cheques.update', ['chequeId' => $cheque->id])}}">
        @csrf @method('PUT')
        
        <div class="modal-body">
          <p>
            <strong>Cliente:</strong> {{$predatado->transacao->venda->pessoaCliente->nome}} <br>
            <strong>Observação:</strong> {{$predatado->observacao}}
          </p>
          <table class="table table-middle table-hover table-condensed">
            <thead>
            <tr>
              <th>Vencimento</th>
              <th>Número</th>
              <th>Banco</th>
              <th>Agência</th>
              <th>Conta</th>
              <th>Valor</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{{$cheque->vencimento->format('d/m/Y')}}</td>
              <td>{{$cheque->numero}}</td>
              <td>{{$cheque->banco}}</td>
              <td>{{$cheque->agencia}}</td>
              <td>{{$cheque->conta}}</td>
              <td>R${{number_format($cheque->valor, 2, ',', '')}}</td>
            </tr>
            </tbody>
          </table>
          <div class="row">
            <div class="form-group col-xs-12 col-md-6">
              <label>Escolha o tipo de processamento</label>
              <select name="tipo_processamento" class="form-control">
                <option value="">Selecione...</option>
                <option value="D">Depósito em conta</option>
                <option value="S">Saque com entrada em caixa</option>
              </select>
            </div>
          </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">
            <i class="fa fa-ban"></i> Cancelar / Fechar
          </button>
          <button type="submit" class="btn btn-success btn-flat">
            <i class="fa fa-save"></i> Salvar
          </button>
        </div>

      </form>
    </div>
  </div>
</div>
