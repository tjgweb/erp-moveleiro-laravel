@extends('caixa.layout')
@section('title', 'Caixa - Cheques | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bank text-light-blue"></i> Cheques
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bank text-light-blue"></i> Cheques</li>
      <li><i class="fa fa-circle-o text-green"></i> processados</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title with-border">Lista de cheques processados.</h3>
        <div class="box-tools">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
              class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <form action="{{route('caixa.cheques.processados')}}" method="get">
          <div class="row">
            <div class="form-group col-md-4">
              <label>Data Inicial</label>
              <input type="date" name="start" value="{{$form['start'] ?? ''}}" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Data Final</label>
              <div class="input-group">
                <input type="date" name="end" value="{{$form['end'] ?? ''}}" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  <a href="{{route('caixa.cheques.processados')}}" class="btn btn-default"><i class="fa fa-eraser"></i></a>
                </span>
              </div>
            </div>
          </div>
        </form>
        
        @if(!$cheques->count())
          <p class="bg-info text-center">Nenhum cheque foi processado.</p>
        @else
          <table class="table table-middle table-hover table-condensed">
            <thead>
            <tr>
              <th>Caixa</th>
              <th>Data do processamento</th>
              <th>Tipo de processamento</th>
              <th>Vencimento</th>
              <th>Número</th>
              <th>Banco</th>
              <th>Agência</th>
              <th>Conta</th>
              <th>Valor</th>
              <th>Cliente</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cheques as $cheque)
              <tr>
                <td>{{$cheque->transacoes->first()->caixa->operador->apelido}}</td>
                <td>{{$cheque->transacoes->first()->created_at->format('d/m/Y - H:i')}}</td>
                <td>{{$cheque->transacoes->first()->pivot->tipo_processamento == 'D' ? 'Depósito em conta' : 'Saque com entrada em caixa'}}</td>
                <td>{{$cheque->vencimento->format('d/m/Y')}}</td>
                <td>{{$cheque->numero}}</td>
                <td>{{$cheque->banco}}</td>
                <td>{{$cheque->agencia}}</td>
                <td>{{$cheque->conta}}</td>
                <td>R${{number_format($cheque->valor, 2, ',', '')}}</td>
                <td>{{$cheque->predatado->transacao->venda->pessoaCliente->nome}}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        @endif
      </div>

      <div class="box-footer text-center">
        {{$cheques->links()}}
      </div>
    </div>
  </section>
@endsection
