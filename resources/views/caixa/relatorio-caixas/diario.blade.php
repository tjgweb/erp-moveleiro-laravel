@extends('caixa.layout')
@section('title', 'Caixa - Relatórios | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bar-chart-o text-info"></i> Relatórios Diários
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bar-chart-o text-info"></i> Relatórios Diários</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">

        <div class="row">
          <form action="{{route('caixa.relatorios.diario.index')}}" method="get">
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Data inicial</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="date" name="start" class="form-control" value="{{$form['start']}}">
              </div>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Data final</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="date" name="end" class="form-control" value="{{$form['end']}}">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </div>
          </form>
        </div>
        <br>
        <div class="table-responseive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th>Abertura</th>
              <th>Última atualização</th>
              <th class="text-center">Aberto</th>
              <th>Relatório</th>
            </tr>
            </thead>
            <tbody>
            @if($caixas->count())
              @foreach($caixas as $caixa)
                <tr>
                  <td>
                    {{date('d/m/Y - H:i', strtotime($caixa->created_at))}}
                  </td>
                  <td>
                    {{date('d/m/Y - H:i', strtotime($caixa->updated_at))}}
                  </td>
                  @if($caixa->aberto)
                    <td class="text-center"><i class="fa fa-circle text-green"></i></td>
                  @else
                    <td class="text-center"><i class="fa fa-circle text-red"></i></td>
                  @endif
                  <td>
                    <a href="{{route('caixa.relatorios.diario.show', ['id' => $caixa->id])}}" target="_blank"
                       class="text-black">
                      <i class="fa fa-file-pdf-o fa-lg text-red"></i> Ver/Imprimir
                    </a>
                  </td>
                </tr>
              @endforeach
            @else
              <tr class="info">
                <td colspan="5" class="text-center">
                  Nenhum caixa encontrado entre as datas configuradas para o usuário
                  <strong>{{auth()->user()->apelido}}</strong>
                </td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-xs-12 text-center">{{$caixas->links()}}</div>
        </div>
      </div>{{--box-body--}}
    </div>
  </section>
@endsection
