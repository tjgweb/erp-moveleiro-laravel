@if(!$meses->count())
  <p>Nada encontrado.</p>
@else
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @php $i=0; @endphp
    @foreach($meses as $mes)

      @php date('m', strtotime($mes[0]->created_at)) == date('m') ? $expanded = true : $expanded = false; @endphp
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading-{{$i}}">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$i}}"
               aria-controls="collapse-{{$i}}" aria-expanded="{{$expanded}}">
              {{ucfirst($mes[0]->created_at->localeMonth)}}
            </a>
            <a
              href="{{route('caixa.relatorios.mensal.show', ['mes' => date('m', strtotime($mes[0]->created_at)), 'ano' => $ano])}}"
              target="_blank"
              class="btn btn-default btn-xs btn-flat pull-right">
              <i class="fa fa-file-pdf-o text-red"></i> Relatório
            </a>
          </h4>
        </div>
        <div id="collapse-{{$i}}" class="panel-collapse collapse {{$expanded ? 'in' : ''}}" role="tabpanel"
             aria-labelledby="heading-{{$i}}">
          <div class="panel-body">
            @foreach($mes as $caixa)
              <h4 class="bg-aqua" style="padding: 5px;">
                {{ucfirst($caixa->created_at->localeDayOfWeek)}} dia {{$caixa->created_at->day}}
                @if($admin)
                  <span class="pull-right">Operador: <strong>{{$caixa->operador->apelido}}</strong></span>
                @endif
              </h4>
              <div class="table-responseive">
                <table class="table table-middle table-condensed">
                  <thead>
                  <tr>
                    <th class="text-center">Troco inicial</th>
                    <th class="text-center text-green">Créditos</th>
                    <th class="text-center">Dinheiro</th>
                    <th class="text-center">C. Débito</th>
                    <th class="text-center">C. Crédito</th>
                    <th class="text-center">Financiamento</th>
                    <th class="text-center">Crediário</th>
                    <th class="text-center">Cheque</th>
                    <th class="text-center text-red">Retirada</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td class="text-right">{{number_format($caixa->troco_inicial, 2,',','.')}}</td>
                    <td class="text-right text-green">{{number_format($caixa->total_credito, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_dinheiro, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_cartao_debito, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_cartao_credito, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_financiamento, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_crediario, 2,',','.')}}</td>
                    <td class="text-right">{{number_format($caixa->total_cheque, 2,',','.')}}</td>
                    <td class="text-right text-red">{{number_format($caixa->total_retirada, 2,',','.')}}</td>
                  </tr>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th colspan="7">
                      <a href="{{route('caixa.relatorios.diario.show', ['caixaId' => $caixa->id])}}"
                         target="_blank" class="btn btn-default btn-xs btn-flat">
                        <i class="fa fa-file-pdf-o text-red"></i> Relatório caixa
                      </a>
                    </th>
                    <th class="text-right text-bold">Fluxo de Caixa</th>
                    <th class="text-right text-bold">{{number_format($caixa->fluxo, 2,',','.')}}</th>
                  </tr>
                  {{--<tr>
                    <th colspan="8" class="text-right text-bold">Em Caixa</th>
                    <th class="text-right text-bold">{{number_format($caixa->em_caixa, 2,',','.')}}</th>
                  </tr>--}}
                  </tfoot>
                </table>

                <br>
                @endforeach
              </div>
          </div>
        </div>
      </div>

      @php $i++; @endphp
    @endforeach
  </div>
@endif
