@extends('caixa.layout')
@section('title', 'Caixa - Relatórios | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-bar-chart-o text-info"></i> Relatórios Mensais
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-bar-chart-o text-info"></i> Relatórios Mensais</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">

        <div class="row">
          <form action="{{route('caixa.relatorios.mensal.index')}}" method="get">
            <div class="col-xs-6 col-md-4 col-lg-3">
              <label>Ano</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <select name="ano" class="form-control">
                  @for($i = 2019; $i < 2030; $i++)
                    <option value="{{$i}}" {{$i == $ano ? 'selected' : ''}}>{{$i}}</option>
                  @endfor
                </select>
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </div>
          </form>
        </div>

        <br>

        @include('caixa.relatorio-caixas._mensal-content', ['admin' => false])

      </div>
    </div>
  </section>
@endsection
