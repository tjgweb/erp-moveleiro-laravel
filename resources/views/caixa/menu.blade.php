<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header text-center text-bold">
        <i class="fa fa-calculator fa-lg"></i> MENU CAIXA
      </li>

      <li>
        <a href="{{route('caixa.index')}}">
          <i class="fa fa-dashboard"></i> <span>Painel</span>
        </a>
      </li>

      <li>
        <a href="{{route('caixa.vendas.index')}}">
          <i class="fa fa-shopping-cart text-green"></i> <span>Receber Vendas</span>
        </a>
      </li>

      <li>
        <a href="{{route('caixa.crediario.index')}}">
          <i class="fa fa-list-ol text-yellow"></i> <span>Crediário</span>
        </a>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-bank text-light-blue"></i> <span>Cheques</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('caixa.cheques.index')}}"><i class="fa fa-circle-o text-yellow"></i> Cheques à processar</a></li>
          <li><a href="{{route('caixa.cheques.processados')}}"><i class="fa fa-circle-o text-green"></i> Cheques processados</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-money text-green"></i> <span>Vencimentos</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle text-green"></i> Salários</a></li>
          <li><a href="#"><i class="fa fa-circle-o text-green"></i> Comissões</a></li>
          <li><a href="{{route('caixa.vencimentos.vales.index')}}"><i class="fa fa-ticket text-red"></i> Vales</a></li>
        </ul>
      </li>

      <li>
        <a href="{{route('caixa.relatorios.diario.index')}}">
          <i class="fa fa-bar-chart-o text-info"></i> <span>Relatórios diários</span>
        </a>
      </li>

      <li>
        <a href="{{route('caixa.relatorios.mensal.index')}}">
          <i class="fa fa-bar-chart-o text-info"></i> <span>Relatórios mensais</span>
        </a>
      </li>

      <li>
        <a href="{{route('caixa.pessoas.index')}}">
          <i class="fa fa-users"></i> <span>Pessoas</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
