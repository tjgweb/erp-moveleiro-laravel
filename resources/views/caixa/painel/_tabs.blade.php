<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active">
    <a href="#resumo-{{$caixa->id}}" aria-controls="resumo-{{$caixa->id}}" role="tab" data-toggle="tab">
      <i class="fa fa-bar-chart-o text-primary"></i> Resumo
    </a>
  </li>
  <li role="presentation">
    <a href="#vendas-{{$caixa->id}}" aria-controls="vendas-{{$caixa->id}}" role="tab" data-toggle="tab">
      <i class="fa fa-shopping-cart text-green"></i> Vendas Recebidas
    </a>
  </li>
  <li role="presentation">
    <a href="#crediarios-{{$caixa->id}}" aria-controls="crediarios-{{$caixa->id}}" role="tab"
       data-toggle="tab">
      <i class="fa fa-list-ol text-yellow"></i> Crediários Recebidos
    </a>
  </li>
  <li role="presentation">
    <a href="#cheques-{{$caixa->id}}" aria-controls="cheques-{{$caixa->id}}" role="tab"
       data-toggle="tab">
      <i class="fa fa-bank"></i> Cheques Processados
    </a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane fade in active" id="resumo-{{$caixa->id}}">
    <br>
    @include('caixa.painel._resumo', ['caixa' => $caixa])
  </div>
  <div role="tabpanel" class="tab-pane fade" id="vendas-{{$caixa->id}}">
    @include('caixa.painel._vendas-recebidas', ['caixa' => $caixa])
  </div>
  <div role="tabpanel" class="tab-pane fade" id="crediarios-{{$caixa->id}}">
    @include('caixa.painel._crediarios-recebidos', ['caixa' => $caixa])
  </div>
  <div role="tabpanel" class="tab-pane fade" id="cheques-{{$caixa->id}}">
    @include('caixa.painel._cheques-processados', ['caixa' => $caixa])
  </div>
</div>
