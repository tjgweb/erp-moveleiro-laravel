<div class="table-responsive">
  <table class="table table-middle table-hover table-condensed">
    <thead>
    <tr>
      <th class="text-center">Transação</th>
      <th class="text-center">Dia/Mês</th>
      <th class="text-center">Hora</th>
      <th class="text-center">Venda</th>
      <th>Vendedor</th>
      <th>Cliente</th>
      <th class="text-center">Valor</th>
      <th class="text-right">Ações</th>
    </tr>
    </thead>
    <tbody>
    @if($caixa->transacoes->where('venda_id', '!=', null)->count())
      @foreach($caixa->transacoes->where('venda_id', '!=', null) as $transacao)
        <tr>
          <td class="text-center">{{$transacao->id}}</td>
          <td class="text-center">{{date('d/m', strtotime($transacao->created_at))}}</td>
          <td class="text-center">{{date('H:i', strtotime($transacao->created_at))}}</td>
          <td class="text-center">{{$transacao->venda->id}}</td>
          <td>{{$transacao->venda->pessoaVendedor->nome}}</td>
          <td>{{$transacao->venda->pessoaCliente->nome}}</td>
          <td class="text-right">{{number_format($transacao->venda->valor_pago, 2, ',', '.')}}</td>
          <td class="btn-actions">
            <a href="{{route('caixa.recibo', ['transacaoId' => $transacao->id])}}" target="_blank"
               class="btn btn-primary btn-flat btn-sm">
              <i class="fa fa-print"></i> Recibo
            </a>
            @if($transacao->venda->vendaPagamentos()->where('tipo_pagamento_id', 4)->first() ?? false)
            <a href="{{route('caixa.carne', ['vendaId' => $transacao->venda->id])}}" target="_blank"
               class="btn btn-primary btn-flat btn-sm">
              <i class="fa fa-print"></i> Carnê
            </a>
            @endif
          </td>
        </tr>
      @endforeach
    @else
      <tr>
        <td colspan="8" class="text-center info">Nenhuma venda recebida.</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
  
