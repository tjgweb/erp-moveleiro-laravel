<div class="table-responsive">
  <table class="table table-middle table-hover table-condensed">
    <thead>
    <tr>
      <th class="text-center">Transação</th>
      <th class="text-center">Dia/Mês</th>
      <th class="text-center">Hora</th>
      <th class="text-center">Crediário</th>
      <th class="text-center">Parcela</th>
      <th class="text-center">Abatimento</th>
      <th class="text-center">Valor</th>
      <th>Cliente</th>
      <th class="text-right">Ações</th>
    </tr>
    </thead>
    <tbody>
    @foreach($caixa->transacoes->where('venda_id', null) as $transacao)
      @foreach($transacao->parcelas as $parcela)
        <tr>
          <td class="text-center">{{$transacao->id}}</td>
          <td class="text-center">{{date('d/m', strtotime($transacao->created_at))}}</td>
          <td class="text-center">{{date('H:i', strtotime($transacao->created_at))}}</td>
          <td class="text-center">{{$parcela->crediario_id}}</td>
          <td class="text-center">{{$parcela->sequencia}}</td>
          <td class="text-center">{{$parcela->pivot->abatimento ? 'Sim' : 'Não'}}</td>
          <td class="text-right">{{number_format($parcela->pivot->valor, 2, ',', '.')}}</td>
          <td>{{$parcela->crediario->transacao->venda->pessoaCliente->nome}}</td>
          <td class="btn-actions">
            <a href="{{route('caixa.recibo', ['transacaoId' => $transacao->id])}}"
               class="btn btn-primary btn-sm btn-flat" target="_blank">
              <i class="fa fa-print"></i> Recibo
            </a>
          </td>
        </tr>
      @endforeach
    @endforeach
    </tbody>
  </table>
</div>
  

