<div class="modal fade" id="aberturaModal" tabindex="-1" role="dialog" aria-labelledby="aberturaModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Abertura de Caixa
          <small>{{date('d/m/Y')}}</small>
        </h4>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('caixa.abrir-caixa')}}">
          @csrf
          <input type="hidden" name="operador_id" value="{{auth()->user()->id}}">
          <div class="form-group">
            <label>Troco inicial</label>
            <div class="input-group">
              <input type="number" name="troco_inicial" min="0" step="0.01" class="form-control" required>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">Abrir caixa</button>
              </span>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">
          <i class="fa fa-ban"></i> Cancelar
        </button>
      </div>
    </div>
  </div>
</div>
