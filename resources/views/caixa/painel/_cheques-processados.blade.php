<div class="table-responsive">
  <table class="table table-middle table-hover table-condensed">
    <thead>
    <tr>
      <th class="text-center">Transação</th>
      <th class="text-center">Nº cheque</th>
      <th class="text-center">Banco</th>
      <th class="text-center">Agência</th>
      <th class="text-center">Conta</th>
      <th>Cliente</th>
      <th>Observação</th>
      <th class="text-center">Tipo processamento</th>
      <th class="text-center">Valor</th>
    </tr>
    </thead>
    <tbody>
    @foreach($caixa->transacoes->where('venda_id', null) as $transacao)
      @foreach($transacao->cheques as $cheque)
        <tr>
          <td class="text-center">{{$transacao->id}}</td>
          <td class="text-center">{{$cheque->numero}}</td>
          <td class="text-center">{{$cheque->banco}}</td>
          <td class="text-center">{{$cheque->agencia}}</td>
          <td class="text-center">{{$cheque->conta}}</td>
          <td>{{$cheque->predatado->transacao->venda->pessoaCliente->nome}}</td>
          <td>{{$cheque->predatado->observacao}}</td>
          <td class="text-center">{{$cheque->pivot->tipo_processamento == 'D' ? 'Depósito em conta' : 'Saque com entrada em caixa'}}</td>
          <td class="text-right">{{number_format($cheque->valor, 2, ',', '.')}}</td>
        </tr>
      @endforeach
    @endforeach
    </tbody>
  </table>
</div>
  

