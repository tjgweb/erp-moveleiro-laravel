@extends('caixa.layout')
@section('title', 'Caixa - Painel | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> Painel
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
    </ol>
  </section>

  <section class="content">
    <h3>Caixas abertos</h3>
    @if($caixas->count())
      @foreach($caixas as $caixa)
        <div class="box">
          <div class="box-header">
            <h3 class="box-title with-border">{{date('d/m/Y - H:i', strtotime($caixa->created_at))}}</h3>
            <div class="box-tools">
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                  class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            @include('caixa.painel._tabs', ['caixa' => $caixa])
          </div>
        </div>
      @endforeach
    @else
      <div class="box">
        <div class="box-body">
          <h5>Não existe caixa aberto para o usuário <b>{{auth()->user()->apelido}}</b>.</h5>
          <button type="button" class="btn btn-primary btn-flat" data-toggle="modal"
                  data-target="#aberturaModal">
            Clique aqui para fazer abertura de caixa
          </button>
          @include('caixa.painel._modal-abertura')
        </div>
      </div>
    @endif

    @if($outrosCaixas->count() && auth()->user()->hasPermission('App\Http\Controllers\Gestao\DashboardController@index'))
      <br>
      <h3>Outros caixas abertos</h3>
      @foreach($outrosCaixas as $outroCaixa)
        <div class="box">
          <div class="box-header">
            <h3 class="box-title with-border">
              <i class="fa fa-user text-aqua"></i> {{$outroCaixa->operador->apelido}} | {{date('d/m/Y - H:i', strtotime($outroCaixa->created_at))}}
            </h3>
            <div class="box-tools">
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                  class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            @include('caixa.painel._tabs', ['caixa' => $outroCaixa])
          </div>
        </div>
      @endforeach
    @endif
  </section>
@endsection
