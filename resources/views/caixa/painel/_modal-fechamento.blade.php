<div class="modal fade" id="fechamentoModal" tabindex="-1" role="dialog" aria-labelledby="fechamentoModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Encerramento de Caixa
        </h4>
      </div>
      <form method="post" action="{{route('caixa.fechar-caixa')}}">
        @csrf
        <input type="hidden" name="caixa_id" value="{{$caixa->id}}">
        <input type="hidden" name="em_caixa" value="{{$caixa->em_caixa}}">
        <div class="modal-body">
          <h4>Deseja fechar este caixa?</h4>
          <p><strong>Abertura:</strong> {{date('d/m/Y H:i', strtotime($caixa->created_at))}}</p>
          <p><strong>Operador:</strong> {{$caixa->operador->apelido}}</p>
          <p><strong>Fundo de caixa para o dia seguinte:</strong> R${{number_format($caixa->em_caixa, 2, ',', '.')}}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">
            <i class="fa fa-ban"></i> Não
          </button>
          <button type="submit" class="btn btn-success btn-flat">
            <i class="fa fa-check"></i> Sim
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
