<div class="modal fade" id="{{$modalId}}" tabindex="-1" role="dialog" aria-labelledby="{{$modalId}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="{{$modalId}}Label">{{$title}}</h4>
      </div>
      <div class="modal-body">

        <form method="post" action="{{$formAction}}">
          @csrf
          <input type="hidden" name="caixa_id" value="{{$caixa->id}}">
          <div class="form-group has-feedback {{ $errors->has('descricao') ? 'has-error' : '' }}">
            <label>Descrição</label>
            {{--<input type="text" name="descricao" value="{{ old('descricao') }}" class="form-control">--}}
            <textarea name="descricao" rows="2" class="form-control">{{ old('descricao') }}</textarea>
            @if ($errors->has('descricao'))
              <span class="text-danger">{{ $errors->first('descricao') }}</span>
            @endif
          </div>
          <div class="row">
            <div class="form-group col-xs-5 col-md-4 has-feedback {{ $errors->has('hora') ? 'has-error' : '' }}">
              <label>Hora</label>
              <input type="time" name="hora" value="{{ old('hora') ?? date('H:i') }}" class="form-control">
              @if ($errors->has('hora'))
                <span class="text-danger">{{ $errors->first('hora') }}</span>
              @endif
            </div>
            <div class="col-xs-7 col-md-8">
              <label class="{{ $errors->has('valor') ? 'text-danger' : '' }}">Valor</label>
              <div class="input-group {{ $errors->has('valor') ? 'has-error' : '' }}">
                <input type="number" name="valor" step="0.01" class="form-control" value="{{old('valor')}}">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"
                  {{!session()->has('caixa_id') || session()->has('caixa_id') && $caixa->id != session()->get('caixa_id') ? 'disabled' : ''}}>
                    <i class="fa fa-save"></i> Salvar
                  </button>
                </span>
              </div>
              @if ($errors->has('valor'))
                <span class="text-danger">{{ $errors->first('valor') }}</span>
              @endif
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">
          <i class="fa fa-ban"></i> Cancelar
        </button>
      </div>
    </div>
  </div>
</div>
