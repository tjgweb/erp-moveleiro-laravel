<div class="row">
  <div class="col-xs-12 btn-actions">
    @if(session()->has('caixa_id') && $caixa->id == session()->get('caixa_id'))
      <button class="btn btn-default btn-flat pull-left" disabled><i class="fa fa-play"></i> Caixa selecionado.</button>
    @else
      <form method="post" action="{{route('caixa.selecionar-caixa')}}">
        @csrf
        <input type="hidden" name="caixa_id" value="{{$caixa->id}}">
        <input type="hidden" name="caixa_apelido" value="{{$caixa->operador->apelido}}">
        <button type="submit" class="btn btn-default btn-flat pull-left">
          <i class="fa fa-play-circle-o"></i> Usar caixa
        </button>
      </form>
    @endif
    @if($caixa->aberto)
      <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#fechamentoModal"
        {{!session()->has('caixa_id') || session()->has('caixa_id') && $caixa->id != session()->get('caixa_id') ? 'disabled' : ''}}>
        <i class="fa fa-ban"></i> Encerrar Caixa
      </button>
      @include('caixa.painel._modal-fechamento', ['caixa' => $caixa])
    @endif
    <a href="{{route('caixa.relatorios.diario.show', ['caixaId' => $caixa->id])}}" target="_blank"
       class="btn btn-default btn-flat pull-left">
      <i class="fa fa-file-pdf-o text-red"></i> Relatório
    </a>
    {{--<button class="btn btn-default btn-flat pull-left" data-toggle="modal" data-target="#chequeModal"
      {{!session()->has('caixa_id') || session()->has('caixa_id') && $caixa->id != session()->get('caixa_id') ? 'disabled' : ''}}>
      <i class="fa fa-bank"></i> Cheques
    </button>
    <vue-modal-cheques modal-id="chequeModal"></vue-modal-cheques>--}}
  </div>
  <div class="col-xs-12">
    <br>
    <div class="table-responsive">
      <table class="table table-middle table-striped">
        <thead>
        <tr>
          <th class="text-center">Troco inicial</th>
          <th class="text-center">Crédito</th>
          <th class="text-center">Dinheiro</th>
          <th class="text-center">Cartão Débito</th>
          <th class="text-center">Cartão Crédito</th>
          <th class="text-center">Financiamento</th>
          <th class="text-center">Vendas Crediário</th>
          <th class="text-center">Cheque Recebidos</th>
          <th class="text-center">Reirada</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td class="text-center text-green">{{number_format($caixa->troco_inicial, 2, ',', '.')}}</td>
          <td class="text-center text-green">{{number_format($caixa->total_credito, 2, ',', '.')}}</td>
          <td class="text-center text-green">{{number_format($caixa->total_dinheiro, 2, ',', '.')}}</td>
          <td class="text-center">{{number_format($caixa->total_cartao_debito, 2, ',', '.')}}</td>
          <td class="text-center">{{number_format($caixa->total_cartao_credito, 2, ',', '.')}}</td>
          <td class="text-center">{{number_format($caixa->total_financiamento, 2, ',', '.')}}</td>
          <td class="text-center">{{number_format($caixa->total_crediario, 2, ',', '.')}}</td>
          <td class="text-center">{{number_format($caixa->total_cheque, 2, ',', '.')}}</td>
          <td class="text-center text-danger">{{number_format($caixa->total_retirada, 2, ',', '.')}}</td>
        </tr>
        <tr>
          <th colspan="8" class="text-right"><h4>Fluxo de caixa</h4></th>
          <th class="text-right"><h4 class="text-bold">{{number_format($caixa->fluxo, 2, ',', '.')}}</h4></th>
        </tr>
        <tr>
          <th colspan="8" class="text-right"><h4>Em caixa</h4></th>
          <th class="text-right"><h4 class="text-bold text-green">{{number_format($caixa->em_caixa, 2, ',', '.')}}</h4>
          </th>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-success">
      <div class="box-header">
        <h3 class="box-title">Créditos concedidos ao Caixa</h3>
      </div>
      <div class="box-body">
        <button class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modalCredito"
          {{!session()->has('caixa_id') || session()->has('caixa_id') && $caixa->id != session()->get('caixa_id') ? 'disabled' : ''}}>
          <i class="fa fa-plus"></i> Novo Crédito
        </button>
        <br>
        @include('caixa.painel._modal-form-credito-debito', [
          'formAction' => route('caixa.credito.store'),
          'modalId' => 'modalCredito',
          'title' => 'Adicionar novo crédito ao caixa'
        ])

        @if($caixa->creditos->count())
          <table class="table table-middle table-hover table-condensed">
            <thead>
            <tr>
              <th>Hora</th>
              <th>Descrição</th>
              <th class="text-right">Valor</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($caixa->creditos as $credito)
              <tr>
                <td>{{date('H:i', strtotime($credito->hora))}}</td>
                <td>{{$credito->descricao}}</td>
                <td class="text-right">{{number_format($credito->valor, 2, ',', '.')}}</td>
                <td class="btn-actions">
                  @if(session()->has('caixa_id') && $caixa->id == session()->get('caixa_id') && $credito->delete_action)
                    <a href="javascript:void(0)"
                       onclick="event.preventDefault(); document.getElementById('cancelar-credito-{{$credito->id}}').submit();">
                      <i class="fa fa-times-circle text-red" title="Cancelar Retirada"></i>
                    </a>
                    <form id="cancelar-credito-{{$credito->id}}"
                          action="{{ route('caixa.credito.destroy', ['id' => $credito->id]) }}" method="POST"
                          style="display: none;">
                      @csrf @method('DELETE')
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
            <tr>
              <th colspan="2" class="text-right"><h4>Total</h4></th>
              <td class="text-right"><h4>{{number_format($caixa->total_credito, 2, ',', '.')}}</h4></td>
              <td></td>
            </tr>
            </tbody>
          </table>
        @else
          <p class="text-center bg-info">Nenhum crédito foi concedido</p>
        @endif
      </div>
    </div>
  </div>

  <div class="col-xs-12">
    <div class="box box-solid box-danger">
      <div class="box-header">
        <h3 class="box-title">Retiradas do Caixa</h3>
      </div>
      <div class="box-body">
        <button class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modalRetirada"
          {{!session()->has('caixa_id') || session()->has('caixa_id') && $caixa->id != session()->get('caixa_id') ? 'disabled' : ''}}>
          <i class="fa fa-plus"></i> Nova Retirada
        </button>
        <br>
        @include('caixa.painel._modal-form-credito-debito', [
          'formAction' => route('caixa.retirada.store'),
          'modalId' => 'modalRetirada',
          'title' => 'Gerar nova retirada de caixa'
        ])

        @if($caixa->retiradas->count())
          <table class="table table-middle table-hover table-condensed">
            <thead>
            <tr>
              <th>Hora</th>
              <th>Descrição</th>
              <th class="text-right">Valor</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($caixa->retiradas as $retirada)
              <tr>
                <td>{{date('H:i', strtotime($retirada->hora))}}</td>
                <td>{{$retirada->descricao}}</td>
                <td class="text-right">{{number_format($retirada->valor, 2, ',', '.')}}</td>
                <td class="btn-actions">
                  @if(session()->has('caixa_id') && $caixa->id == session()->get('caixa_id') && $retirada->delete_action)
                    <a href="javascript:void(0)"
                       onclick="event.preventDefault(); document.getElementById('cancelar-retirada-{{$retirada->id}}').submit();">
                      <i class="fa fa-times-circle text-red" title="Cancelar Retirada"></i>
                    </a>
                    <form id="cancelar-retirada-{{$retirada->id}}"
                          action="{{ route('caixa.retirada.destroy', ['id' => $retirada->id]) }}" method="POST"
                          style="display: none;">
                      @csrf @method('DELETE')
                    </form>
                  @endif
                </td>
              </tr>
            @endforeach
            <tr>
              <th colspan="2" class="text-right"><h4>Total</h4></th>
              <td class="text-right"><h4>{{number_format($caixa->total_retirada, 2, ',', '.')}}</h4></td>
              <td></td>
            </tr>
            </tbody>
          </table>
        @else
          <p class="text-center bg-info">Nenhum retirada foi feita</p>
        @endif
      </div>
    </div>
  </div>
</div>
