@extends('caixa.layout')
@section('title', 'Caixa - Vales | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-ticket text-red"></i> Vales
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-money text-green"></i> Vencimentos</li>
      <li><i class="fa fa-ticket text-red"></i> Vales</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title with-border">
          @if($form['type'] == 'create')
            Adicionar Vale
          @elseif($form['type'] == 'edit')
            Editar Vale
          @elseif($form['type'] == 'remove')
            Deseja remover o vale?
          @endif
        </h3>
      </div>
      <div class="box-body">

        <form action="{{$form['action']}}" method="post">
          @csrf @method($form['method'])

          <input type="hidden" name="vencimento_tipo_id" value="{{$form['vencimentoTipoId']}}">

          <div class="row">
            <div class="form-group col-xs-12 {{$errors->has('pessoa_id') ? 'has-error' : ''}}">
              <label>Funcionário</label>
              <select name="pessoa_id" id="funcionarios"
                      class="form-control" {{$form['type'] == 'edit' || $form['type'] == 'remove' ? 'disabled' : ''}}>
                <option value="">Selecione...</option>
                @foreach($funcionarios as $funcionario)
                  <option value="{{$funcionario->id}}" {{old('pessoa_id') && old('pessoa_id') == $funcionario->id ? 'selected' : 
                    isset($vencimento) && $vencimento->pessoa->id == $funcionario->id ? 'selected' : ''}}>
                    {{$funcionario->nome}}
                  </option>
                @endforeach
              </select>
              <span class="small text-danger">{{$errors->first('pessoa_id')}}</span>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-6 col-md-4 {{$errors->has('mes') ? 'has-error' : ''}}">
              <label>Mês referênca</label>
              <select name="mes" class="form-control" {{$form['type'] == 'remove' ? 'disabled' : ''}}>
                <option value="">Selecione...</option>
                @for($i = 0; $i < 12; $i++)
                  <option value="{{$i+1}}" {{old('mes') && old('mes') == $i+1 ? 'selected' : 
                    isset($vencimento) && $vencimento->mes == $i+1 ? 'selected' : ''}}>
                    {{ucfirst(now()->firstOfYear()->addMonth($i)->localeMonth)}}
                  </option>
                @endfor
              </select>
              <span class="small text-danger">{{$errors->first('mes')}}</span>
            </div>

            <div class="form-group col-xs-6 col-md-4 {{$errors->has('ano') ? 'has-error' : ''}}">
              <label>Ano referênca</label>
              <select name="ano" class="form-control" {{$form['type'] == 'remove' ? 'disabled' : ''}}>
                <option value="">Selecione...</option>
                @for($i = date('Y'); $i > 2018; $i--)
                  <option value="{{$i}}" {{old('ano') && old('ano') == $i ? 'selected' : 
                    isset($vencimento) && $vencimento->ano == $i ? 'selected' : ''}}>
                    {{$i}}
                  </option>
                @endfor
              </select>
              <span class="small text-danger">{{$errors->first('ano')}}</span>
            </div>

            <div class="form-group col-xs-12 col-md-4 {{$errors->has('valor') ? 'has-error' : ''}}">
              <label>Valor</label>
              <input type="number" name="valor" step="0.01" class="form-control" placeholder="100,00"
                     value="{{old('valor') ?? $vencimento->valor ?? ''}}"
                    {{$form['type'] == 'remove' ? 'disabled' : ''}}>
              <span class="small text-danger">{{$errors->first('valor')}}</span>
            </div>
          </div>

          <a href="{{route('caixa.vencimentos.vales.index')}}" class="btn btn-default btn-flat">
            <i class="fa fa-ban"></i> Cancelar / Voltar
          </a>
          
          <button type="submit" class="btn btn-flat pull-right {{$form['type'] == 'create' ? 'btn-primary' : ''}} 
                  {{$form['type'] == 'edit' ? 'btn-warning' : ''}} {{$form['type'] == 'remove' ? 'btn-danger': ''}}">
            @if($form['type'] == 'remove')
              <i class="fa fa-trash-o"></i> Remover
            @else
              <i class="fa fa-save"></i> Salvar
            @endif
          </button>
          
        </form>

      </div>
    </div>
  </section>
@endsection
@push('scripts')
  <script type="text/javascript">
    $('#funcionarios').select2();
  </script>
@endpush
