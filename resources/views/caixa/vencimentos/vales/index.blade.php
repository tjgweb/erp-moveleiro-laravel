@extends('caixa.layout')
@section('title', 'Caixa - Vales | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-ticket text-red"></i> Vales
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-money text-green"></i> Vencimentos</li>
      <li><i class="fa fa-ticket text-red"></i> Vales</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title with-border">Lista de vales concedidos.</h3>
      </div>
      <div class="box-body">

        <a href="{{route('caixa.vencimentos.vales.create')}}" class="btn btn-primary btn-xs btn-flat pull-right">
          <i class="fa fa-plus"></i> adicionar
        </a>

        <form action="{{route('caixa.vencimentos.vales.index')}}" method="get">
          <div class="row">
            <div class="form-group col-md-4">
              <label>Data Inicial</label>
              <input type="date" name="start" value="{{$form['start'] ?? ''}}" class="form-control">
            </div>
            <div class="col-md-4">
              <label>Data Final</label>
              <div class="input-group">
                <input type="date" name="end" value="{{$form['end'] ?? ''}}" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  <a href="{{route('caixa.vencimentos.vales.index')}}" class="btn btn-default"><i
                      class="fa fa-eraser"></i></a>
                </span>
              </div>
            </div>
          </div>
        </form>

        @if($vencimentos->count() == 0)
          <p class="text-center bg-info">Nenhum resultado encontrado entre as datas informadas.</p>
        @else

          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th>Pessoa / Funcionário</th>
              <th>Valor</th>
              <th>Referência</th>
              <th class="text-right">
                <button class="btn btn-default btn-xs btn-flat" data-container="body" data-toggle="popover" data-placement="left"
                        data-content="Vencimentos não podem ser editados ou removidos se o caixa de seu lançamento estiver encerrado.">
                  <i class="fa fa-info"></i> nfo
                </button>
              </th>
            </tr>
            </thead>
            <tbody>
            @foreach($vencimentos as $vencimento)
              <tr>
                <td>{{$vencimento->pessoa->nome}}</td>
                <td>R${{number_format($vencimento->valor, 2, ',', '.')}}</td>
                <td>{{$vencimento->referencia}}</td>
                <td class="btn-actions">
                  @if($vencimento->transacao->caixa->aberto)
                    <a href="{{route('caixa.vencimentos.vales.show', ['id' => $vencimento->id])}}" class="btn btn-flat btn-xs btn-danger">
                      <i class="fa fa-trash-o"></i>
                    </a>
                    <a href="{{route('caixa.vencimentos.vales.edit', ['id' => $vencimento->id])}}" class="btn btn-flat btn-xs btn-warning">
                      <i class="fa fa-edit"></i>
                    </a>
                  @else
                    
                  @endif
                </td>
              </tr>
            </tbody>
            @endforeach
          </table>

          <div class="text-center">{{$vencimentos->links()}}</div>
        @endif

      </div>
    </div>
  </section>
@endsection
