@extends('caixa.layout')
@section('title', 'Caixa - Vendas | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-shopping-cart"></i> Vendas a receber
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-shopping-cart"></i> Vendas a receber</li>
    </ol>
  </section>

  <section class="content">
    <vue-vendas></vue-vendas>
  </section>
@endsection
