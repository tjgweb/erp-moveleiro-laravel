@extends('caixa.layout')
@section('title', 'Caixa - Recebimento | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-money"></i> Recebimento Venda
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><a href="{{route('caixa.vendas.index')}}"><i class="fa fa-shopping-cart"></i> Vendas</a></li>
      <li><i class="fa fa-money"></i> Recebimento Venda</li>
    </ol>
  </section>

  <section class="content">
    <vue-vendas-recebimento data-venda="{{$data}}" caixa-id="{{$caixa_id}}"></vue-vendas-recebimento>
  </section>
@endsection
