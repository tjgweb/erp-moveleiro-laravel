@extends('caixa.layout')
@section('title', 'Caixa - Crediário | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-list-ol text-yellow"></i> Crediário
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-list-ol text-yellow"></i> Crediário</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">

        <form action="{{route('caixa.crediario.index')}}" method="get">
          <div class="row">
            <div class="form-group col-xs-5 col-md-4 col-lg-3">
              <label>Status</label>
              <select name="status" value="{{$form['status']}}" class="form-control">
                <option value="A" {{$form['status'] == 'A' ? 'selected' : ''}}>Com parcelas em aberto</option>
                <option value="F" {{$form['status'] == 'F' ? 'selected' : ''}}>Finalizado / Concluído</option>
              </select>
            </div>
            <div class="col-xs-7 col-md-6">
              <label>Buscar por cliente
                <small class="small text-light-blue">(Opcional)</small>
              </label>
              <div class="input-group">
                <input type="text" name="search" value="{{$form['search'] ?? ''}}"
                       class="form-control" placeholder="Código, CPF, CNPJ ou nome do cliente">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                  <a href="{{route('caixa.crediario.index')}}" class="btn btn-default"><i class="fa fa-eraser"></i></a>
                </span>
              </div>
            </div>
          </div>
        </form>

        @foreach($crediarios as $clienteCrediarios)
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
                {{$clienteCrediarios->first()->transacao->venda->pessoaCliente->nome}} - 
                {{$clienteCrediarios->first()->transacao->venda->pessoaCliente->contatos_string}}
              </h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">

              @if($form['status'] == 'A')
                <a
                  href="{{route('caixa.crediario.show', ['pessoaId' => $clienteCrediarios->first()->transacao->venda->pessoaCliente->id])}}"
                  class="btn btn-xs btn-flat btn-primary">
                  Receber Parcelas
                </a>
              @endif

              <div class="table-responsive">
                <table class="table table-middle table-hover table-condensed">
                  <thead>
                  <tr>
                    <th class="text-center"><i class="fa fa-hashtag"></i> Código
                      Crediário
                    </th>
                    <th class="text-center"><i class="fa fa-calendar"></i> Data Venda
                    </th>
                    <th class="text-center">Vendedor</th>
                    <th class="text-center">Parcelas</th>
                    <th class="text-center text-green">Pagas</th>
                    <th class="text-center text-yellow">Em aberto</th>
                    <th class="text-center text-danger">Vencidas</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($clienteCrediarios as $crediario)
                    @php
                      $vencidas = $crediario->parcelas->where('status', 'A')->filter(function ($parcela){
                          return $parcela->vencimento < now()->format('Y-m-d');
                        })->count()
                    @endphp
                    <tr class="{{$vencidas > 0 ? 'bg-danger' : ''}}">
                      <td class="text-center">{{$crediario->id}}</td>
                      <td
                        class="text-center">{{date('d/m/Y', strtotime($crediario->transacao->venda->data_venda))}}</td>
                      <td class="text-center">{{$crediario->transacao->venda->pessoaVendedor->nome}}</td>
                      <td class="text-center">{{$crediario->parcelas->count()}}</td>
                      <td class="text-center">{{$crediario->parcelas->where('status', 'P')->count()}}</td>
                      <td class="text-center">{{$crediario->parcelas->where('status', 'A')->count()}}</td>
                      <td class="text-center">{{$vencidas}}</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>

            </div>{{--box-body--}}
          </div>
        @endforeach

      </div>{{--box-body--}}
    </div>
  </section>
@endsection
