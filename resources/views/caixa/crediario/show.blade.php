@extends('caixa.layout')
@section('title', 'Caixa - Crediário | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-list-ol text-yellow"></i> Crediário
      <small>Caixa</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('caixa.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><a href="{{route('caixa.crediario.index')}}"><i class="fa fa-list-ol text-yellow"></i> Crediário</a></li>
      <li><i class="fa fa-circle text-green"></i> Receber Crediário</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">
        <vue-crediario-recebimento
          data-crediarios="{{$crediarios}}"
          data-bandeiras="{{$bandeiras}}"
          data-tipo-pagamentos="{{$tipoPagamentos}}"
          data-configuracoes="{{$configuracoes}}">
        </vue-crediario-recebimento>
      </div>
    </div>
  </section>
@endsection
