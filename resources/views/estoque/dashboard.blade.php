@extends('estoque.layout')
@section('title', 'Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dashboard"></i> Painel
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
    
    </div>
  </section>
@endsection
