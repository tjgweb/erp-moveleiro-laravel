@extends('estoque.layout')
@section('title', 'Categorias - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-tags"></i> Categorias
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-tags"></i> Categorias</li>
    </ol>
  </section>

  <section class="content">
    <vue-categorias base-url="{{route('estoque.categorias.index')}}"></vue-categorias>
  </section>
@endsection
