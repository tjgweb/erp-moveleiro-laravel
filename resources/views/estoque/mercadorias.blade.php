@extends('estoque.layout')
@section('title', 'Mercadorias - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-circle"></i> Mercadorias
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle"></i> Mercadorias</li>
    </ol>
  </section>

  <section class="content">
    <vue-mercadorias base-url="{{route('estoque.mercadorias.index')}}"></vue-mercadorias>
  </section>
@endsection
