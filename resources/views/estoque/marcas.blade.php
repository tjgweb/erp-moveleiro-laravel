@extends('estoque.layout')
@section('title', 'Marcas - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-copyright"></i> Marcas
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-copyright"></i> Marcas</li>
    </ol>
  </section>

  <section class="content">
    <vue-marcas
      table-id="table-marcas"
      base-url="{{route('estoque.marcas.index')}}">
    </vue-marcas>
  </section>
@endsection
