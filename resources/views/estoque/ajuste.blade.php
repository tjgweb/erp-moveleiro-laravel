@extends('estoque.layout')
@section('title', 'Ajuste - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-dollar"></i> Ajuste
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-dollar"></i> Ajuste</li>
    </ol>
  </section>

  <section class="content">
    <vue-ajuste></vue-ajuste>
  </section>
@endsection
