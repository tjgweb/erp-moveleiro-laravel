<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header text-center text-bold">
        <i class="fa fa-cubes fa-lg"></i> MENU ESTOQUE
      </li>

      <li>
        <a href="{{route('estoque.index')}}">
          <i class="fa fa-dashboard"></i> <span>Painel</span>
        </a>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-circle text-yellow"></i> <span>Pós-venda</span>
          <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('estoque.pos-venda.retiradas.index')}}">Retiradas</a></li>
          <li><a href="{{route('estoque.pos-venda.entregas.index')}}">Entregas</a></li>
          <li><a href="#">Trocas</a></li>
          <li><a href="{{route('estoque.pos-venda.assistencias.index')}}">Assistências</a></li>
          {{--<li><a href="#">Montagens</a></li>--}}
          {{--<li><a href="#">Outros</a></li>--}}
        </ul>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-list-alt text-aqua"></i> <span>Notas Fiscais</span>
          <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('estoque.notas-entrada.index')}}">Compra</a></li>
          <li><a href="{{route('estoque.notas-transferencia.index')}}">Transferência</a></li>
          <li><a href="{{route('estoque.notas-venda.index')}}">Venda</a></li>
          <li><a href="{{route('estoque.natureza-operacao.index')}}">Natureza Operação</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="javascript:void(0)">
          <i class="fa fa-cubes"></i> <span>Produtos</span>
          <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{route('estoque.ajuste.index')}}">
              <i class="fa fa-dollar"></i> Ajuste
            </a>
          </li>
          <li>
            <a href="{{route('estoque.produtos.index')}}">
              <i class="fa fa-cube"></i> <span>Produtos</span>
            </a>
          </li>

          <li>
            <a href="{{route('estoque.produtos-combinados.index')}}">
              <i class="fa fa-cubes"></i> <span>Produtos Combinados</span>
            </a>
          </li>

          <li>
            <a href="{{route('estoque.mercadorias.index')}}">
              <i class="fa fa-circle"></i> <span>Mercadorias</span>
            </a>
          </li>

          <li>
            <a href="{{route('estoque.categorias.index')}}">
              <i class="fa fa-tags"></i> <span>Categorias</span>
            </a>
          </li>

          <li>
            <a href="{{route('estoque.marcas.index')}}">
              <i class="fa fa-copyright"></i> <span>Marcas</span>
            </a>
          </li>
        </ul>
      </li>

      <li>
        <a href="{{route('estoque.pessoas.index')}}">
          <i class="fa fa-users"></i> <span>Pessoas</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
