@extends('estoque.layout')
@section('title', 'Nota Fiscal de transferência - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-list-alt"></i> Nota Fiscal de transferência
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-list-alt"></i> Nota Fiscal de transferência</li>
    </ol>
  </section>

  <section class="content">
    <vue-notas-transferencia></vue-notas-transferencia>
  </section>
@endsection
