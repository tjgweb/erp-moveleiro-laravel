@extends('estoque.layout')
@section('title', 'Nota Fiscal de compra - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-list-alt"></i> Nota Fiscal de compra
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-list-alt"></i> Nota Fiscal de compra</li>
    </ol>
  </section>

  <section class="content">
    @if(isset($notaId))
      <vue-notas-entrada-form nota-id="{{$notaId}}"></vue-notas-entrada-form>
    @else
      <vue-notas-entrada-form></vue-notas-entrada-form>
    @endif
  </section>
@endsection
