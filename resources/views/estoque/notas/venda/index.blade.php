@extends('estoque.layout')
@section('title', 'Nota Fiscal de venda - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-list-alt"></i> Nota Fiscal de venda
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-list-alt"></i> Nota Fiscal de venda</li>
    </ol>
  </section>

  <section class="content">
    <vue-notas-venda-emissao></vue-notas-venda-emissao>
    <vue-notas-venda-emitidas></vue-notas-venda-emitidas>
  </section>
@endsection
