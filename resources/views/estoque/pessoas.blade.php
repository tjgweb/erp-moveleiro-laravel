@extends('estoque.layout')
@section('title', 'Gestão de Pessoas | ')
@section('content')
  <section class="content-header">
    <h1><i class="fa fa-users"></i> Gestão de Pessoas</h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-users"></i> Gestão de Pessoas</li>
    </ol>
  </section>

  <section class="content">
    <vue-pessoas></vue-pessoas>
  </section>
@endsection
