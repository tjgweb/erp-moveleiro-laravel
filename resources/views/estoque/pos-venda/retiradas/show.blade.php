@extends('estoque.layout')
@section('title', 'Retirada de mercadoria - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Retirada de mercadoria
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li>Retirada de mercadoria</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">
        @include('estoque.pos-venda._dados-cliente', ['venda' => $movimentoProdutos->first()->venda])
        <br>
        <form action="{{route('estoque.pos-venda.retiradas.store')}}" method="post">
          @csrf
          <input type="hidden" name="tipo_pos_venda_id" value="1">
          <input type="hidden" name="venda_id" value="{{$movimentoProdutos->first()->venda->id}}">
          @include('estoque.pos-venda._lista-produtos', ['movimentoProdutos' => $movimentoProdutos])
          @include('estoque.pos-venda.retiradas._inputForm')
          <button type="submit" class="btn btn-primary btn-flat">Retirar produtos selecionados</button>
        </form>
      </div>
    </div>
  </section>
@endsection
