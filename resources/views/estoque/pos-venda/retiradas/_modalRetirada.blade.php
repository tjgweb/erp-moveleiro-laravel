<div class="modal fade" id="entregaModal" tabindex="-1" role="dialog" aria-labelledby="entregaModalLabel">
  <div class="modal-dialog" role="document">
    <form action="{{route('estoque.pos-venda.entregas.store')}}" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="entregaModalLabel">Guia de entrega</h4>
        </div>
        <div class="modal-body">

          @csrf
          <input type="hidden" name="tipo_pos_venda_id" value="1">
          <input type="hidden" name="venda_id" value="{{$venda->id}}">
          <div class="row">
            <div class="form-group col-xs-12 col-md-4">
              <label>Tipo Retirada</label>
              <select name="tipo_retirada" class="form-control">
                <option value="1" {{old('tipo_retirada') == 1 ? 'selected' : ''}}>Retirada na Loja</option>
                <option value="2" {{old('tipo_retirada') == 2 ? 'selected' : ''}}>Entrega</option>
              </select>
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Data Entrega</label>
              <input type="date" name="data" class="form-control"
                     value="{{old('data') ?? $venda->previsao_entrega ?? now()->format('Y-m-d')}}">
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Hora Entrega</label>
              <input type="time" name="hora" class="form-control" value="{{old('hora') ?? now()->format('H:i')}}">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12">
              <label>Observação</label>
              <textarea name="descricao" class="form-control" rows="2">{{old('descricao')}}</textarea>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-xs-12 {{$errors->has('equipe') ? 'has-error' : ''}}">
              <label>Equipe</label>
              <select name="equipe[]" class="form-control" multiple>
                @foreach($equipe as $membro)
                  <option value="{{$membro->id}}">{{$membro->nome}}</option>
                @endforeach
              </select>
              <span class="small text-danger">{{$errors->first('equipe')}}</span>
              <p class="small text-info">Segure a tecla <strong>Ctrl</strong> para selecionar os membros da equipe.</p>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Fechar/Cancelar
          </button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </div>
    </form>
  </div>
</div>
