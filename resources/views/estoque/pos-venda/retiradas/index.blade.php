@extends('estoque.layout')
@section('title', 'Retirada de mercadoria pelo cliente - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Retirada de mercadoria pelo cliente
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li>Retirada de mercadoria pelo cliente</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-md-offset-6 col-lg-4 col-lg-offset-8">
        <form action="{{route('estoque.pos-venda.retiradas.index')}}" method="get">
          @csrf
          <div class="input-group">
            <input type="text" name="search" value="{{$search ?? ''}}" class="form-control"
                   placeholder="Nome ou CPF/CNPJ">
            <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  <a href="{{route('estoque.pos-venda.retiradas.index')}}" class="btn btn-default"><i class="fa fa-eraser"></i></a>
                </span>
          </div>
        </form>
      </div>
    </div>
    <br>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Produtos à retirar</h3>
        <div class="box-tools">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th>Venda</th>
              <th>Cliente</th>
              <th>CPF/CNPJ</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($vendas as $venda)
              <tr>
                <td>{{$venda->id}}</td>
                <td>{{$venda->pessoaCliente->nome}}</td>
                <td>
                  {{$venda->pessoaCliente->fisica ? 
                    formatar('cpf', $venda->pessoaCliente->fisica->cpf) : 
                    formatar('cnpj', $venda->pessoaCliente->juridica->cnpj)}}
                </td>
                <td class="btn-actions">
                  <a href="{{route('estoque.pos-venda.retiradas.show', ['id' => $venda->id])}}"
                     class="btn btn-primary btn-flat btn-xs">Retirar produto</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer text-center">
        {{$vendas->links()}}
      </div>
    </div>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Produtos já retirados</h3>
        <div class="box-tools">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-middle table-hover">
            <thead>
            <tr>
              <th>Venda</th>
              <th>Data</th>
              <th>Hora</th>
              <th>Cliente</th>
              <th>CPF/CNPJ</th>
              <th>Descrição</th>
              <th>Atendente
                <small>(s)</small>
              </th>
              <th>Detalhes</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posVendas as $pv)
              <tr>
                <td>{{$pv->movimentoProdutos->first()->venda_id}}</td>
                <td>{{date('d/m/Y', strtotime($pv->data))}}</td>
                <td>{{date('H:i', strtotime($pv->hora))}}</td>
                <td>{{$pv->movimentoProdutos->first()->venda->pessoaCliente->nome}}</td>
                <td>{{$pv->movimentoProdutos->first()->venda->pessoaCliente->fisica ? 
                    formatar('cpf', $pv->movimentoProdutos->first()->venda->pessoaCliente->fisica->cpf) :
                    $pv->movimentoProdutos->first()->venda->pessoaCliente->juridica ?  
                    formatar('cnpj', $pv->movimentoProdutos->first()->venda->pessoaCliente->juridica->cnpj) : ''}}</td>
                
                <td>{{$pv->descricao}}</td>
                <td>
                  @foreach($pv->equipe as $pessoa)
                    {{$pessoa->nome}};&nbsp;
                  @endforeach
                </td>
                <td>
                  <i class="fa fa-search-plus"></i>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer text-center">
        {{$posVendas->links()}}
      </div>
    </div>
  </section>
@endsection
