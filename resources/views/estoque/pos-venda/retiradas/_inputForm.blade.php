<div class="row">
  <div class="form-group col-xs-6 col-md-4">
    <label>Data Entrega</label>
    <input type="date" name="data" class="form-control"
           value="{{old('data') ?? $venda->previsao_entrega ?? now()->format('Y-m-d')}}">
  </div>
  <div class="form-group col-xs-6 col-md-4">
    <label>Hora Entrega</label>
    <input type="time" name="hora" class="form-control" value="{{old('hora') ?? now()->format('H:i')}}">
  </div>
</div>

<div class="row">
  <div class="form-group col-xs-12">
    <label>Observação</label>
    <textarea name="descricao" class="form-control" rows="2">{{old('descricao')}}</textarea>
    <p class="small text-info">
      Indique aqui o documento e o nome da pessoa que retirou o produto caso não seja o próprio cliente. 
      Anote também qualquer dado relevante.
    </p>
  </div>
</div>

<div class="row">
  <div class="form-group col-xs-12 {{$errors->has('equipe') ? 'has-error' : ''}}">
    <label>Atendente
      <small>(s)</small>
    </label>
    <select name="equipe[]" class="form-control" multiple>
      @foreach($equipe as $membro)
        <option value="{{$membro->id}}">{{$membro->nome}}</option>
      @endforeach
    </select>
    <span class="small text-danger">{{$errors->first('equipe')}}</span>
    <p class="small text-info">Segure a tecla <strong>Ctrl</strong> para selecionar os membros da equipe.</p>
  </div>
</div>

