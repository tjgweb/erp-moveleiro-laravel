<h4><i class="fa fa-cubes"></i> Lista de Produtos</h4>
<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
    <tr>
      <th></th>
      <th>A Retirar</th>
      <th>Código Produto</th>
      <th>Imagem</th>
      <th>Descrição</th>
      <th class="text-center">
        {{--<button type="button" class="btn btn-default btn-flat btn-xs" data-container="body" data-toggle="popover"
                data-placement="top"
                data-content="Esta quantidade corresponde aos produtos que ainda não foram retirados ou agendados para entrega.">--}}
        <span data-toggle="tooltip" data-placement="top" style="cursor: pointer;"
              title="Esta quantidade corresponde aos produtos que ainda não foram retirados ou agendados para entrega.">
            Qtd <i class="fa fa-info-circle"></i>
          </span>
        </button>
      </th>
      <th class="text-center">P. Combinado</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($movimentoProdutos as $mp)
      @php
        $retiradas = 0;
        foreach($mp->posVendas->where('tipo_pos_venda_id', 1) as $retirada){
          $retiradas += $retirada->pivot->quantidade;
        };
        $entregas = 0;
        foreach($mp->posVendas->where('tipo_pos_venda_id', 2) as $entrega){
          $entregas += $entrega->pivot->quantidade;
        };
        $abatimento = $retiradas + $entregas;
      @endphp
      <tr>
        <td>
          <input type="checkbox" name="produtos[]" value="{{$mp->produto_id}}">
        </td>
        <td>
          <input type="number" step="1" min="1" max="{{$mp->quantidade - $abatimento}}" value="1"
                 name="qtd[]" class="form-control">
        </td>
        <td>{{$mp->produto->codigo_fabrica}}</td>
        <td>
          @if($mp->produto->imagens->count())
            <img src="{{$mp->produto->imagens->first()->src}}"
                 alt="{{$mp->produto->imagens->first()->legenda}}" height="60">
          @else
            <img src="/images/no-image.png" alt="No Image" height="60">
          @endif
        </td>
        <td>{{$mp->produto->descricao_completa}}</td>
        <td class="text-center">{{$mp->quantidade - $abatimento}}</td>
        <td class="text-center">
          @if($mp->produto->produtoVendas()->where('venda_id', $mp->venda_id)->first()->produto_combinado)
            <i class="fa fa-cubes"></i> 
            {{$mp->produto->produtoVendas()->where('venda_id', $mp->venda_id)->first()->combinado->descricao}}
          @else
            <i class="fa fa-cube"></i> Não
          @endif
        </td>
        <td></td>
      </tr>
    @endforeach
    
    @if($errors->first('produtos'))
      <tr>
        <td colspan="7" class="text-danger">Selecione os produtos a serem retirados pelo cliente.</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
