<h4><i class="fa fa-user"></i> Dados do cliente</h4>
<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
    <tr>
      <th>Nome</th>
      <th>CPF/CNPJ</th>
      <th>Contato</th>
      <th>Endereço Entrega</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>{{$venda->pessoaCliente->nome}}</td>
      <td>
        {{$venda->pessoaCliente->fisica ? formatar('cpf', $venda->pessoaCliente->fisica->cpf) : formatar('cnpj', $venda->pessoaCliente->juridica->cnpj) }}
      </td>
      <td>
        {{$venda->pessoaCliente->contatos->where('principal', true) ? 
          $venda->pessoaCliente->contatos->where('principal', true)->first()->numero :
          $venda->pessoaCliente->contatos->first()->numero
        }}
      </td>
      <td>
        {{$venda->enderecoEntrega ? $venda->enderecoEntrega->full :
        $venda->pessoaCliente->enderecos->where('principal', true)->first() ?
        $venda->pessoaCliente->enderecos->where('principal', true)->first()->full :
        $venda->pessoaCliente->enderecos->first()->full}}
      </td>
    </tr>
    </tbody>
  </table>
</div>
