@extends('estoque.layout')
@section('title', 'Assistência ao cliente - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Assistência ao cliente
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle text-yellow"></i> Pós-venda</li>
      <li>Assistência ao cliente</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">
        @include('estoque.pos-venda.assistencias._dados-cliente', ['venda' => $movimentoProdutos->first()->venda])
        <br>
        <form action="{{route('estoque.pos-venda.assistencias.update', ['posVendaId' => $posVenda->id])}}"
              method="post">
          @csrf
          @method('PUT')
          <input type="hidden" name="venda_id" value="{{$movimentoProdutos->first()->venda->id}}">
          @include('estoque.pos-venda.assistencias._lista-produtos', ['movimentoProdutos' => $movimentoProdutos])

          <div class="row">
            <div class="form-group col-xs-6 col-md-4 {{$errors->has('tipo_pos_venda_id') ? 'has-error' : ''}}">
              <label>Tipo Assistência</label>
              <select name="tipo_pos_venda_id" class="form-control">
                <option value="0">Selecione...</option>
                <option value="4" {{old('tipo_pos_venda_id') == '4' ? 'selected' : 
                $posVenda->tipo_pos_venda_id == '4' ? 'selected' : ''}}>Montagem
                </option>
                <option value="5" {{old('tipo_pos_venda_id') == '5' ? 'selected' : 
                $posVenda->tipo_pos_venda_id == '5' ? 'selected' : ''}}>Outros
                </option>
              </select>
              <span class="small text-danger">{{$errors->first('tipo_pos_venda_id')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Data da Assistência</label>
              <input type="date" name="data" id="data" class="form-control"
                     value="{{old('data') ?? date('Y-m-d', strtotime($posVenda->data))}}">
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Hora da Assistência</label>
              <input type="time" name="hora" class="form-control"
                     value="{{old('hora') ?? date('H:i', strtotime($posVenda->hora))}}">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-6 col-md-4">
              <label>Status</label>
              <select name="status" class="form-control" id="status">
                <option value="0" {{old('status') == '0' ? 'selected' : $posVenda->status == '0' ? 'selected' : ''}}>
                  Em aberto
                </option>
                <option value="1" {{old('status') == '1' ? 'selected' : $posVenda->status == '1' ? 'selected' : ''}}>
                  Concluída
                </option>
              </select>
            </div>
            <div id="div-fim" class="form-group col-xs-6 col-md-4 {{$errors->has('finalizado_em') ? 'has-error' : ''}}">
              <label>Data encerramento</label>
              <input type="date" id="dataFim" name="finalizado_em" class="form-control"
                     value="{{old('finalizado_em') ? date('Y-m-d', strtotime(old('finalizado_em'))) : $posVenda->finalizado_em ? date('Y-m-d', strtotime($posVenda->finalizado_em)) : date('Y-m-d')}}">
              <span class="small text-danger">{{$errors->first('finalizado_em')}}</span>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12">
              <label>Observação / Descrição</label>
              <textarea name="descricao" class="form-control"
                        rows="2">{{old('descricao') ?? $posVenda->descricao}}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12 {{$errors->has('equipe') ? 'has-error' : ''}}">
              <label>Atendente
                <small>(s)</small>
              </label>
              <select name="equipe[]" class="form-control" multiple>
                @foreach($equipe as $membro)
                  <option
                    value="{{$membro->id}}" {{$posVenda->equipe->where('id', $membro->id)->first() ? 'selected' : ''}}>
                    {{$membro->nome}}
                  </option>
                @endforeach
              </select>
              <span class="small text-danger">{{$errors->first('equipe')}}</span>
              <p class="small text-info">Segure a tecla <strong>Ctrl</strong> para selecionar os membros da equipe.</p>
            </div>
          </div>

          <button type="submit" class="btn btn-warning btn-flat">
            <i class="fa fa-gear"></i> Editar Assistência
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function () {

      let dataInicio = moment($('#data').val());
      let dataFim = moment($('#dataFim').val());

      const showDataEncerramento = function (value) {
        if (value == 1) {
          $('#div-fim').show();
        } else {
          $('#div-fim').hide();
        }
      };

      const disableSubmit = function (inicio, fim) {
        if (inicio.diff(fim, 'days') > 0 && $('#status').val() == 1) {
          $("button[type='submit']").attr('disabled', true);
          $('#div-fim').addClass('has-error');
        } else {
          $("button[type='submit']").attr('disabled', false);
          $('#div-fim').removeClass('has-error');
        }
      };

      showDataEncerramento($('#status').val());
      disableSubmit(dataInicio, dataFim);

      $('#dataFim').change(function () {
        disableSubmit(dataInicio, moment($(this).val()));
      });

      $('#status').change(function () {
        showDataEncerramento($(this).val());
        disableSubmit(dataInicio, moment($('#dataFim').val()));
      });

    });
  </script>
@endpush
