<h4>
  <i class="fa fa-cubes"></i> Produtos relacionados a
  <strong>venda Nº{{$movimentoProdutos->first()->venda->id}}</strong>, realizada no dia
  <strong>{{date('d/m/Y', strtotime($movimentoProdutos->first()->venda->data_venda))}}</strong>
</h4>
<div class="table-responsive">
  <table class="table table-middle table-hover">
    <thead>
    <tr>
      <th></th>
      <th class="text-center">Assistência</th>
      <th>Código Produto</th>
      <th class="text-center">Imagem</th>
      <th>Descrição</th>
      <th class="text-center">U. Medida</th>
      <th class="text-center">Qtd</th>
      <th class="text-center">P. Combinado</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    @php $i = 0; @endphp
    @foreach($movimentoProdutos as $mp)
      <tr>
        <td>
          <input type="checkbox" name="m_produtos[]" value="{{$mp->id}}"
            {{old('m_produtos.' . $i) == $mp->id ? 'checked' : 
            isset($posVenda) && $posVenda->movimentoProdutos->where('id', $mp->id)->first() ? 'checked' :''}}>
        </td>
        @if($mp->quantidade > 1)
          <td>
            <input type="number" step="1" min="1" max="{{$mp->quantidade}}" value="1"
                   name="qtd[]" class="form-control" required>
          </td>
        @else
          <td class="text-center">
            {{$mp->quantidade}}
            <input type="hidden" value="1" name="qtd[]" class="form-control">
          </td>
        @endif
        <td>{{$mp->produto->codigo_fabrica}}</td>
        <td class="text-center">
          @if($mp->produto->imagens->count())
            <img src="{{$mp->produto->imagens->first()->src}}"
                 alt="{{$mp->produto->imagens->first()->legenda}}" height="60">
          @else
            <img src="/images/no-image.png" alt="No Image" height="60">
          @endif
        </td>
        <td>{{$mp->produto->descricao_completa}}</td>
        <td class="text-center">{{$mp->unidadeMedida->unidade}}</td>
        <td class="text-center">{{$mp->quantidade}}</td>
        <td class="text-center">
          @if($mp->produto->produtoVendas()->where('venda_id', $mp->venda_id)->first()->produto_combinado)
            <i class="fa fa-cubes"></i>
            {{$mp->produto->produtoVendas()->where('venda_id', $mp->venda_id)->first()->combinado->descricao}}
          @else
            <i class="fa fa-cube"></i> Não
          @endif
        </td>
        <td></td>
      </tr>
      @php $i++; @endphp
    @endforeach

    @if($errors->has('m_produtos'))
      <tr>
        <td colspan="7" class="text-danger">{{$errors->has('m_produtos') ? $errors->first('m_produtos') : ''}}</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
