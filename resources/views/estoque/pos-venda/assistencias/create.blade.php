@extends('estoque.layout')
@section('title', 'Assistência ao cliente - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Assistência ao cliente
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle text-yellow"></i> Pós-venda</li>
      <li>Assistência ao cliente</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">
        @include('estoque.pos-venda.assistencias._dados-cliente', ['venda' => $movimentoProdutos->first()->venda])
        <br>
        <form action="{{route('estoque.pos-venda.assistencias.store')}}" method="post">
          @csrf
          <input type="hidden" name="venda_id" value="{{$movimentoProdutos->first()->venda->id}}">
          @include('estoque.pos-venda.assistencias._lista-produtos', ['movimentoProdutos' => $movimentoProdutos])
          
          <div class="row">
            <div class="form-group col-xs-6 col-md-4 {{$errors->has('tipo_pos_venda_id') ? 'has-error' : ''}}">
              <label>Tipo Assistência</label>
              <select name="tipo_pos_venda_id" class="form-control">
                <option value="0">Selecione...</option>
                <option value="4" {{old('tipo_pos_venda_id') == '4' ? 'selected' : ''}}>Montagem</option>
                <option value="5" {{old('tipo_pos_venda_id') == '5' ? 'selected' : ''}}>Outros</option>
              </select>
              <span class="small text-danger">{{$errors->first('tipo_pos_venda_id')}}</span>
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Data da Assistência</label>
              <input type="date" name="data" class="form-control" value="{{old('data') ?? now()->format('Y-m-d')}}">
            </div>
            <div class="form-group col-xs-6 col-md-4">
              <label>Hora da Assistência</label>
              <input type="time" name="hora" class="form-control" value="{{old('hora') ?? now()->format('H:i')}}">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12">
              <label>Observação / Descrição</label>
              <textarea name="descricao" class="form-control" rows="2">{{old('descricao')}}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12 {{$errors->has('equipe') ? 'has-error' : ''}}">
              <label>Atendente
                <small>(s)</small>
              </label>
              <select name="equipe[]" class="form-control" multiple>
                @foreach($equipe as $membro)
                  <option value="{{$membro->id}}">{{$membro->nome}}</option>
                @endforeach
              </select>
              <span class="small text-danger">{{$errors->first('equipe')}}</span>
              <p class="small text-info">Segure a tecla <strong>Ctrl</strong> para selecionar os membros da equipe.</p>
            </div>
          </div>

          <button type="submit" class="btn btn-primary btn-flat">
            <i class="fa fa-gear"></i> Gerar Assistência
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection
