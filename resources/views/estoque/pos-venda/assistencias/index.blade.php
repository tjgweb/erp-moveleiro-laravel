@extends('estoque.layout')
@section('title', 'Assistência ao cliente - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Assistência ao cliente
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle text-yellow"></i> Pós-venda</li>
      <li>Assistência ao cliente</li>
    </ol>
  </section>

  <section class="content">
    <vue-assistencias 
      url-base="{{route('estoque.pos-venda.assistencias.index')}}"
      url-search="{{route('estoque.pos-venda.assistencias.search')}}"></vue-assistencias>
  </section>
@endsection
