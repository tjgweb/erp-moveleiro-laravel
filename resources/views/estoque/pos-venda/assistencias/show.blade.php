@extends('estoque.layout')
@section('title', 'Assistência ao cliente - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      Assistência Nº {{$posVenda->id}}
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle text-yellow"></i> Pós-venda</li>
      <li>Assistência Nº {{$posVenda->id}}</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-body">
        <h4><i class="fa fa-file-text"></i> Dados da assistência</h4>
        <p>
          <strong>Data:</strong> {{date('d/m/Y', strtotime($posVenda->data))}}
          <span class="pull-right">
            <a href="{{route('estoque.pos-venda.assistencias.edit', ['posVendaId' => $posVenda->id])}}"
               class="btn btn-warning btn-sm btn-flat"><i class="fa fa-edit"></i> Editar</a>
            <a href="{{route('estoque.pos-venda.assistencias.print', ['posVendaId' => $posVenda->id])}}"
               target="_blank" class="btn btn-default btn-sm btn-flat"><i class="fa fa-print"></i> Imprimir guia</a>
          </span>
        </p>
        <p><strong>Hora:</strong> {{date('H:i', strtotime($posVenda->hora))}}</p>
        @if($posVenda->status)
          <p><strong>Concluída em:</strong> {{date('d/m/Y', strtotime($posVenda->finalizado_em))}}</p>
        @endif
        <p><strong>Tipo:</strong> {{$posVenda->tipoPosVenda->descricao}}</p>
        <p><strong>Descrição:</strong> {{$posVenda->descricao}}</p>
        <p><strong>Equipe:</strong>
          @foreach($posVenda->equipe as $membro)
            {{$membro->nome}} {{$posVenda->equipe->count() > 1 ? ';' : ''}}
          @endforeach
        </p>
        <p><strong>Quantidade | Produtos:</strong> <br>
        <ul>
          @foreach($posVenda->movimentoProdutos as $mp)
            <li>{{$mp->posVendas->where('id', $posVenda->id)->first()->pivot->quantidade}}
              | {{$mp->produto->descricao_completa}}</li>
          @endforeach
        </ul>
        </p>
        <br>
        @include('estoque.pos-venda.assistencias._dados-cliente', [
        'venda' => $posVenda->movimentoProdutos->first()->venda
        ])
      </div>
    </div>
  </section>
@endsection
