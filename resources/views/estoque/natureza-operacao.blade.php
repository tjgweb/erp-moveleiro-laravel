@extends('estoque.layout')
@section('title', 'Natureza da Operação - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-circle-o"></i> Natureza da Operação
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-circle-o"></i> Natureza da Operação</li>
    </ol>
  </section>

  <section class="content">
    <vue-natureza-operacao></vue-natureza-operacao>
  </section>
@endsection
