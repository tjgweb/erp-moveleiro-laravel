@extends('estoque.layout')
@section('title', 'Produtos Combinados - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-cubes"></i> Produtos Combinados
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.produtos-combinados.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-cubes"></i> Produtos Combinados</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <vue-produtos-combinados></vue-produtos-combinados>
    </div>
  </section>
@endsection
