@extends('estoque.layout')
@section('title', 'Produtos - Estoque | ')
@section('content')
  <section class="content-header">
    <h1>
      <i class="fa fa-cube"></i> Produtos
      <small>Estoque</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('estoque.index')}}"><i class="fa fa-dashboard"></i> Painel</a></li>
      <li><i class="fa fa-cube"></i> Produtos</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <vue-produtos base-url="{{route('estoque.produtos.index')}}"></vue-produtos>
    </div>
  </section>
@endsection
