<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layout-parts.head')

<body class="hold-transition sidebar-mini fixed skin-purple">
<div class="wrapper">
  <div id="app">

    @include('layout-parts.top-bar', ['linkLogo' => '/estoque'])

    @include('estoque.menu')

    <main class="content-wrapper">
      @yield('content')
    </main>

    @include('layout-parts.footer')
  </div>
</div>

<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
{!! toastr()->render() !!}
</body>
</html>
