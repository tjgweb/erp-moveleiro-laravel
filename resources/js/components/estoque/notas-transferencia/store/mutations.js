export default {
  LOADING(state, value) {
    state.loading = value;
  },

  ACTION(state, value) {
    state.action = value;
  },

  ERRORS(state, obj = {}) {
    state.errors = obj;
  },

  NOTA(state, data) {
    state.nota = data;
  },

  PRODUTO_CFOP(state, payload) {
    state.nota.movimento_produtos[payload.index].cfop_id = payload.value;
  },

  PRODUTO_UNIDADE_MEDIDA(state, payload) {
    state.nota.movimento_produtos[payload.index].unidade_medida_id = payload.value;
  },

  CARGA(state, data) {
    state.carga = data;
  },

  NATUREZA_OPERACAO(state, data) {
    state.naturezaOperacao = data;
  },

  TRANSPORTADORES(state, data) {
    state.transportadores = data;
  },

  LOJAS(state, data) {
    state.lojas = data;
  },

  UNIDADES_MEDIDA(state, data) {
    state.unidadesMedida = data;
  },

  CLIENTES(state, data) {
    state.clientes = data;
  },
}