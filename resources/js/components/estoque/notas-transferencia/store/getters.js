export default {
  
  clientesParseLoja: state => {
    const lojasIds = state.lojas.map(l => l.id);
    const lojas = state.clientes.filter(c => lojasIds.some(el => el == c.id));
    
    return lojas;
  },

  checkProdutoManager: (state, getters) => (index) => {
    let mp = state.nota.movimento_produtos[index];

    if (mp.unidade_medida_id != '' && mp.cfop_id != '') {
      return true;
    }

    return false;
  },

  disableBtnSave: state => {
    let disabled = false;

    state.nota.movimento_produtos.some((mp, index) => {
      if (mp.unidade_medida_id == '' || mp.cfop_id == '') {
        disabled = true;
        return true;
      }
    });

    return disabled;
  }

}