import Helpers from '../../../../helpers/helpers';

export default {
  
  startForm(context) {
    context.commit('LOADING', true);

    return Promise.all([
      context.dispatch('getNaturezaOperacao'),
      context.dispatch('getTransportadores'),
      context.dispatch('getLojas'),
      context.dispatch('getUnidadesMedida'),
      context.dispatch('getClientes')
    ])
      .then(() => context.commit('LOADING', false))
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      })
  },

  getNaturezaOperacao(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/natureza-operacao')
      .then(resp => {
        context.commit('NATUREZA_OPERACAO', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getTransportadores(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/transportadores')
      .then(resp => {
        context.commit('TRANSPORTADORES', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getLojas(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/lojas')
      .then(resp => {
        context.commit('LOJAS', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getUnidadesMedida(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/unidades-medida')
      .then(resp => {
        context.commit('UNIDADES_MEDIDA', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getClientes(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/clientes')
      .then(resp => {
        context.commit('CLIENTES', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  gerarNovaNota(context) {
    context.commit('LOADING', true);
    window.axios.post('/estoque/notas-venda', {
      nota: context.state.nota,
      carga: context.state.carga,
      transferencias: context.state.transferencias,
      venda_id: context.state.venda.id
    })
      .then(resp => {
        //window.location = '/estoque/notas-venda';
      })
      .catch(error => {
        context.commit('LOADING', false);
        console.log(error.response);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Verifique todos os campos e tente novamente.', 'Erro no preenchimento!');
        } else {
          context.commit('ERRORS');
          Helpers.msgErrorDefault();
        }
      })
  },

  reenviarConsultarAutorizacao(context) {
    context.commit('LOADING', true);
    window.axios.put(`/estoque/notas-venda/${context.state.nota.id}`, {
      nota: context.state.nota,
      carga: context.state.carga,
      venda_id: context.state.venda.id,
      action: context.state.action
    })
      .then(resp => {
        // window.location = '/estoque/notas-venda';
      })
      .catch(error => {
        context.commit('LOADING', false);
        console.log(error.response);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Verifique todos os campos e tente novamente.', 'Erro no preenchimento!');
        } else {
          context.commit('ERRORS');
          Helpers.msgErrorDefault();
        }
      })
  },

};