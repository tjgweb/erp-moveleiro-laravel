export default {
  LOADING(state, value) {
    state.loading = value;
  },

  ACTION(state, value) {
    state.action = value;
  },

  ERRORS(state, obj = {}) {
    state.errors = obj;
  },

  CONFIGS_VENDAS(state, obj) {
    state.configsVendas = obj;
  },

  VENDA(state, data) {
    state.venda = data;
  },

  VENDAS(state, data) {
    state.vendas = data;
  },

  NOTA(state, data) {
    state.nota = data;
  },

  NOTA_NATUREZA_OPERACAO_ID(state, id) {
    state.nota.natureza_operacao_id = id;
  },

  CARGA(state, data) {
    state.carga = data;
  },

  CARGA_TRANSPORTADOR_ID(state, id) {
    state.carga.transportador_id = id;
  },

  NATUREZA_OPERACAO(state, data) {
    state.naturezaOperacao = data;
  },

  TRANSPORTADORES(state, data) {
    state.transportadores = data;
  },

  LOJAS(state, data) {
    state.lojas = data;
  },

  UNIDADES_MEDIDA(state, data) {
    state.unidadesMedida = data;
  },

  PRODUTO_CFOP(state, payload) {
    state.nota.movimento_produtos[payload.index].cfop_id = payload.value;
  },

  PRODUTO_UNIDADE_MEDIDA(state, payload) {
    state.nota.movimento_produtos[payload.index].unidade_medida_id = payload.value;
  },

  ADD_TRANSFERENCIAS(state, payload) {
    if (state.transferencias.length) {
      const lojaIndex = state.transferencias.findIndex(t => t.loja_id == payload.loja_id);

      if (lojaIndex != -1) {
        const produtoIndex = state.transferencias[lojaIndex].produtos.findIndex(p => p.produto_id == payload.produto_id);

        if (produtoIndex != -1) {
          state.transferencias[lojaIndex].produtos[produtoIndex].quantidade += +payload.quantidade;
        } else {
          state.transferencias[lojaIndex].produtos.push({
            produto_id: payload.produto_id,
            descricao: payload.descricao,
            quantidade: payload.quantidade
          })
        }

      } else {
        state.transferencias.push({
          loja_id: payload.loja_id,
          loja_descricao: payload.loja_descricao,
          produtos: [{
            produto_id: payload.produto_id,
            descricao: payload.descricao,
            quantidade: payload.quantidade
          }]
        });
      }
    } else {
      state.transferencias.push({
        loja_id: payload.loja_id,
        loja_descricao: payload.loja_descricao,
        produtos: [{
          produto_id: payload.produto_id,
          descricao: payload.descricao,
          quantidade: payload.quantidade
        }]
      });
    }
  },

  REMOVE_TRANSFERENCIAS(state, payload) {
    state.transferencias[payload.indexLoja].produtos.splice(payload.indexProduto, 1);
    
    if(!state.transferencias[payload.indexLoja].produtos.length){
      state.transferencias.splice(payload.indexLoja, 1);
    }
  },
}