import Helpers from '../../../../../helpers/helpers';

export default {

  gerarNovaNota(context) {
    context.commit('LOADING', true);
    window.axios.post('/estoque/notas-venda', {
      nota: context.state.nota,
      carga: context.state.carga,
      transferencias: context.state.transferencias,
      venda_id: context.state.venda.id
    })
      .then(resp => {
        //window.location = '/estoque/notas-venda';
      })
      .catch(error => {
        context.commit('LOADING', false);
        console.log(error.response);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Verifique todos os campos e tente novamente.', 'Erro no preenchimento!');
        } else {
          context.commit('ERRORS');
          Helpers.msgErrorDefault();
        }
      })
  },

  reenviarConsultarAutorizacao(context) {
    context.commit('LOADING', true);
    window.axios.put(`/estoque/notas-venda/${context.state.nota.id}`, {
      nota: context.state.nota,
      carga: context.state.carga,
      venda_id: context.state.venda.id,
      action: context.state.action
    })
      .then(resp => {
        // window.location = '/estoque/notas-venda';
      })
      .catch(error => {
        context.commit('LOADING', false);
        console.log(error.response);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Verifique todos os campos e tente novamente.', 'Erro no preenchimento!');
        } else {
          context.commit('ERRORS');
          Helpers.msgErrorDefault();
        }
      })
  },

  startForm(context) {
    context.commit('LOADING', true);

    return Promise.all([
      context.dispatch('getNaturezaOperacao'),
      context.dispatch('getTransportadores'),
      context.dispatch('getLojas'),
      context.dispatch('getUnidadesMedida')
    ])
      .then(() => context.commit('LOADING', false))
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      })
  },

  getVendas(context, payload = {page: 1, loading: false}) {
    payload.loading ? context.commit('LOADING', true) : null;
    let url = `/estoque/notas-venda/emissao?per_page=${context.state.configsVendas.perPage}&page=${payload.page}`;
    if (context.state.configsVendas.search !== '') {
      url = `/estoque/notas-venda/emissao?per_page=${context.state.configsVendas.perPage}&page=${payload.page}&search=${context.state.configsVendas.search}`;
    }

    return window.axios.get(url)
      .then(resp => {
        context.commit('VENDAS', resp.data);
        payload.loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        payload.loading ? context.commit('LOADING', false) : null;
      })
  },

  getNaturezaOperacao(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/natureza-operacao')
      .then(resp => {
        context.commit('NATUREZA_OPERACAO', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getTransportadores(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/transportadores')
      .then(resp => {
        context.commit('TRANSPORTADORES', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getLojas(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/lojas')
      .then(resp => {
        context.commit('LOJAS', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  getUnidadesMedida(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;

    return window.axios.get('/shared/unidades-medida')
      .then(resp => {
        context.commit('UNIDADES_MEDIDA', resp.data);
        loading ? context.commit('LOADING', false) : null;
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        loading ? context.commit('LOADING', false) : null;
      })
  },

  vendaParseNota(context) {
    let endereco_id, data_saida;
    let mp = [];
    let nota = {...context.state.nota};

    if (context.state.venda.endereco_entrega_id) {
      endereco_id = context.state.venda.endereco_entrega_id
    } else if (!context.state.venda.endereco_entrega_id) {
      context.state.venda.pessoa_cliente.enderecos.length > 1 ?
        endereco_id = context.state.venda.pessoa_cliente.enderecos.find(e => e.principal == 1)[0].id :
        endereco_id = context.state.venda.pessoa_cliente.enderecos[0].id;
    }

    data_saida = moment();
    
    if (context.state.venda.previsao_entrega) {
      let data_previsao = moment(context.state.venda.previsao_entrega);
      data_previsao.diff(data_saida, 'days') > 0 ? data_saida = data_previsao : null;
    }

    nota.destinatario_id = context.state.venda.cliente_id;
    nota.endereco_destinatario_id = endereco_id;
    nota.data_saida = data_saida.format('YYYY-MM-DD');
    nota.hora_saida = data_saida.add(10, 'm').format('HH:mm');

    context.state.venda.produto_vendas.forEach(pv => {
      mp.push({
        venda_id: pv.venda_id,
        produto_id: pv.produto_id,
        unidade_medida_id: '',
        cfop_id: '',
        lote_id: null,
        id_na_nota: pv.produto.codigo_fabrica,
        quantidade: pv.quantidade,
        quantidade_por_caixa: null,
        valor_unitario: pv.valor_unitario,
        desconto: pv.valor_desconto,
        observacao: null,
        aliquota_icms: 0,
        aliquota_ipi: 0,
        mva: 0,
        deleted_at: null,
        valor_total: pv.valor_total,
        valor_icms: 0,
        valor_ipi: 0,
        valor_icms_substituicao: 0,
        base_calculo_icms_substituicao: 0,
        produto: pv.produto
      });
    });

    nota.movimento_produtos = mp;

    context.commit('NOTA', nota);
  },

};