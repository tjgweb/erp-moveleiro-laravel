import Helpers from '../../../../helpers/helpers';

export default {
  startFormComponent(context, id) {
    context.commit('LOADING', true);

    let fnArray = [
      context.dispatch('getNaturezaOperacao'),
      context.dispatch('getEmissores'),
      context.dispatch('getTransportadores'),
      context.dispatch('getUnidadesMedida'),
      context.dispatch('getLojas')
    ];

    if (id) {
      fnArray.push(context.dispatch('getNota'));
      fnArray.push(context.dispatch('getProdutosNota'));
    }

    Promise.all(fnArray)
      .then(() => {
        if (context.getters.emissorEstadoId) {
          context.dispatch('getAliquotaIcms', {
            emissorEstadoId: context.getters.emissorEstadoId,
            destinatarioEstadoId: context.state.destinatarioEstadoId
          });
        }
        context.commit('LOADING', false);
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      });
  },

  getNotas(context, page = 1) {
    context.commit('LOADING', true);
    let url = `/estoque/notas-entrada/lista/${context.state.configs.perPage}?page=${page}`;
    if (context.state.configs.search !== '') {
      url = `/estoque/notas-entrada/lista/${context.state.configs.perPage}?page=${page}&search=${context.state.configs.search}`;
    }
    return window.axios.get(url)
      .then(resp => {
        context.commit('NOTAS', resp.data);
        context.commit('CONFIGS_PER_PAGE', resp.data.per_page);
        context.commit('LOADING', false);
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      });
  },

  getNota(context) {
    return window.axios.get(`/estoque/notas-entrada/${context.state.nota_id}`)
      .then(resp => {
        console.log(resp);
        context.commit('NOTA', resp.data.nota);
        context.commit('CARGA', resp.data.carga);
        context.commit('DUPLICATAS', resp.data.duplicatas);
      })
      .catch(error => Helpers.msgErrorDefault());
  },

  getProdutosNota(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get(`/estoque/notas-entrada/movimento-produtos/lista/${context.state.nota_id}`)
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('MOVIMENTO_PRODUTOS', resp.data);
        context.dispatch('refreshMovimentoProdutos');
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getNaturezaOperacao(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get('/shared/natureza-operacao')
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('NATUREZA_OPERACAO', resp.data);
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getEmissores(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get('/shared/emissores')
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('EMISSORES', resp.data);
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getTransportadores(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get('/shared/transportadores')
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('TRANSPORTADORES', resp.data);
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getUnidadesMedida(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get('/shared/unidades-medida')
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('UNIDADES_MEDIDA', resp.data);
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getLojas(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    return window.axios.get('/shared/lojas')
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('LOJAS', resp.data);
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        Helpers.msgErrorDefault();
      });
  },

  getAliquotaIcms(context, payload = {
    emissorEstadoId: 11,
    destinatarioEstadoId: context.state.destinatarioEstadoId
  }) {
    return window.axios.get(`/shared/aliquota-icms/${payload.emissorEstadoId}/${payload.destinatarioEstadoId}`)
      .then(resp => {
        context.commit('ALIQUOTAS', resp.data);
        context.commit('MOVIMENTO_PRODUTO', {
          ...context.state.movimentoProduto,
          aliquota_icms: resp.data.aliquota
        })
      })
      .catch(error => Helpers.msgErrorDefault());
  },

  criarNota(context) {
    context.commit('LOADING', true);
    window.axios.post('/estoque/notas-entrada', {
      nota: context.state.nota,
      carga: context.state.carga
    })
      .then(resp => {
        console.log(resp);
        context.commit('MOVIMENTO_PRODUTO', {
          ...context.state.movimentoProduto,
          nota_fiscal_id: resp.data.id
        });
        context.commit('NOTA_ID', resp.data.id);
        context.commit('ERRORS');
        context.commit('LOADING', false);
        Helpers.msgAdd();
      })
      .catch(error => {
        context.commit('LOADING', false);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Formulário da Nota contém erro');
        } else {
          Helpers.msgErrorDefault();
        }
      });
  },

  atualizarNota(context, loading = false) {
    loading ? context.commit('LOADING', true) : null;
    window.axios.put(`/estoque/notas-entrada/${context.state.nota_id}`, {
      nota: context.state.nota,
      carga: context.state.carga
    })
      .then(resp => {
        loading ? context.commit('LOADING', false) : null;
        context.commit('ERRORS');
        Helpers.msgEdit();
      })
      .catch(error => {
        loading ? context.commit('LOADING', false) : null;
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Formulário da Nota contém erro');
        } else {
          Helpers.msgErrorDefault();
        }
      });
  },

  removeNota(context, id) {
    context.commit('LOADING', true);
    return window.axios.delete(`/estoque/notas-entrada/${id}`)
      .then(resp => {
        context.dispatch('getNotas');
      })
      .catch(error => {
        context.commit('LOADING', false);
        if (error.response.status == 403) {
          Helpers.msgErrorDefault('Usuário não tem permissão.')
        } else {
          Helpers.msgErrorDefault();
        }
        console.log(error.response);
      });
  },

  addMovimentoProduto(context) {
    context.commit('LOADING', true);
    return window.axios.post('/estoque/notas-entrada/movimento-produtos', context.state.movimentoProduto)
      .then(resp => {
        context.dispatch('resetMovimentoProdutoForm');
        return Promise.all([context.dispatch('getProdutosNota'), context.dispatch('atualizarNota')])
          .then(() => context.commit('LOADING', false))
          .catch(error => {
            context.commit('LOADING', false);
            Helpers.msgErrorDefault();
          });
      })
      .catch(error => {
        context.commit('LOADING', false);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Formulário de Produto contém erro');
        } else {
          Helpers.msgErrorDefault();
        }
      });
  },

  atualizarMovimentoProduto(context) {
    context.commit('LOADING', true);
    return window.axios.put(`/estoque/notas-entrada/movimento-produtos/${context.state.movimentoProduto.id}`, context.state.movimentoProduto)
      .then(resp => {
        context.commit('OPEN_MODAL_MOVIMENTO_PRODUTO_FORM', false);
        context.dispatch('resetMovimentoProdutoForm');
        Promise.all([context.dispatch('getProdutosNota'), context.dispatch('atualizarNota')])
          .then(() => context.commit('LOADING', false))
          .catch(error => {
            context.commit('LOADING', false);
            Helpers.msgErrorDefault();
          });
      })
      .catch(error => {
        context.commit('LOADING', false);
        if (error.response.status == 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Formulário de Produto contém erro!');
        } else {
          Helpers.msgErrorDefault();
        }
      });
  },

  removeMovimentoProduto(context) {
    context.commit('LOADING', true);
    return window.axios.delete(`/estoque/notas-entrada/movimento-produtos/${context.state.movimentoProduto.id}`)
      .then(resp => {
        Promise.all([context.dispatch('getProdutosNota'), context.dispatch('atualizarNota')])
          .then(() => context.commit('LOADING', false))
          .catch(error => {
            context.commit('LOADING', false);
            Helpers.msgErrorDefault();
          });
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      });
  },

  mpCalculaValorIcmsSub(context) {
    if (context.state.movimentoProduto.mva != 0) {
      let result = context.getters.mpBaseCalculoIcmsSub * 1 - context.getters.mpValorIcms * 1;
      context.commit('MOVIMENTO_PRODUTO', {
        ...context.state.movimentoProduto,
        valor_icms_substituicao: parseFloat(result).toFixed(2)
      });
    } else {
      context.commit('MOVIMENTO_PRODUTO', {
        ...context.state.movimentoProduto,
        valor_icms_substituicao: 0
      });
    }
  },

  /* mpCalculaValorIcmsSub2018(context) {
    if (context.state.movimentoProduto.mva != 0) {
      let result = (context.getters.mpBaseCalculoIcmsSub * context.aliquotaICMSInterna) / 100;
      result = result - context.getters.mpValorIcms;
      context.commit('MOVIMENTO_PRODUTO', {
        ...context.state.movimentoProduto,
        valor_icms_substituicao: parseFloat(result).toFixed(2)
      });
    } else {
      context.commit('MOVIMENTO_PRODUTO', {
        ...context.state.movimentoProduto,
        valor_icms_substituicao: 0
      });
    }
  } */

  refreshMovimentoProdutos(context) {
    let valor_total_produtos = 0;
    let valor_icms_produtos = 0;
    let valor_ipi = 0;
    let desconto = 0;
    if (context.state.movimentoProdutos.length) {
      context.state.movimentoProdutos.forEach(mp => {
        valor_total_produtos += +mp.valor_total;
        valor_icms_produtos += +mp.valor_icms;
        valor_ipi += +mp.valor_ipi;
        desconto += +mp.desconto;
      })
    }
    context.commit('NOTA', {
      ...context.state.nota,
      valor_total_produtos: valor_total_produtos,
      valor_icms_produtos: valor_icms_produtos,
      valor_ipi: valor_ipi,
      desconto: desconto
    });
    context.dispatch('notaCalculaIcms');
    context.dispatch('notaCalculaTotal');
  },

  notaCalculaIcms(context) {
    let nota = {
      ...context.state.nota
    };
    nota.base_calculo_icms = +nota.valor_total_produtos + +nota.valor_seguro + +nota.valor_frete + +nota.outras_despesas_acessorias;
    let diferencaIcms = ((+nota.valor_seguro + +nota.valor_frete + +nota.outras_despesas_acessorias) * +context.getters.aliquotaICMS) / 100;
    nota.valor_icms = parseFloat(+nota.valor_icms_produtos + +diferencaIcms).toFixed(2);
    context.commit('NOTA', nota);
  },

  notaCalculaTotal(context) {
    let nota = {
      ...context.state.nota
    };
    nota.valor_total_nota = parseFloat(+nota.valor_total_produtos + +nota.valor_icms_substituicao + +nota.valor_ipi + +nota.outras_despesas_acessorias + +nota.valor_seguro + +nota.valor_frete).toFixed(2);
    context.commit('NOTA', nota);
  },

  resetMovimentoProdutoForm(context) {
    context.commit('MOVIMENTO_PRODUTO', {
      id: null,
      produto_id: null,
      nota_fiscal_id: context.state.nota_id,
      unidade_medida_id: '4',
      cfop_id: '',
      id_na_nota: '',
      quantidade: 1,
      quantidade_por_caixa: 0,
      valor_unitario: 0,
      desconto: 0,
      observacao: '',
      aliquota_icms: context.getters.aliquotaICMS,
      aliquota_ipi: 5,
      mva: 0,
      loja_entrada_id: context.state.loja_entrada_id,
      valor_icms_substituicao: 0
    });
    context.commit('PRODUTO');
    context.commit('ERRORS');
  }

};