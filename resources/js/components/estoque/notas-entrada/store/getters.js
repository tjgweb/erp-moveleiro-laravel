export default {
  emissorEnderecos: state => {
    let enderecos = [];
    if (state.nota.emissor_id != null && state.nota.emissor_id != '') {
      let emissores = state.emissores.filter(e => e.id == state.nota.emissor_id);
      emissores.length > 0 ? enderecos = emissores[0].enderecos : null;
    }
    return enderecos;
  },

  emissorEstadoId: (state, getters) => {
    let id = null;
    if (getters.emissorEnderecos.length && state.nota.endereco_emissor_id) {
      let endereco = getters.emissorEnderecos.filter(e => e.id == state.nota.endereco_emissor_id)[0];
      endereco ? id = endereco.estado_id : id = null;
    }
    return id;
  },

  aliquotaICMS: state => {
    return state.aliquotas.aliquota;
  },

  descricaoProduto: state => {
    if (state.produto) {
      return `${state.produto.codigo_fabrica} - ${state.produto.mercadoria.categoria.descricao} - ${state.produto.mercadoria.descricao} - ${state.produto.marca.nome} - ${state.produto.descricao}`;
    } else {
      return '';
    }
  },

  mpValorTotal: state => {
    return parseFloat(state.movimentoProduto.valor_unitario * state.movimentoProduto.quantidade).toFixed(2);
  },

  mpValorIpi: (state, getters) => {
    return parseFloat((getters.mpValorTotal * state.movimentoProduto.aliquota_ipi) / 100).toFixed(2);
  },

  mpValorIcms: (state, getters) => {
    return parseFloat((getters.mpValorTotal * state.movimentoProduto.aliquota_icms) / 100).toFixed(2);
  },

  mpBaseCalculoIcmsSub: (state, getters) => {
    if (state.movimentoProduto.mva != 0) {
      let valorMva = parseFloat((getters.mpValorTotal * state.movimentoProduto.mva) / 100).toFixed(2);
      let baseCalculo = getters.mpValorTotal * 1 + valorMva * 1;
      baseCalculo = parseFloat((baseCalculo * state.aliquotaICMSInterna) / 100).toFixed(2);
      return baseCalculo;
    }
    return 0;
  },

  /* mpBaseCalculoIcmsSub2018: (state, getters) => {
    if (state.movimentoProduto.mva != 0) {
      let valorMva = parseFloat((getters.mpValorTotal * state.movimentoProduto.mva) / 100).toFixed(2);
      let baseCalculo = getters.mpValorTotal * 1 + valorMva * 1 - getters.mpValorIcms * 1;
      baseCalculo = parseFloat(baseCalculo / (1 - state.aliquotaICMSInterna / 100)).toFixed(2);
      return baseCalculo;
    }
    return 0;
  }, */
}