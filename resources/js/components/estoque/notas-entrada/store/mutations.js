export default {
  LOADING(state, value) {
    state.loading = value;
  },

  ERRORS(state, obj = {}) {
    state.errors = obj;
  },

  CONFIGS_ORDER_BY(state, value) {
    state.configs.orderBy = value;
  },

  CONFIGS_ORDER(state, value) {
    state.configs.order = value;
  },

  CONFIGS_PER_PAGE(state, value) {
    state.configs.perPage = value;
  },

  CONFIGS_SEARCH(state, value) {
    state.configs.search = value;
  },

  NOTAS(state, data) {
    state.notas = data;
  },

  NOTA_ID(state, id) {
    state.nota_id = id;
    state.nota.id = id;
  },

  NOTA(state, obj) {
    !obj.valor_icms_produtos ? obj.valor_icms_produtos = 0 : null;
    state.nota = obj;
  },

  CARGA(state, obj) {
    state.carga = obj;
  },

  DUPLICATAS(state, data) {
    state.duplicatas = data;
  },

  MOVIMENTO_PRODUTO(state, obj = {
    id: null,
    produto_id: null,
    nota_fiscal_id: null,
    unidade_medida_id: '4',
    cfop_id: '',
    id_na_nota: '',
    quantidade: 1,
    quantidade_por_caixa: 0,
    valor_unitario: 0,
    desconto: 0,
    observacao: '',
    aliquota_icms: 0,
    aliquota_ipi: 5,
    mva: 0,
    loja_entrada_id: state.loja_entrada_id,
    valor_icms_substituicao: 0
  }) {
    state.movimentoProduto = obj;
  },

  MOVIMENTO_PRODUTOS(state, data) {
    state.movimentoProdutos = data;
  },

  PRODUTO(state, obj = null) {
    state.produto = obj;
  },

  NATUREZA_OPERACAO(state, data) {
    state.naturezaOperacao = data;
  },

  EMISSORES(state, data) {
    state.emissores = data;
  },

  TRANSPORTADORES(state, data) {
    state.transportadores = data;
  },

  UNIDADES_MEDIDA(state, data) {
    state.unidadesMedida = data;
  },

  LOJAS(state, data) {
    state.lojas = data;
  },

  LOJA_ENTRADA_ID(state, id) {
    state.loja_entrada_id = id;
  },

  ALIQUOTAS(state, data) {
    state.aliquotas = data;
  },

  OPEN_MODAL_NOTAS_REMOVE(state, value) {
    state.openModalNotasRemove = value;
  },

  OPEN_MODAL_MOVIMENTO_PRODUTO_FORM(state, value) {
    state.openModalMovimentoProdutoForm = value;
  },

  OPEN_MODAL_MOVIMENTO_PRODUTO_REMOVE(state, value) {
    state.openModalMovimentoProdutoRemove = value;
  },
}