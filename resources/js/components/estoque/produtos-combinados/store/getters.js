export default {

  estoqueTotal: state => {
    if (state.produto && state.produto.movimento_produtos.length) {
      let entrada = 0,
        saida = 0;
      state.produto.movimento_produtos.filter(e => {
          return e.nota_fiscal.movimento_tipo_id === 2;
        })
        .forEach(e => {
          if (e.unidade_medida.agrupado == 1) {
            entrada += e.quantidade * e.quantidade_por_caixa;
          } else {
            entrada += e.quantidade;
          }
        });

      state.produto.movimento_produtos.filter(e => {
          return e.nota_fiscal.movimento_tipo_id === 1;
        })
        .forEach(e => {
          saida += e.quantidade;
        });

      return entrada - saida;
    }
    return 0;
  },

  preVendaTotal: state => {
    if (state.produto && state.produto.produto_vendas.length) {
      let soma = 0;
      let now = window.moment();
      state.produto.produto_vendas.filter(e => {
          let updated_at = window.moment(e.venda.updated_at);
          return e.venda.estatus_venda.produto_reservado && now.diff(updated_at, 'minutes') <= 24 * 10 * 60;
        })
        .forEach(e => {
          soma += e.quantidade;
        });
      return soma;
    }
    return 0;
  },

  disponivel: (state, getters) => {
    return getters.estoqueTotal - getters.preVendaTotal;
  },

  kits: state => {
    return state.combinado.produtos;
  },

  inKit: state => {
    let disabled = false;
    if (state.combinado.produtos && state.produto) {
      state.combinado.produtos.forEach(e => {
        if (e.id == state.produto.id) {
          disabled = true;
        }
      });
    }
    return disabled;
  },
}