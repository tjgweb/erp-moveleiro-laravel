import Helpers from '../../../../helpers/helpers';

export default {
  getCombinados(context, page = 1) {
    context.commit('LOADING', true);
    let url = `/estoque/produtos-combinados/lista/${context.state.configs.perPage}?page=${page}`;
    if (context.state.configs.search !== '') {
      url = `/estoque/produtos-combinados/lista/${context.state.configs.perPage}?page=${page}&search=${context.state.configs.search}`;
    }
    window.axios.get(url)
      .then(resp => {
        context.commit('COMBINADOS', resp.data);
        context.commit('CONFIGS_PER_PAGE', resp.data.per_page);
        context.commit('LOADING', false);
      })
      .catch(error => {
        Helpers.msgErrorDefault();
        context.commit('LOADING', false);
      });
  },

  buscarProduto(context, search) {
    context.commit('LOADING', true);
    window.axios
      .get(`/estoque/produtos/lista/100?page=1&search=${search}`)
      .then(resp => {
        context.commit('LOADING', false);
        context.commit('PRODUTOS', resp.data.data);
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      });
  },

  addKit(context) {
    let produto = window._.clone(context.state.produto);
    produto.pivot = {};
    produto.pivot.produto_id = produto.id;
    produto.pivot.quantidade = 1;
    produto.pivot.valor = produto.valor_atual;
    context.commit('ADD_KIT', produto);
    context.dispatch('valorTotal');
  },
  
  removeKit(context, index) {
    context.commit('REMOVE_KIT', index);
    context.dispatch('valorTotal');
  },

  changeDescricao(context, value) {
    let combinado = window._.clone(context.state.combinado);
    combinado.descricao = value;
    context.commit('COMBINADO', combinado);
  },

  changeMargem(context, value) {
    let combinado = window._.clone(context.state.combinado);
    combinado.margem = value;
    context.commit('COMBINADO', combinado);
  },

  valorTotal(context) {
    setTimeout(function () {
      let combinado = window._.clone(context.state.combinado);
      let soma = 0;
      combinado.produtos.forEach(e => {
        soma += (e.pivot.valor * e.pivot.quantidade);
      });
      combinado.valor_total = parseFloat(soma).toFixed(2);
      context.commit('COMBINADO', combinado);
    }, 500);
  },

  updateValor(context, payload) {
    context.commit('UPDATE_VALOR', payload);
    context.dispatch('valorTotal');
  },

  updateQuantidade(context, payload) {
    context.commit('UPDATE_QUANTIDADE', payload);
    context.dispatch('valorTotal');
  },

  onCreate(context) {
    context.commit('LOADING', true);
    window.axios.post(`/estoque/produtos-combinados`, context.state.combinado)
      .then(resp => {
        window.location = '/estoque/produtos-combinados';
      })
      .catch(error => {
        if (error.response.status = 422) {
          context.commit('LOADING', false);
          context.commit('ERRORS', error.response.data);
          Helpers.msgErrorDefault('Corrija os campos em vermelho', 'Erro de preenchimento')
        } else {
          context.commit('LOADING', false);
          Helpers.msgErrorDefault();
        }
      })
  },

  onUpdate(context) {
    context.commit('LOADING', true);
    window.axios.put(`/estoque/produtos-combinados/${context.state.combinado.id}`, context.state.combinado)
      .then(resp => {
        window.location = '/estoque/produtos-combinados';
      })
      .catch(error => {
        if (error.response.status = 422) {
          context.commit('LOADING', false);
          context.commit('ERRORS', error.response.data);
          Helpers.msgErrorDefault('Corrija os campos em vermelho', 'Erro de preenchimento')
        } else {
          context.commit('LOADING', false);
          Helpers.msgErrorDefault();
        }
      })
  },

  onRemove(context) {
    context.commit('LOADING', true);
    window.axios.delete(`/estoque/produtos-combinados/${context.state.combinado.id}`)
      .then(resp => {
        window.location = '/estoque/produtos-combinados';
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      })
  }

}