export default {
  action: null,
  errors: {},
  configs: {
    orderBy: 'descricao',
    order: 1,
    perPage: 25,
    search: '',
  },
  loading: false,
  combinados: [],
  combinado: {
    descricao: '',
    margem: 0,
    valor_total: 0,
    produtos: []
  },
  produtos: [],
  produto: null,
}