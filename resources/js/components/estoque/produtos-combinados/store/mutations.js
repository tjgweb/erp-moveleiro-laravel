export default {
  ACTION(state, value) {
    state.action = value;
  },

  CONFIGS_ORDER_BY(state, value) {
    state.configs.orderBy = value;
  },

  CONFIGS_ORDER(state, value) {
    state.configs.order = value;
  },

  CONFIGS_PER_PAGE(state, value) {
    state.configs.perPage = value;
  },

  CONFIGS_SEARCH(state, value) {
    state.configs.search = value;
  },

  LOADING(state, value) {
    state.loading = value;
  },

  ERRORS(state, value) {
    state.errors = value;
  },

  COMBINADOS(state, data = []) {
    state.combinados = data;
  },

  COMBINADO(state, data = {}) {
    state.combinado = data;
  },

  PRODUTOS(state, data = []) {
    state.produtos = data;
  },

  PRODUTO(state, data = null) {
    state.produto = data;
  },

  ADD_KIT(state, produto) {
    state.combinado.produtos.push(produto);
  },

  REMOVE_KIT(state, index) {
    state.combinado.produtos.splice(index, 1);
  },

  UPDATE_VALOR(state, payload) {
    state.combinado.produtos[payload.index].pivot.valor = payload.value;
  },

  UPDATE_QUANTIDADE(state, payload) {
    state.combinado.produtos[payload.index].pivot.quantidade = payload.value;
  },

}