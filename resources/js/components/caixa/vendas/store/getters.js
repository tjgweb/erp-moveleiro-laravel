export default {

  itensCarrinho: state => {
    let produtos = [], combinados = [];
    if (state.venda.produto_vendas && state.venda.produto_vendas.length) {
      let total = 0, desconto = 0, quantidade = 0, includes = [];

      state.venda.produto_vendas.forEach(pv => {
        if (pv.produto_combinado == 1) {
          if (!includes.includes(pv.combinado_id)) {
            includes.push(pv.combinado_id);
            quantidade = pv.quantidade / pv.produto.combinados[0].pivot.quantidade;
            total = quantidade * pv.produto.combinados[0].valor_total;
            desconto = total * pv.desconto / 100;
            total = parseFloat(total - desconto).toFixed(2);

            combinados.push({
              desconto: pv.desconto,
              quantidade: quantidade,
              valor_unitario: pv.produto.combinados[0].valor_total,
              total: total,
              imagem: pv.produto.imagens.length ? pv.produto.imagens[0].src : '/images/no-image.png',
              descricao: pv.produto.combinados[0].descricao
            })
          }
        } else {
          total = pv.quantidade * pv.valor_unitario;
          desconto = total * pv.desconto / 100;
          total = parseFloat(total - desconto).toFixed(2);
          produtos.push({
            desconto: pv.desconto,
            quantidade: pv.quantidade,
            valor_unitario: pv.valor_unitario,
            total: total,
            imagem: pv.produto.imagens.length ? pv.produto.imagens[0].src : '/images/no-image.png',
            descricao: `${pv.produto.mercadoria.descricao} ${pv.produto.marca.nome} ${pv.produto.descricao}`
          });
        }
      });
    }
    return {produtos, combinados};
  },

  total: (state, getters) => {
    let soma = 0;
    if (getters.itensCarrinho.produtos.length) {
      getters.itensCarrinho.produtos.forEach(e => {
        soma += e.total * 1;
      });
    }
    if (getters.itensCarrinho.combinados.length) {
      getters.itensCarrinho.combinados.forEach(e => {
        soma += e.total * 1;
      });
    }
    return parseFloat(soma).toFixed(2);
  },

  cliente: state => {
    return state.venda.pessoa_cliente ? state.venda.pessoa_cliente : null;
  },

  enderecoEntrega: state => {
    const enderecos = state.venda.pessoa_cliente ? state.venda.pessoa_cliente.enderecos : [];
    const id = state.venda.endereco_entrega_id ? state.venda.endereco_entrega_id : null;
    if (enderecos.length && id) {
      const endereco = enderecos.filter(e => e.id == id);
      const ref = endereco[0].referencia != null ? ` (${endereco[0].referencia})` : '';
      return `${endereco[0].full} | ${endereco[0].titulo}${ref}`;
    }
    return '<span class="text-danger">Não informado</span>';
  },

  finalizarRecebimento: state => {
    let value = false;
    if (state.venda.venda_pagamentos && state.venda.venda_pagamentos.length) {
      state.venda.venda_pagamentos.forEach(vp => {
        if (vp.tipo_pagamento_id == 2) {
          vp.cheques.forEach(cheque => {
            if (cheque.agencia == '' || cheque.banco == '' || cheque.conta == '' || cheque.numero == '') {
              value = true;
            }
          });
        }
      });
    }

    return value;
  }

}