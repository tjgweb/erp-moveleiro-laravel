export default {

  LOADING(state, data) {
    state.loading = data;
  },

  FINANCEIRAS(state, data) {
    state.financeiras = data;
  },

  VENDA(state, data) {
    state.venda = data;
  },

  PAGAMENTO(state, data) {
    state.pagamento = data;
  },

  OBSERVACAO(state, payload) {
    state.venda.venda_pagamentos[payload.index].observacao = payload.value;
  },

  PROTOCOLO(state, payload) {
    state.venda.venda_pagamentos[payload.index].protocolo = payload.value;
  },

  CONTRATO(state, payload) {
    state.venda.venda_pagamentos[payload.index].contrato = payload.value;
  },
  
  CHEQUES_BANCO(state, payload) {
    state.venda.venda_pagamentos[payload.index].cheques.forEach(cheque => {
      cheque.banco = payload.value;
    });
  },

  CHEQUES_AGENCIA(state, payload) {
    state.venda.venda_pagamentos[payload.index].cheques.forEach(cheque => {
      cheque.agencia = payload.value;
    });
  },

  CHEQUES_CONTA(state, payload) {
    state.venda.venda_pagamentos[payload.index].cheques.forEach(cheque => {
      cheque.conta = payload.value;
    });
  },

  CHEQUE_NUMERO(state, payload) {
    state.venda.venda_pagamentos[payload.index].cheques[payload.indexC].numero = payload.value;
  }
}