import Helpers from "../../../../helpers/helpers";

export default {

  start(context, payload) {
    let vencimento = null;
    payload.venda.venda_pagamentos.forEach(vp => {
      vp.caixa_id = payload.caixa_id;
      vp.protocolo = null;
      if (vp.tipo_pagamento_id == 2) {
        vp.cheques = [];
        vencimento = moment(vp.vencimento);
        for (let i = 0; i < vp.parcelas; i++) {
          vp.cheques.push({
            numero: '',
            banco: '',
            agencia: '',
            conta: '',
            valor: parseFloat(vp.valor_parcelas).toFixed(2),
            vencimento: i > 0 ? vencimento.clone().add(i, 'months').format('YYYY-MM-DD') : vencimento.format('YYYY-MM-DD'),
            status: false
          });
        }
      }
    });
    context.commit('VENDA', payload.venda);
  },

  salvar(context) {
    context.commit('LOADING', true);
    return window.axios.post('/caixa/vendas', context.state.venda)
      .then(resp => {
        let recibo = document.createElement('a');
        recibo.href = `/caixa/recibo/${resp.data.id}`;
        recibo.target = '_blank';
        recibo.click();
        window.location = '/caixa';
      })
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      })
  },


}