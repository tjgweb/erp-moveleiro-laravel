export default {
  LOADING(state, data) {
    state.loading = data;
  },

  BANDEIRAS(state, data) {
    state.bandeiras = data;
  },

  TIPO_PAGAMENTOS(state, data) {
    state.tipoPagamentos = data;
  },

  CREDIARIOS(state, data) {
    state.crediarios = data;
  },

  CONFIGURACOES(state, data) {
    state.percentual_multa = parseFloat(data.multa).toFixed(2);
    state.percentual_juros_dia = parseFloat(data.juros_dia).toFixed(2);
  },

  MULTA(state, payload) {
    state.crediarios[payload.indexC].parcelas[payload.indexP].multa = payload.value;
  },

  JUROS(state, payload) {
    state.crediarios[payload.indexC].parcelas[payload.indexP].juros = payload.value;
  },

  DESCONTO(state, payload) {
    state.crediarios[payload.indexC].parcelas[payload.indexP].desconto = payload.value;
  },

  ADD_ABATIMENTO(state, payload) {
    state.crediarios[payload.indexC].parcelas[payload.indexP].transacoes.push({pivot: payload.form});
  },

  REMOVE_ABATIMENTO(state, payload) {
    state.crediarios[payload.indexC].parcelas[payload.indexP].transacoes.splice(payload.index, 1);
  },

  ADD_PARCELAS_A_PAGAR(state, parcela) {
    state.parcelas_a_pagar.push(parcela);
  },

  REMOVE_PARCELAS_A_PAGAR(state, parcela) {
    let index = state.parcelas_a_pagar.indexOf(parcela.id);
    state.parcelas_a_pagar.splice(index, 1);
  },

  PAGAMENTO(state, payload) {
    state.pagamento = payload;
  },

  ADD_PAGAMENTO(state) {
    if (state.parcela_pagamentos.length && state.pagamento.tipo_pagamento_id == 1) {
      let notFind = true;
      state.parcela_pagamentos.forEach((pagamento, index) => {
        if (pagamento.tipo_pagamento_id == 1) {
          notFind = false;
          state.parcela_pagamentos[index].valor = parseFloat(+state.parcela_pagamentos[index].valor + +state.pagamento.valor).toFixed(2);
        }
      });
      notFind ? state.parcela_pagamentos.push(state.pagamento) : null;
    } else {
      state.parcela_pagamentos.push(state.pagamento);
    }
  },

  REMOVE_PAGAMENTO(state, index) {
    state.parcela_pagamentos.splice(index, 1);
  },

}