export default {

  cliente: state => {
    return state.crediarios.length ? state.crediarios[0].transacao.venda.pessoa_cliente : {};
  },

  totalParcela: state => (indexC, indexP) => {
    let parcela = state.crediarios[indexC].parcelas[indexP];
    return parseFloat(+parcela.valor + +parcela.multa + +parcela.juros - +parcela.desconto).toFixed(2);
  },

  novoAbatimento: state => parcela => {
    let item = false;
    if (parcela.transacoes.length) {
      parcela.transacoes.forEach(e => {
        if (!e.id) {
          item = true;
        }
      });
    }
    return item;
  },

  somaAbatimentos: state => (indexC, indexP) => {
    let soma = 0;
    if (state.crediarios[indexC].parcelas[indexP].transacoes.length) {
      state.crediarios[indexC].parcelas[indexP].transacoes.forEach(e => soma += +e.pivot.valor);
    }
    return parseFloat(soma).toFixed(2);
  },

  valorRestante: (state, getters) => (indexC, indexP) => {
    return parseFloat(+getters.totalParcela(indexC, indexP) - +getters.somaAbatimentos(indexC, indexP)).toFixed(2);
  },

  totalParcelaPagar: state => parcela => {
    let total = 0;

    if (parcela.transacoes.length) {
      let abatimento = parcela.transacoes.filter(e => e.id ? false : true)[0];
      total += +abatimento.pivot.valor;
    } else {
      total = +parcela.valor + +parcela.multa + +parcela.juros - +parcela.desconto;
    }

    return parseFloat(total).toFixed(2);
  },

  totalPagar: state => {
    let total = 0;

    if (state.parcelas_a_pagar.length) {
      let soma = 0;
      state.parcelas_a_pagar.forEach(parcela => {
        soma = 0;

        if (parcela.transacoes.length) {
          let abatimento = parcela.transacoes.filter(e => e.id ? false : true)[0];
          soma += +abatimento.pivot.valor;
        } else {
          soma = +parcela.valor + +parcela.multa + +parcela.juros - +parcela.desconto;
        }

        total += +soma;
      })
    }

    return parseFloat(total).toFixed(2);
  },

  pagamentoRegistrado: state => {
    let valor = 0;
    if (state.parcela_pagamentos.length) {
      state.parcela_pagamentos.forEach(e => valor += +e.valor);
    }
    return parseFloat(valor).toFixed(2);
  },

  pagamentoRestante: (state, getters) => {
    return parseFloat(+getters.totalPagar - +getters.pagamentoRegistrado).toFixed(2);
  },

  disabledBtnAddPagamento: (state) => {
    let disabled = false;

    if ((state.pagamento.tipo_pagamento_id == '3' || state.pagamento.tipo_pagamento_id == '4') &&
      (state.pagamento.bandeira_id == '' || state.pagamento.protocolo == '')) {
      disabled = true;
    }

    return disabled;
  }
}