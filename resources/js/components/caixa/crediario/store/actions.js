import Helpers from "../../../../helpers/helpers";

export default {

  calculaMulta(context, payload) {
    let multa = 0;
    let hoje = moment();
    let vencimento = moment(payload.parcela.vencimento);
    let dias = hoje.diff(vencimento, 'days');

    if (dias > 0) {
      multa = parseFloat(payload.parcela.valor * context.state.percentual_multa / 100).toFixed(2);
    }
    context.commit('MULTA', {...payload, value: multa});
    return multa;
  },

  calculaJuros(context, payload) {
    let juros = 0;
    let hoje = moment();
    let vencimento = moment(payload.parcela.vencimento);
    let dias = hoje.diff(vencimento, 'days');

    if (dias > 0) {
      let montante = payload.parcela.valor * Math.pow(1 + (context.state.percentual_juros_dia / 100), dias);
      juros = montante - payload.parcela.valor;
    }
    juros = parseFloat(juros).toFixed(2);
    context.commit('JUROS', {...payload, value: juros});
    return juros;
  },

  addParcelasPagar(context, parcela) {
    context.commit('ADD_PARCELAS_A_PAGAR', parcela);
    context.commit('PAGAMENTO', {
      tipo_pagamento_id: '1',
      bandeira_id: '',
      valor: context.getters.pagamentoRestante,
      protocolo: ''
    });
  },

  addPagamento(context) {
    context.commit('ADD_PAGAMENTO');
    context.commit('PAGAMENTO', {
      tipo_pagamento_id: '1',
      bandeira_id: '',
      valor: context.getters.pagamentoRestante,
      protocolo: ''
    });
  },

  removePagamento(context, index) {
    context.commit('REMOVE_PAGAMENTO', index);
    context.commit('PAGAMENTO', {
      tipo_pagamento_id: '1',
      bandeira_id: '',
      valor: context.getters.pagamentoRestante,
      protocolo: ''
    });
  },

  finalizarRecebimento(context) {
    context.commit('LOADING', true);
    return window.axios.post('/caixa/crediario', {
      parcelas: context.state.parcelas_a_pagar,
      pagamentos: context.state.parcela_pagamentos
    })
      .then(resp => {    
        let recibo = document.createElement('a');
        recibo.href = `/caixa/recibo/${resp.data.id}`;
        recibo.target = '_blank';
        recibo.click();
        window.location = '/caixa';
      })
      .catch(error => {
        context.commit('LOADING', false);
        console.log(error.response);
        Helpers.msgErrorDefault();
      })
  },
}