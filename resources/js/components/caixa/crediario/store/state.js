export default {
  loading: false,
  percentual_multa: null,
  percentual_juros_dia: null,
  bandeiras: [],
  tipoPagamentos: [],
  crediarios: [],
  parcelas_a_pagar: [],
  parcela_pagamentos: [],
  pagamento: {
    tipo_pagamento_id: '1',
    bandeira_id: '',
    valor: 0,
    protocolo: '',
  }
}