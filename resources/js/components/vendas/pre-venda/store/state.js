export default {
  vendedores: [],
  clientes: [],
  produtos: [],
  combinados: [],
  tipoPagamentos: [],
  bandeiras: [],
  financeiras: [],
  cliente: null,
  produto: null,
  combinado: null,
  overDiscont: {
    combinados: [],
    produtos: []
  },
  checkOverDiscont: false,
  openModalClientes: false,
  openModalRestaurar: false,
  openModalProdutos: false,
  openModalProdutoDetalhes: false,
  openModalProdutoDetalhes2: false,
  openModalCombinadoDetalhes: false,
  openModalCombinadoDetalhes2: false,
  errors: {},
  loading: false,
  venda: {
    id: null,
    vendedor_id: '',
    cliente_id: '',
    endereco_entrega_id: '',
    estatus_venda_id: '',
    tipo_venda_id: '1',
    previsao_entrega: '',
    observacao: '',
    produto_vendas: [],
    combinado_vendas: [],
    venda_pagamentos: [],
  },
  pagamento: {
    tipo_pagamento_id: '1',
    bandeira_id: '',
    financeira_id: '',
    valor: 0,
    parcelas: 1,
    valor_parcelas: 0,
    vencimento: null,
    observacao: ''
  }
};