import Helpers from '../../../../helpers/helpers';

export default {
  getVendedores(context) {
    return window.axios.get(`/shared/vendedores`).then(resp => context.commit('VENDEDORES', resp.data));
  },

  getTipoPagamentos(context) {
    return window.axios
      .get(`/shared/tipo-pagamentos`)
      .then(resp => context.commit('TIPO_PAGAMENTOS', resp.data));
  },

  buscarCliente(context, search) {
    return window.axios.get(`/shared/clientes?search=${search}`)
      .then(resp => context.commit('CLIENTES', resp.data))
      .catch(error => {
        context.commit('OPEN_MODAL_CLIENTES', false);
        Helpers.msgErrorDefault();
      });
  },

  buscarProduto(context, search) {
    return window.axios
      .get(`/vendas/pre-venda/produtos?search=${search}`)
      .then(resp => {
        context.commit('PRODUTOS', resp.data.produtos);
        context.commit('COMBINADOS', resp.data.combinados);
      })
      .catch(error => {
        context.commit('OPEN_MODAL_PRODUTOS', false);
        Helpers.msgErrorDefault();
      });
  },

  getBandeiras(context) {
    return window.axios.get('/shared/bandeiras').then(resp => context.commit('BANDEIRAS', resp.data));
  },

  getFinanceiras(context) {
    return window.axios.get('/shared/financeiras').then(resp => context.commit('FINANCEIRAS', resp.data));
  },

  loadEdit(context, vendaId) {
    return window.axios.get(`/vendas/pre-venda/${vendaId}/edit`).then(resp => {
      const venda = {
        id: resp.data.id,
        vendedor_id: resp.data.vendedor_id,
        cliente_id: resp.data.cliente_id,
        endereco_entrega_id: resp.data.endereco_entrega_id,
        estatus_venda_id: resp.data.estatus_venda_id,
        tipo_venda_id: resp.data.tipo_venda_id,
        previsao_entrega: resp.data.previsao_entrega,
        observacao: resp.data.observacao,
        produto_vendas: [],
        combinado_vendas: [],
        venda_pagamentos: resp.data.venda_pagamentos,
      };

      let valorTotal = 0,
        valorDesconto = 0,
        quantidadeCombinado = 0,
        combinado = [];

      resp.data.produto_vendas.forEach(item => {
        if (item.combinado) {
          if (!combinado.includes(item.combinado_id)) {
            item.combinado.produtos.forEach(produto => {
              if (item.produto_id == produto.id) {
                quantidadeCombinado = produto.pivot.quantidade;
              }
            });

            valorDesconto = ((item.combinado.valor_total * (item.quantidade / quantidadeCombinado)) * item.desconto) / 100;

            venda.combinado_vendas.push({
              quantidade: item.quantidade / quantidadeCombinado,
              valor_unitario: item.combinado.valor_total,
              desconto: item.desconto,
              produto_combinado: true,
              combinado_id: item.combinado_id,
              subtotal: parseFloat((item.combinado.valor_total * (item.quantidade / quantidadeCombinado)) - valorDesconto).toFixed(2),
              disponivel: context.getters.disponivelCombinado(item.combinado),
              combinado: item.combinado,
            });
          }

          combinado.push(item.combinado_id);
        } else {
          valorTotal = item.valor_unitario * item.quantidade;
          valorDesconto = (valorTotal * item.desconto) / 100;

          venda.produto_vendas.push({
            produto_id: item.produto_id,
            quantidade: item.quantidade,
            valor_unitario: item.valor_unitario,
            desconto: item.desconto,
            produto_combinado: false,
            combinado_id: item.produto.combinado_id ? item.produto.combinado_id : null,
            subtotal: parseFloat(valorTotal - valorDesconto).toFixed(2),
            disponivel: context.getters.disponivel(item.produto),
            produto: item.produto,
          });
        }
      });
      context.commit('CLIENTE', resp.data.pessoa_cliente);
      context.commit('VENDA', venda);

      if (venda.estatus_venda_id === 3) {
        context.commit('CHECK_OVER_DISCONT', true);
      }
      if (venda.estatus_venda_id === 5) {
        context.commit('OPEN_MODAL_RESTAURAR', true);
      }
    });
  },

  startComponent(context, vendaId = null) {
    context.commit('LOADING', true);
    const fnArray = [
      context.dispatch('getTipoPagamentos'),
      context.dispatch('getVendedores'),
      context.dispatch('getBandeiras'),
      context.dispatch('getFinanceiras'),
    ];
    if (vendaId) {
      fnArray.push(context.dispatch('loadEdit', vendaId));
    }
    Promise.all(fnArray)
      .then(() => context.commit('LOADING', false))
      .catch(error => {
        context.commit('LOADING', false);
        Helpers.msgErrorDefault();
      });
  },

  setCliente(context, obj) {
    context.commit('CLIENTE', obj);
    context.commit('VENDA_CLIENTE_ID', obj.id);
    context.commit('VENDA_ENDERECO_ENTREGA_ID', (obj.enderecos && obj.enderecos.length) ? obj.enderecos[0].id : '');
    context.commit('OPEN_MODAL_CLIENTES', false);
  },

  addCarrinho(context, produto) {
    let data = {
      produto_id: produto.id,
      quantidade: 1,
      valor_unitario: produto.valor_atual,
      desconto: 0,
      produto_combinado: false,
      combinado_id: null,
      subtotal: produto.valor_atual,
      disponivel: context.getters.disponivel(produto),
      produto: produto,
    };
    context.commit('ADD_CARRINHO', data);
    context.dispatch('refreshPagamentoRestante');
    context.commit('OPEN_MODAL_PRODUTO_DETALHES', false);
    context.commit('OPEN_MODAL_PRODUTOS', false);
  },

  addCarrinhoCombinado(context, combinado = false) {
    if (!combinado) {
      combinado = context.state.combinado;
    }
    let data = {
      quantidade: 1,
      valor_unitario: combinado.valor_total,
      desconto: 0,
      produto_combinado: true,
      combinado_id: combinado.id,
      subtotal: combinado.valor_total,
      disponivel: context.getters.disponivelCombinado(combinado),
      combinado: combinado,
    };
    context.commit('ADD_CARRINHO_COMBINADO', data);
    context.dispatch('refreshPagamentoRestante');
    context.commit('OPEN_MODAL_COMBINADO_DETALHES', false);
    context.commit('OPEN_MODAL_PRODUTOS', false);
  },

  updateQuantidadeCarrinho(context, payload) {
    context.commit('UPDATE_QUANTIDADE_CARRINHO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  updateQuantidadeCarrinhoCombinado(context, payload) {
    context.commit('UPDATE_QUANTIDADE_CARRINHO_COMBINADO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  updateDescontoCarrinho(context, payload) {
    context.commit('UPDATE_DESCONTO_CARRINHO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  updateDescontoCarrinhoCombinado(context, payload) {
    context.commit('UPDATE_DESCONTO_CARRINHO_COMBINADO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  updateSubtotalCarrinho(context, payload) {
    context.commit('UPDATE_SUBTOTAL_CARRINHO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  updateSubtotalCarrinhoCombinado(context, payload) {
    context.commit('UPDATE_SUBTOTAL_CARRINHO_COMBINADO', payload);
    context.dispatch('refreshPagamentoRestante');
  },

  setPagamentoValor(context, value) {
    const pagamento = _.clone(context.state.pagamento);
    pagamento.valor = parseFloat(value).toFixed(2);
    pagamento.valor_parcelas = parseFloat(pagamento.valor / pagamento.parcelas).toFixed(2);
    context.commit('PAGAMENTO', pagamento);
  },

  setPagamentoParcelas(context, value) {
    const pagamento = _.clone(context.state.pagamento);
    pagamento.parcelas = parseInt(value);
    pagamento.valor_parcelas = parseFloat(pagamento.valor / pagamento.parcelas).toFixed(2);
    context.commit('PAGAMENTO', pagamento);
  },

  addPagamento(context) {
    if (context.state.pagamento.tipo_pagamento_id == 1 && context.state.venda.venda_pagamentos.length) {
      context.state.venda.venda_pagamentos.forEach((pagamento, index) => {
        if (pagamento.tipo_pagamento_id == 1) {
          let obj = _.clone(pagamento);
          obj.valor = parseFloat(+obj.valor + +context.state.pagamento.valor).toFixed(2);
          obj.valor_parcelas = obj.valor;
          context.commit('UPDATE_PAGAMENTO_DINHEIRO', {
            obj,
            index,
          });
        }
      });
    } else {
      context.commit('ADD_PAGAMENTO');
    }
    context.commit('PAGAMENTO', {
      tipo_pagamento_id: '1',
      bandeira_id: '',
      financeira_id: '',
      valor: context.getters.pagamentoRestante,
      parcelas: 1,
      valor_parcelas: context.getters.pagamentoRestante,
      vencimento: null,
      observacao: '',
    });
  },

  removePagamento(context, index) {
    context.commit('REMOVE_PAGAMENTO', index);
    context.commit('PAGAMENTO', {
      tipo_pagamento_id: '1',
      bandeira_id: '',
      financeira_id: '',
      valor: context.getters.pagamentoRestante,
      parcelas: 1,
      valor_parcelas: context.getters.pagamentoRestante,
      vencimento: null,
      observacao: '',
    });
  },

  refreshPagamentoRestante(context) {
    let pagamento = _.clone(context.state.pagamento);
    setTimeout(() => {
      pagamento.valor = context.getters.pagamentoRestante;
      pagamento.valor_parcelas = context.getters.pagamentoRestante;
      context.commit('PAGAMENTO', pagamento);
    }, 1500);
  },

  editar(context) {
    return window.axios.put(`/vendas/pre-venda/${context.state.venda.id}`, context.state.venda)
      .then(resp => (window.location = '/vendas/pre-venda'))
      .catch(error => {
        console.log(error.response);
        if (error.response && error.response.status === 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Corrija os erros no formulário');
        }
      });
  },

  salvar(context) {
    return window.axios.post('/vendas/pre-venda', context.state.venda)
      .then(resp => (window.location = '/vendas/pre-venda'))
      .catch(error => {
        if (error.response && error.response.status === 422) {
          context.commit('ERRORS', error.response.data.errors);
          Helpers.msgErrorDefault('Corrija os erros no formulário');
        }
      });
  },
};