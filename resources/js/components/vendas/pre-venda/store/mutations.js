export default {
  VENDA(state, data) {
    state.venda = data;
  },

  VENDA_VENDEDOR_ID(state, id) {
    state.venda.vendedor_id = id;
  },

  VENDA_CLIENTE_ID(state, id) {
    state.venda.cliente_id = id;
  },

  VENDA_ENDERECO_ENTREGA_ID(state, id) {
    state.venda.endereco_entrega_id = id;
  },

  VENDA_PREVISAO_ENTREGA(state, value) {
    state.venda.previsao_entrega = value;
  },

  VENDA_ESTATUS_VENDA_ID(state, value) {
    state.venda.estatus_venda_id = value;
  },

  VENDA_TIPO_VENDA_ID(state, value) {
    state.venda.tipo_venda_id = value;
  },

  VENDA_OBSERVACAO(state, value) {
    state.venda.observacao = value;
  },

  VENDEDORES(state, data) {
    state.vendedores = data;
  },

  CLIENTES(state, data) {
    state.clientes = data;
  },

  CLIENTE(state, obj = null) {
    state.cliente = obj;
  },

  PRODUTOS(state, data) {
    state.produtos = data;
  },

  COMBINADOS(state, data) {
    state.combinados = data;
  },

  TIPO_PAGAMENTOS(state, data) {
    state.tipoPagamentos = data;
  },

  BANDEIRAS(state, data) {
    state.bandeiras = data;
  },

  FINANCEIRAS(state, data) {
    state.financeiras = data;
  },

  PRODUTO(state, data) {
    state.produto = data;
  },

  COMBINADO(state, data) {
    state.combinado = data;
  },

  ADD_CARRINHO(state, data) {
    state.venda.produto_vendas.push(data);
  },

  ADD_CARRINHO_COMBINADO(state, data) {
    state.venda.combinado_vendas.push(data);
  },

  UPDATE_QUANTIDADE_CARRINHO(state, payload) {
    let somaQtdProduto = 0,
      desconto = 0;
    let pv = state.venda.produto_vendas;

    somaQtdProduto = parseFloat(pv[payload.index].valor_unitario) * parseInt(payload.value);
    desconto = (somaQtdProduto * pv[payload.index].desconto) / 100;
    pv[payload.index].quantidade = parseInt(payload.value);
    pv[payload.index].subtotal = (somaQtdProduto - desconto).toFixed(2);

    state.venda.produto_vendas = pv;
  },

  UPDATE_QUANTIDADE_CARRINHO_COMBINADO(state, payload) {
    let somaQtdProduto = 0,
      desconto = 0;
    let cv = state.venda.combinado_vendas;

    somaQtdProduto = parseFloat(cv[payload.index].valor_unitario) * parseInt(payload.value);
    desconto = (somaQtdProduto * cv[payload.index].desconto) / 100;
    cv[payload.index].quantidade = parseInt(payload.value);
    cv[payload.index].subtotal = (somaQtdProduto - desconto).toFixed(2);

    state.venda.combinado_vendas = cv;
  },

  UPDATE_DESCONTO_CARRINHO(state, payload) {
    let somaQtdProduto = 0,
      desconto = 0;
    let pv = state.venda.produto_vendas;

    somaQtdProduto = parseFloat(pv[payload.index].valor_unitario) * pv[payload.index].quantidade;
    desconto = (somaQtdProduto * parseFloat(payload.value)) / 100;
    pv[payload.index].desconto = parseFloat(payload.value).toFixed(3);
    pv[payload.index].subtotal = (somaQtdProduto - desconto).toFixed(2);

    state.venda.produto_vendas = pv;
  },

  UPDATE_DESCONTO_CARRINHO_COMBINADO(state, payload) {
    let somaQtdProduto = 0,
      desconto = 0;
    let pv = state.venda.combinado_vendas;

    somaQtdProduto = parseFloat(pv[payload.index].valor_unitario) * pv[payload.index].quantidade;
    desconto = (somaQtdProduto * parseFloat(payload.value)) / 100;
    pv[payload.index].desconto = parseFloat(payload.value).toFixed(3);
    pv[payload.index].subtotal = (somaQtdProduto - desconto).toFixed(2);

    state.venda.combinado_vendas = pv;
  },

  UPDATE_SUBTOTAL_CARRINHO(state, payload) {
    let pv = state.venda.produto_vendas;
    pv[payload.index].subtotal = payload.value;

    state.venda.produto_vendas = pv;
  },

  UPDATE_SUBTOTAL_CARRINHO_COMBINADO(state, payload) {
    let pv = state.venda.combinado_vendas;
    pv[payload.index].subtotal = payload.value;

    state.venda.combinado_vendas = pv;
  },

  REMOVE_CARRINHO(state, index) {
    state.venda.produto_vendas.splice(index, 1);
    state.overDiscont.produtos.splice(index, 1);
  },

  REMOVE_CARRINHO_COMBINADO(state, index) {
    state.venda.combinado_vendas.splice(index, 1);
    state.overDiscont.combinados.splice(index, 1);
  },

  RESET_OVER_DISCONT(state) {
    state.overDiscont = {
      combinados: [],
      produtos: []
    };
  },

  OVER_DISCONT(state, payload) {
    if (payload.produtos) {
      state.overDiscont.produtos[payload.produtos.index] = payload.produtos.value;
    } else if (payload.combinados) {
      state.overDiscont.combinados[payload.combinados.index] = payload.combinados.value;
    }
  },

  CHECK_OVER_DISCONT(state, value) {
    state.checkOverDiscont = value;
  },

  OPEN_MODAL_RESTAURAR(state, value) {
    state.openModalRestaurar = value;
  },

  OPEN_MODAL_CLIENTES(state, value) {
    state.openModalClientes = value;
  },

  OPEN_MODAL_PRODUTOS(state, value) {
    state.openModalProdutos = value;
  },

  OPEN_MODAL_PRODUTO_DETALHES(state, value) {
    state.openModalProdutoDetalhes = value;
  },

  OPEN_MODAL_PRODUTO_DETALHES2(state, value) {
    state.openModalProdutoDetalhes2 = value;
  },

  OPEN_MODAL_COMBINADO_DETALHES(state, value) {
    state.openModalCombinadoDetalhes = value;
  },

  OPEN_MODAL_COMBINADO_DETALHES2(state, value) {
    state.openModalCombinadoDetalhes2 = value;
  },

  ERRORS(state, data) {
    state.errors = data;
  },

  LOADING(state, value) {
    state.loading = value;
  },

  PAGAMENTO(state, payload) {
    state.pagamento = payload;
  },

  ADD_PAGAMENTO(state) {
    state.venda.venda_pagamentos.push(state.pagamento);
  },

  UPDATE_PAGAMENTO_DINHEIRO(state, payload) {
    state.venda.venda_pagamentos.splice(payload.index, 1);
    state.venda.venda_pagamentos.push(payload.obj);
  },

  REMOVE_PAGAMENTO(state, index) {
    state.venda.venda_pagamentos.splice(index, 1);
  },

  PAGAMENTO_TIPO_PAGAMENTO_ID(state, id) {
    state.pagamento.tipo_pagamento_id = id;
  },

  PAGAMENTO_BANDEIRA_ID(state, id) {
    state.pagamento.bandeira_id = id;
  },

  PAGAMENTO_FINANCEIRA_ID(state, id) {
    state.pagamento.financeira_id = id;
  },

  PAGAMENTO_OBSERVACAO(state, value) {
    state.pagamento.observacao = value;
  },

  PAGAMENTO_VENCIMENTO(state, value) {
    state.pagamento.vencimento = value;
  }
};