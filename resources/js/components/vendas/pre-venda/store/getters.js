import Estoque from './../../../../helpers/estoque';

export default {
  clienteDescricao: state => {
    let descricao = null;
    if (state.cliente) {
      let documento = state.cliente.tipo_pessoa_id == 1 ? `CPF: ${state.cliente.fisica.cpf}` : `CNPJ: ${state.cliente.juridica.cnpj}`;
      // descricao = `${state.cliente.nome} - ${documento}`;
      descricao = `${state.cliente.nome}`;
    }
    return descricao;
  },

  enderecos: state => {
    let enderecos = [];
    if (state.cliente) {
      enderecos = state.cliente.enderecos;
    }
    return enderecos;
  },

  produto_vendas: state => {
    return state.venda.produto_vendas;
  },

  combinado_vendas: state => {
    return state.venda.combinado_vendas;
  },

  venda_pagamentos: state => {
    return state.venda.venda_pagamentos;
  },

  estoqueTotal: state => produto => {
    return Estoque.total(produto);
  },

  preVendaTotal: state => produto => {
    if (produto && produto.produto_vendas.length) {
      let soma = 0;
      let now = window.moment();
      produto.produto_vendas.filter(e => {
          let updated_at = window.moment(e.venda.updated_at);
          return e.venda.estatus_venda.produto_reservado && now.diff(updated_at, 'minutes') <= 24 * 10 * 60;
        }).forEach(e => {
          soma += +e.quantidade;
        });
      return soma;
    }
    return 0;
  },

  disponivel: state => produto => {
    return Estoque.disponivel(produto);
  },

  estoqueTotalCombinado: state => combinado => {
    return Estoque.totalCombinado(combinado);
  },

  disponivelCombinado: state => combinado => {
    return Estoque.disponivelCombinado(combinado);
  },

  inCart: (state, getters) => produto => {
    let disabled = false;
    if (getters.produto_vendas.length) {
      getters.produto_vendas.forEach(e => {
        if (e.produto_id == produto.id) {
          disabled = true;
        }
      });
    }
    return disabled;
  },

  inCartCombinado: (state, getters) => combinado => {
    let disabled = false;
    if (getters.combinado_vendas.length) {
      getters.combinado_vendas.forEach(e => {
        if (e.combinado_id == combinado.id) {
          disabled = true;
        }
      });
    }
    return disabled;
  },

  disableAddCartBtn: (state, getters) => produto => {
    let disabled = false;
    if (getters.disponivel(produto) <= 0) {
      disabled = true;
    } else if (getters.inCart(produto)) {
      disabled = true;
    }
    return disabled;
  },

  disableAddCartBtnCombinado: (state, getters) => (combinado = false) => {
    if (!combinado) {
      combinado = state.combinado;
    }
    let disabled = false;
    if (getters.disponivelCombinado(combinado) <= 0) {
      disabled = true;
    } else if (getters.inCartCombinado(combinado)) {
      disabled = true;
    }
    return disabled;
  },

  somaCarrinho: (state, getters) => {
    let soma = 0;

    if (!getters.produto_vendas.length && !getters.combinado_vendas.length) {
      return soma;
    }

    if (getters.produto_vendas.length) {
      getters.produto_vendas.forEach(e => {
        soma += parseFloat(e.subtotal);
      });
    }

    if (getters.combinado_vendas.length) {
      getters.combinado_vendas.forEach(e => {
        soma += parseFloat(e.subtotal);
      });
    }

    return soma.toFixed(2);
  },

  somaPagamentos: (state) => {
    let soma = 0;
    if (state.venda.venda_pagamentos.length) {
      state.venda.venda_pagamentos.forEach(e => {
        soma += parseFloat(e.valor);
      });
    }
    return soma.toFixed(2);
  },

  pagamentoRestante: (state, getters) => {
    return (getters.somaCarrinho - getters.somaPagamentos).toFixed(2);
  },

  valorParcelas: (state, getters) => {
    return parseFloat(state.pagamento.valor / state.pagamento.parcelas).toFixed(2);
  },
};