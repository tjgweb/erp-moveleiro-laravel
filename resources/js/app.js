require('./bootstrap');

import Vue from 'vue';
import store from './plugins/vuex';
import EventBus from './plugins/event-bus';
import * as uiv from 'uiv';
import Vue2Filters from 'vue2-filters';
import VueTheMask from 'vue-the-mask';
import money from 'v-money';

Vue.use(EventBus);
Vue.use(uiv);
Vue.use(Vue2Filters);
Vue.use(VueTheMask);
Vue.use(money, {
  decimal: ',',
  thousands: '.',
  prefix: 'R$',
  precision: 2,
  masked: false
});
Vue.filter('formatDate', function (value) {
  if (!value) return '';
  if (value) {
    return window.moment(String(value)).format('DD/MM/YYYY')
  }
});
Vue.filter('documento', function (value, tipo) {
  if (!value) return '';
  let doc = value;
  if (tipo == 'cpf') {
    doc = doc.substr(0, 3) + '.' + doc.substr(3, 3) + '.' + doc.substr(6, 3) + '-' + doc.substr(9, 2);
    return doc;
  }
  if (tipo == 'cnpj') {
    doc = doc.substr(0, 2) + '.' + doc.substr(2, 3) + '.' + doc.substr(5, 3) + '/' + doc.substr(8, 4) + '-' + doc.substr(12, 2);
    return doc;
  }
  return '';
});

Vue.component('vue-box', require('./components/Box.vue').default);
Vue.component('vue-data-table', require('./components/DataTable.vue').default);
Vue.component('vue-select-estados', require('./components/SelectEstados.vue').default);
Vue.component('vue-select-cidades', require('./components/SelectCidades.vue').default);

Vue.component('vue-profile-modal', require('./components/auth/ProfileModal.vue').default);
Vue.component('vue-users', require('./components/gestao/Users.vue').default);
Vue.component('vue-acl', require('./components/gestao/Acl.vue').default);
Vue.component('vue-pessoas', require('./components/gestao/Pessoas.vue').default);

Vue.component('vue-notas-entrada', require('./components/estoque/notas-entrada/NotasEntrada.vue').default);
Vue.component('vue-notas-entrada-form', require('./components/estoque/notas-entrada/NotasEntradaForm.vue').default);
Vue.component('vue-notas-transferencia', require('./components/estoque/notas-transferencia/NotasTransferencia.vue').default);
Vue.component('vue-notas-transferencia-form', require('./components/estoque/notas-transferencia/NotasTransferenciaForm.vue').default);
Vue.component('vue-notas-venda-emitidas', require('./components/estoque/notas-venda/emitidas/NotasVendaEmitidas.vue').default);
Vue.component('vue-notas-venda-emissao', require('./components/estoque/notas-venda/emissao/NotasVendaEmissao.vue').default);
Vue.component('vue-notas-venda-form', require('./components/estoque/notas-venda/emissao/NotasVendaForm.vue').default);
Vue.component('vue-ajuste', require('./components/estoque/Ajuste.vue').default);
Vue.component('vue-assistencias', require('./components/estoque/assistencias/Assistencias.vue').default);
Vue.component('vue-natureza-operacao', require('./components/estoque/NaturezaOperacao.vue').default);
Vue.component('vue-produtos', require('./components/estoque/Produtos.vue').default);
Vue.component('vue-produtos-combinados', require('./components/estoque/produtos-combinados/ProdutosCombinados.vue').default);
Vue.component('vue-produtos-combinados-form', require('./components/estoque/produtos-combinados/ProdutosCombinadosForm.vue').default);
Vue.component('vue-mercadorias', require('./components/estoque/Mercadorias.vue').default);
Vue.component('vue-categorias', require('./components/estoque/Categorias.vue').default);
Vue.component('vue-marcas', require('./components/estoque/Marcas.vue').default);

Vue.component('vue-pre-venda-lista', require('./components/vendas/pre-venda/PreVendaLista.vue').default);
Vue.component('vue-pre-venda', require('./components/vendas/pre-venda/PreVenda.vue').default);

Vue.component('vue-modal-cheques', require('./components/caixa/painel/ModalCheques.vue').default);
Vue.component('vue-vendas', require('./components/caixa/vendas/Vendas.vue').default);
Vue.component('vue-vendas-recebimento', require('./components/caixa/vendas/VendasRecebimento.vue').default);
Vue.component('vue-crediario-recebimento', require('./components/caixa/crediario/CrediarioRecebimento.vue').default);

const app = new Vue({
  name: 'app',
  el: '#app',
  store
});


require('./scripts');