export default {

  total(produto) {
    if (produto && produto.movimento_produtos.length) {
      let entrada = 0, saida = 0;

      produto.movimento_produtos.filter(e => {
        return e.nota_fiscal.movimento_tipo_id == 2;
      }).forEach(e => {
        if (e.unidade_medida.agrupado == 1) {
          entrada += e.quantidade * e.quantidade_por_caixa;
        } else {
          entrada += +e.quantidade;
        }
      });

      produto.movimento_produtos.filter(e => {
        return e.nota_fiscal.movimento_tipo_id == 1;
      }).forEach(e => {
        saida += +e.quantidade;
      });

      return entrada - saida;
    }
    return 0;
  },

  vendas(produto) {
    if (produto && produto.produto_vendas.length) {
      let soma = 0;
      produto.produto_vendas.filter(el => {
        return el.venda.estatus_venda_id == 2 || el.venda.estatus_venda_id == 6 || el.venda.estatus_venda_id == 7;
      }).forEach(el => {
        soma += +el.quantidade;
      });
      return soma;
    }
    return 0;
  },

  reservadoPago(produto) {
    if (produto && produto.produto_vendas.length) {
      let soma = 0;
      produto.produto_vendas.filter(el => el.venda.estatus_venda_id == 6)
        .forEach(el => {
          soma += +el.quantidade;
        });
      return soma;
    }
    return 0;
  },

  reservadoPrevenda(produto) {
    if (produto && produto.produto_vendas.length) {
      let soma = 0;
      let now = window.moment();
      produto.produto_vendas.filter(el => {
        let updated_at = window.moment(el.venda.updated_at);
        return el.venda.estatus_venda_id != 6 && el.venda.estatus_venda.produto_reservado && now.diff(updated_at, "minutes") <= 24 * 10 * 60;
      }).forEach(el => {
        soma += +el.quantidade;
      });
      return soma;
    }
    return 0;
  },

  disponivel(produto) {
    return produto ? this.total(produto) - this.reservadoPago(produto) - this.reservadoPrevenda(produto) : 0;
  },

  
  /************* Produtos combinados ***********************/
  
  totalCombinado(combinado) {
    if (combinado && combinado.produtos.length) {
      let total = 0, divisao = 0;

      combinado.produtos.forEach((produto, index) => {
        divisao = this.total(produto) / produto.pivot.quantidade;
        if (index === 0) {
          total = divisao;
        }
        if (divisao < total) {
          total = divisao;
        }
      });
      return total;
    }
    return 0;
  },

  disponivelCombinado(combinado) {
    if (combinado && combinado.produtos.length) {
      let total = 0, divisao = 0;

      combinado.produtos.forEach((produto, index) => {
        divisao = this.disponivel(produto) / produto.pivot.quantidade;
        if (index === 0) {
          total = divisao;
        }
        if (divisao < total) {
          total = divisao;
        }
      });
      return total;
    }
    return 0;
  },
  
}