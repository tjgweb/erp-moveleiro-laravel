export default {

  msgAdd: function (msg = 'Adicionado com sucesso.', title = null, options = this.toastr.options) {
    window.toastr.success(msg, title, options);
  },

  msgEdit: function (msg = 'Atualizado com sucesso.', title = null, options = this.toastr.options) {
    window.toastr.success(msg, title, options);
  },

  msgRemove: function (msg = 'Removido com sucesso.', title = null, options = this.toastr.options) {
    window.toastr.success(msg, title, options);
  },

  msgErrorDefault: function (msg = 'Ocorreu um erro inesperado, informe ao administrador do sistema', title = null, options = this.toastr.options) {
    window.toastr.error(msg, title, options);
  },

  msgWarning: function (msg, title = null, options = this.toastr.options) {
    window.toastr.warning(msg, title, options);
  },

  toastr: {
    options: {
      closeButton: true,
      progressBar: true,
      timeOut: 10000
    }
  },

  dataTableLTE: {
    pt_br: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  }
}