/* Funções Globais AdminLTE */
$(document).ready(function () {

  var url = window.location;
  // Will only work if string in href matches with location
  $('ul.sidebar-menu a[href="' + url + '"]').parent().addClass('active');

  // Will also work for relative and absolute hrefs
  $('ul.sidebar-menu a').filter(function () {
    return this.href == url;
  }).parent().addClass('active');

  // Will also work for relative and absolute hrefs
  $('ul.sidebar-menu a').filter(function () {
    var regex = new RegExp('\\b' + this.href + '\\b');
    return url.href.search(regex) !== -1;
  }).parent().addClass('active');

  $('li.treeview li.active').parent().addClass('menu-open');
  $('ul.treeview-menu li.active').parent().attr('style', 'display: block');

  $(".money").maskMoney({
    prefix: 'R$ ',
    allowZero: true,
    thousands: '.',
    decimal: ',',
    affixesStay: true
  });


  $('[data-toggle="tooltip"]').tooltip();

  $('[data-toggle="popover"]').popover();

  $('.cpf-mask').mask('000.000.000-00');

  $('.cnpj-mask').mask('00.000.000/0000-00');
  
  $('.inscricao-estadual-mask').mask('000.000.000/0000');
  
  $('.inscricao-municipal-mask').mask('000.000.000/0000');
  
  $('.cep-mask').mask('00000-00');

});