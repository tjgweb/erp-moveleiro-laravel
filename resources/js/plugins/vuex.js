import Vue from 'vue';
import Vuex from 'vuex';

import NotasEntrada from '../components/estoque/notas-entrada/store';
import NotasTransferencia from '../components/estoque/notas-transferencia/store';
import NotasVendaEmissao from '../components/estoque/notas-venda/emissao/store';
import ProdutosCombinados from '../components/estoque/produtos-combinados/store';
import PreVenda from '../components/vendas/pre-venda/store';
import VendasRecebimento from '../components/caixa/vendas/store';
import CrediarioRecebimento from '../components/caixa/crediario/store';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    NotasEntrada,
    NotasTransferencia,
    NotasVendaEmissao,
    ProdutosCombinados,
    PreVenda,
    VendasRecebimento,
    CrediarioRecebimento
  }
});